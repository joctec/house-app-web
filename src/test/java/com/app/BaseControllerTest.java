package com.app;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import junit.framework.TestCase;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration("src/main/webapp")
@ContextConfiguration(locations = { "classpath:/spring/spring-context.xml", "classpath:/spring/spring-mvc-app.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class, DirtiesContextTestExecutionListener.class })
@Transactional
@TransactionConfiguration(transactionManager = "transactionManager", defaultRollback = true)
public class BaseControllerTest extends TestCase {

    private static final Logger logger = LoggerFactory.getLogger(BaseControllerTest.class);

    // @Autowired
    // private WebApplicationContext wac;
    //
    // protected MockMvc mockMvc;

    @BeforeClass
    public static void start() {
        logger.info("=====================  start  =======================");
    }

    @AfterClass
    public static void end() {
        logger.info("=====================  end  =======================");
    }

    @Test
    public void test() {
        logger.info("==test==");
    }

    // @Before
    // public void setup() {
    // mockMvc = MockMvcBuilders.standaloneSetup(wac).build();
    // }

}
