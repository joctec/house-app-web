package com.guda.mp.web.rest.app;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.app.BaseControllerTest;

public class LoginApiControllerTest extends BaseControllerTest {

    @Autowired
    private WebApplicationContext wac;
    private MockMvc mockMvc;

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.wac).build();
    }
    
    @Test
    public void testTologin() throws Exception {
        ResultActions resultActions = this.mockMvc
            .perform(post("/app/api/login").param("username", "app").param("password", "123456"))
            .andExpect(status().isOk());

        MvcResult mvcResult = resultActions.andReturn();
        System.out.println("status :\t" + mvcResult.getResponse().getStatus());
        String resposne = mvcResult.getResponse().getContentAsString();
        System.out.println("reponse :\t" + resposne);

    }

    @Test
    public void demopost() throws Exception {
//        ResultActions resultActions = this.mockMvc
//            .perform(post("/api/login").param("username", "johndoe").param("password", "TestR0ck"))
//            .andExpect(status().isOk());
//        MvcResult mvcResult = resultActions.andReturn();
//        System.out.println("status :\t" + mvcResult.getResponse().getStatus());
//        String resposne = mvcResult.getResponse().getContentAsString();
//        System.out.println("reponse :\t" + resposne);
    }

}
