package com.app.core.web.util;

import org.springframework.web.context.WebApplicationContext;

import com.app.core.cache.redis.RedisCacheService;
import com.app.core.common.AppCons;
import com.app.core.web.context.RequestContext;
import com.app.util.DateUtil;

/**
 * 系统上下文，一些常用的系统操作，放在这个类里
 * 
 * @author afei
 *
 */
public class AppContextUtil {

    public static WebApplicationContext webApplicationContext;

    public static final String EXPRIED_KEY = "expried";

    public static WebApplicationContext getContext() {
        return webApplicationContext;
    }

    public static void setContext(WebApplicationContext context) {
        webApplicationContext = context;
    }

    @SuppressWarnings("unchecked")
    public static <W> W getSpringBean(String name) {
        return (W) webApplicationContext.getBean(name);
    }

    public static RedisCacheService getRedisCache() {
        WebApplicationContext context = AppContextUtil.getContext();
        RedisCacheService redis = (RedisCacheService) context.getBean("cacheService");
        if (redis == null) {
            throw new RuntimeException("从spring context中获取redis cache为空,请确认缓存是否配置正确！");
        }
        return redis;
    }

    public static Object getSessionValue(String key) {
        // 单机模式运行
        if (AppCons.RUN_MODE_ALONE) {
            Object value = RequestContext.getRequest().getSession().getAttribute(key);
            if (value == null) {
                return null;
            }

            // 判断是否超时
            Object exObj = RequestContext.getRequest().getSession().getAttribute(key + EXPRIED_KEY);
            if (exObj != null && DateUtil.now().getTime() > (long) exObj) {
                return value;
            }

            return value;
        }
        // 中心缓存模式运行
        else {
            /* 存放到公共缓存 */
            return AppContextUtil.getRedisCache().get(key);
        }
    }

    /**
     * 
     * @param key
     * @param value
     * @param expried 0永不过期
     *        秒
     */
    public static void setSessionValue(String key, Object value, int expried) {
        if (value == null) {
            value = "";
        }
        // 单机模式运行
        if (AppCons.RUN_MODE_ALONE) {
            RequestContext.getRequest().getSession().setAttribute(key, value);
            RequestContext.getRequest().getSession().setAttribute(key + EXPRIED_KEY,
                DateUtil.now().getTime() + (expried * 1000));
        }
        // 中心缓存模式运行
        else {
            /* 存放到公共缓存 */
            AppContextUtil.getRedisCache().set(key, value, expried);
        }
    }

    public static void delSessionValue(String key) {
        // 单机模式运行
        if (AppCons.RUN_MODE_ALONE) {
            RequestContext.getRequest().getSession().removeAttribute(key);
        }
        // 中心缓存模式运行
        else {
            /* 存放到公共缓存 */
            AppContextUtil.getRedisCache().del(key);
        }
    }

    public static void refreshSession(String key, int expried) {
        // 单机模式运行
        if (AppCons.RUN_MODE_ALONE) {
            RequestContext.getRequest().getSession().setAttribute(key + EXPRIED_KEY,
                DateUtil.now().getTime() + (expried * 1000));
        }
        // 中心缓存模式运行
        else {
            /* 存放到公共缓存 */
            AppContextUtil.getRedisCache().setExpriedTime(key, expried);
        }
    }
}
