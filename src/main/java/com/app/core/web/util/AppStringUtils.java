package com.app.core.web.util;


public class AppStringUtils {
    
    /**
     * 判断字符串是否以指定字符串开头
     * 
     * @param str
     * @param prifix
     * @return
     * @since v#{version}
     * @author zhangxf
     * @created 2015年10月26日 下午3:51:23
     */
    public static boolean startWith(String str, String prifix){
        String regex = "^" +prifix+ ".*"; 
        return str.matches(regex);
    }
    
    public static String createStartWithReg(String... prifix){
        if(prifix == null){
            return null;
        }
        
        StringBuffer sb = new StringBuffer();
        for (String p : prifix) {
            sb.append("(^" + p + ".*)").append("|");
        }
        
        String reg = sb.substring(0, sb.length()-1);
        
        return reg;
    }
    
    public static void main(String[] args) {
        String createStartWith = createStartWithReg("/ab/", "/abcv/");
        boolean matches = "/abcv/cdefg".matches(createStartWith);
        System.out.println(matches);
    }
}
