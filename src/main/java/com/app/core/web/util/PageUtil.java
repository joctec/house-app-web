package com.app.core.web.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.github.pagehelper.Page;

public class PageUtil {

	@SuppressWarnings("rawtypes")
	public static Map<String, Object> convertPage(Page page) {
		Map<String, Object> result = new HashMap<String, Object>();

		// 当前页码
		result.put("pageNum", page.getPageNum());
		// 每页显示多少条
		result.put("pageSize", page.getPageSize());
		// 总共有多少页
		result.put("totalPage", page.getPages());
		// 总共有多少行
		result.put("total", page.getTotal());
		// 分页数据
		result.put("rows", page.getResult());

		return result;
	}

	public static Map<String, Object> listToPage(List<?> list) {
		Map<String, Object> result = new HashMap<String, Object>();
		int size = (list == null ? 0 : list.size());
		// 当前页码
		result.put("pageNum", 1);
		// 每页显示多少条
		result.put("pageSize", size);
		// 总共有多少页
		result.put("totalPage", 1);
		// 总共有多少行
		result.put("total", size);
		// 分页数据
		result.put("rows", list);

		return result;
	}

	@SuppressWarnings("rawtypes")
	public static Map<String, Object> convertPage(Page page, Map data) {
		Map<String, Object> result = new HashMap<String, Object>();

		// 当前页码
		result.put("pageNum", page.getPageNum());
		// 每页显示多少条
		result.put("pageSize", page.getPageSize());
		// 总共有多少页
		result.put("totalPage", page.getPages());
		// 总共有多少行
		result.put("total", page.getTotal());
		// 分页数据
		result.put("rows", page.getResult());
		if (null != data) {
			result.putAll(data);
		}
		return result;
	}
}
