package com.app.core.web.support;

import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.method.support.ModelAndViewContainer;

import com.app.core.validator.annotation.LoginUser;
import com.app.core.web.context.RequestContext;

/**
 * 用户session注解自动注入
 * 
 * @author zhangxf
 * @created 2015年11月18日 下午1:58:32
 */
public class LoginUserArgumentResolver implements HandlerMethodArgumentResolver {

    /**
     * 检查解析器是否支持解析该参数
     */
    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        // 如果该参数注解有@LoginUser
        if (parameter.getParameterAnnotation(LoginUser.class) != null) {
            // 支持解析该参数
            return true;
        }
        return false;
    }

    @Override
    public Object resolveArgument(MethodParameter parameter, ModelAndViewContainer mavContainer,
        NativeWebRequest webRequest, WebDataBinderFactory binderFactory) throws Exception {
        return RequestContext.getUserSession();
    }

}
