package com.app.core.web.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

/**
 * 覆盖spring date转换类， 用DateTime来转换
 * 
 * @author zhangxf
 * @created 2017年4月4日 下午7:12:27
 */
public class DateConverter implements Converter<String, Date> {

    @Override
    public Date convert(String source) {
        // 2017-04-04 23:59:59
        try {
            if (source.length() == 10 && source.contains("-")) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(source);
            } else if (source.length() == 8) {
                return new SimpleDateFormat("yyyyMMdd").parse(source);
            } else if (source.length() == 19 && source.contains("-")) {
                return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(source);
            } else if (source.length() == 14 && !source.contains("-")) {
                return new SimpleDateFormat("yyyyMMddHHmmss").parse(source);
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return null;
    }

}
