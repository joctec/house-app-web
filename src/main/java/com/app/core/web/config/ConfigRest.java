package com.app.core.web.config;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.core.common.Result;
import com.app.core.validator.ValidatorContext;

@RestController
@RequestMapping("/sys/config")
@SuppressWarnings("rawtypes")
public class ConfigRest {

    @RequestMapping("/reload")
    @ResponseBody
    public Result reload() {
        ValidatorContext.init();
        return Result.success();
    }

    @RequestMapping("/validatorReload")
    @ResponseBody
    public Result reloadValidator() {
        ValidatorContext.init();
        return Result.success();
    }
}
