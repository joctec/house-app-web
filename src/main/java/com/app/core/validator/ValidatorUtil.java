package com.app.core.validator;

import java.util.regex.Pattern;

/**
 * 
 * 添加Pattern的缓存，避免每次都compile Pattern pattern = Pattern.compile("^[-\\+]?[\\d]*$");
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月28日 下午5:24:35
 */
public class ValidatorUtil {

    /**
     * 是否是整数
     * TODO Comment.
     * 
     * @param str
     * @return
     * @since v#{version}
     * @author zhangxf
     * @created 2015年10月28日 下午6:24:00
     */
    public static boolean isDigits(String str) {
        Pattern pattern = Pattern.compile("[0-9]*");
        return pattern.matcher(str).matches();
    }
    
    /**
     *是否是2位小数
     * 
     * @param str
     * @return
     * @since v#{version}
     * @author zhangxf
     * @created 2015年10月28日 下午6:23:17
     */
    public static boolean is2Number(String str) {
        Pattern pattern = Pattern.compile("^\\d+(\\.\\d{1,3})?$");  
        return pattern.matcher(str).matches();
    }
    
    /**
     * 是否是数字
     * TODO Comment.
     * 
     * @param str
     * @param decimalLen
     * @return
     * @since v#{version}
     * @author zhangxf
     * @created 2015年10月28日 下午6:23:42
     */
    public static boolean isNumber(String str, int decimalLen) {
        if(str == null){return true;}
        Pattern pattern = Pattern.compile("^\\d+(\\.\\d{1," +decimalLen+ "})?$");  
        return pattern.matcher(str).matches();
    }
    
    public static boolean isEmail(String str) {
        Pattern pattern = Pattern.compile("\\w+@(\\w+.)+[a-z]{2,3}");   
        return pattern.matcher(str).matches();
    }
    
    public static boolean isUrl(String str) {
        return true;
    }
}
