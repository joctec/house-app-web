package com.app.core.validator;

public class ValidateException extends RuntimeException {

    /**  */
    private static final long serialVersionUID = 1L;

    public ValidateException(Exception e) {
        super("Validator exception", e);
    }
}
