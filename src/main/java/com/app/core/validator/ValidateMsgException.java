package com.app.core.validator;


/**
 * 业务异常
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月24日 下午4:14:43
 */
public class ValidateMsgException extends RuntimeException {

    /**  */
    private static final long serialVersionUID = 1L;

    private ValidatorResult result;

    public ValidateMsgException(ValidatorResult result) {
        this.result = result;
    }

    public ValidatorResult getResult() {
        return result;
    }

    public void setResult(ValidatorResult result) {
        this.result = result;
    }

}
