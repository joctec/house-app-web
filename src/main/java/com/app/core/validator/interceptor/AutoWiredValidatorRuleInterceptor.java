package com.app.core.validator.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.app.core.validator.ValidatorContext;
import com.app.core.validator.annotation.ValidatorRule;

/**
 * 自动注入bean 验证规则拦截器
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月28日 上午7:42:26
 */
public class AutoWiredValidatorRuleInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
        ModelAndView modelAndView) throws Exception {
        if (modelAndView != null) {
            HandlerMethod handlerMethod = ((HandlerMethod) handler);
            ValidatorRule validatorRule = handlerMethod.getMethodAnnotation(ValidatorRule.class);
            if (validatorRule != null && validatorRule.value() != null) {
                StringBuilder validRule = new StringBuilder();
                Class<?>[] value = validatorRule.value();
                int size = value.length;
                for (Class<?> cla : value) {
                    String name = cla.getName();
                    /**
                     * 在这里可以生成单个的，也可以生成包含所有字段的json对象
                     */
                    String formRuleName = "formRule";
                    if (size > 1) {
                        formRuleName = formRuleName + "_" + cla.getSimpleName();
                    }
                    if (ValidatorContext.configMap.containsKey(name)) {
                        validRule.append("<script> var " + formRuleName + "="
                            + ValidatorContext.configMap.get(name).getJson() + ";</script>");
                    } else {
                        validRule.append("<script> var " + formRuleName + "={};</script>");
                    }
                }

                String[] names = validatorRule.name();
                size = names.length;
                for (String name : names) {
                    /**
                     * 在这里可以生成单个的，也可以生成包含所有字段的json对象
                     */
                    String formRuleName = "formRule";
                    if (size > 1) {
                        formRuleName = formRuleName + "_" + name;
                    }
                    if (ValidatorContext.configMap.containsKey(name)) {
                        validRule.append("<script> var " + formRuleName + "="
                            + ValidatorContext.configMap.get(name).getJson() + ";</script>");
                    } else {
                        validRule.append("<script> var " + formRuleName + "={};</script>");
                    }
                }
                modelAndView.addObject("validRule", validRule.toString());
            }
        }
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
        throws Exception {
    }

}
