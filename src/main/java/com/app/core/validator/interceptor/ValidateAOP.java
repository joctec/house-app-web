package com.app.core.validator.interceptor;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;

import com.app.core.util.Out;
import com.app.core.validator.ValidateBean;
import com.app.core.validator.annotation.Validate;

/**
 * 自动校验类拦截器
 * TODO Comment.
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月30日 下午4:48:10
 */
@Aspect
public class ValidateAOP {

    public void init() {
        Out.i("ValidateAOP init");
    }

    @Around(value = "execution(* com.guda.mp.web.controller.admin.*.*(..)) && @annotation(validate)")
    private Object doAround(ProceedingJoinPoint pjp, Validate validate) throws Throwable {
        Object[] args = pjp.getArgs();
        for (Object object : args) {
        	Class<?>[] value = validate.value();
            String code = validate.code();
            if (value != null && value.length > 0) {
                for (Class<?> v : value) {
                    if (object.getClass().getName().equals(v.getName())) {
                    	if(code.equals("")){
                    		ValidateBean.vali(object, object.getClass().getName());
                    	}else{
                    		ValidateBean.vali(object, object.getClass().getName() + "_" + code);
                    	}
                    } else {
                        Class<?> superclass = object.getClass().getSuperclass();
                        if (superclass != null && superclass.getName().equals(v.getName())) {
                        	if(code.equals("")){
                        		ValidateBean.vali(object, superclass.getName());
                        	}else{
                        		ValidateBean.vali(object, object.getClass().getName() + "_" + code);
                        	}
                        }
                    }
                }
            }

//            String[] name = validate.name();
//            if (name != null && name.length > 0) {
//                for (String n : name) {
//                    if (object.getClass().getName().equals(n)) {
//                        ValidateBean.vali(object, n);
//                    }
//                }
//            }
        }

        Object retVal = pjp.proceed();

        return retVal;
    }
}