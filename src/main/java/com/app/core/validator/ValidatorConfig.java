package com.app.core.validator;

import java.util.Map;

public class ValidatorConfig {

    private String object;
    private String json;
    public Map<String, String> names;
    public Map<String, Map<String, String>> rules;
    public Map<String, Map<String, String>> messages;

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public Map<String, Map<String, String>> getRules() {
        return rules;
    }

    public void setRules(Map<String, Map<String, String>> rules) {
        this.rules = rules;
    }

    public Map<String, Map<String, String>> getMessages() {
        return messages;
    }

    public void setMessages(Map<String, Map<String, String>> messages) {
        this.messages = messages;
    }

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    public Map<String, String> getNames() {
        return names;
    }

    public void setNames(Map<String, String> names) {
        this.names = names;
    }
}
