package com.app.core.validator;

import java.util.Map;

/**
 * 
 * TODO Comment.
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年11月7日 上午11:55:32
 */
public class ValidatorResult {

    private boolean validateFrom = true;
    private boolean validateAll;
    private String field;
    private String fieldName;
    private String msg;
    private Map<String, String> all;

    public String getField() {
        return field;
    }

    public ValidatorResult setField(String field) {
        this.field = field;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public ValidatorResult setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public String getFieldName() {
        return fieldName;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public boolean isValidateAll() {
        return validateAll;
    }

    public ValidatorResult setValidateAll(boolean validateAll) {
        this.validateAll = validateAll;
        return this;
    }

    public Map<String, String> getAll() {
        return all;
    }

    public ValidatorResult setAll(Map<String, String> all) {
        this.all = all;
        return this;
    }

    public boolean isValidateFrom() {
        return validateFrom;
    }

    public void setValidateFrom(boolean validateFrom) {
        this.validateFrom = validateFrom;
    }

}
