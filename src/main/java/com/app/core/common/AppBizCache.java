package com.app.core.common;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.lang.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.app.core.session.UserSession;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;
import com.app.core.web.util.AppContextUtil;
import com.guda.mp.entity.SysDict;

/**
 * 1. 所有业务缓存集中处理类
 * 2. 字典项保存在这里
 * 
 * @author zhangxf
 * @created 2017年3月24日 下午9:30:28
 */
public class AppBizCache {

    public static Map<String, List<SysDict>> dicts = new HashMap<String, List<SysDict>>();;
    public static Map<String, SysDict> dictVauleMap = new HashMap<String, SysDict>();
    public static JSONObject baseConfig;

    /** 鲁尔中间库中存在的设备的编码 */
    public static Map<Long, String> luerTransferDeviceSnMap;

    public static void initDict(String dictStr) {
        JSONObject jsonDicts = JSON.parseObject(dictStr);
        Set<Entry<String, Object>> entrySet = jsonDicts.entrySet();
        for (Entry<String, Object> entry : entrySet) {
            String key = entry.getKey();
            dicts.put(key, new ArrayList<SysDict>());
            JSONArray ja = (JSONArray) entry.getValue();
            for (Object object : ja) {
                JSONObject jo = (JSONObject) object;
                SysDict dict = new SysDict();
                dict.setName(jo.getString("name"));
                dict.setCode(jo.getString("code"));
                dicts.get(key).add(dict);
                dictVauleMap.put(key + "_" + jo.getString("code"), dict);
            }
        }
        System.out.println("json dict init end.");
    }

    public static void initDictDB(List<SysDict> dictList) {
        /*
         * 把字典项分为code-list结构,1：字典项， 2：字典项值
         */
        Map<Long, SysDict> dictItemMap = new HashMap<Long, SysDict>();
        for (SysDict sysDict : dictList) {
            if (sysDict.getType() == null) {
                continue;
            }
            if (sysDict.getType().intValue() == 1) {
                dictItemMap.put(sysDict.getId(), sysDict);
                dicts.put(sysDict.getCode(), new ArrayList<SysDict>());
            }
        }

        for (SysDict sysDict : dictList) {
            if (sysDict.getType() == null) {
                continue;
            }

            if (sysDict.getType().intValue() == 2) {
                String dictItemCode = dictItemMap.get(sysDict.getPid()).getCode();
                dictVauleMap.put(dictItemCode + "_" + sysDict.getCode(), sysDict);
                dicts.get(dictItemCode).add(sysDict);
            }
        }

        System.out.println("db dict init end.");
    }

    /**
     * 对应的字典项指
     */
    public static List<SysDict> dicts(String code) {
        return dicts.get(code);
    }

    public static SysDict dict(String code, String value) {
        return dictVauleMap.get(code + "_" + value);
    }

    public static String dictDecode(String code, String value) {
        if (StringUtils.isBlank(code)) {
            return "无此字典项";
        }

        if (StringUtils.isBlank(value)) {
            return "";
        }
        try {
            return dictVauleMap.get(code + "_" + value).getName();
        } catch (Exception e) {
            return value;
        }
    }

    public static String dictDecode(String code, Object value) {
        if (StringUtils.isBlank(code)) {
            return "";
        }
        if (value == null) {
            return "";
        }

        String val = value.toString();
        if (StringUtils.isBlank(val)) {
            return "";
        }

        try {
            return dictVauleMap.get(code + "_" + val).getName();

        } catch (Exception e) {
            return val;
        }
    }

    public static String baseConfig(String code) {
        if (StringUtils.isBlank(code)) {
            return "";
        }
        try {
            return baseConfig.getString(code);
        } catch (Exception e) {
            return "";
        }
    }

    public static Object get(String key) {
        return AppContextUtil.getSessionValue(key);
    }

    /**
     * 判断是否有权限
     * 
     * @param code
     * @return
     * @since v#{version}
     * @author zhangxf
     * @created 2017年4月12日 下午1:57:12
     */
    public static boolean hasPer(String code) {
        UserSession userSession = RequestContext.getUserSession();
        if (userSession.getLoginName().equals(SysConfig.get(AppCons.APP_SUPER_ACCOUNT))) {
            return true;
        }

        Map<String, Integer> userHasMap = userSession.getUserPermissionCode();
        // 有权限
        if (userHasMap != null && userHasMap.containsKey(code)) {
            return true;
        }

        return false;
    }

}
