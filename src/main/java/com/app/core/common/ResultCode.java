package com.app.core.common;

/**
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月24日 下午3:51:43
 */
public enum ResultCode {
    SUCCESS("200", "成功"), DEFAULT_ERROR("1000", "系统出错, 请联系管理员!"), ERROR("1001", "系统出错,错误未知!"), PARAM_ERROR("1002",
        "提交参数异常, 请联系管理员!"), SERVER_INNER_ERROR("1003", "请求错误，服务器内部发生异常！请联系管理员!"), SESSION_EXPIRE("1004",
            "未登录或会话已过期, 请重新登录!"), ILLEGAL_ACCESS("1005", "无权限,非法访问!"), OTHER_LOGIN("1006", "账号异常: 您的账号在异地同时登录！"),
    // 表单提交错误码-- start
    PARAM_VALIDATE_ERROR("6001", "输入参数不正确！"), PARAM_VALIDATE_ERROR_TIP("6002", "请填写正确的必填项"),
    // 表单提交错误码-- end

    RECORD_NOT_EXSIT("5001", "不存在此记录!"), DATA_SAVE_ERROR("5002", "保存出错!"), URL_NOT_FOUND("5004",
        "访问的地址不存在!"), STR_FAILED("444", "FAILED");

    private String code;

    private String msg;

    private ResultCode(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
