package com.app.core.common;

import java.io.Serializable;

public class Dict implements Serializable {
	private static final long serialVersionUID = 1L;

	/**
	 * 数据字典键
	 */
	private String name;
	/**
	 * 数据字典编码
	 */
	private String code;

	/**
	 * 字典项的值
	 */
	private String value;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
