package com.app.core.common;

import java.io.Serializable;

/**
 * 此类主要用于接口返回Result时，不需要返回任何数据时，给出明确结果
 */
public class NoData implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8068100915529573309L;

}
