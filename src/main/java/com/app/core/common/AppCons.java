package com.app.core.common;

/**
 * AppCons只存放系统级别参数配置
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年12月15日 下午4:46:08
 */
public class AppCons {

    /**
     * 运行模式 true是单机 false集群
     */
    public static final boolean RUN_MODE_ALONE = false;

    public static final boolean OTHER_LOGIN_AUTH = false;

    public static final String APP_SUPER_ACCOUNT = "app.superAccount";

    /**
     * 登录党组织用户会话对象
     */
    public static final String LOGIN_USER_SESSION_ADMIN = "login_user_session_admin";

    /**
     * 登录党组织用户session id
     */
    public static final String LOGIN_USER_SESSION_ID_ADMIN = "session_id_admin";
    /**
     * H5登录 session id
     */
    public static final String LOGIN_USER_SESSION_ID_H5 = "session_id_h5";

    /**
     * 
     */
    public static final String LOGIN_USER_SESSION_CUSER = "LOGIN_USER_SESSION_CUSER";

    /**
     * 映射微信用户openid的key
     */
    public static final String LOGIN_USER_SESSION_ID_WX_TMP = "session_id_wx_tmp";
    /**
     * 微信openid的key
     */
    public static final String OPEN_ID = "openId";
    /**
     * 微信头像地址key
     */
    public static final String WX_USER_HEAD_IMG_URL = "wxUserHeadimgurl";

    /**
     * 会话超时时间
     */
    public static int USER_SESSION_TIMEOUT = 60 * 60 * 6;

    /**
     * 微信二维码登陆系统，二维码过期时间 2分钟过期
     * 
     */
    public static int QR_LOGIN_TIMEOUT = 60 * 2;
    /**
     * cookie过期
     */
    public static int COOKIE_EXPIRE = -1;

    public static String LOGIN_RETRY = "login_retry";

    public static int LOGIN_MAX_RETRY_TIMES = 5;

    /**
     * session和用户名映射key
     */
    public static String SESSION_USER_MAPPING = "sess_user_mapping";
    /**
     * 异地登陆key
     */
    public static String OTHER_LOGIN = "other_login";

    /**
     * 文件服务器路径
     */
    public static String FILEUPLOAD_SERVER;
    /**
     * 文件上传路径(鉴权)
     */
    public static boolean FILEUPLOAD_AUTH_PATH_IS_CONTEXT;
    public static String FILEUPLOAD_AUTH_PATH;
    public static String FILEUPLOAD_AUTH_REAL_PATH;
    /**
     * 文件上传路径(开放)
     */
    public static boolean FILEUPLOAD_OPEN_PATH_IS_CONTEXT;
    public static String FILEUPLOAD_OPEN_PATH;
    public static String FILEUPLOAD_OPEN_REAL_PATH;

    /**
     * 系统菜单缓存key
     */
    public static String SYS_MENU_URL_MAP_CACHE_KEY = "sys_menu_url_map";
    /**
     * 角色对应的登录用户map key，用于刷新用户的权限
     */
    public static String ROLE_LINK_USER_CACHE_KEY_PRIFIX = "role_link_user_";

}
