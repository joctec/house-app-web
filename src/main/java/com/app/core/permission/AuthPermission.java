package com.app.core.permission;

import com.app.core.common.AppCons;
import com.app.core.session.UserSession;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;

public class AuthPermission {

    /**
     * 验证用户是否具备某权限。
     * 
     * @param permission
     *        权限名称
     * @return 用户是否具备某权限
     */
    public boolean has(String permissionCode) {
        UserSession userSession = RequestContext.getUserSession();
        if (userSession.getLoginName().equals(SysConfig.get(AppCons.APP_SUPER_ACCOUNT))) {
            return true;
        } else if (userSession.getUserPermissionCode().containsKey(permissionCode)) {
            return true;
        }

        return false;
    }

}
