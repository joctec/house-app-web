package com.app.core.session;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.collections.CollectionUtils;

import com.app.core.util.MD5Util;
import com.guda.mp.vo.SysResourceVo;

/**
 * 用户会话对象<br>
 * 存储会话基本字段，如果需要额外存储登录用户信息，可以传入自定义用户对象：T user
 * 
 * @param <T>
 * @since v#{version}
 * @author zhangxf
 * @created 2015年12月15日 下午3:24:27
 */
public class BaseUserSession<T> implements Serializable {

    /**  */
    private static final long serialVersionUID = 1L;

    /**
     * 会话id
     */
    private String sessionId;

    /**
     * 登录用户组织id
     */
    private Long orgId;

    /**
     * 登录用户id
     */
    private Long loginUserId;
    /**
     * session会话主要字段： 用户id
     */
    private Long userId;
    /**
     * session会话主要字段： 用户名
     */
    private String loginName;
    /**
     * session会话主要字段： 账号状态(用作实现用户会话实时锁定)
     */
    private String status;
    /**
     * 用户权限resouce
     */
    private List<SysResourceVo> userResouceMenu;
    /**
     * 用户权限resouce
     */
    private List<SysResourceVo> userResouceAll;
    /**
     * 用户拥有权限,url作为key,value为对应的index【List<SysResourceVo> userResouce】
     */
    private Map<String, Integer> userPermissionUrl;
    /**
     * 用户拥有权限,code作为key,value为对应的index【List<SysResourceVo> userResouce】
     */
    private Map<String, Integer> userPermissionCode;
    /**
     * 用户起始登录时间戳
     */
    private long timestamp;
    /**
     * 用户登录后主页
     */
    private String homePage;

    /**
     * 当前用户登录对象
     */
    private T loginUser;

    public BaseUserSession() {
        super();
        this.sessionId = BaseUserSession.generateSessionId();
        timestamp = System.currentTimeMillis();
    }

    public Long getOrgId() {
        return orgId;
    }

    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Map<String, Integer> getUserPermissionUrl() {
        return userPermissionUrl;
    }

    public void setUserPermissionUrl(Map<String, Integer> userPermissionUrl) {
        this.userPermissionUrl = userPermissionUrl;
    }

    public Map<String, Integer> getUserPermissionCode() {
        return userPermissionCode;
    }

    public void setUserPermissionCode(Map<String, Integer> userPermissionCode) {
        this.userPermissionCode = userPermissionCode;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getLoginName() {
        return loginName;
    }

    public void setLoginName(String loginName) {
        this.loginName = loginName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHomePage() {
        return homePage;
    }

    public void setHomePage(String homePage) {
        this.homePage = homePage;
    }

    public T getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(T loginUser) {
        this.loginUser = loginUser;
    }

    public Long getLoginUserId() {
        return loginUserId;
    }

    public void setLoginUserId(Long loginUserId) {
        this.loginUserId = loginUserId;
    }

    public List<SysResourceVo> getUserResouceMenu() {
        return userResouceMenu;
    }

    public void setUserResouceMenu(List<SysResourceVo> userResouceMenu) {
        this.userResouceMenu = userResouceMenu;
    }

    public List<SysResourceVo> getUserResouceAll() {
        return userResouceAll;
    }

    public void setUserResouceAll(List<SysResourceVo> userResouceAll) {
        this.userResouceAll = userResouceAll;
        if (CollectionUtils.isEmpty(userResouceAll)) {
            return;
        }

        Map<String, Integer> codeMap = new HashMap<String, Integer>();
        Map<String, Integer> urlMap = new HashMap<String, Integer>();
        setUserPermissionCode(codeMap);
        setUserPermissionUrl(urlMap);

        int size = userResouceAll.size();
        for (int i = 0; i < size; i++) {
            SysResourceVo vo = userResouceAll.get(i);
            codeMap.put(vo.getCode(), i);
            urlMap.put(vo.getUrl(), i);
        }
    }

    /**
     * 生成seesionId
     * 
     * @return
     * @since v#{version}
     * @author zhangxf
     * @created 2015年10月12日 上午8:25:21
     */
    public static String generateSessionId() {
        return MD5Util.getMD5String(MD5Util.passWordMd5(UUID.randomUUID().toString().replaceAll("-", "")));
    }
}
