package com.app.core.base;

import java.io.Serializable;

/**
 * 
 * BaseEntity
 * 
 * @since v0.1
 * @author zxf
 * @created 2015年6月4日 下午4:30:50
 */
public class BaseEntityId implements Serializable {

    private static final long serialVersionUID = 1L;

    protected Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
