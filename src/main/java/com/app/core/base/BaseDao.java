package com.app.core.base;

import java.util.List;

public interface BaseDao<T> {

    int add(T bean);

    int updateById(T bean);

    int logicDeleteById(long id);

    int deleteById(long id);

    T selectById(long id);

    List<T> select(T query);

    Long selectCount(T query);

    List<T> selectById(List<Long> list);

    int deleteByIds(List<Long> list);
}
