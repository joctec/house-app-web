package com.app.core.base;

import java.io.Serializable;
import java.util.List;

import com.app.core.common.Result;
import com.github.pagehelper.Page;

/**
 * 
 * BaseService
 * 
 * @param <T>
 * @since v0.1
 * @author zxf
 * @created 2015年6月4日 下午4:31:14
 */
@SuppressWarnings("rawtypes")
public interface BaseService<T extends Serializable> {

    Result add(T record);

    Result updateById(T record);

    Result deleteById(long id);

    Result deleteByIds(String ids);

    T selectById(long id);

    List<T> select(T record);

    Long selectCount(T record);

    Page<T> selectBasePage(PageQuery pageParams, T bean);

    Result logicDeleteById(long id);

}
