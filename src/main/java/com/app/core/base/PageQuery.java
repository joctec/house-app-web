package com.app.core.base;

public class PageQuery implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 默认每页显示行数常量
     */
    public static final int DEFAULT_PAGE_SIZE = 10;
    /**
     * 当前页码
     */
    private int pageNum;
    /**
     * 每页显示行数
     */
    private int pageSize;
    /**
     * 总记录数
     */
    private long total;
    /**
     * 排序
     */
    private String orderBy;
    /**
     * 是否查询全部数据
     */
    private Boolean queryAll;

    private int startRow;

    /**
     * 查询类型。1下拉 2上拉
     */
    private int queryType;
    /**
     * 当前数据id
     */
    private int curId;

    public static PageQuery getInstance() {
        return new PageQuery().setPageSize(DEFAULT_PAGE_SIZE).setPageNum(1);
    }

    public int getPageNum() {
        if (pageNum == 0) {
            pageNum = 1;
        }
        return pageNum;
    }

    public PageQuery setPageNum(int pageNum) {
        this.pageNum = pageNum;
        return this;
    }

    public int getPageSize() {
        if (pageSize == 0) {
            pageSize = DEFAULT_PAGE_SIZE;
        }
        return pageSize;
    }

    public PageQuery setPageSize(int pageSize) {
        this.pageSize = pageSize;
        return this;
    }

    public long getTotal() {
        return total;
    }

    public PageQuery setTotal(long total) {
        this.total = total;
        return this;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public PageQuery setOrderBy(String orderBy) {
        this.orderBy = orderBy;
        return this;
    }

    public Boolean getQueryAll() {
        return queryAll;
    }

    public PageQuery setQueryAll(Boolean queryAll) {
        this.queryAll = queryAll;
        return this;
    }

    public int getStartRow() {
        startRow = (pageNum - 1) * pageSize;
        return startRow;
    }

    public void setStartRow(int startRow) {
        this.startRow = startRow;
    }

    public int getQueryType() {
        return queryType;
    }

    public void setQueryType(int queryType) {
        this.queryType = queryType;
    }

    public int getCurId() {
        return curId;
    }

    public void setCurId(int curId) {
        this.curId = curId;
    }
}
