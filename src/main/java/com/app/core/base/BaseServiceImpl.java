package com.app.core.base;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.common.Result;
import com.app.core.common.ResultCode;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;

/**
 * 
 * 
 * @param <D>
 * @param <T>
 * @since v0.1
 * @author zxf
 * @created 2015年6月2日 下午2:02:47
 */
@Transactional
@SuppressWarnings("rawtypes")
public abstract class BaseServiceImpl<T extends Serializable> implements BaseService<T> {

    /**
     * 服务层默认dao 待子类实现，调用子类的方法 警告:子类未实现时显示返回null
     * 
     * @return
     * @since v#{version}
     * @author zhangxf
     * @created 2015年10月24日 下午3:58:01
     */
    protected BaseDao<T> getDao() {
        throw new NullPointerException("您调用了BaseServiceImpl的公共方法，而未重写getDao()方法，所以抛出异常！");
    }

    @Override
    public List<T> select(T record) {
        return getDao().select(record);
    }

    @Override
    public Long selectCount(T record) {
        return getDao().selectCount(record);
    }

    @Override
    public T selectById(long id) {
        return getDao().selectById(id);
    }

    @Override
    public Result add(T record) {
        return getDao().add(record) > 0 ? Result.success() : Result.error(ResultCode.DATA_SAVE_ERROR);
    }

    @Override
    public Result logicDeleteById(long id) {
        return getDao().logicDeleteById(id) > 0 ? Result.success() : Result.error(ResultCode.RECORD_NOT_EXSIT);
    }

    @Override
    public Result deleteById(long id) {
        return getDao().deleteById(id) > 0 ? Result.success() : Result.error(ResultCode.RECORD_NOT_EXSIT);
    }

    @Override
    public Result deleteByIds(String ids) {
        if (StringUtils.isBlank(ids)) {
            return Result.error(ResultCode.RECORD_NOT_EXSIT);
        }

        String[] idArray = StringUtils.split(ids, ",");
        List<Long> idList = new ArrayList<Long>();
        for (String id : idArray) {
            idList.add(Long.valueOf(StringUtils.trim(id)));
        }
        return getDao().deleteByIds(idList) > 0 ? Result.success() : Result.error(ResultCode.RECORD_NOT_EXSIT);
    }

    @Override
    public Result updateById(T record) {
        return getDao().updateById(record) > 0 ? Result.success() : Result.error(ResultCode.RECORD_NOT_EXSIT);
    }

    @Override
    public Page<T> selectBasePage(PageQuery pageParams, T bean) {
        if (pageParams == null) {
            PageHelper.startPage(1, PageQuery.DEFAULT_PAGE_SIZE);
        } else {
            PageHelper.startPage(pageParams.getPageNum(), pageParams.getPageSize());
        }
        return (Page<T>) getDao().select(bean);
    }

    protected void setPageQuery(PageQuery pageParams) {
        if (pageParams == null) {
            PageHelper.startPage(1, PageQuery.DEFAULT_PAGE_SIZE);
        } else {
            PageHelper.startPage(pageParams.getPageNum(), pageParams.getPageSize());
        }
    }

    protected void setPageQuery(BaseEntity baseEntity) {
        if (baseEntity.getPageQueryType() == null) {
            baseEntity.setPageQueryType(1);
        }
        if (baseEntity.getPageCurId() == null) {
            baseEntity.setPageCurId(0);
        }
        if (baseEntity.getPageSize() == null) {
            baseEntity.setPageSize(20);
        }
    }

}
