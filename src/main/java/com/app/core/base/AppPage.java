package com.app.core.base;

import java.util.List;

public class AppPage<T> implements java.io.Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 当前页最小id
     */
    private long curPageMinId;
    /**
     * 当前页最大id
     */
    private long curPageMaxId;
    /**
     * 总记录数
     */
    private long total;
    /**
     * 数据list
     */
    private List<T> rows;

    public long getCurPageMinId() {
        return curPageMinId;
    }

    public void setCurPageMinId(long curPageMinId) {
        this.curPageMinId = curPageMinId;
    }

    public long getCurPageMaxId() {
        return curPageMaxId;
    }

    public void setCurPageMaxId(long curPageMaxId) {
        this.curPageMaxId = curPageMaxId;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
