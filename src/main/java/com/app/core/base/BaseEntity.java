package com.app.core.base;

import java.io.Serializable;
import java.util.Date;

import org.apache.commons.lang3.builder.ToStringBuilder;

import com.alibaba.fastjson.JSON;
import com.app.core.dao.annotation.BaseEntityAnnotation;
import com.app.core.util.ASEUtil;
import com.app.core.util.Out;

/**
 * 
 * BaseEntity
 * 
 * @since v0.1
 * @author zxf
 * @created 2015年6月4日 下午4:30:50
 */
@BaseEntityAnnotation
public class BaseEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    protected Long id;

    /**
     * 新增时间
     */
    protected Date createTime;
    /**
     * 新增人
     */
    protected String createUser;
    /**
     * 修改时间
     */
    protected Date updateTime;
    /**
     * 修改人
     */
    protected String updateUser;

    /**
     * 删除标志
     */
    protected Integer delFlag;

    /**
     * 根据id数字返回加密后的id字符串，前台所有参数的传递需要传递该值
     */
    protected String encodeId;

    /**
     * 当前查询id
     */
    protected Integer pageCurId;
    /**
     * 当前查询方式 1下拉刷新 2上拉加载
     */
    protected Integer pageQueryType;

    /**
     * 每页显示条数
     */
    protected Integer pageSize;

    public String getEncodId() {
        String returnId = "";
        if (this.getId() != null) {
            returnId = ASEUtil.aesEncrypt(this.getId() + "");
        }
        return returnId;
    }

    public void setEncodId(String encodeId) {
        this.encodeId = encodeId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public Integer getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(Integer delFlag) {
        this.delFlag = delFlag;
    }

    public Integer getPageCurId() {
        return pageCurId;
    }

    public void setPageCurId(Integer pageCurId) {
        this.pageCurId = pageCurId;
    }

    public Integer getPageQueryType() {
        return pageQueryType;
    }

    public void setPageQueryType(Integer pageQueryType) {
        this.pageQueryType = pageQueryType;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public void debug() {
        Out.i("debug info: " + JSON.toJSONString(this));
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }
}
