package com.app.core.exception;

/**
 * 业务异常
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月24日 下午4:14:43
 */
public class BizException extends RuntimeException {

	/**  */
	private static final long serialVersionUID = 1L;

	public BizException(String msg) {
		super(msg);
	}

	public BizException(String msg, Throwable e) {
		super(msg, e);
	}
}
