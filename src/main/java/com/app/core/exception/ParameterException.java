package com.app.core.exception;

/**
 * 参数异常
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月24日 下午4:15:00
 */
public class ParameterException extends RuntimeException {

    /**  */
    private static final long serialVersionUID = 1L;

}
