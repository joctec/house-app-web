package com.app.core.task;

public class Event implements Runnable {
	@Override
	public void run() {
		exec();
	}

	protected void exec() {
		// 延迟5秒执行
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
