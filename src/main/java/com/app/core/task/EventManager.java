package com.app.core.task;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class EventManager {

    private static ExecutorService executorService;

    public static void init() {
        if (executorService == null) {
            // 获取CPU合数作为线程池数量
            int availableProcessors = Runtime.getRuntime().availableProcessors();
            executorService = Executors.newFixedThreadPool(availableProcessors);
        }
    }

    /**
     * 触发执行事件
     * 
     * @param event
     */
    public static void trigger(Event event) {
        init();
        executorService.execute(event);
    }

    public static void destory() {
        if (executorService != null) {
            executorService.shutdown();
        }
    }

}
