package com.app.core.velocity;

import java.io.IOException;
import java.io.Writer;

import org.apache.velocity.context.InternalContextAdapter;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.directive.Directive;
import org.apache.velocity.runtime.parser.node.Node;

import com.app.core.common.AppBizCache;

/**
 * 权限标签
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2017年4月12日 下午1:56:56
 */
public class HasPermission extends Directive {

    public boolean render(InternalContextAdapter context, Writer writer, Node node)
        throws IOException, ResourceNotFoundException, ParseErrorException, MethodInvocationException {

        String code = node.jjtGetChild(0).value(context).toString();

        boolean hasPer = AppBizCache.hasPer(code);

        if (hasPer) {
            node.jjtGetChild(node.jjtGetNumChildren() - 1).render(context, writer);
        }

        return hasPer;
    }

    @Override
    public String getName() {
        return "permission";
    }

    @Override
    public int getType() {
        return BLOCK;
    }
}
