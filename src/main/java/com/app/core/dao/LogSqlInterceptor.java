package com.app.core.dao;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.apache.ibatis.executor.Executor;
import org.apache.ibatis.mapping.BoundSql;
import org.apache.ibatis.mapping.MappedStatement;
import org.apache.ibatis.mapping.ParameterMapping;
import org.apache.ibatis.plugin.Interceptor;
import org.apache.ibatis.plugin.Intercepts;
import org.apache.ibatis.plugin.Invocation;
import org.apache.ibatis.plugin.Plugin;
import org.apache.ibatis.plugin.Signature;
import org.apache.ibatis.reflection.MetaObject;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.ResultHandler;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.type.TypeHandlerRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Intercepts({ @Signature(type = Executor.class, method = "update", args = { MappedStatement.class, Object.class }),
        @Signature(type = Executor.class, method = "query", args = { MappedStatement.class, Object.class,
                RowBounds.class, ResultHandler.class }) })
public class LogSqlInterceptor implements Interceptor {

    private static Logger logger = LoggerFactory.getLogger(LogSqlInterceptor.class);

    /**
     * 是否记录sql
     */
    private boolean logSql;

    public Object intercept(Invocation invocation) throws Throwable {
        if (!logSql) {
            return invocation.proceed();
        }

        MappedStatement mappedStatement = (MappedStatement) invocation.getArgs()[0];
        Object parameter = null;
        if (invocation.getArgs().length > 1) {
            parameter = invocation.getArgs()[1];
        }
        String sqlId = mappedStatement.getId();
        BoundSql boundSql = mappedStatement.getBoundSql(parameter);
        Configuration configuration = mappedStatement.getConfiguration();
        Object returnValue = null;
        long start = System.currentTimeMillis();
        returnValue = invocation.proceed();
        long end = System.currentTimeMillis();
        long time = (end - start);
        try {
            String sql = getSql(configuration, boundSql, sqlId, time);
            logger.info("\n======sql detail:========================================================\n" + sql + "\n");
        } catch (Exception e) {
            logger.error("======>log sql error!", e);
        }
        return returnValue;
    }

    public static String getSql(Configuration configuration, BoundSql boundSql, String sqlId, long time) {
        String sql = showSql(configuration, boundSql);
        List<ParameterMapping> params = boundSql.getParameterMappings();
        int psize = params == null ? 0 : params.size();

        StringBuilder str = new StringBuilder();
        str.append("======> method: " + sqlId);
        str.append("\n======> SQL length: ").append(boundSql.getSql().length());
        str.append("\n======> params size: ").append(psize);
        str.append("\n======> times: ");
        str.append(time);
        str.append(" ms");
        str.append("\n======> source sql:\n");
        str.append(boundSql.getSql().replaceAll("(?m)^\\s*$(\\n|\\r\\n)", ""));
        str.append("\n======> debug sql:\n");
        str.append(sql.replaceAll("(?m)^\\s*$(\\n|\\r\\n)", ""));
        return str.toString();
    }

    private static String getParameterValue(Object obj) {
        String value = null;
        if (obj == null) {
            return "null";
        }

        if (obj instanceof String) {
            value = "'" + obj.toString() + "'";
        } else if (obj instanceof Date) {
            value = "'" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(obj) + "'";
        } else {
            value = obj.toString();
        }

        return value;
    }

    public static String showSql(Configuration configuration, BoundSql boundSql) {
        Object parameterObject = boundSql.getParameterObject();
        List<ParameterMapping> parameterMappings = boundSql.getParameterMappings();
        // String sql = boundSql.getSql().replaceAll("[\\s]+", " ");
        String sql = boundSql.getSql();
        if (parameterMappings.size() > 0 && parameterObject != null) {
            TypeHandlerRegistry typeHandlerRegistry = configuration.getTypeHandlerRegistry();
            if (typeHandlerRegistry.hasTypeHandler(parameterObject.getClass())) {
                sql = sql.replaceFirst("\\?", getParameterValue(parameterObject));

            } else {
                MetaObject metaObject = configuration.newMetaObject(parameterObject);
                for (ParameterMapping parameterMapping : parameterMappings) {
                    String propertyName = parameterMapping.getProperty();
                    if (metaObject.hasGetter(propertyName)) {
                        Object obj = metaObject.getValue(propertyName);
                        sql = sql.replaceFirst("\\?", getParameterValue(obj));
                    } else if (boundSql.hasAdditionalParameter(propertyName)) {
                        Object obj = boundSql.getAdditionalParameter(propertyName);
                        sql = sql.replaceFirst("\\?", getParameterValue(obj));
                    }
                }
            }
        }
        return sql;
    }

    public Object plugin(Object target) {
        return Plugin.wrap(target, this);
    }

    public void setProperties(Properties properties0) {
        Object object = properties0.get("logSql");
        this.logSql = (object != null && object.equals("true")) ? true : false;
    }
}