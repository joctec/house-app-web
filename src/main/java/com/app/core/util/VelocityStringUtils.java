package com.app.core.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;

public class VelocityStringUtils {

	// 计算传入日期和当天的关系
	public String dateFrom(String mils) {
		if (null == mils) {
			return "10天前";

		}
		Date date = new Date(Long.parseLong(mils));
		String result = "";
		Long time = date.getTime();
		Long nowtime = (new Date()).getTime();
		Long splitTime = (nowtime - time) * 1000;
		// 一小时内
		if (splitTime <= 60 * 60) {
			result = "刚刚";
		} else if (splitTime > 60 * 60 && splitTime <= 24 * 60 * 60) {
			result = "今天";
		} else if (splitTime > 24 * 60 * 60 && splitTime <= 3 * 24 * 60 * 60) {
			result = "1天前";
		} else if (splitTime > 3 * 24 * 60 * 60 && splitTime <= 5 * 24 * 60 * 60) {
			result = "3天前";
		} else {
			result = "5天前";
		}
		return result;
	}

	// 计算传入日期和当天的关系
	public String dateFrom(Date date) {
		if (null == date) {
			return "10天前";
		}
		String result = "";
		Long time = date.getTime();
		Long nowtime = (new Date()).getTime();
		Long splitTime = (nowtime - time) * 1000;
		// 一小时内
		if (splitTime <= 60 * 60) {
			result = "刚刚";
		} else if (splitTime > 60 * 60 && splitTime <= 24 * 60 * 60) {
			result = "今天";
		} else if (splitTime > 24 * 60 * 60 && splitTime <= 3 * 24 * 60 * 60) {
			result = "1天前";
		} else if (splitTime > 3 * 24 * 60 * 60 && splitTime <= 5 * 24 * 60 * 60) {
			result = "3天前";
		} else {
			result = "5天前";
		}
		return result;
	}

	public String strDefault(String str, String defaultStr) {
		if (StringUtils.isBlank(str)) {
			return defaultStr;
		}

		return str;
	}

	public boolean isNull(String str) {
		return StringUtils.isBlank(str);
	}

	public String[] split(String str, String split) {
		if (StringUtils.isBlank(str)) {
			return new String[] {};
		}

		return str.split(split);
	}

	public static Integer toInt(String str) {
		if (StringUtils.isBlank(str)) {
			return null;
		}
		return Integer.parseInt(str);
	}

	public static boolean greater(String num1, String num2) {
		if (StringUtils.isBlank(num1) || StringUtils.isBlank(num2)) {
			return false;
		}
		return Integer.parseInt(num1) >= Integer.parseInt(num2);
	}

	public static String dateFormat(String format, String dateLongStr) {
		if (StringUtils.isBlank(dateLongStr) || StringUtils.isBlank(format)) {
			return "";
		}
		String str;
		try {
			str = new SimpleDateFormat(format).format(new Date(Long.parseLong(dateLongStr)));
		} catch (Exception e) {
			return "";
		}

		return str;
	}

	public static String point2(String str) {
		// BigDecimal bd = new BigDecimal(Double.parseDouble(str));
		// System.out.println(bd.setScale(2,
		// BigDecimal.ROUND_HALF_UP).doubleValue());
		// System.out.println("=================");
		// DecimalFormat df = new DecimalFormat("#.00");
		// System.out.println(df.format(Double.parseDouble(str)));
		// System.out.println("=================");
		// System.out.println(String.format("%.2f", Double.parseDouble(str)));
		// System.out.println("=================");
		if (StringUtils.isBlank(str)) {
			str = "0.00";
		}
		NumberFormat nf = NumberFormat.getNumberInstance();
		nf.setMaximumFractionDigits(2);
		return nf.format(Double.parseDouble(str));
	}

	public static long doubleToInt(double d) {
		return Math.round(d);
	}

	public static String debug(Object object) {
		Out.debugJson(object);
		return JSON.toJSONString(object);
	}

	public static String longTime2Str(Long time, String format) {
		Date d = new Date(time);
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(d);
	}

	public static String longTime2Str(String time, String format) {
		Date d = new Date(Long.parseLong(time));
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		return sdf.format(d);
	}

	public static String stringTodouble(String s) {
		if (StringUtils.isBlank(s)) {
			s = "0";
		}
		Double b = Double.parseDouble(s);
		b = b * 10;
		return b + "";
	}

	public static Double doubleHalfUp(Double val) {
		if (val == null) {
			return null;
		}
		
		BigDecimal b = new BigDecimal(val);
		Double newVal = b.setScale(2, RoundingMode.HALF_UP).doubleValue();
		
		return newVal;
	}
	
	public static Double price(Long val) {
		if (val == null) {
			return null;
		}

		BigDecimal b = new BigDecimal(val * 0.01);
		Double newVal = b.setScale(2, RoundingMode.HALF_UP).doubleValue();

		return newVal;
	}
	
	public static boolean stringIncludeChar(String str,String comminuteFlag,String targetChar) {
		if(StringUtils.isBlank(str)){
			return false;
		}
		String[] arr = str.split(comminuteFlag);
		return Arrays.asList(arr).contains(targetChar);
	}
	
	public static void main(String[] args) {
		System.out.println(doubleHalfUp(230*0.01));
	}
}