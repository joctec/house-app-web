package com.app.core.util;

import java.util.HashMap;

public class MapUtil {

    public static HashMap<String, String> newHashMap(String[][] prams) {
        HashMap<String, String> hashMap = new HashMap<String, String>();
        for (String[] data : prams) {
            hashMap.put(data[0], data[1]);
        }
        return hashMap;
    }
}
