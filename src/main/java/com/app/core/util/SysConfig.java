package com.app.core.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Properties;

/**
 * @ClassName: ConfigLoadUtil
 * @Description: properties 加载工具
 * @author zhangxiaofei
 * @date 2015年4月25日 下午1:40:11
 */
public class SysConfig extends Properties {

	/**  */
	private static final long serialVersionUID = 1L;

	public static SysConfig CONFIG;

	public static SysConfig loadProperties(URL url) {
		try {
			CONFIG = new SysConfig();
			InputStream openStream = url.openStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(openStream, "utf-8"));
			CONFIG.load(reader);

			return CONFIG;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	public static String get(String key) {
		return CONFIG.getProperty(key, "");
	}
}
