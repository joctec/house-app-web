package com.app.core.util;

import org.apache.log4j.Logger;

import com.alibaba.fastjson.JSON;

/**
 * 公共日志输出类
 * TODO Comment.
 * 
 * @since v#{version}
 * @author zhangxf
 * @created 2015年10月28日 上午7:45:31
 */
public class Out {

    public static final Logger log = Logger.getLogger(Out.class.getName() + "系统公共日志输出类.");

    public static void debugJson(Object object) {
        if (object == null) {
            System.err.println("null");
        } else {
            System.err.println(JSON.toJSON(object));
        }
    }

    public static void debug(Object message) {
        log.debug(message);
    }

    public static void info(Object message) {
        log.info(message);
    }

    public static void warn(Object message) {
        log.warn(message);
    }

    public static void error(Object message, Throwable t) {
        log.error(message, t);
    }

    public static void fatal(Object message) {
        log.fatal(message);
    }

    /**
     * 下面为简化方法
     */
    public static void d(Object message) {
        log.debug(message);
    }

    public static void i(Object message) {
        log.info(message);
    }

    public static void w(Object message) {
        log.warn(message);
    }

    public static void e(Object message) {
        log.error(message);
    }

    public static void e(Object message, Throwable t) {
        log.error(message, t);
    }

    public static void f(Object message) {
        log.fatal(message);
    }
}
