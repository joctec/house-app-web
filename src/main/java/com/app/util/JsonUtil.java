package com.app.util;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

public class JsonUtil {

	public JSONObject parse(String data) {
		if (StringUtils.isBlank(data)) {
			return new JSONObject();
		}

		try {
			return JSON.parseObject(data);
		} catch (Exception e) {
			e.printStackTrace();
			return new JSONObject();
		}
	}

	public static String string(Object obj) {
		return JSON.toJSONString(obj);
	}

	public static Map<String, Object> getMap4Json(String jsonString) {
		if (jsonString == null) {
			jsonString = "{}";
		}
		Map<String, Object> valueMap = new HashMap<String, Object>();
		JSONObject jsonObject = JSONObject.parseObject(jsonString);
		Iterator<String> keyIter = jsonObject.keySet().iterator();
		String key;
		Object value;
		while (keyIter.hasNext()) {
			key = keyIter.next();
			value = jsonObject.get(key);
			valueMap.put(key, value);
		}
		return valueMap;
	}
}
