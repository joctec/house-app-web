package com.app.util;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.util.ImageIOUtil;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class PdfUtil {
	public static final String PAGE_COUNT="pageCount";
	public static Map<String,String> pdf2img(String pdfUrl,String imgpath) throws DocumentException, IOException{
		Map<String,String> resutl=new HashMap<String,String>();
		PdfReader pdfReader = new PdfReader(pdfUrl);
		int pageCount = pdfReader.getNumberOfPages();
		//获取指定页码
		ByteArrayOutputStream out = new ByteArrayOutputStream();
    	PdfStamper pdfStamper = null;
    	
    	for(int i=1;i<=pageCount;i++){
    	    pdfReader.selectPages(String.valueOf(i)); 
            pdfStamper = new PdfStamper(pdfReader, out);
            pdfStamper.close();
            //利用PdfBox生成图像
            PDDocument pdDocument = PDDocument.load(new ByteArrayInputStream(out.toByteArray()));
            OutputStream outputStream = new FileOutputStream(imgpath);
            ImageOutputStream output = ImageIO.createImageOutputStream(outputStream);
            PDPage page = (PDPage)pdDocument.getDocumentCatalog().getAllPages().get(0);
            BufferedImage image = page.convertToImage(BufferedImage.TYPE_INT_RGB, 96);
            ImageIOUtil.writeImage(image, "png", outputStream, 96);
            //关闭所有流
            output.flush();
            outputStream.flush();
            output.close();
            outputStream.close();
            pdDocument.close();
    	}
    	
        resutl.put(PAGE_COUNT, pageCount+"");
        return resutl;
	}
	
	/**
     * PDFBOX转图片
     * @param pdfUrl pdf的路径
     * @param imgTempUrl 图片输出路径
     */
    public static void pdfToImage(String pdfUrl , String imgTempUrl){
        try{
            //读入PDF
             PdfReader pdfReader = new PdfReader(pdfUrl);
            //计算PDF页码数
             int pageCount = pdfReader.getNumberOfPages();
            //循环每个页码
             for(int i=pageCount ; i>=pdfReader.getNumberOfPages() ; i--){
                 ByteArrayOutputStream out = new ByteArrayOutputStream();
                 PdfStamper pdfStamper = null;
                 PDDocument pdDocument=null;
                 pdfReader = new PdfReader(pdfUrl);
                 pdfReader.selectPages(String.valueOf(i)); 
                 pdfStamper = new PdfStamper(pdfReader, out);
                 pdfStamper.close();
                 //利用PdfBox生成图像
                 pdDocument = PDDocument.load(new ByteArrayInputStream(out.toByteArray()));
                 OutputStream outputStream = new FileOutputStream(imgTempUrl +File.separator+ i + ".png");
                
                 ImageOutputStream output = ImageIO.createImageOutputStream(outputStream);
                 PDPage page = (PDPage)pdDocument.getDocumentCatalog().getAllPages().get(0);
                 BufferedImage image = page.convertToImage(BufferedImage.TYPE_INT_RGB, 186);
                 ImageIOUtil.writeImage(image, "png", outputStream, 186);
                 if (output != null) {
                  output.flush();
                  output.close();
                }
                 outputStream.flush();
                 outputStream.close();
                 pdDocument.close();
              }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
	
	public static void main(String[] args) throws IOException, DocumentException {
	    PdfUtil.pdfToImage("E:/22.pdf","E:/");
	}
}
