package com.app.util;

import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.client.utils.URLEncodedUtils;

import com.app.core.base.BaseEntity;

public class BizUtil {

    public static List<Long> getBeanId(List<? extends BaseEntity> list) {
        List<Long> idList = new ArrayList<Long>();
        if (list == null || list.isEmpty()) {
            return idList;
        }

        for (BaseEntity entity : list) {
            idList.add(entity.getId());
        }

        return idList;
    }

    public static String urlDecoder(String data) {
        try {
            // 系统的转换如果有换行符的话有问题
            // return URLDecoder.decode(data, "utf-8");

            data = data.replace("%a%", "%0a%");

            List<NameValuePair> parse = URLEncodedUtils.parse(data, Charset.forName("utf-8"));
            // 换行符会转换为%a，要转换回来
            String str = parse.get(0).toString();
            // str = str.replace("%a%", "%");
            return str;

        } catch (Exception e) {
            e.printStackTrace();
            return data;
        }
    }

    /**
     * 
     * 生成下载文件名称
     * 
     * @param prefixName 文件名前缀，一般为业务模块名称
     * @param userId 用户id，区别所属用户，作用便于鉴权，让不同的用户只能下载自己的文件
     * @return
     * @author zhangxf
     * @created 2017年3月21日 下午8:42:38
     */
    public static String getDownloadFileNameExcel(String prefixName, Long userId) {
        String downFilename = String.format("%s-%s-%s.xls",
            new SimpleDateFormat("yyyyMMddHHmm").format(new Date()), UUIDUtil.uuid(), userId + "");

        return downFilename;
    }

}
