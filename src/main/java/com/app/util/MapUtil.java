package com.app.util;

import java.util.Comparator;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapUtil {

	/**
	 * 使用 Map按key进行排序
	 */
	public static Map<Long, Object> sortMapByKey(Map<Long, Object> map) {
		if (map == null || map.isEmpty()) {
			return null;
		}

		Map<Long, Object> sortMap = new TreeMap<Long, Object>(new Comparator<Long>() {

			@Override
			public int compare(Long str1, Long str2) {

				return str1.compareTo(str2);
			}
		});

		sortMap.putAll(map);

		return sortMap;
	}

	public static void main(String[] args) {
		Map<Long, Object> map = new TreeMap<Long, Object>();

		map.put(40027L, "wnba");
		map.put(40028L, "kfc");
		map.put(3002L, "kfc");
		map.put(102L, "kfc");
		map.put(42L, "kfc");
		map.put(10014L, "cba");
		map.put(20030L, "cba");
		sortMapByKey(map);
		Set<Long> keySet = map.keySet();
		for (Long string : keySet) {
			System.out.println(string);
		}

	}
}
