/**
 * Project Name:dj-web
 * File Name:OrganCodeUtil.java
 * Package Name:com.app.biz.util
 * Date:2016年3月29日下午1:36:24
 * Copyright (c) 2016, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.app.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

/**
 * ClassName:OrganCodeUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年3月29日 下午1:36:24 <br/>
 * @author   liuliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class OrganCodeUtil {
    public static final String ADD_ORGAN_CODES="add_organ_codes";
    public static final String DEL_ORGAN_CODES="del_organ_codes";
    
    /**
     * 
     * getDepth:(根据code获取该code对应的深度). <br/>
     *
     * @author liuliang
     * @param code
     * @return
     * @since JDK 1.6
     */
    public static Integer getDepth(String code){
        return code.split("\\.").length;
    }
    
    /**
     * 
     * getParent:(获取上级组织机构的code). <br/>
     *
     * @author liuliang
     * @param code
     * @return
     * @since JDK 1.6
     */
    public static String getParent(String code){
        String autocode="";
        if(StringUtils.isNotBlank(code)){
            String[] arr=code.split("\\.");
            if(arr.length>1){
                autocode=code.substring(0,code.lastIndexOf("."));
                autocode=autocode.substring(0,autocode.lastIndexOf(".")+1);
            }
        }
        return autocode;
    }
    
    /**
     * 
     * getParentCodes:(获取所有上级组织机构code). <br/>
     *
     * @author liuliang
     * @param codes
     * @return
     * @since JDK 1.6
     */
    public static List<String> getParentCodes(String codes){
        String[] arr=codes.split("\\.");
        String s2="";
        List<String> list=new ArrayList<String>();
        for(int i=0;i<arr.length-1;i++){
            s2=s2+arr[i]+".";
            list.add(s2);
        }
        return list;
    }
    
    
    public static Map<String,List<String>> getChangeCodes(String oldCode,String newCode){
        Map map=new HashMap<String,List<String>>();
        List<String> delList=new ArrayList<String>();
        List<String> addList=new ArrayList<String>();
        
        String [] arr1=oldCode.split("\\.");
        String [] arr2=newCode.split("\\.");
        Integer length=arr1.length>arr2.length?arr1.length:arr2.length;
        //记录一个数字，标记不相等是第几位
        String sameStr="";
        for(int i=0;i<length;i++){
            if(arr1[i].equals(arr2[i])){
                sameStr=sameStr+arr1[i]+".";
            }else{
                //找到不相等
                break;
            }
        }
        //把每个字符串后面不同的截取出来
        String noSameStr1=oldCode.substring(sameStr.length(),oldCode.length());
        String noSameStr2=newCode.substring(sameStr.length(),newCode.length());
        String[] noSameArr1=noSameStr1.split("\\.");
        String noSameReturnStr1=sameStr;
        for(int i=0;i<noSameArr1.length;i++){
            noSameReturnStr1=noSameReturnStr1+noSameArr1[i]+".";
            delList.add(noSameReturnStr1);
        }
        
      //把每个字符串后面不同的截取出来
        String[] noSameArr2=noSameStr2.split("\\.");
        String noSameReturnStr2=sameStr;
        for(int i=0;i<noSameArr2.length;i++){
            noSameReturnStr2=noSameReturnStr2+noSameArr2[i]+".";
            addList.add(noSameReturnStr2);
        }
        
        arr1=null;
        arr2=null;
        noSameStr1=null;
        noSameStr2=null;
        noSameArr1=null;
        noSameArr2=null;
        noSameReturnStr1=null;
        noSameReturnStr2=null;
        map.put(ADD_ORGAN_CODES, addList);
        map.put(DEL_ORGAN_CODES, delList);
        return map;
    }
    
    public static void main(String[] arr){
        System.out.println(getChangeCodes("1.1.1.","1.1.12.4.5."));
    }
}

