package com.app.util;

public class NumUtil {

    public static Double doubleNullTo0(Double d) {
        if (d == null) {
            return 0D;
        }
        return d;
    }

    public static Integer intNullTo0(Integer i) {
        if (i == null) {
            return 0;
        }
        return i;
    }

    public static String intToStr(Integer i) {
        if (i == null) {
            return "";
        }
        return i.toString();
    }

    public static String doubleToStr(Double i) {
        if (i == null) {
            return "";
        }
        return i.toString();
    }

    public static String numToStr(Object val) {
        if (val == null) {
            return "";
        }
        return val.toString();
    }
}
