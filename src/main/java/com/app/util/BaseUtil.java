package com.app.util;

import java.util.ArrayList;
import java.util.List;

import com.app.core.base.BaseEntity;

public class BaseUtil {

	public static List<Long> getBeanId(List<? extends BaseEntity> list) {
		List<Long> idList = new ArrayList<Long>();

		if (list == null || list.isEmpty()) {
			return idList;
		}

		for (BaseEntity entity : list) {
			idList.add(entity.getId());
		}

		return idList;
	}
}
