package com.app.util;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;

public class URLUtil {

	/**
	 * 编码
	 */
	public static String urlEncoderUTF8(String str) {
		if (StringUtils.isBlank(str)) {
			return "";
		}

		try {
			return URLEncoder.encode(str, "utf-8");
		} catch (Exception e) {
			return "";
		}
	}

	/**
	 * 解码
	 */
	public static String urlDecoderUTF8(String str) {
		if (StringUtils.isBlank(str)) {
			return "";
		}

		try {
			return URLDecoder.decode(str, "utf-8");
		} catch (UnsupportedEncodingException e) {
			return "";
		}
	}

}
