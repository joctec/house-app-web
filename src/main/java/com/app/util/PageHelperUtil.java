/**
 * Project Name:dj-web
 * File Name:PageHelperUtil.java
 * Package Name:com.app.biz.util
 * Date:2016年4月6日下午1:38:20
 * Copyright (c) 2016, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.app.util;

import com.app.core.base.PageQuery;

/**
 * ClassName:PageHelperUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年4月6日 下午1:38:20 <br/>
 * @author   liuliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class PageHelperUtil {
    /**
     * 
     * getPageNum:(获取页码). <br/>
     *
     * @author liuliang
     * @param pageParams
     * @return
     * @since JDK 1.6
     */
    public static Integer getPageNum(PageQuery pageParams){
        return pageParams == null ? 1 : pageParams.getPageNum();
    }
    
    /**
     * 
     * getPageNum:(获取每页条数). <br/>
     *
     * @author liuliang
     * @param pageParams
     * @return
     * @since JDK 1.6
     */
    public static Integer getPageSize(PageQuery pageParams){
        return pageParams == null ? 1 : pageParams.getPageSize();
    }
}

