/**
 * Project Name:hr-web
 * File Name:EntityUtil.java
 * Package Name:com.app.biz.util
 * Date:2016年1月9日下午7:48:38
 * Copyright (c) 2016, chenzhou1025@126.com All Rights Reserved.
 *
*/

package com.app.util;

import java.util.ArrayList;
import java.util.List;

import com.app.core.base.BaseEntity;

/**
 * ClassName:EntityUtil <br/>
 * Function: TODO ADD FUNCTION. <br/>
 * Reason:	 TODO ADD REASON. <br/>
 * Date:     2016年1月9日 下午7:48:38 <br/>
 * @author   liuliang
 * @version  
 * @since    JDK 1.6
 * @see 	 
 */
public class EntityUtil {
  
  /**
   * 
   * getIds:(获取根据一个集合获取ids). <br/>
   *
   * @author liuliang
   * @param list
   * @return
   * @since JDK 1.6
   */
  public static List<Long> getIds(List<? extends BaseEntity> list){
    List<Long> ids=new ArrayList<Long>();
    for (BaseEntity vo : list) {
      ids.add(vo.getId());
    }
    return ids;
  }
}

