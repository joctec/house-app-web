package com.app.common.enums;

/**
 * h5 client 类型
 * 
 * @author afei
 *
 */
public enum H5ClientEnum {
	/** 支付宝 */
	zfb(1),
	/** 微信 */
	wx(2),
	/** 钉钉 */
	dd(6),
	/** 余额 */
	balance(3),
	/** 翼支付 */
	yzf(4),
	/** 微信小程序 */
	wxxcx(5),
	/** 其他 */
	other(100);

	private Integer code;

	H5ClientEnum(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}