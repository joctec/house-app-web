package com.app.common.enums;

public enum DeleteEnum {
	/** 是 */
	yes(1),
	/** 否 */
	no(0);

	private Integer code;

	DeleteEnum(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}