package com.app.common.enums;

public enum UserStatusEnum {
	/** 正常 */
	normal(1),
	/** 锁定 */
	locked(2),
	/** 删除 */
	deleted(2);

	private Integer code;

	UserStatusEnum(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

}