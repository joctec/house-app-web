package com.guda.mp.common.context;

/**
 * 任务周期类型
 */
public enum TaskCycleTypeEnum {
    /** 2周 */
    week(2),
    /** 3月 */
    month(3),
    /** 4年 */
    year(4);

    private Integer code;

    TaskCycleTypeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}