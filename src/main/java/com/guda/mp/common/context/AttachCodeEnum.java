package com.guda.mp.common.context;

/**
 * 附件code类型
 */
public enum AttachCodeEnum {
    /** 1房屋 */
    house(1),
    /** 巡检检查项 */
    inspectionRecordCheckItem(2),
    /** 巡检异常问题 */
    inspectionRecordProblem(3);

    private Integer code;

    AttachCodeEnum(Integer code) {
        this.code = code;
    }

    public Integer getCode() {
        return code;
    }

    public String getCodeStr() {
        return code.toString();
    }

    public void setCode(Integer code) {
        this.code = code;
    }
}