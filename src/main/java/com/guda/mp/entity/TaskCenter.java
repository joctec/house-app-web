package com.guda.mp.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.core.base.BaseEntity;

/**
 * ClassName: TaskCenter <br/>
 * Description: 任务中心 <br/>
 */
public class TaskCenter extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    private Long orgId;
    /**
     * 任务分配id
     */
    private Long taskAssignId;
    /**
     * 项目id
     */
    private Long projectId;
    /**
     * 房屋id
     */
    private Long monitorTargetId;
    /**
     * 
     */
    private String houseName;
    /**
     * 人员id
     */
    private Long userId;
    /**
     * 人员姓名
     */
    private String personName;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 任务类型 1：周期任务， 2临时任务
     */
    private Integer taskType;
    /**
     * 周期类型
     */
    private Integer cycleType;
    /**
     * 周几
     */
    private Integer dayOfWeek;
    /**
     * 描述
     */
    private String descr;
    /**
     * 完成状态. 0待完成, 1正在执行, 2已完成，3逾期未完成
     */
    private Integer completeStatus;
    /**
     * 任务日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date taskDate;
    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date planStartDate;
    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date planEndDate;
    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date actualStartDate;
    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date actualEndDate;
    /**
     * 是否有问题。 1：是， 0：否
     */
    private Integer hasProblem;
    /**
     * nfc签到0否1是
     */
    private Integer nfcSignIn;

    public TaskCenter() {
    }

    public TaskCenter(long id) {
        super.id = id;
    }

    /**
     * 组织id
     */
    public TaskCenter setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 任务分配id
     */
    public TaskCenter setTaskAssignId(Long taskAssignId) {
        this.taskAssignId = taskAssignId;
        return this;
    }

    /**
     * 任务分配id
     */
    public Long getTaskAssignId() {
        return this.taskAssignId;
    }

    /**
     * 项目id
     */
    public TaskCenter setProjectId(Long projectId) {
        this.projectId = projectId;
        return this;
    }

    /**
     * 项目id
     */
    public Long getProjectId() {
        return this.projectId;
    }

    /**
     * 房屋id
     */
    public TaskCenter setMonitorTargetId(Long monitorTargetId) {
        this.monitorTargetId = monitorTargetId;
        return this;
    }

    /**
     * 房屋id
     */
    public Long getMonitorTargetId() {
        return this.monitorTargetId;
    }

    /**
     * 
     */
    public TaskCenter setHouseName(String houseName) {
        this.houseName = houseName;
        return this;
    }

    /**
     * 
     */
    public String getHouseName() {
        return this.houseName;
    }

    /**
     * 人员id
     */
    public TaskCenter setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    /**
     * 人员id
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * 人员姓名
     */
    public TaskCenter setPersonName(String personName) {
        this.personName = personName;
        return this;
    }

    /**
     * 人员姓名
     */
    public String getPersonName() {
        return this.personName;
    }

    /**
     * 任务名称
     */
    public TaskCenter setTaskName(String taskName) {
        this.taskName = taskName;
        return this;
    }

    /**
     * 任务名称
     */
    public String getTaskName() {
        return this.taskName;
    }

    /**
     * 任务类型 1：周期任务， 2临时任务
     */
    public TaskCenter setTaskType(Integer taskType) {
        this.taskType = taskType;
        return this;
    }

    /**
     * 任务类型 1：周期任务， 2临时任务
     */
    public Integer getTaskType() {
        return this.taskType;
    }

    /**
     * 周期类型
     */
    public TaskCenter setCycleType(Integer cycleType) {
        this.cycleType = cycleType;
        return this;
    }

    /**
     * 周期类型
     */
    public Integer getCycleType() {
        return this.cycleType;
    }

    /**
     * 周几
     */
    public TaskCenter setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
        return this;
    }

    /**
     * 周几
     */
    public Integer getDayOfWeek() {
        return this.dayOfWeek;
    }

    /**
     * 描述
     */
    public TaskCenter setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 完成状态. 0待完成, 1正在执行, 2已完成，3逾期未完成
     */
    public TaskCenter setCompleteStatus(Integer completeStatus) {
        this.completeStatus = completeStatus;
        return this;
    }

    /**
     * 完成状态. 0待完成, 1正在执行, 2已完成，3逾期未完成
     */
    public Integer getCompleteStatus() {
        return this.completeStatus;
    }

    /**
     * 任务日期
     */
    public TaskCenter setTaskDate(Date taskDate) {
        this.taskDate = taskDate;
        return this;
    }

    /**
     * 任务日期
     */
    public Date getTaskDate() {
        return this.taskDate;
    }

    /**
     * 
     */
    public TaskCenter setPlanStartDate(Date planStartDate) {
        this.planStartDate = planStartDate;
        return this;
    }

    /**
     * 
     */
    public Date getPlanStartDate() {
        return this.planStartDate;
    }

    /**
     * 
     */
    public TaskCenter setPlanEndDate(Date planEndDate) {
        this.planEndDate = planEndDate;
        return this;
    }

    /**
     * 
     */
    public Date getPlanEndDate() {
        return this.planEndDate;
    }

    /**
     * 
     */
    public TaskCenter setActualStartDate(Date actualStartDate) {
        this.actualStartDate = actualStartDate;
        return this;
    }

    /**
     * 
     */
    public Date getActualStartDate() {
        return this.actualStartDate;
    }

    /**
     * 
     */
    public TaskCenter setActualEndDate(Date actualEndDate) {
        this.actualEndDate = actualEndDate;
        return this;
    }

    /**
     * 
     */
    public Date getActualEndDate() {
        return this.actualEndDate;
    }

    /**
     * 是否有问题。 1：是， 0：否
     */
    public TaskCenter setHasProblem(Integer hasProblem) {
        this.hasProblem = hasProblem;
        return this;
    }

    /**
     * 是否有问题。 1：是， 0：否
     */
    public Integer getHasProblem() {
        return this.hasProblem;
    }

    /**
     * nfc签到0否1是
     */
    public TaskCenter setNfcSignIn(Integer nfcSignIn) {
        this.nfcSignIn = nfcSignIn;
        return this;
    }

    /**
     * nfc签到0否1是
     */
    public Integer getNfcSignIn() {
        return this.nfcSignIn;
    }

}