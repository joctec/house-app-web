package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/**
 * ClassName: Person <br/>
 * Description: 人员 <br/>
 */
public class Person extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属公司/组织id
     */
    private Long orgId;
    /**
     * 姓名
     */
    private String name;
    /**
     * 性别
     */
    private Integer gender;
    /**
     * 出生日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date birthdate;
    /**
     * 身份证号
     */
    private String idCard;
    /**
     * 居住地址
     */
    private String address;

    public Person() {
    }

    public Person(long id) {
        super.id = id;
    }

    /**
     * 所属公司/组织id
     */
    public Person setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 所属公司/组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 姓名
     */
    public Person setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 姓名
     */
    public String getName() {
        return this.name;
    }

    /**
     * 性别
     */
    public Person setGender(Integer gender) {
        this.gender = gender;
        return this;
    }

    /**
     * 性别
     */
    public Integer getGender() {
        return this.gender;
    }

    /**
     * 出生日期
     */
    public Person setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
        return this;
    }

    /**
     * 出生日期
     */
    public Date getBirthdate() {
        return this.birthdate;
    }

    /**
     * 身份证号
     */
    public Person setIdCard(String idCard) {
        this.idCard = idCard;
        return this;
    }

    /**
     * 身份证号
     */
    public String getIdCard() {
        return this.idCard;
    }

    /**
     * 居住地址
     */
    public Person setAddress(String address) {
        this.address = address;
        return this;
    }

    /**
     * 居住地址
     */
    public String getAddress() {
        return this.address;
    }

}