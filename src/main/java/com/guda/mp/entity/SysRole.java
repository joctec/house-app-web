package com.guda.mp.entity;

import java.util.Date;
import com.app.core.base.BaseEntity;

/** 
* ClassName: SysRole <br/>
* Description: 用户登录账号表 <br/>
*/ 
public class SysRole extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 角色名称
	 */
    private String name;
	/**
	 * 角色描述
	 */
    private String descr;
	
	public SysRole() {
    }

    public SysRole(long id) {
        super.id = id;
    }

	/**
	 * 角色名称
	 */
	public SysRole setName(String name){
		this.name = name;
		return this;
	}
	
	/**
	 * 角色名称
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * 角色描述
	 */
	public SysRole setDescr(String descr){
		this.descr = descr;
		return this;
	}
	
	/**
	 * 角色描述
	 */
	public String getDescr(){
		return this.descr;
	}
	
}