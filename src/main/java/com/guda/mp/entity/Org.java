package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: Org <br/>
 * Description: 组织 <br/>
 */
public class Org extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 父ID
     */
    private Long pid;
    /**
     * 组织名称
     */
    private String orgName;
    /**
     * 组织编码
     */
    private String orgCode;
    /**
     * 描述
     */
    private String descr;

    public Org() {
    }

    public Org(long id) {
        super.id = id;
    }

    /**
     * 父ID
     */
    public Org setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    /**
     * 父ID
     */
    public Long getPid() {
        return this.pid;
    }

    /**
     * 组织名称
     */
    public Org setOrgName(String orgName) {
        this.orgName = orgName;
        return this;
    }

    /**
     * 组织名称
     */
    public String getOrgName() {
        return this.orgName;
    }

    /**
     * 组织编码
     */
    public Org setOrgCode(String orgCode) {
        this.orgCode = orgCode;
        return this;
    }

    /**
     * 组织编码
     */
    public String getOrgCode() {
        return this.orgCode;
    }

    /**
     * 描述
     */
    public Org setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

}