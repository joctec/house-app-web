package com.guda.mp.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.core.base.BaseEntity;

/**
 * ClassName: TaskAssign <br/>
 * Description: 任务分配 <br/>
 */
public class TaskAssign extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    private Long orgId;
    /**
     * 项目id
     */
    private Long projectId;
    /**
     * 监测目标ID
     */
    private Long monitorTargetId;
    /**
     * 人员id
     */
    private Long userId;
    /**
     * 周期类型：1:日, 2:周, 3:月,4:年
     */
    private Integer cycleType;
    /**
     * 执行时间
     */
    private Integer dayOfWeek;
    /**
     * 每月的几号
     */
    private String dayOfMonth;
    /**
     * 任务名称
     */
    private String taskName;
    /**
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    /**
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
    /**
     * 任务可提前天数
     */
    private Integer canBeforeDay;
    /**
     * 任务可延后天数
     */
    private Integer canAfterDay;
    /**
     * 描述
     */
    private String descr;
    /**
     * 最后执行时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date lastExecTime;

    public TaskAssign() {
    }

    public TaskAssign(long id) {
        super.id = id;
    }

    /**
     * 组织id
     */
    public void setOrgId(Long orgId) {
        this.orgId = orgId;
    }

    /**
     * 组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 项目id
     */
    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    /**
     * 项目id
     */
    public Long getProjectId() {
        return this.projectId;
    }

    /**
     * 监测目标ID
     */
    public void setMonitorTargetId(Long monitorTargetId) {
        this.monitorTargetId = monitorTargetId;
    }

    /**
     * 监测目标ID
     */
    public Long getMonitorTargetId() {
        return this.monitorTargetId;
    }

    /**
     * 人员id
     */
    public void setUserId(Long userId) {
        this.userId = userId;
    }

    /**
     * 人员id
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * 周期类型：1:日, 2:周, 3:月,4:年
     */
    public void setCycleType(Integer cycleType) {
        this.cycleType = cycleType;
    }

    /**
     * 周期类型：1:日, 2:周, 3:月,4:年
     */
    public Integer getCycleType() {
        return this.cycleType;
    }

    /**
     * 执行时间
     */
    public void setDayOfWeek(Integer dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    /**
     * 执行时间
     */
    public Integer getDayOfWeek() {
        return this.dayOfWeek;
    }

    /**
     * 每月的几号
     */
    public void setDayOfMonth(String dayOfMonth) {
        this.dayOfMonth = dayOfMonth;
    }

    /**
     * 每月的几号
     */
    public String getDayOfMonth() {
        return this.dayOfMonth;
    }

    /**
     * 任务名称
     */
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    /**
     * 任务名称
     */
    public String getTaskName() {
        return this.taskName;
    }

    /**
     * 开始日期
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * 开始日期
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * 结束日期
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * 结束日期
     */
    public Date getEndDate() {
        return this.endDate;
    }

    /**
     * 任务可提前天数
     */
    public void setCanBeforeDay(Integer canBeforeDay) {
        this.canBeforeDay = canBeforeDay;
    }

    /**
     * 任务可提前天数
     */
    public Integer getCanBeforeDay() {
        return this.canBeforeDay;
    }

    /**
     * 任务可延后天数
     */
    public void setCanAfterDay(Integer canAfterDay) {
        this.canAfterDay = canAfterDay;
    }

    /**
     * 任务可延后天数
     */
    public Integer getCanAfterDay() {
        return this.canAfterDay;
    }

    /**
     * 描述
     */
    public void setDescr(String descr) {
        this.descr = descr;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 最后执行时间
     */
    public void setLastExecTime(Date lastExecTime) {
        this.lastExecTime = lastExecTime;
    }

    /**
     * 最后执行时间
     */
    public Date getLastExecTime() {
        return this.lastExecTime;
    }

}