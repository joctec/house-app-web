package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: BaseDictData <br/>
 * Description: 业务-字典项数据 <br/>
 */
public class BaseDictData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属公司/组织id
     */
    private Long orgId;
    /**
     * 
     */
    private Long pid;
    /**
     * 名称
     */
    private String name;
    /**
     * 
     */
    private String code;
    /**
     * 顺序
     */
    private Integer seq;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 描述
     */
    private String descr;
    /**
     * 自身描述
     */
    private String descrSelf;

    public BaseDictData() {
    }

    public BaseDictData(long id) {
        super.id = id;
    }

    /**
     * 所属公司/组织id
     */
    public BaseDictData setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 所属公司/组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 
     */
    public BaseDictData setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    /**
     * 
     */
    public Long getPid() {
        return this.pid;
    }

    /**
     * 名称
     */
    public BaseDictData setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 
     */
    public BaseDictData setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * 
     */
    public String getCode() {
        return this.code;
    }

    /**
     * 顺序
     */
    public BaseDictData setSeq(Integer seq) {
        this.seq = seq;
        return this;
    }

    /**
     * 顺序
     */
    public Integer getSeq() {
        return this.seq;
    }

    /**
     * 类型
     */
    public BaseDictData setType(Integer type) {
        this.type = type;
        return this;
    }

    /**
     * 类型
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 描述
     */
    public BaseDictData setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 自身描述
     */
    public BaseDictData setDescrSelf(String descrSelf) {
        this.descrSelf = descrSelf;
        return this;
    }

    /**
     * 自身描述
     */
    public String getDescrSelf() {
        return this.descrSelf;
    }
}