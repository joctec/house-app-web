package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: TaskCenterSlant <br/>
 * Description: 倾斜数据 <br/>
 */
public class TaskCenterSlant extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    private Long taskId;
    /**
     * 任务检查项id
     */
    private Long taskCenterCheckItemId;
    /**
     * 房屋高度
     */
    private Integer houseHeight;
    /**
     * x轴位移量
     */
    private Integer xDisplacement;
    /**
     * x轴斜率
     */
    private Integer xSlope;
    /**
     * x轴斜角
     */
    private Integer xBevelAngle;
    /**
     * y轴位移量
     */
    private Integer yDisplacement;
    /**
     * y轴斜率
     */
    private Integer ySlope;
    /**
     * y轴斜角
     */
    private Integer yBevelAngle;

    public TaskCenterSlant() {
    }

    public TaskCenterSlant(long id) {
        super.id = id;
    }

    /**
     * 任务id
     */
    public TaskCenterSlant setTaskId(Long taskId) {
        this.taskId = taskId;
        return this;
    }

    /**
     * 任务id
     */
    public Long getTaskId() {
        return this.taskId;
    }

    /**
     * 任务检查项id
     */
    public TaskCenterSlant setTaskCenterCheckItemId(Long taskCenterCheckItemId) {
        this.taskCenterCheckItemId = taskCenterCheckItemId;
        return this;
    }

    /**
     * 任务检查项id
     */
    public Long getTaskCenterCheckItemId() {
        return this.taskCenterCheckItemId;
    }

    /**
     * 房屋高度
     */
    public TaskCenterSlant setHouseHeight(Integer houseHeight) {
        this.houseHeight = houseHeight;
        return this;
    }

    /**
     * 房屋高度
     */
    public Integer getHouseHeight() {
        return this.houseHeight;
    }

    /**
     * x轴位移量
     */
    public TaskCenterSlant setXDisplacement(Integer xDisplacement) {
        this.xDisplacement = xDisplacement;
        return this;
    }

    /**
     * x轴位移量
     */
    public Integer getXDisplacement() {
        return this.xDisplacement;
    }

    /**
     * x轴斜率
     */
    public TaskCenterSlant setXSlope(Integer xSlope) {
        this.xSlope = xSlope;
        return this;
    }

    /**
     * x轴斜率
     */
    public Integer getXSlope() {
        return this.xSlope;
    }

    /**
     * x轴斜角
     */
    public TaskCenterSlant setXBevelAngle(Integer xBevelAngle) {
        this.xBevelAngle = xBevelAngle;
        return this;
    }

    /**
     * x轴斜角
     */
    public Integer getXBevelAngle() {
        return this.xBevelAngle;
    }

    /**
     * y轴位移量
     */
    public TaskCenterSlant setYDisplacement(Integer yDisplacement) {
        this.yDisplacement = yDisplacement;
        return this;
    }

    /**
     * y轴位移量
     */
    public Integer getYDisplacement() {
        return this.yDisplacement;
    }

    /**
     * y轴斜率
     */
    public TaskCenterSlant setYSlope(Integer ySlope) {
        this.ySlope = ySlope;
        return this;
    }

    /**
     * y轴斜率
     */
    public Integer getYSlope() {
        return this.ySlope;
    }

    /**
     * y轴斜角
     */
    public TaskCenterSlant setYBevelAngle(Integer yBevelAngle) {
        this.yBevelAngle = yBevelAngle;
        return this;
    }

    /**
     * y轴斜角
     */
    public Integer getYBevelAngle() {
        return this.yBevelAngle;
    }

}