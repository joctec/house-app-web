package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/**
 * ClassName: SensorSlantDay <br/>
 * Description: <br/>
 */
public class SensorSlantDay extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 设备表id主键
     */
    private Long deviceId;
    /**
     * 设备编号
     */
    private Long deviceSn;
    /**
     * 设备供应商1鲁尔 2葛南
     */
    private Integer deviceVender;
    /**
     * 1倾斜率X日速率, 2倾斜率Y日速率
     */
    private Integer funType;
    /**
     * 数据时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dataTime;
    /**
     * 日期数字long
     */
    private Long dataTimeLong;
    /**
     * 传感器值
     */
    private Double val;

    public SensorSlantDay() {
    }

    public SensorSlantDay(long id) {
        super.id = id;
    }

    /**
     * 设备表id主键
     */
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 设备表id主键
     */
    public Long getDeviceId() {
        return this.deviceId;
    }

    public Long getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(Long deviceSn) {
        this.deviceSn = deviceSn;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public void setDeviceVender(Integer deviceVender) {
        this.deviceVender = deviceVender;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public Integer getDeviceVender() {
        return this.deviceVender;
    }

    /**
     * 1倾斜率X日速率, 2倾斜率Y日速率
     */
    public void setFunType(Integer funType) {
        this.funType = funType;
    }

    /**
     * 1倾斜率X日速率, 2倾斜率Y日速率
     */
    public Integer getFunType() {
        return this.funType;
    }

    /**
     * 数据时间
     */
    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }

    /**
     * 数据时间
     */
    public Date getDataTime() {
        return this.dataTime;
    }

    /**
     * 日期数字long
     */
    public void setDataTimeLong(Long dataTimeLong) {
        this.dataTimeLong = dataTimeLong;
    }

    /**
     * 日期数字long
     */
    public Long getDataTimeLong() {
        return this.dataTimeLong;
    }

    /**
     * 传感器值
     */
    public void setVal(Double val) {
        this.val = val;
    }

    /**
     * 传感器值
     */
    public Double getVal() {
        return this.val;
    }

}