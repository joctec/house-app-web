package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: SysRoleUser <br/>
 * Description: 用户登录账号表 <br/>
 */
public class SysRoleUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 角色ID
     */
    private Long roleId;
    /**
     * 用户ID
     */
    private Long userId;

    public SysRoleUser() {
    }

    public SysRoleUser(long id) {
        super.id = id;
    }

    /**
     * 角色ID
     */
    public SysRoleUser setRoleId(Long roleId) {
        this.roleId = roleId;
        return this;
    }

    /**
     * 角色ID
     */
    public Long getRoleId() {
        return this.roleId;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

}