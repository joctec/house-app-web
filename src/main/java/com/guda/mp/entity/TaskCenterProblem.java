package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: TaskCenterProblem <br/>
 * Description: 巡检-巡检记录问题 <br/>
 */
public class TaskCenterProblem extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 巡查记录id
     */
    private Long taskId;
    /**
     * 所属检查项ID
     */
    private Long taskCenterCheckItemId;
    /**
     * 问题id
     */
    private Long checkProblemId;
    /**
     * 
     */
    private String checkProblemName;
    /**
     * 问题级别
     */
    private Integer checkProblemLevel;
    /**
     * 问题文本
     */
    private String checkProblemText;
    /**
     * 问题图片
     */
    private String checkProblemImg;
    /**
     * 问题录音
     */
    private String checkProblemRecord;
    /**
     * 问题视频
     */
    private String checkProblemVideo;
    /**
     * 问题状态
     */
    private Integer status;

    public TaskCenterProblem() {
    }

    public TaskCenterProblem(long id) {
        super.id = id;
    }

    /**
     * 巡查记录id
     */
    public TaskCenterProblem setTaskId(Long taskId) {
        this.taskId = taskId;
        return this;
    }

    /**
     * 巡查记录id
     */
    public Long getTaskId() {
        return this.taskId;
    }

    /**
     * 所属检查项ID
     */
    public TaskCenterProblem setTaskCenterCheckItemId(Long taskCenterCheckItemId) {
        this.taskCenterCheckItemId = taskCenterCheckItemId;
        return this;
    }

    /**
     * 所属检查项ID
     */
    public Long getTaskCenterCheckItemId() {
        return this.taskCenterCheckItemId;
    }

    /**
     * 问题id
     */
    public TaskCenterProblem setCheckProblemId(Long checkProblemId) {
        this.checkProblemId = checkProblemId;
        return this;
    }

    /**
     * 问题id
     */
    public Long getCheckProblemId() {
        return this.checkProblemId;
    }

    /**
     * 
     */
    public TaskCenterProblem setCheckProblemName(String checkProblemName) {
        this.checkProblemName = checkProblemName;
        return this;
    }

    /**
     * 
     */
    public String getCheckProblemName() {
        return this.checkProblemName;
    }

    /**
     * 问题级别
     */
    public TaskCenterProblem setCheckProblemLevel(Integer checkProblemLevel) {
        this.checkProblemLevel = checkProblemLevel;
        return this;
    }

    /**
     * 问题级别
     */
    public Integer getCheckProblemLevel() {
        return this.checkProblemLevel;
    }

    /**
     * 问题文本
     */
    public TaskCenterProblem setCheckProblemText(String checkProblemText) {
        this.checkProblemText = checkProblemText;
        return this;
    }

    /**
     * 问题文本
     */
    public String getCheckProblemText() {
        return this.checkProblemText;
    }

    /**
     * 问题图片
     */
    public TaskCenterProblem setCheckProblemImg(String checkProblemImg) {
        this.checkProblemImg = checkProblemImg;
        return this;
    }

    /**
     * 问题图片
     */
    public String getCheckProblemImg() {
        return this.checkProblemImg;
    }

    /**
     * 问题录音
     */
    public TaskCenterProblem setCheckProblemRecord(String checkProblemRecord) {
        this.checkProblemRecord = checkProblemRecord;
        return this;
    }

    /**
     * 问题录音
     */
    public String getCheckProblemRecord() {
        return this.checkProblemRecord;
    }

    /**
     * 问题视频
     */
    public TaskCenterProblem setCheckProblemVideo(String checkProblemVideo) {
        this.checkProblemVideo = checkProblemVideo;
        return this;
    }

    /**
     * 问题视频
     */
    public String getCheckProblemVideo() {
        return this.checkProblemVideo;
    }

    /**
     * 问题状态
     */
    public TaskCenterProblem setStatus(Integer status) {
        this.status = status;
        return this;
    }

    /**
     * 问题状态
     */
    public Integer getStatus() {
        return this.status;
    }

}