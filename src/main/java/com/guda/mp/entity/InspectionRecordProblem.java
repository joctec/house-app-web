package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: InspectionRecordProblem <br/>
* Description: 巡检-巡检记录问题 <br/>
*/ 
public class InspectionRecordProblem extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 巡查记录id
	 */
    private Long inspectionRecordId;
	/**
	 * 问题id
	 */
    private Long checkProblemId;
	/**
	 * 
	 */
    private String checkProblemName;
	/**
	 * 问题级别
	 */
    private String checkProblemLevel;
	/**
	 * 问题文本
	 */
    private String checkProblemText;
	/**
	 * 问题图片
	 */
    private String checkProblemImg;
	/**
	 * 问题录音
	 */
    private String checkProblemRecord;
	/**
	 * 问题视频
	 */
    private String checkProblemVideo;
	/**
	 * 问题状态
	 */
    private Integer status;
	public InspectionRecordProblem() {
    }

    public InspectionRecordProblem(long id) {
        super.id = id;
    }

	/**
	 * 巡查记录id
	 */
	public InspectionRecordProblem setInspectionRecordId(Long inspectionRecordId){
		this.inspectionRecordId = inspectionRecordId;
		return this;
	}
	
	/**
	 * 巡查记录id
	 */
	public Long getInspectionRecordId(){
		return this.inspectionRecordId;
	}
	
	/**
	 * 问题id
	 */
	public InspectionRecordProblem setCheckProblemId(Long checkProblemId){
		this.checkProblemId = checkProblemId;
		return this;
	}
	
	/**
	 * 问题id
	 */
	public Long getCheckProblemId(){
		return this.checkProblemId;
	}
	
	/**
	 * 
	 */
	public InspectionRecordProblem setCheckProblemName(String checkProblemName){
		this.checkProblemName = checkProblemName;
		return this;
	}
	
	/**
	 * 
	 */
	public String getCheckProblemName(){
		return this.checkProblemName;
	}
	
	/**
	 * 问题级别
	 */
	public InspectionRecordProblem setCheckProblemLevel(String checkProblemLevel){
		this.checkProblemLevel = checkProblemLevel;
		return this;
	}
	
	/**
	 * 问题级别
	 */
	public String getCheckProblemLevel(){
		return this.checkProblemLevel;
	}
	
	/**
	 * 问题文本
	 */
	public InspectionRecordProblem setCheckProblemText(String checkProblemText){
		this.checkProblemText = checkProblemText;
		return this;
	}
	
	/**
	 * 问题文本
	 */
	public String getCheckProblemText(){
		return this.checkProblemText;
	}
	
	/**
	 * 问题图片
	 */
	public InspectionRecordProblem setCheckProblemImg(String checkProblemImg){
		this.checkProblemImg = checkProblemImg;
		return this;
	}
	
	/**
	 * 问题图片
	 */
	public String getCheckProblemImg(){
		return this.checkProblemImg;
	}
	
	/**
	 * 问题录音
	 */
	public InspectionRecordProblem setCheckProblemRecord(String checkProblemRecord){
		this.checkProblemRecord = checkProblemRecord;
		return this;
	}
	
	/**
	 * 问题录音
	 */
	public String getCheckProblemRecord(){
		return this.checkProblemRecord;
	}
	
	/**
	 * 问题视频
	 */
	public InspectionRecordProblem setCheckProblemVideo(String checkProblemVideo){
		this.checkProblemVideo = checkProblemVideo;
		return this;
	}
	
	/**
	 * 问题视频
	 */
	public String getCheckProblemVideo(){
		return this.checkProblemVideo;
	}
	
	/**
	 * 问题状态
	 */
	public InspectionRecordProblem setStatus(Integer status){
		this.status = status;
		return this;
	}
	
	/**
	 * 问题状态
	 */
	public Integer getStatus(){
		return this.status;
	}
	
}