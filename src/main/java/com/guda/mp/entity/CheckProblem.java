package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: CheckProblem <br/>
 * Description: 检查异常问题 <br/>
 */
public class CheckProblem extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属公司/组织id
     */
    private Long orgId;
    /**
     * 
     */
    private Long pid;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 名称
     */
    private String name;
    /**
     * 描述
     */
    private String descr;
    /**
     * 
     */
    private Integer seq;
    /**
     * 
     */
    private Integer delFlag;

    public CheckProblem() {
    }

    public CheckProblem(long id) {
        super.id = id;
    }

    /**
     * 所属公司/组织id
     */
    public CheckProblem setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 所属公司/组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 
     */
    public CheckProblem setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    /**
     * 
     */
    public Long getPid() {
        return this.pid;
    }

    /**
     * 类型
     */
    public CheckProblem setType(Integer type) {
        this.type = type;
        return this;
    }

    /**
     * 类型
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 名称
     */
    public CheckProblem setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 描述
     */
    public CheckProblem setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 
     */
    public CheckProblem setSeq(Integer seq) {
        this.seq = seq;
        return this;
    }

    /**
     * 
     */
    public Integer getSeq() {
        return this.seq;
    }
}