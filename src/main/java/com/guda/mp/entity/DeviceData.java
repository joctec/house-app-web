package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: DeviceData <br/>
 * Description: <br/>
 */
public class DeviceData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 设备厂家类型
     */
    private Integer deviceFactoryType;
    /**
     * 设备id
     */
    private String deviceId;
    /**
     * 类型
     */
    private String funcType;
    /**
     * 数据
     */
    private String data;
    /**
     * 业务实际数据
     */
    private String bizDataMsg;
    /**
     * 解析描述
     */
    private String parseDescr;
    /**
     * 解析错误信息
     */
    private String parseErrorMsg;

    public DeviceData() {
    }

    public DeviceData(long id) {
        super.id = id;
    }

    /**
     * 设备id
     */
    public DeviceData setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        return this;
    }

    /**
     * 设备id
     */
    public String getDeviceId() {
        return this.deviceId;
    }

    /**
     * 类型
     */
    public DeviceData setFuncType(String funcType) {
        this.funcType = funcType;
        return this;
    }

    /**
     * 类型
     */
    public String getFuncType() {
        return this.funcType;
    }

    /**
     * 数据
     */
    public DeviceData setData(String data) {
        this.data = data;
        return this;
    }

    /**
     * 数据
     */
    public String getData() {
        return this.data;
    }

    /**
     * 业务实际数据
     */
    public DeviceData setBizDataMsg(String bizDataMsg) {
        this.bizDataMsg = bizDataMsg;
        return this;
    }

    /**
     * 业务实际数据
     */
    public String getBizDataMsg() {
        return this.bizDataMsg;
    }

    /**
     * 解析错误信息
     */
    public DeviceData setParseErrorMsg(String parseErrorMsg) {
        this.parseErrorMsg = parseErrorMsg;
        return this;
    }

    /**
     * 解析错误信息
     */
    public String getParseErrorMsg() {
        return this.parseErrorMsg;
    }

    public String getParseDescr() {
        return parseDescr;
    }

    public void setParseDescr(String parseDescr) {
        this.parseDescr = parseDescr;
    }

    public Integer getDeviceFactoryType() {
        return deviceFactoryType;
    }

    public void setDeviceFactoryType(Integer deviceFactoryType) {
        this.deviceFactoryType = deviceFactoryType;
    }

}