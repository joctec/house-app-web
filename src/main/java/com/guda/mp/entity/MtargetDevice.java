package com.guda.mp.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.core.base.BaseEntity;

/**
 * ClassName: MtargetDevice <br/>
 * Description: 监测目标设备表 <br/>
 */
public class MtargetDevice extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 监测目标id
     */
    private Long targetId;
    /**
     * 监测目标
     */
    private String name;
    /**
     * 设备供应商1鲁尔 2葛南
     */
    private Integer deviceVender;
    /**
     * 设备类型
     */
    private Integer deviceType;
    /**
     * DTU ID
     */
    private Long dtuSn;
    /**
     * 设备编号
     */
    private Long deviceSn;
    /**
     * sim卡号
     */
    private String simSn;
    /**
     * 监测内容
     */
    private String monitorContent;
    /**
     * 安装时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date installTime;
    /**
     * 安装位置
     */
    private String installAddress;
    /**
     * x朝向
     */
    private Integer xDirection;
    /**
     * y朝向
     */
    private Integer yDirection;

    public MtargetDevice() {
    }

    public MtargetDevice(long id) {
        super.id = id;
    }

    /**
     * 监测目标id
     */
    public void setTargetId(Long targetId) {
        this.targetId = targetId;
    }

    /**
     * 监测目标id
     */
    public Long getTargetId() {
        return this.targetId;
    }

    /**
     * 监测目标
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 监测目标
     */
    public String getName() {
        return this.name;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public void setDeviceVender(Integer deviceVender) {
        this.deviceVender = deviceVender;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public Integer getDeviceVender() {
        return this.deviceVender;
    }

    /**
     * 设备类型
     */
    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * 设备类型
     */
    public Integer getDeviceType() {
        return this.deviceType;
    }

    /**
     * DTU ID
     */
    public void setDtuSn(Long dtuSn) {
        this.dtuSn = dtuSn;
    }

    /**
     * DTU ID
     */
    public Long getDtuSn() {
        return this.dtuSn;
    }

    /**
     * 设备编号
     */
    public void setDeviceSn(Long deviceSn) {
        this.deviceSn = deviceSn;
    }

    /**
     * 设备编号
     */
    public Long getDeviceSn() {
        return this.deviceSn;
    }

    /**
     * sim卡号
     */
    public void setSimSn(String simSn) {
        this.simSn = simSn;
    }

    /**
     * sim卡号
     */
    public String getSimSn() {
        return this.simSn;
    }

    /**
     * 监测内容
     */
    public void setMonitorContent(String monitorContent) {
        this.monitorContent = monitorContent;
    }

    /**
     * 监测内容
     */
    public String getMonitorContent() {
        return this.monitorContent;
    }

    /**
     * 安装时间
     */
    public void setInstallTime(Date installTime) {
        this.installTime = installTime;
    }

    /**
     * 安装时间
     */
    public Date getInstallTime() {
        return this.installTime;
    }

    /**
     * 安装位置
     */
    public void setInstallAddress(String installAddress) {
        this.installAddress = installAddress;
    }

    /**
     * 安装位置
     */
    public String getInstallAddress() {
        return this.installAddress;
    }

    /**
     * x朝向
     */
    public void setXDirection(Integer xDirection) {
        this.xDirection = xDirection;
    }

    /**
     * x朝向
     */
    public Integer getXDirection() {
        return this.xDirection;
    }

    /**
     * y朝向
     */
    public void setYDirection(Integer yDirection) {
        this.yDirection = yDirection;
    }

    /**
     * y朝向
     */
    public Integer getYDirection() {
        return this.yDirection;
    }

}