package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/**
 * ClassName: TaskCenterSubsidence <br/>
 * Description: 沉降数据 <br/>
 */
public class TaskCenterSubsidence extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 任务id
     */
    private Long taskId;
    /**
     * 任务检查项id
     */
    private Long taskCenterCheckItemId;
    /**
     * 监测点类型1基准点 2复核点
     */
    private Integer pointType;
    /**
     * 监测点名称
     */
    private String name;
    /**
     * 监测点高差mm
     */
    private Integer val;
    /**
     * 
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date reportTime;

    public TaskCenterSubsidence() {
    }

    public TaskCenterSubsidence(long id) {
        super.id = id;
    }

    /**
     * 任务id
     */
    public TaskCenterSubsidence setTaskId(Long taskId) {
        this.taskId = taskId;
        return this;
    }

    /**
     * 任务id
     */
    public Long getTaskId() {
        return this.taskId;
    }

    /**
     * 任务检查项id
     */
    public TaskCenterSubsidence setTaskCenterCheckItemId(Long taskCenterCheckItemId) {
        this.taskCenterCheckItemId = taskCenterCheckItemId;
        return this;
    }

    /**
     * 任务检查项id
     */
    public Long getTaskCenterCheckItemId() {
        return this.taskCenterCheckItemId;
    }

    /**
     * 监测点类型1基准点 2复核点
     */
    public TaskCenterSubsidence setPointType(Integer pointType) {
        this.pointType = pointType;
        return this;
    }

    /**
     * 监测点类型1基准点 2复核点
     */
    public Integer getPointType() {
        return this.pointType;
    }

    /**
     * 监测点名称
     */
    public TaskCenterSubsidence setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 监测点名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 监测点高差mm
     */
    public TaskCenterSubsidence setVal(Integer val) {
        this.val = val;
        return this;
    }

    /**
     * 监测点高差mm
     */
    public Integer getVal() {
        return this.val;
    }

    /**
     * 
     */
    public TaskCenterSubsidence setReportTime(Date reportTime) {
        this.reportTime = reportTime;
        return this;
    }

    /**
     * 
     */
    public Date getReportTime() {
        return this.reportTime;
    }

}