package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: CheckPackage <br/>
* Description: 检查包，检查项的集合 <br/>
*/ 
public class CheckPackage extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 所属公司/组织id
	 */
    private Long orgId;
	/**
	 * 名称
	 */
    private String name;
	/**
	 * 类型id
	 */
    private Long typeId;
	/**
	 * 描述
	 */
    private String descr;

	public CheckPackage() {
    }

    public CheckPackage(long id) {
        super.id = id;
    }

	/**
	 * 所属公司/组织id
	 */
	public CheckPackage setOrgId(Long orgId){
		this.orgId = orgId;
		return this;
	}
	
	/**
	 * 所属公司/组织id
	 */
	public Long getOrgId(){
		return this.orgId;
	}
	
	/**
	 * 名称
	 */
	public CheckPackage setName(String name){
		this.name = name;
		return this;
	}
	
	/**
	 * 名称
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * 类型id
	 */
	public CheckPackage setTypeId(Long typeId){
		this.typeId = typeId;
		return this;
	}
	
	/**
	 * 类型id
	 */
	public Long getTypeId(){
		return this.typeId;
	}
	
	/**
	 * 描述
	 */
	public CheckPackage setDescr(String descr){
		this.descr = descr;
		return this;
	}
	
	/**
	 * 描述
	 */
	public String getDescr(){
		return this.descr;
	}
	
}