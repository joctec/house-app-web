package com.guda.mp.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.core.base.BaseEntity;

/**
 * ClassName: DeviceAlarm <br/>
 * Description: <br/>
 */
public class DeviceAlarm extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 告警类型。1数据监测告警 2设备故障告警
     */
    private Integer alarmType;
    /**
     * 目标id
     */
    private Long mtargetId;
    /**
     * 目标名称
     */
    private String mtargetName;
    /**
     * 设备类型
     */
    private Integer deviceType;
    /**
     * 告警等级
     */
    private Integer alarmLevel;
    /**
     * 数据时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataTime;
    /**
     * 告警时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date alarmTime;
    /**
     * 告警内容
     */
    private String alarmContent;
    /**
     * 告警数据id
     */
    private Long refDataId;
    /**
     * 状态 1告警 2手动解除告警 3已处理
     */
    private Integer status;

    public DeviceAlarm() {
    }

    public DeviceAlarm(long id) {
        super.id = id;
    }

    /**
     * 告警类型。1数据监测告警 2设备故障告警
     */
    public void setAlarmType(Integer alarmType) {
        this.alarmType = alarmType;
    }

    /**
     * 告警类型。1数据监测告警 2设备故障告警
     */
    public Integer getAlarmType() {
        return this.alarmType;
    }

    /**
     * 目标id
     */
    public void setMtargetId(Long mtargetId) {
        this.mtargetId = mtargetId;
    }

    /**
     * 目标id
     */
    public Long getMtargetId() {
        return this.mtargetId;
    }

    /**
     * 目标名称
     */
    public void setMtargetName(String mtargetName) {
        this.mtargetName = mtargetName;
    }

    /**
     * 目标名称
     */
    public String getMtargetName() {
        return this.mtargetName;
    }

    /**
     * 设备类型
     */
    public void setDeviceType(Integer deviceType) {
        this.deviceType = deviceType;
    }

    /**
     * 设备类型
     */
    public Integer getDeviceType() {
        return this.deviceType;
    }

    /**
     * 告警等级
     */
    public void setAlarmLevel(Integer alarmLevel) {
        this.alarmLevel = alarmLevel;
    }

    /**
     * 告警等级
     */
    public Integer getAlarmLevel() {
        return this.alarmLevel;
    }

    /**
     * 数据时间
     */
    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }

    /**
     * 数据时间
     */
    public Date getDataTime() {
        return this.dataTime;
    }

    /**
     * 告警时间
     */
    public void setAlarmTime(Date alarmTime) {
        this.alarmTime = alarmTime;
    }

    /**
     * 告警时间
     */
    public Date getAlarmTime() {
        return this.alarmTime;
    }

    /**
     * 告警内容
     */
    public void setAlarmContent(String alarmContent) {
        this.alarmContent = alarmContent;
    }

    /**
     * 告警内容
     */
    public String getAlarmContent() {
        return this.alarmContent;
    }

    /**
     * 告警数据id
     */
    public void setRefDataId(Long refDataId) {
        this.refDataId = refDataId;
    }

    /**
     * 告警数据id
     */
    public Long getRefDataId() {
        return this.refDataId;
    }

    /**
     * 状态 1告警 2手动解除告警 3已处理
     */
    public void setStatus(Integer status) {
        this.status = status;
    }

    /**
     * 状态 1告警 2手动解除告警 3已处理
     */
    public Integer getStatus() {
        return this.status;
    }

}