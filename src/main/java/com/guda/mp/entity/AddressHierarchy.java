package com.guda.mp.entity;

import java.util.Date;
import com.app.core.base.BaseEntity;

/** 
* ClassName: AddressHierarchy <br/>
* Description: 地址层级表 <br/>
*/ 
public class AddressHierarchy extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 订单id
	 */
    private Long pid;
	/**
	 * 名称
	 */
    private String name;
	/**
	 * 等级
	 */
    private Integer orderSeq;
	/**
	 * 描述
	 */
    private Long desc;
	
	public AddressHierarchy() {
    }

    public AddressHierarchy(long id) {
        super.id = id;
    }

	/**
	 * 订单id
	 */
	public AddressHierarchy setPid(Long pid){
		this.pid = pid;
		return this;
	}
	
	/**
	 * 订单id
	 */
	public Long getPid(){
		return this.pid;
	}
	
	/**
	 * 名称
	 */
	public AddressHierarchy setName(String name){
		this.name = name;
		return this;
	}
	
	/**
	 * 名称
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * 排序号
	 */
	public AddressHierarchy setOrderSeq(Integer orderSeq){
		this.orderSeq = orderSeq;
		return this;
	}
	
	/**
	 * 排序号
	 */
	public Integer getOrderSeq(){
		return this.orderSeq;
	}
	
	/**
	 * 描述
	 */
	public AddressHierarchy setDesc(Long desc){
		this.desc = desc;
		return this;
	}
	
	/**
	 * 描述
	 */
	public Long getDesc(){
		return this.desc;
	}
	
}