package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: MonitorTargetCheckPackage <br/>
* Description:  <br/>
*/ 
public class MonitorTargetCheckPackage extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
    private Long monitorTargetId;
	/**
	 * 
	 */
    private Long checkPackageId;
	public MonitorTargetCheckPackage() {
    }

    public MonitorTargetCheckPackage(long id) {
        super.id = id;
    }

	/**
	 * 
	 */
	public MonitorTargetCheckPackage setMonitorTargetId(Long monitorTargetId){
		this.monitorTargetId = monitorTargetId;
		return this;
	}
	
	/**
	 * 
	 */
	public Long getMonitorTargetId(){
		return this.monitorTargetId;
	}
	
	/**
	 * 
	 */
	public MonitorTargetCheckPackage setCheckPackageId(Long checkPackageId){
		this.checkPackageId = checkPackageId;
		return this;
	}
	
	/**
	 * 
	 */
	public Long getCheckPackageId(){
		return this.checkPackageId;
	}
	
}