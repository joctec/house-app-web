package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: TaskCenterCheckItem <br/>
* Description: 任务-检查项 <br/>
*/ 
public class TaskCenterCheckItem extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 组织id
	 */
    private Long orgId;
	/**
	 * 任务id
	 */
    private Long taskId;
	/**
	 * 检查包ID
	 */
    private Long checkPackageId;
	/**
	 * 检查包名称
	 */
    private String checkPackageName;
	/**
	 * 检查项ID
	 */
    private Long checkItemId;
	/**
	 * 检查项名称
	 */
    private String checkItemName;
	/**
	 * 检查项描述
	 */
    private String checkItemDescr;
	/**
	 * 问题数量
	 */
    private Integer problemNum;
	/**
	 * 是否检查过
	 */
    private Integer isCheck;
	public TaskCenterCheckItem() {
    }

    public TaskCenterCheckItem(long id) {
        super.id = id;
    }

	/**
	 * 组织id
	 */
	public TaskCenterCheckItem setOrgId(Long orgId){
		this.orgId = orgId;
		return this;
	}
	
	/**
	 * 组织id
	 */
	public Long getOrgId(){
		return this.orgId;
	}
	
	/**
	 * 任务id
	 */
	public TaskCenterCheckItem setTaskId(Long taskId){
		this.taskId = taskId;
		return this;
	}
	
	/**
	 * 任务id
	 */
	public Long getTaskId(){
		return this.taskId;
	}
	
	/**
	 * 检查包ID
	 */
	public TaskCenterCheckItem setCheckPackageId(Long checkPackageId){
		this.checkPackageId = checkPackageId;
		return this;
	}
	
	/**
	 * 检查包ID
	 */
	public Long getCheckPackageId(){
		return this.checkPackageId;
	}
	
	/**
	 * 检查包名称
	 */
	public TaskCenterCheckItem setCheckPackageName(String checkPackageName){
		this.checkPackageName = checkPackageName;
		return this;
	}
	
	/**
	 * 检查包名称
	 */
	public String getCheckPackageName(){
		return this.checkPackageName;
	}
	
	/**
	 * 检查项ID
	 */
	public TaskCenterCheckItem setCheckItemId(Long checkItemId){
		this.checkItemId = checkItemId;
		return this;
	}
	
	/**
	 * 检查项ID
	 */
	public Long getCheckItemId(){
		return this.checkItemId;
	}
	
	/**
	 * 检查项名称
	 */
	public TaskCenterCheckItem setCheckItemName(String checkItemName){
		this.checkItemName = checkItemName;
		return this;
	}
	
	/**
	 * 检查项名称
	 */
	public String getCheckItemName(){
		return this.checkItemName;
	}
	
	/**
	 * 检查项描述
	 */
	public TaskCenterCheckItem setCheckItemDescr(String checkItemDescr){
		this.checkItemDescr = checkItemDescr;
		return this;
	}
	
	/**
	 * 检查项描述
	 */
	public String getCheckItemDescr(){
		return this.checkItemDescr;
	}
	
	/**
	 * 问题数量
	 */
	public TaskCenterCheckItem setProblemNum(Integer problemNum){
		this.problemNum = problemNum;
		return this;
	}
	
	/**
	 * 问题数量
	 */
	public Integer getProblemNum(){
		return this.problemNum;
	}
	
	/**
	 * 是否检查过
	 */
	public TaskCenterCheckItem setIsCheck(Integer isCheck){
		this.isCheck = isCheck;
		return this;
	}
	
	/**
	 * 是否检查过
	 */
	public Integer getIsCheck(){
		return this.isCheck;
	}
	
}