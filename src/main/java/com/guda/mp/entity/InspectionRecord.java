package com.guda.mp.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.core.base.BaseEntity;

/**
 * ClassName: InspectionRecord <br/>
 * Description: 巡检-巡检记录 <br/>
 */
public class InspectionRecord extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 组织ID
     */
    private Long orgId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 监测目标id
     */
    private Long monitorTargetId;
    /**
     * 任务id
     */
    private Long taskId;
    /**
     * 项目id
     */
    private Long projectId;
    /**
     * 人员名称-巡查人员
     */
    private String personName;
    /**
     * 房屋名称
     */
    private String houseName;
    /**
     * 开始时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date startTime;
    /**
     * 结束时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date endTime;
    /**
     * 巡查结果状态：1正常， 2异常
     */
    private Integer status;
    /**
     * 描述
     */
    private String descr;

    public InspectionRecord() {
    }

    public InspectionRecord(long id) {
        super.id = id;
    }

    /**
     * 组织ID
     */
    public InspectionRecord setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 组织ID
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 用户id
     */
    public InspectionRecord setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    /**
     * 用户id
     */
    public Long getUserId() {
        return this.userId;
    }

    /**
     * 监测目标id
     */
    public InspectionRecord setMonitorTargetId(Long monitorTargetId) {
        this.monitorTargetId = monitorTargetId;
        return this;
    }

    /**
     * 监测目标id
     */
    public Long getMonitorTargetId() {
        return this.monitorTargetId;
    }

    /**
     * 任务id
     */
    public InspectionRecord setTaskId(Long taskId) {
        this.taskId = taskId;
        return this;
    }

    /**
     * 任务id
     */
    public Long getTaskId() {
        return this.taskId;
    }

    /**
     * 项目id
     */
    public InspectionRecord setProjectId(Long projectId) {
        this.projectId = projectId;
        return this;
    }

    /**
     * 项目id
     */
    public Long getProjectId() {
        return this.projectId;
    }

    /**
     * 人员名称-巡查人员
     */
    public InspectionRecord setPersonName(String personName) {
        this.personName = personName;
        return this;
    }

    /**
     * 人员名称-巡查人员
     */
    public String getPersonName() {
        return this.personName;
    }

    /**
     * 房屋名称
     */
    public InspectionRecord setHouseName(String houseName) {
        this.houseName = houseName;
        return this;
    }

    /**
     * 房屋名称
     */
    public String getHouseName() {
        return this.houseName;
    }

    /**
     * 开始时间
     */
    public InspectionRecord setStartTime(Date startTime) {
        this.startTime = startTime;
        return this;
    }

    /**
     * 开始时间
     */
    public Date getStartTime() {
        return this.startTime;
    }

    /**
     * 结束时间
     */
    public InspectionRecord setEndTime(Date endTime) {
        this.endTime = endTime;
        return this;
    }

    /**
     * 结束时间
     */
    public Date getEndTime() {
        return this.endTime;
    }

    /**
     * 巡查结果状态：1正常， 2异常
     */
    public InspectionRecord setStatus(Integer status) {
        this.status = status;
        return this;
    }

    /**
     * 巡查结果状态：1正常， 2异常
     */
    public Integer getStatus() {
        return this.status;
    }

    /**
     * 描述
     */
    public InspectionRecord setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

}