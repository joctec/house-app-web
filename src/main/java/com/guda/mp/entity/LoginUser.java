package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: LoginUser <br/>
 * Description: 用户登录账号表 <br/>
 */
public class LoginUser extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 组织ID
     */
    private Long orgId;
    /**
     * 登录用户名
     */
    private String username;
    /**
     * 昵称
     */
    private String nickName;
    /**
     * 密码
     */
    private String password;
    /**
     * 账号状态1正常2锁定3删除
     */
    private Integer status;
    /**
     * 用户类型-预留
     */
    private Integer userType;
    /**
     * 人员id
     */
    private Long personId;
    /**
     * 备注
     */
    private String descr;

    public LoginUser() {
    }

    public LoginUser(long id) {
        super.id = id;
    }

    /**
     * 组织ID
     */
    public LoginUser setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 组织ID
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 登录用户名
     */
    public LoginUser setUsername(String username) {
        this.username = username;
        return this;
    }

    /**
     * 登录用户名
     */
    public String getUsername() {
        return this.username;
    }

    /**
     * 昵称
     */
    public LoginUser setNickName(String nickName) {
        this.nickName = nickName;
        return this;
    }

    /**
     * 昵称
     */
    public String getNickName() {
        return this.nickName;
    }

    /**
     * 密码
     */
    public LoginUser setPassword(String password) {
        this.password = password;
        return this;
    }

    /**
     * 密码
     */
    public String getPassword() {
        return this.password;
    }

    /**
     * 账号状态1正常2锁定3删除
     */
    public LoginUser setStatus(Integer status) {
        this.status = status;
        return this;
    }

    /**
     * 账号状态1正常2锁定3删除
     */
    public Integer getStatus() {
        return this.status;
    }

    /**
     * 用户类型-预留
     */
    public LoginUser setUserType(Integer userType) {
        this.userType = userType;
        return this;
    }

    /**
     * 用户类型-预留
     */
    public Integer getUserType() {
        return this.userType;
    }

    /**
     * 人员id
     */
    public LoginUser setPersonId(Long personId) {
        this.personId = personId;
        return this;
    }

    /**
     * 人员id
     */
    public Long getPersonId() {
        return this.personId;
    }

    /**
     * 备注
     */
    public LoginUser setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 备注
     */
    public String getDescr() {
        return this.descr;
    }

}