package com.guda.mp.entity;

import java.util.Date;

import com.app.core.base.BaseEntity;

/**
 * ClassName: SysLog <br/>
 * Description: 日志表 <br/>
 */
public class SysLog extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	 * 用户ID
	 */
	private Long userId;
	/**
	 * 用户姓名
	 */
	private String userName;
	/**
	 * IP
	 */
	private String userIp;
	/**
	 * 数据ID
	 */
	private String dataId;
	/**
	 * 操作名称
	 */
	private String oprName;
	/**
	 * 操作类型
	 */
	private String oprType;
	/**
	 * 操作时间
	 */
	private Date oprTime;
	/**
	 * 模块
	 */
	private String module;
	/**
	 * URL地址
	 */
	private String url;
	/**
	 * 入参
	 */
	private String inParams;
	/**
	 * 出参
	 */
	private String outParams;
	/**
     *
     */
	private Date createTime;

	/**
	 * 用户ID
	 */
	public Long getUserId() {
		return this.userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * 用户姓名
	 */
	public String getUserName() {
		return this.userName;
	}

	public void setUserIp(String userIp) {
		this.userIp = userIp;
	}

	/**
	 * IP
	 */
	public String getUserIp() {
		return this.userIp;
	}

	public void setDataId(String dataId) {
		this.dataId = dataId;
	}

	/**
	 * 数据ID
	 */
	public String getDataId() {
		return this.dataId;
	}

	public void setOprName(String oprName) {
		this.oprName = oprName;
	}

	/**
	 * 操作名称
	 */
	public String getOprName() {
		return this.oprName;
	}

	public void setOprType(String oprType) {
		this.oprType = oprType;
	}

	/**
	 * 操作类型
	 */
	public String getOprType() {
		return this.oprType;
	}

	public void setOprTime(Date oprTime) {
		this.oprTime = oprTime;
	}

	/**
	 * 操作时间
	 */
	public Date getOprTime() {
		return this.oprTime;
	}

	public void setModule(String module) {
		this.module = module;
	}

	/**
	 * 模块
	 */
	public String getModule() {
		return this.module;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * URL地址
	 */
	public String getUrl() {
		return this.url;
	}

	public void setInParams(String inParams) {
		this.inParams = inParams;
	}

	/**
	 * 入参
	 */
	public String getInParams() {
		return this.inParams;
	}

	public void setOutParams(String outParams) {
		this.outParams = outParams;
	}

	/**
	 * 出参
	 */
	public String getOutParams() {
		return this.outParams;
	}

	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}

	/**
     *
     */
	public Date getCreateTime() {
		return this.createTime;
	}

}