package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: SysAttach <br/>
 * Description: <br/>
 */
public class SysAttach extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 外部id
     */
    private Long refId;
    /**
     * 外部附件类型code
     */
    private String refCode;
    /**
     * 文件名称
     */
    private String name;
    /**
     * 文件类型。 1:图片 2:音频，3:视频，4文档
     */
    private Integer fileType;
    /**
     * 文件路径
     */
    private String url;
    /**
     * 相对路径
     */
    private String relativeUrl;

    public SysAttach() {
    }

    public SysAttach(long id) {
        super.id = id;
    }

    /**
     * 外部id
     */
    public SysAttach setRefId(Long refId) {
        this.refId = refId;
        return this;
    }

    /**
     * 外部id
     */
    public Long getRefId() {
        return this.refId;
    }

    /**
     * 外部附件类型code
     */
    public SysAttach setRefCode(String refCode) {
        this.refCode = refCode;
        return this;
    }

    /**
     * 外部附件类型code
     */
    public String getRefCode() {
        return this.refCode;
    }

    /**
     * 文件名称
     */
    public SysAttach setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 文件名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 文件类型。 1:图片 2:音频，3:视频，4文档
     */
    public SysAttach setFileType(Integer fileType) {
        this.fileType = fileType;
        return this;
    }

    /**
     * 文件类型。 1:图片 2:音频，3:视频，4文档
     */
    public Integer getFileType() {
        return this.fileType;
    }

    /**
     * 文件路径
     */
    public SysAttach setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * 文件路径
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * 相对路径
     */
    public SysAttach setRelativeUrl(String relativeUrl) {
        this.relativeUrl = relativeUrl;
        return this;
    }

    /**
     * 相对路径
     */
    public String getRelativeUrl() {
        return this.relativeUrl;
    }

}