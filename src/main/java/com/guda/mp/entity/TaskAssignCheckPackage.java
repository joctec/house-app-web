package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: TaskAssignCheckPackage <br/>
* Description: 巡检-巡检任务 <br/>
*/ 
public class TaskAssignCheckPackage extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * BIGINT(20)
	 */
    private Long orgId;
	/**
	 * 人员id
	 */
    private Long taskAssignId;
	/**
	 * 名称
	 */
    private Long checkPackageId;

	public TaskAssignCheckPackage() {
    }

    public TaskAssignCheckPackage(long id) {
        super.id = id;
    }

	/**
	 * BIGINT(20)
	 */
	public TaskAssignCheckPackage setOrgId(Long orgId){
		this.orgId = orgId;
		return this;
	}
	
	/**
	 * BIGINT(20)
	 */
	public Long getOrgId(){
		return this.orgId;
	}
	
	/**
	 * 人员id
	 */
	public TaskAssignCheckPackage setTaskAssignId(Long taskAssignId){
		this.taskAssignId = taskAssignId;
		return this;
	}
	
	/**
	 * 人员id
	 */
	public Long getTaskAssignId(){
		return this.taskAssignId;
	}
	
	/**
	 * 名称
	 */
	public TaskAssignCheckPackage setCheckPackageId(Long checkPackageId){
		this.checkPackageId = checkPackageId;
		return this;
	}
	
	/**
	 * 名称
	 */
	public Long getCheckPackageId(){
		return this.checkPackageId;
	}
	
}