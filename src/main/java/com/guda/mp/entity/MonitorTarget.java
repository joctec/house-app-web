package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: MonitorTarget <br/>
 * Description: 监测目标 <br/>
 */
public class MonitorTarget extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属公司/组织id
     */
    private Long orgId;
    /**
     * 
     */
    private Long pid;
    /**
     * 名称
     */
    private String name;
    /**
     * 
     */
    private String code;
    /**
     * 关联房屋id
     */
    private Long refId;
    /**
     * 
     */
    private Integer seq;
    /**
     * 类型,20:房屋
     */
    private Integer type;
    /**
     * 描述
     */
    private String descr;
    /**
     * 地址
     */
    private String address;
    /**
     * 绑卡标志1.绑卡0未绑卡
     */
    private Integer bindCardFlag;
    /**
     * uuid
     */
    private String dataUuid;
    /**
     * 1人工巡检房屋2动态监测房屋
     */
    private String monitorType;

    public MonitorTarget() {
    }

    public MonitorTarget(long id) {
        super.id = id;
    }

    /**
     * 所属公司/组织id
     */
    public MonitorTarget setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 所属公司/组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 
     */
    public MonitorTarget setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    /**
     * 
     */
    public Long getPid() {
        return this.pid;
    }

    /**
     * 名称
     */
    public MonitorTarget setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 
     */
    public MonitorTarget setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * 
     */
    public String getCode() {
        return this.code;
    }

    /**
     * 关联房屋id
     */
    public MonitorTarget setRefId(Long refId) {
        this.refId = refId;
        return this;
    }

    /**
     * 关联房屋id
     */
    public Long getRefId() {
        return this.refId;
    }

    /**
     * 
     */
    public MonitorTarget setSeq(Integer seq) {
        this.seq = seq;
        return this;
    }

    /**
     * 
     */
    public Integer getSeq() {
        return this.seq;
    }

    /**
     * 类型,20:房屋
     */
    public MonitorTarget setType(Integer type) {
        this.type = type;
        return this;
    }

    /**
     * 类型,20:房屋
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 描述
     */
    public MonitorTarget setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 地址
     */
    public MonitorTarget setAddress(String address) {
        this.address = address;
        return this;
    }

    /**
     * 地址
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * 绑卡标志1.绑卡0未绑卡
     */
    public MonitorTarget setBindCardFlag(Integer bindCardFlag) {
        this.bindCardFlag = bindCardFlag;
        return this;
    }

    /**
     * 绑卡标志1.绑卡0未绑卡
     */
    public Integer getBindCardFlag() {
        return this.bindCardFlag;
    }

    /**
     * uuid
     */
    public MonitorTarget setDataUuid(String dataUuid) {
        this.dataUuid = dataUuid;
        return this;
    }

    /**
     * uuid
     */
    public String getDataUuid() {
        return this.dataUuid;
    }

    public String getMonitorType() {
        return monitorType;
    }

    public void setMonitorType(String monitorType) {
        this.monitorType = monitorType;
    }

}