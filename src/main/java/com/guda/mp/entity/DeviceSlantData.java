package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/**
 * ClassName: DeviceSlantData <br/>
 * Description: 设备倾斜数据 <br/>
 */
public class DeviceSlantData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 设备编号
     */
    private Long deviceSn;
    /**
     * 设备供应商1鲁尔 2葛南
     */
    private Integer deviceVender;
    /**
     * 数据日期-天
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dataDay;
    /**
     * 数据时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataTime;
    /**
     * x轴斜率
     */
    private Double xSlope;
    /**
     * x轴倾斜角度
     */
    private Double xBevelAngle;
    /**
     * y轴斜率
     */
    private Double ySlope;
    /**
     * y轴倾斜角度
     */
    private Double yBevelAngle;
    /**
     * 当前状态。 0数据已接收 1已告警 2临时解除告警 3已处理
     */
    private Integer curStatus;
    /**
     * 告警时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date alarmTime;

    public DeviceSlantData() {
    }

    public DeviceSlantData(long id) {
        super.id = id;
    }

    /**
     * 设备编号
     */
    public void setDeviceSn(Long deviceSn) {
        this.deviceSn = deviceSn;
    }

    /**
     * 设备编号
     */
    public Long getDeviceSn() {
        return this.deviceSn;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public void setDeviceVender(Integer deviceVender) {
        this.deviceVender = deviceVender;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public Integer getDeviceVender() {
        return this.deviceVender;
    }

    /**
     * 数据日期-天
     */
    public void setDataDay(Date dataDay) {
        this.dataDay = dataDay;
    }

    /**
     * 数据日期-天
     */
    public Date getDataDay() {
        return this.dataDay;
    }

    /**
     * 数据时间
     */
    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }

    /**
     * 数据时间
     */
    public Date getDataTime() {
        return this.dataTime;
    }

    /**
     * x轴斜率
     */
    public void setXSlope(Double xSlope) {
        this.xSlope = xSlope;
    }

    /**
     * x轴斜率
     */
    public Double getXSlope() {
        return this.xSlope;
    }

    /**
     * x轴倾斜角度
     */
    public void setXBevelAngle(Double xBevelAngle) {
        this.xBevelAngle = xBevelAngle;
    }

    /**
     * x轴倾斜角度
     */
    public Double getXBevelAngle() {
        return this.xBevelAngle;
    }

    /**
     * y轴斜率
     */
    public void setYSlope(Double ySlope) {
        this.ySlope = ySlope;
    }

    /**
     * y轴斜率
     */
    public Double getYSlope() {
        return this.ySlope;
    }

    /**
     * y轴倾斜角度
     */
    public void setYBevelAngle(Double yBevelAngle) {
        this.yBevelAngle = yBevelAngle;
    }

    /**
     * y轴倾斜角度
     */
    public Double getYBevelAngle() {
        return this.yBevelAngle;
    }

    /**
     * 当前状态。 0数据已接收 1已告警 2临时解除告警 3已处理
     */
    public void setCurStatus(Integer curStatus) {
        this.curStatus = curStatus;
    }

    /**
     * 当前状态。 0数据已接收 1已告警 2临时解除告警 3已处理
     */
    public Integer getCurStatus() {
        return this.curStatus;
    }

    /**
     * 告警时间
     */
    public void setAlarmTime(Date alarmTime) {
        this.alarmTime = alarmTime;
    }

    /**
     * 告警时间
     */
    public Date getAlarmTime() {
        return this.alarmTime;
    }

}