package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/**
 * ClassName: CheckItem <br/>
 * Description: 检查项 <br/>
 */
public class CheckItem extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属公司/组织id
     */
    private Long orgId;
    /**
     * 
     */
    private Long pid;
    /**
     * 名称
     */
    private String name;
    /**
     * 类型
     */
    private Integer type;
    /**
     * 描述
     */
    private String descr;
    /**
     * 是否需要图片
     */
    private Integer needPic;
    /**
     * 是否需要NFC
     */
    private Integer needNfc;
    /**
     * 是否需要录音
     */
    private Integer needRecord;
    /**
     * 是否需要视频
     */
    private Integer needVedio;

    public CheckItem() {
    }

    public CheckItem(long id) {
        super.id = id;
    }

    /**
     * 所属公司/组织id
     */
    public CheckItem setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 所属公司/组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 
     */
    public CheckItem setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    /**
     * 
     */
    public Long getPid() {
        return this.pid;
    }

    /**
     * 名称
     */
    public CheckItem setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 类型
     */
    public CheckItem setType(Integer type) {
        this.type = type;
        return this;
    }

    /**
     * 类型
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 描述
     */
    public CheckItem setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 是否需要图片
     */
    public CheckItem setNeedPic(Integer needPic) {
        this.needPic = needPic;
        return this;
    }

    /**
     * 是否需要图片
     */
    public Integer getNeedPic() {
        return this.needPic;
    }

    /**
     * 是否需要NFC
     */
    public CheckItem setNeedNfc(Integer needNfc) {
        this.needNfc = needNfc;
        return this;
    }

    /**
     * 是否需要NFC
     */
    public Integer getNeedNfc() {
        return this.needNfc;
    }

    /**
     * 是否需要录音
     */
    public CheckItem setNeedRecord(Integer needRecord) {
        this.needRecord = needRecord;
        return this;
    }

    /**
     * 是否需要录音
     */
    public Integer getNeedRecord() {
        return this.needRecord;
    }

    /**
     * 是否需要视频
     */
    public CheckItem setNeedVedio(Integer needVedio) {
        this.needVedio = needVedio;
        return this;
    }

    /**
     * 是否需要视频
     */
    public Integer getNeedVedio() {
        return this.needVedio;
    }

}