package com.guda.mp.entity;

import java.util.Date;
import com.app.core.base.BaseEntity;

/** 
* ClassName: SysRoleResource <br/>
* Description: 用户登录账号表 <br/>
*/ 
public class SysRoleResource extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 *  角色ID
	 */
    private Long roleId;
	/**
	 * 资源id
	 */
    private Long resourceId;
	
	public SysRoleResource() {
    }

    public SysRoleResource(long id) {
        super.id = id;
    }

	/**
	 *  角色ID
	 */
	public SysRoleResource setRoleId(Long roleId){
		this.roleId = roleId;
		return this;
	}
	
	/**
	 *  角色ID
	 */
	public Long getRoleId(){
		return this.roleId;
	}
	
	/**
	 * 资源id
	 */
	public SysRoleResource setResourceId(Long resourceId){
		this.resourceId = resourceId;
		return this;
	}
	
	/**
	 * 资源id
	 */
	public Long getResourceId(){
		return this.resourceId;
	}
	
}