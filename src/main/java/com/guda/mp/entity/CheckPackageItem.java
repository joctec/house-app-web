package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: CheckPackageItem <br/>
* Description: 检查包，检查项的集合 <br/>
*/ 
public class CheckPackageItem extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 所属公司/组织id
	 */
    private Long orgId;
	/**
	 * 检查包id
	 */
    private Long checkPackageId;
	/**
	 * 检查项id
	 */
    private Long checkItemId;

	public CheckPackageItem() {
    }

    public CheckPackageItem(long id) {
        super.id = id;
    }

	/**
	 * 所属公司/组织id
	 */
	public CheckPackageItem setOrgId(Long orgId){
		this.orgId = orgId;
		return this;
	}
	
	/**
	 * 所属公司/组织id
	 */
	public Long getOrgId(){
		return this.orgId;
	}
	
	/**
	 * 检查包id
	 */
	public CheckPackageItem setCheckPackageId(Long checkPackageId){
		this.checkPackageId = checkPackageId;
		return this;
	}
	
	/**
	 * 检查包id
	 */
	public Long getCheckPackageId(){
		return this.checkPackageId;
	}
	
	/**
	 * 检查项id
	 */
	public CheckPackageItem setCheckItemId(Long checkItemId){
		this.checkItemId = checkItemId;
		return this;
	}
	
	/**
	 * 检查项id
	 */
	public Long getCheckItemId(){
		return this.checkItemId;
	}
	
}