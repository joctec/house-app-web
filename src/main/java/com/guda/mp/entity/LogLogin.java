package com.guda.mp.entity;

import java.util.Date;
import com.app.core.base.BaseEntity;

/** 
* ClassName: LogLogin <br/>
* Description:  <br/>
*/ 
public class LogLogin extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 登陆用户id
	 */
    private Long loginUserId;
	/**
	 * 登陆用户中文名称
	 */
    private String loginUserName;
	/**
	 * 登录用户名
	 */
    private String loginName;
	/**
	 * 登陆用户所属的组织机构id
	 */
    private Long organId;
	/**
	 * 登陆用户所属的组织机构code
	 */
    private String organCode;
	/**
	 * 用户类型，1组织机构，2用户
	 */
    private Integer userType;
	/**
	 * 登陆时间
	 */
    private Date loginTime;
	/**
	 * 登陆ip
	 */
    private String loginIp;
	
	public LogLogin() {
    }

    public LogLogin(long id) {
        super.id = id;
    }

	/**
	 * 登陆用户id
	 */
	public LogLogin setLoginUserId(Long loginUserId){
		this.loginUserId = loginUserId;
		return this;
	}
	
	/**
	 * 登陆用户id
	 */
	public Long getLoginUserId(){
		return this.loginUserId;
	}
	
	/**
	 * 登陆用户中文名称
	 */
	public LogLogin setLoginUserName(String loginUserName){
		this.loginUserName = loginUserName;
		return this;
	}
	
	/**
	 * 登陆用户中文名称
	 */
	public String getLoginUserName(){
		return this.loginUserName;
	}
	
	/**
	 * 登录用户名
	 */
	public LogLogin setLoginName(String loginName){
		this.loginName = loginName;
		return this;
	}
	
	/**
	 * 登录用户名
	 */
	public String getLoginName(){
		return this.loginName;
	}
	
	/**
	 * 登陆用户所属的组织机构id
	 */
	public LogLogin setOrganId(Long organId){
		this.organId = organId;
		return this;
	}
	
	/**
	 * 登陆用户所属的组织机构id
	 */
	public Long getOrganId(){
		return this.organId;
	}
	
	/**
	 * 登陆用户所属的组织机构code
	 */
	public LogLogin setOrganCode(String organCode){
		this.organCode = organCode;
		return this;
	}
	
	/**
	 * 登陆用户所属的组织机构code
	 */
	public String getOrganCode(){
		return this.organCode;
	}
	
	/**
	 * 用户类型，1组织机构，2用户
	 */
	public LogLogin setUserType(Integer userType){
		this.userType = userType;
		return this;
	}
	
	/**
	 * 用户类型，1组织机构，2用户
	 */
	public Integer getUserType(){
		return this.userType;
	}
	
	/**
	 * 登陆时间
	 */
	public LogLogin setLoginTime(Date loginTime){
		this.loginTime = loginTime;
		return this;
	}
	
	/**
	 * 登陆时间
	 */
	public Date getLoginTime(){
		return this.loginTime;
	}
	
	/**
	 * 登陆ip
	 */
	public LogLogin setLoginIp(String loginIp){
		this.loginIp = loginIp;
		return this;
	}
	
	/**
	 * 登陆ip
	 */
	public String getLoginIp(){
		return this.loginIp;
	}
	
}