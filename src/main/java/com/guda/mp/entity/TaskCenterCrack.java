package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: TaskCenterCrack <br/>
* Description: 裂缝数据 <br/>
*/ 
public class TaskCenterCrack extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 任务id
	 */
    private Long taskId;
	/**
	 * 任务检查项id
	 */
    private Long taskCenterCheckItemId;
	/**
	 * 裂缝长度mm
	 */
    private Integer length;
	/**
	 * 裂缝宽度mm
	 */
    private Integer width;
	/**
	 * 所在房间
	 */
    private Integer room;
	/**
	 * 方位
	 */
    private Integer direction;
	/**
	 * 楼层
	 */
    private Integer floorNum;
	/**
	 * 裂缝所在位置
	 */
    private Integer place;
	public TaskCenterCrack() {
    }

    public TaskCenterCrack(long id) {
        super.id = id;
    }

	/**
	 * 任务id
	 */
	public TaskCenterCrack setTaskId(Long taskId){
		this.taskId = taskId;
		return this;
	}
	
	/**
	 * 任务id
	 */
	public Long getTaskId(){
		return this.taskId;
	}
	
	/**
	 * 任务检查项id
	 */
	public TaskCenterCrack setTaskCenterCheckItemId(Long taskCenterCheckItemId){
		this.taskCenterCheckItemId = taskCenterCheckItemId;
		return this;
	}
	
	/**
	 * 任务检查项id
	 */
	public Long getTaskCenterCheckItemId(){
		return this.taskCenterCheckItemId;
	}
	
	/**
	 * 裂缝长度mm
	 */
	public TaskCenterCrack setLength(Integer length){
		this.length = length;
		return this;
	}
	
	/**
	 * 裂缝长度mm
	 */
	public Integer getLength(){
		return this.length;
	}
	
	/**
	 * 裂缝宽度mm
	 */
	public TaskCenterCrack setWidth(Integer width){
		this.width = width;
		return this;
	}
	
	/**
	 * 裂缝宽度mm
	 */
	public Integer getWidth(){
		return this.width;
	}
	
	/**
	 * 所在房间
	 */
	public TaskCenterCrack setRoom(Integer room){
		this.room = room;
		return this;
	}
	
	/**
	 * 所在房间
	 */
	public Integer getRoom(){
		return this.room;
	}
	
	/**
	 * 方位
	 */
	public TaskCenterCrack setDirection(Integer direction){
		this.direction = direction;
		return this;
	}
	
	/**
	 * 方位
	 */
	public Integer getDirection(){
		return this.direction;
	}
	
	/**
	 * 楼层
	 */
	public TaskCenterCrack setFloorNum(Integer floorNum){
		this.floorNum = floorNum;
		return this;
	}
	
	/**
	 * 楼层
	 */
	public Integer getFloorNum(){
		return this.floorNum;
	}
	
	/**
	 * 裂缝所在位置
	 */
	public TaskCenterCrack setPlace(Integer place){
		this.place = place;
		return this;
	}
	
	/**
	 * 裂缝所在位置
	 */
	public Integer getPlace(){
		return this.place;
	}
	
}