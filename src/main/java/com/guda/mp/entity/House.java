package com.guda.mp.entity;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.app.core.base.BaseEntity;

/**
 * ClassName: House <br/>
 * Description: 房屋信息 <br/>
 */
public class House extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 组织id
     */
    private Long orgId;
    /**
     * 项目id
     */
    private Long projectId;
    /**
     * 名称
     */
    private String name;
    /**
     * 编号
     */
    private String number;
    /**
     * 年份
     */
    private Integer year;
    /**
     * 面积
     */
    private Double area;
    /**
     * 地址
     */
    private String address;
    /**
     * 坐标-经度
     */
    private Double lat;
    /**
     * 坐标-维度
     */
    private Double lng;
    /**
     * 结构类型
     */
    private Integer structType;
    /**
     * 土地性质
     */
    private Integer landNature;
    /**
     * 产权性质
     */
    private Integer ownNature;
    /**
     * 基础情况类型
     */
    private Integer baseTypeInfo;
    /**
     * 竣工日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date completeDate;
    /**
     * 总建筑面积
     */
    private Double builtArea;
    /**
     * 总层数
     */
    private Integer totalFloorNum;
    /**
     * 地上层数
     */
    private Integer groundFloorNum;
    /**
     * 地下层数
     */
    private Integer undergroundFloorNum;
    /**
     * 单元数量
     */
    private Integer unitNum;
    /**
     * 建设单位
     */
    private String buildUnitName;
    /**
     * 设计单位
     */
    private String designUnitName;
    /**
     * 施工单位
     */
    private String constructionUnitName;
    /**
     * 监理单位
     */
    private String supervisorUnitName;
    /**
     * 总套数
     */
    private Integer totalNumber;
    /**
     * 直管公房套数
     */
    private Integer directPubHouseNum;
    /**
     * 单位自管公房套数
     */
    private Integer unitSelfHouseNum;
    /**
     * 私房数量
     */
    private Integer privateHouseNum;
    /**
     * 商品房数量
     */
    private Integer commodityHouseNum;
    /**
     * 房改房数量
     */
    private Integer housingReformhouseNum;
    /**
     * 拆迁安置房数量
     */
    private Integer resettlementHouseNum;
    /**
     * 其它数量
     */
    private Integer otherHouseNum;
    /**
     * 房屋场地类型
     */
    private Integer housePlaceType;
    /**
     * 化学侵蚀类型
     */
    private Integer chemicalAttackType;
    /**
     * 结构拆改类型
     */
    private Integer changeType;
    /**
     * 加层改造类型
     */
    private Integer addLayerType;
    /**
     * 修缮类型
     */
    private Integer repairType;
    /**
     * 历史灾害类型
     */
    private Integer historicalDisasterType;
    /**
     * 使用功能变更类型
     */
    private Integer useFuncChangeType;
    /**
     * 其他调查内容
     */
    private String otherQuiryContent;
    /**
     * 鉴定情况类型
     */
    private Integer appraisalType;
    /**
     * 解危处置情况
     */
    private String disposalDangerInfo;
    /**
     * 图纸资料类型
     */
    private Integer drawingDataType;
    /**
     * 图纸资料存档信息
     */
    private String drawingArchive;
    /**
     * 调查登记机构
     */
    private String investigationOrg;
    /**
     * 主管部门
     */
    private String competentDept;
    /**
     * 描述
     */
    private String descr;
    /**
     * 物业单位名称
     */
    private String propertyUnitName;
    /**
     * 物业单位负责人
     */
    private String propertyUnitDirector;
    /**
     * 物业单位负责人电话
     */
    private String propertyUnitDirectorPhone;
    /**
     * 物业单位联系人
     */
    private String propertyUnitContact;
    /**
     * 物业单位联系人电话
     */
    private String propertyUnitContactPhone;
    /**
     * 产权单位名称
     */
    private String ownerUnitName;
    /**
     * 产权单位负责人
     */
    private String ownerUnitDirector;
    /**
     * 产权单位负责人电话
     */
    private String ownerUnitDirectorPhone;
    /**
     * 产权单位联系人
     */
    private String ownerUnitContact;
    /**
     * 产权单位联系人电话
     */
    private String ownerUnitContactPhone;
    /**
     * 属地管理社区负责人
     */
    private String localManager;
    /**
     * 属地管理社区负责人电话
     */
    private String localManagerPhone;
    /**
     * 房屋变形监测方案
     */
    private String deformationMonitorPlan;
    /**
     * 结构主要缺陷
     */
    private String structDefect;
    /**
     * 鉴定等级
     */
    private String appraisalLevel;
    /**
     * 房屋图片
     */
    private String houseImg;

    public House() {
    }

    public House(long id) {
        super.id = id;
    }

    /**
     * 组织id
     */
    public House setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 名称
     */
    public House setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 编号
     */
    public House setNumber(String number) {
        this.number = number;
        return this;
    }

    /**
     * 编号
     */
    public String getNumber() {
        return this.number;
    }

    /**
     * 年份
     */
    public House setYear(Integer year) {
        this.year = year;
        return this;
    }

    /**
     * 年份
     */
    public Integer getYear() {
        return this.year;
    }

    /**
     * 面积
     */
    public House setArea(Double area) {
        this.area = area;
        return this;
    }

    /**
     * 面积
     */
    public Double getArea() {
        return this.area;
    }

    /**
     * 地址
     */
    public House setAddress(String address) {
        this.address = address;
        return this;
    }

    /**
     * 地址
     */
    public String getAddress() {
        return this.address;
    }

    /**
     * 坐标-经度
     */
    public House setLat(Double lat) {
        this.lat = lat;
        return this;
    }

    /**
     * 坐标-经度
     */
    public Double getLat() {
        return this.lat;
    }

    /**
     * 坐标-维度
     */
    public House setLng(Double lng) {
        this.lng = lng;
        return this;
    }

    /**
     * 坐标-维度
     */
    public Double getLng() {
        return this.lng;
    }

    /**
     * 结构类型
     */
    public House setStructType(Integer structType) {
        this.structType = structType;
        return this;
    }

    /**
     * 结构类型
     */
    public Integer getStructType() {
        return this.structType;
    }

    /**
     * 土地性质
     */
    public House setLandNature(Integer landNature) {
        this.landNature = landNature;
        return this;
    }

    /**
     * 土地性质
     */
    public Integer getLandNature() {
        return this.landNature;
    }

    /**
     * 产权性质
     */
    public House setOwnNature(Integer ownNature) {
        this.ownNature = ownNature;
        return this;
    }

    /**
     * 产权性质
     */
    public Integer getOwnNature() {
        return this.ownNature;
    }

    /**
     * 基础情况类型
     */
    public House setBaseTypeInfo(Integer baseTypeInfo) {
        this.baseTypeInfo = baseTypeInfo;
        return this;
    }

    /**
     * 基础情况类型
     */
    public Integer getBaseTypeInfo() {
        return this.baseTypeInfo;
    }

    /**
     * 竣工日期
     */
    public House setCompleteDate(Date completeDate) {
        this.completeDate = completeDate;
        return this;
    }

    /**
     * 竣工日期
     */
    public Date getCompleteDate() {
        return this.completeDate;
    }

    /**
     * 总建筑面积
     */
    public House setBuiltArea(Double builtArea) {
        this.builtArea = builtArea;
        return this;
    }

    /**
     * 总建筑面积
     */
    public Double getBuiltArea() {
        return this.builtArea;
    }

    /**
     * 总层数
     */
    public House setTotalFloorNum(Integer totalFloorNum) {
        this.totalFloorNum = totalFloorNum;
        return this;
    }

    /**
     * 总层数
     */
    public Integer getTotalFloorNum() {
        return this.totalFloorNum;
    }

    /**
     * 地上层数
     */
    public House setGroundFloorNum(Integer groundFloorNum) {
        this.groundFloorNum = groundFloorNum;
        return this;
    }

    /**
     * 地上层数
     */
    public Integer getGroundFloorNum() {
        return this.groundFloorNum;
    }

    /**
     * 地下层数
     */
    public House setUndergroundFloorNum(Integer undergroundFloorNum) {
        this.undergroundFloorNum = undergroundFloorNum;
        return this;
    }

    /**
     * 地下层数
     */
    public Integer getUndergroundFloorNum() {
        return this.undergroundFloorNum;
    }

    /**
     * 单元数量
     */
    public House setUnitNum(Integer unitNum) {
        this.unitNum = unitNum;
        return this;
    }

    /**
     * 单元数量
     */
    public Integer getUnitNum() {
        return this.unitNum;
    }

    /**
     * 建设单位
     */
    public House setBuildUnitName(String buildUnitName) {
        this.buildUnitName = buildUnitName;
        return this;
    }

    /**
     * 建设单位
     */
    public String getBuildUnitName() {
        return this.buildUnitName;
    }

    /**
     * 设计单位
     */
    public House setDesignUnitName(String designUnitName) {
        this.designUnitName = designUnitName;
        return this;
    }

    /**
     * 设计单位
     */
    public String getDesignUnitName() {
        return this.designUnitName;
    }

    /**
     * 施工单位
     */
    public House setConstructionUnitName(String constructionUnitName) {
        this.constructionUnitName = constructionUnitName;
        return this;
    }

    /**
     * 施工单位
     */
    public String getConstructionUnitName() {
        return this.constructionUnitName;
    }

    /**
     * 监理单位
     */
    public House setSupervisorUnitName(String supervisorUnitName) {
        this.supervisorUnitName = supervisorUnitName;
        return this;
    }

    /**
     * 监理单位
     */
    public String getSupervisorUnitName() {
        return this.supervisorUnitName;
    }

    /**
     * 总套数
     */
    public House setTotalNumber(Integer totalNumber) {
        this.totalNumber = totalNumber;
        return this;
    }

    /**
     * 总套数
     */
    public Integer getTotalNumber() {
        return this.totalNumber;
    }

    /**
     * 直管公房套数
     */
    public House setDirectPubHouseNum(Integer directPubHouseNum) {
        this.directPubHouseNum = directPubHouseNum;
        return this;
    }

    /**
     * 直管公房套数
     */
    public Integer getDirectPubHouseNum() {
        return this.directPubHouseNum;
    }

    /**
     * 单位自管公房套数
     */
    public House setUnitSelfHouseNum(Integer unitSelfHouseNum) {
        this.unitSelfHouseNum = unitSelfHouseNum;
        return this;
    }

    /**
     * 单位自管公房套数
     */
    public Integer getUnitSelfHouseNum() {
        return this.unitSelfHouseNum;
    }

    /**
     * 私房数量
     */
    public House setPrivateHouseNum(Integer privateHouseNum) {
        this.privateHouseNum = privateHouseNum;
        return this;
    }

    /**
     * 私房数量
     */
    public Integer getPrivateHouseNum() {
        return this.privateHouseNum;
    }

    /**
     * 商品房数量
     */
    public House setCommodityHouseNum(Integer commodityHouseNum) {
        this.commodityHouseNum = commodityHouseNum;
        return this;
    }

    /**
     * 商品房数量
     */
    public Integer getCommodityHouseNum() {
        return this.commodityHouseNum;
    }

    /**
     * 房改房数量
     */
    public House setHousingReformhouseNum(Integer housingReformhouseNum) {
        this.housingReformhouseNum = housingReformhouseNum;
        return this;
    }

    /**
     * 房改房数量
     */
    public Integer getHousingReformhouseNum() {
        return this.housingReformhouseNum;
    }

    /**
     * 拆迁安置房数量
     */
    public House setResettlementHouseNum(Integer resettlementHouseNum) {
        this.resettlementHouseNum = resettlementHouseNum;
        return this;
    }

    /**
     * 拆迁安置房数量
     */
    public Integer getResettlementHouseNum() {
        return this.resettlementHouseNum;
    }

    /**
     * 其它数量
     */
    public House setOtherHouseNum(Integer otherHouseNum) {
        this.otherHouseNum = otherHouseNum;
        return this;
    }

    /**
     * 其它数量
     */
    public Integer getOtherHouseNum() {
        return this.otherHouseNum;
    }

    /**
     * 房屋场地类型
     */
    public House setHousePlaceType(Integer housePlaceType) {
        this.housePlaceType = housePlaceType;
        return this;
    }

    /**
     * 房屋场地类型
     */
    public Integer getHousePlaceType() {
        return this.housePlaceType;
    }

    /**
     * 化学侵蚀类型
     */
    public House setChemicalAttackType(Integer chemicalAttackType) {
        this.chemicalAttackType = chemicalAttackType;
        return this;
    }

    /**
     * 化学侵蚀类型
     */
    public Integer getChemicalAttackType() {
        return this.chemicalAttackType;
    }

    /**
     * 结构拆改类型
     */
    public House setChangeType(Integer changeType) {
        this.changeType = changeType;
        return this;
    }

    /**
     * 结构拆改类型
     */
    public Integer getChangeType() {
        return this.changeType;
    }

    /**
     * 加层改造类型
     */
    public House setAddLayerType(Integer addLayerType) {
        this.addLayerType = addLayerType;
        return this;
    }

    /**
     * 加层改造类型
     */
    public Integer getAddLayerType() {
        return this.addLayerType;
    }

    /**
     * 修缮类型
     */
    public House setRepairType(Integer repairType) {
        this.repairType = repairType;
        return this;
    }

    /**
     * 修缮类型
     */
    public Integer getRepairType() {
        return this.repairType;
    }

    /**
     * 历史灾害类型
     */
    public House setHistoricalDisasterType(Integer historicalDisasterType) {
        this.historicalDisasterType = historicalDisasterType;
        return this;
    }

    /**
     * 历史灾害类型
     */
    public Integer getHistoricalDisasterType() {
        return this.historicalDisasterType;
    }

    /**
     * 使用功能变更类型
     */
    public House setUseFuncChangeType(Integer useFuncChangeType) {
        this.useFuncChangeType = useFuncChangeType;
        return this;
    }

    /**
     * 使用功能变更类型
     */
    public Integer getUseFuncChangeType() {
        return this.useFuncChangeType;
    }

    /**
     * 其他调查内容
     */
    public House setOtherQuiryContent(String otherQuiryContent) {
        this.otherQuiryContent = otherQuiryContent;
        return this;
    }

    /**
     * 其他调查内容
     */
    public String getOtherQuiryContent() {
        return this.otherQuiryContent;
    }

    /**
     * 鉴定情况类型
     */
    public House setAppraisalType(Integer appraisalType) {
        this.appraisalType = appraisalType;
        return this;
    }

    /**
     * 鉴定情况类型
     */
    public Integer getAppraisalType() {
        return this.appraisalType;
    }

    /**
     * 解危处置情况
     */
    public House setDisposalDangerInfo(String disposalDangerInfo) {
        this.disposalDangerInfo = disposalDangerInfo;
        return this;
    }

    /**
     * 解危处置情况
     */
    public String getDisposalDangerInfo() {
        return this.disposalDangerInfo;
    }

    /**
     * 图纸资料类型
     */
    public House setDrawingDataType(Integer drawingDataType) {
        this.drawingDataType = drawingDataType;
        return this;
    }

    /**
     * 图纸资料类型
     */
    public Integer getDrawingDataType() {
        return this.drawingDataType;
    }

    /**
     * 图纸资料存档信息
     */
    public House setDrawingArchive(String drawingArchive) {
        this.drawingArchive = drawingArchive;
        return this;
    }

    /**
     * 图纸资料存档信息
     */
    public String getDrawingArchive() {
        return this.drawingArchive;
    }

    /**
     * 调查登记机构
     */
    public House setInvestigationOrg(String investigationOrg) {
        this.investigationOrg = investigationOrg;
        return this;
    }

    /**
     * 调查登记机构
     */
    public String getInvestigationOrg() {
        return this.investigationOrg;
    }

    /**
     * 主管部门
     */
    public House setCompetentDept(String competentDept) {
        this.competentDept = competentDept;
        return this;
    }

    /**
     * 主管部门
     */
    public String getCompetentDept() {
        return this.competentDept;
    }

    /**
     * 描述
     */
    public House setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 物业单位名称
     */
    public House setPropertyUnitName(String propertyUnitName) {
        this.propertyUnitName = propertyUnitName;
        return this;
    }

    /**
     * 物业单位名称
     */
    public String getPropertyUnitName() {
        return this.propertyUnitName;
    }

    /**
     * 物业单位负责人
     */
    public House setPropertyUnitDirector(String propertyUnitDirector) {
        this.propertyUnitDirector = propertyUnitDirector;
        return this;
    }

    /**
     * 物业单位负责人
     */
    public String getPropertyUnitDirector() {
        return this.propertyUnitDirector;
    }

    /**
     * 物业单位负责人电话
     */
    public House setPropertyUnitDirectorPhone(String propertyUnitDirectorPhone) {
        this.propertyUnitDirectorPhone = propertyUnitDirectorPhone;
        return this;
    }

    /**
     * 物业单位负责人电话
     */
    public String getPropertyUnitDirectorPhone() {
        return this.propertyUnitDirectorPhone;
    }

    /**
     * 物业单位联系人
     */
    public House setPropertyUnitContact(String propertyUnitContact) {
        this.propertyUnitContact = propertyUnitContact;
        return this;
    }

    /**
     * 物业单位联系人
     */
    public String getPropertyUnitContact() {
        return this.propertyUnitContact;
    }

    /**
     * 物业单位联系人电话
     */
    public House setPropertyUnitContactPhone(String propertyUnitContactPhone) {
        this.propertyUnitContactPhone = propertyUnitContactPhone;
        return this;
    }

    /**
     * 物业单位联系人电话
     */
    public String getPropertyUnitContactPhone() {
        return this.propertyUnitContactPhone;
    }

    /**
     * 产权单位名称
     */
    public House setOwnerUnitName(String ownerUnitName) {
        this.ownerUnitName = ownerUnitName;
        return this;
    }

    /**
     * 产权单位名称
     */
    public String getOwnerUnitName() {
        return this.ownerUnitName;
    }

    /**
     * 产权单位负责人
     */
    public House setOwnerUnitDirector(String ownerUnitDirector) {
        this.ownerUnitDirector = ownerUnitDirector;
        return this;
    }

    /**
     * 产权单位负责人
     */
    public String getOwnerUnitDirector() {
        return this.ownerUnitDirector;
    }

    /**
     * 产权单位负责人电话
     */
    public House setOwnerUnitDirectorPhone(String ownerUnitDirectorPhone) {
        this.ownerUnitDirectorPhone = ownerUnitDirectorPhone;
        return this;
    }

    /**
     * 产权单位负责人电话
     */
    public String getOwnerUnitDirectorPhone() {
        return this.ownerUnitDirectorPhone;
    }

    /**
     * 产权单位联系人
     */
    public House setOwnerUnitContact(String ownerUnitContact) {
        this.ownerUnitContact = ownerUnitContact;
        return this;
    }

    /**
     * 产权单位联系人
     */
    public String getOwnerUnitContact() {
        return this.ownerUnitContact;
    }

    /**
     * 产权单位联系人电话
     */
    public House setOwnerUnitContactPhone(String ownerUnitContactPhone) {
        this.ownerUnitContactPhone = ownerUnitContactPhone;
        return this;
    }

    /**
     * 产权单位联系人电话
     */
    public String getOwnerUnitContactPhone() {
        return this.ownerUnitContactPhone;
    }

    /**
     * 属地管理社区负责人
     */
    public House setLocalManager(String localManager) {
        this.localManager = localManager;
        return this;
    }

    /**
     * 属地管理社区负责人
     */
    public String getLocalManager() {
        return this.localManager;
    }

    /**
     * 属地管理社区负责人电话
     */
    public House setLocalManagerPhone(String localManagerPhone) {
        this.localManagerPhone = localManagerPhone;
        return this;
    }

    /**
     * 属地管理社区负责人电话
     */
    public String getLocalManagerPhone() {
        return this.localManagerPhone;
    }

    /**
     * 房屋变形监测方案
     */
    public House setDeformationMonitorPlan(String deformationMonitorPlan) {
        this.deformationMonitorPlan = deformationMonitorPlan;
        return this;
    }

    /**
     * 房屋变形监测方案
     */
    public String getDeformationMonitorPlan() {
        return this.deformationMonitorPlan;
    }

    /**
     * 结构主要缺陷
     */
    public House setStructDefect(String structDefect) {
        this.structDefect = structDefect;
        return this;
    }

    /**
     * 结构主要缺陷
     */
    public String getStructDefect() {
        return this.structDefect;
    }

    /**
     * 鉴定等级
     */
    public House setAppraisalLevel(String appraisalLevel) {
        this.appraisalLevel = appraisalLevel;
        return this;
    }

    /**
     * 鉴定等级
     */
    public String getAppraisalLevel() {
        return this.appraisalLevel;
    }

    public String getHouseImg() {
        return houseImg;
    }

    public void setHouseImg(String houseImg) {
        this.houseImg = houseImg;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

}