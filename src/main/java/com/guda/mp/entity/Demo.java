package com.guda.mp.entity;

import java.util.Date;
import com.app.core.base.BaseEntity;

/** 
* ClassName: Demo <br/>
* Description:  <br/>
*/ 
public class Demo extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
    private String username;
	/**
	 * 
	 */
    private String password;
	
	public Demo() {
    }

    public Demo(long id) {
        super.id = id;
    }

	/**
	 * 
	 */
	public Demo setUsername(String username){
		this.username = username;
		return this;
	}
	
	/**
	 * 
	 */
	public String getUsername(){
		return this.username;
	}
	
	/**
	 * 
	 */
	public Demo setPassword(String password){
		this.password = password;
		return this;
	}
	
	/**
	 * 
	 */
	public String getPassword(){
		return this.password;
	}
	
}