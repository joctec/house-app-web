package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: SysDict <br/>
* Description: 系统字典项 <br/>
*/ 
public class SysDict extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 
	 */
    private Long pid;
	/**
	 * 名称
	 */
    private String name;
	/**
	 * 
	 */
    private String code;
	/**
	 * 顺序
	 */
    private Integer seq;
	/**
	 * 类型
	 */
    private Integer type;
	/**
	 * 描述
	 */
    private String descr;
	/**
	 * 状态。 1启用， 2停用
	 */
    private Integer status;

	public SysDict() {
    }

    public SysDict(long id) {
        super.id = id;
    }

	/**
	 * 
	 */
	public SysDict setPid(Long pid){
		this.pid = pid;
		return this;
	}
	
	/**
	 * 
	 */
	public Long getPid(){
		return this.pid;
	}
	
	/**
	 * 名称
	 */
	public SysDict setName(String name){
		this.name = name;
		return this;
	}
	
	/**
	 * 名称
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * 
	 */
	public SysDict setCode(String code){
		this.code = code;
		return this;
	}
	
	/**
	 * 
	 */
	public String getCode(){
		return this.code;
	}
	
	/**
	 * 顺序
	 */
	public SysDict setSeq(Integer seq){
		this.seq = seq;
		return this;
	}
	
	/**
	 * 顺序
	 */
	public Integer getSeq(){
		return this.seq;
	}
	
	/**
	 * 类型
	 */
	public SysDict setType(Integer type){
		this.type = type;
		return this;
	}
	
	/**
	 * 类型
	 */
	public Integer getType(){
		return this.type;
	}
	
	/**
	 * 描述
	 */
	public SysDict setDescr(String descr){
		this.descr = descr;
		return this;
	}
	
	/**
	 * 描述
	 */
	public String getDescr(){
		return this.descr;
	}
	
	/**
	 * 状态。 1启用， 2停用
	 */
	public SysDict setStatus(Integer status){
		this.status = status;
		return this;
	}
	
	/**
	 * 状态。 1启用， 2停用
	 */
	public Integer getStatus(){
		return this.status;
	}
	
}