package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/**
 * ClassName: Project <br/>
 * Description: 项目 <br/>
 */
public class Project extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 所属公司/组织id
     */
    private Long orgId;
    /**
     * 合同ID
     */
    private Long contractId;
    /**
     * 项目名称
     */
    private String name;
    /**
     * 项目状态
     */
    private Integer status;
    /**
     * 开始日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date startDate;
    /**
     * 结束日期
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;

    public Project() {
    }

    public Project(long id) {
        super.id = id;
    }

    /**
     * 所属公司/组织id
     */
    public Project setOrgId(Long orgId) {
        this.orgId = orgId;
        return this;
    }

    /**
     * 所属公司/组织id
     */
    public Long getOrgId() {
        return this.orgId;
    }

    /**
     * 合同ID
     */
    public Project setContractId(Long contractId) {
        this.contractId = contractId;
        return this;
    }

    /**
     * 合同ID
     */
    public Long getContractId() {
        return this.contractId;
    }

    /**
     * 项目名称
     */
    public Project setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 项目名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * 项目状态
     */
    public Project setStatus(Integer status) {
        this.status = status;
        return this;
    }

    /**
     * 项目状态
     */
    public Integer getStatus() {
        return this.status;
    }

    /**
     * 开始日期
     */
    public Project setStartDate(Date startDate) {
        this.startDate = startDate;
        return this;
    }

    /**
     * 开始日期
     */
    public Date getStartDate() {
        return this.startDate;
    }

    /**
     * 结束日期
     */
    public Project setEndDate(Date endDate) {
        this.endDate = endDate;
        return this;
    }

    /**
     * 结束日期
     */
    public Date getEndDate() {
        return this.endDate;
    }

}