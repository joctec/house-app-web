package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/**
 * ClassName: SensorData <br/>
 * Description: <br/>
 */
public class SensorData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 设备表id主键
     */
    private Long deviceId;
    /**
     * 设备编号
     */
    private Long deviceSn;
    /**
     * 设备供应商1鲁尔 2葛南
     */
    private Integer deviceVender;
    /**
     * 传感器类型。1倾斜 3位移 4应变计 5静力水准仪 6气温计 7湿度计
     */
    private Integer sensorType;
    /**
     * 11倾斜率X, 12倾斜率Y
     */
    private Integer funType;
    /**
     * 数据时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date dataTime;
    /**
     * 日期数字long
     */
    private Long dataTimeLong;
    /**
     * 
     */
    private Integer dataTimeDayLong;
    /**
     * 传感器值
     */
    private Double val;
    /**
     * 
     */
    private Long outId;
    /**
     * 0未告警 1已告警
     */
    private Integer alarmStatus;

    public SensorData() {
    }

    public SensorData(long id) {
        super.id = id;
    }

    /**
     * 设备表id主键
     */
    public void setDeviceId(Long deviceId) {
        this.deviceId = deviceId;
    }

    /**
     * 设备表id主键
     */
    public Long getDeviceId() {
        return this.deviceId;
    }

    /**
     * 设备编号
     */
    public void setDeviceSn(Long deviceSn) {
        this.deviceSn = deviceSn;
    }

    /**
     * 设备编号
     */
    public Long getDeviceSn() {
        return this.deviceSn;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public void setDeviceVender(Integer deviceVender) {
        this.deviceVender = deviceVender;
    }

    /**
     * 设备供应商1鲁尔 2葛南
     */
    public Integer getDeviceVender() {
        return this.deviceVender;
    }

    /**
     * 传感器类型。1倾斜 3位移 4应变计 5静力水准仪 6气温计 7湿度计
     */
    public void setSensorType(Integer sensorType) {
        this.sensorType = sensorType;
    }

    /**
     * 传感器类型。1倾斜 3位移 4应变计 5静力水准仪 6气温计 7湿度计
     */
    public Integer getSensorType() {
        return this.sensorType;
    }

    /**
     * 11倾斜率X, 12倾斜率Y
     */
    public void setFunType(Integer funType) {
        this.funType = funType;
    }

    /**
     * 11倾斜率X, 12倾斜率Y
     */
    public Integer getFunType() {
        return this.funType;
    }

    /**
     * 数据时间
     */
    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }

    /**
     * 数据时间
     */
    public Date getDataTime() {
        return this.dataTime;
    }

    /**
     * 日期数字long
     */
    public void setDataTimeLong(Long dataTimeLong) {
        this.dataTimeLong = dataTimeLong;
    }

    /**
     * 日期数字long
     */
    public Long getDataTimeLong() {
        return this.dataTimeLong;
    }

    /**
     * 
     */
    public void setDataTimeDayLong(Integer dataTimeDayLong) {
        this.dataTimeDayLong = dataTimeDayLong;
    }

    /**
     * 
     */
    public Integer getDataTimeDayLong() {
        return this.dataTimeDayLong;
    }

    /**
     * 传感器值
     */
    public void setVal(Double val) {
        this.val = val;
    }

    /**
     * 传感器值
     */
    public Double getVal() {
        return this.val;
    }

    /**
     * 
     */
    public void setOutId(Long outId) {
        this.outId = outId;
    }

    /**
     * 
     */
    public Long getOutId() {
        return this.outId;
    }

    /**
     * 0未告警 1已告警
     */
    public void setAlarmStatus(Integer alarmStatus) {
        this.alarmStatus = alarmStatus;
    }

    /**
     * 0未告警 1已告警
     */
    public Integer getAlarmStatus() {
        return this.alarmStatus;
    }

}