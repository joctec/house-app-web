package com.guda.mp.entity;

import com.app.core.base.BaseEntity;

/**
 * ClassName: SysResource <br/>
 * Description: 资源权限表 <br/>
 */
public class SysResource extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 
     */
    private Long pid;
    /**
     * 资源名称
     */
    private String name;
    /**
     * url地址
     */
    private String url;
    /**
     * 资源编码
     */
    private String code;
    /**
     * 序号
     */
    private Integer seq;
    /**
     * 类型 1：菜单 2：按钮
     */
    private Integer type;
    /**
     * 角色描述
     */
    private String descr;
    /**
     * 
     */
    private String icon;
    /**
     * 菜单状态。 1正常， 2禁用， 3开发中
     */
    private Integer menuStatus;

    public SysResource() {
    }

    public SysResource(long id) {
        super.id = id;
    }

    /**
     * 
     */
    public SysResource setPid(Long pid) {
        this.pid = pid;
        return this;
    }

    /**
     * 
     */
    public Long getPid() {
        return this.pid;
    }

    /**
     * 资源名称
     */
    public SysResource setName(String name) {
        this.name = name;
        return this;
    }

    /**
     * 资源名称
     */
    public String getName() {
        return this.name;
    }

    /**
     * url地址
     */
    public SysResource setUrl(String url) {
        this.url = url;
        return this;
    }

    /**
     * url地址
     */
    public String getUrl() {
        return this.url;
    }

    /**
     * 资源编码
     */
    public SysResource setCode(String code) {
        this.code = code;
        return this;
    }

    /**
     * 资源编码
     */
    public String getCode() {
        return this.code;
    }

    /**
     * 序号
     */
    public SysResource setSeq(Integer seq) {
        this.seq = seq;
        return this;
    }

    /**
     * 序号
     */
    public Integer getSeq() {
        return this.seq;
    }

    /**
     * 类型 1：菜单 2：按钮
     */
    public SysResource setType(Integer type) {
        this.type = type;
        return this;
    }

    /**
     * 类型 1：菜单 2：按钮
     */
    public Integer getType() {
        return this.type;
    }

    /**
     * 角色描述
     */
    public SysResource setDescr(String descr) {
        this.descr = descr;
        return this;
    }

    /**
     * 角色描述
     */
    public String getDescr() {
        return this.descr;
    }

    /**
     * 
     */
    public SysResource setIcon(String icon) {
        this.icon = icon;
        return this;
    }

    /**
     * 
     */
    public String getIcon() {
        return this.icon;
    }

    /**
     * 菜单状态。 1正常， 2禁用， 3开发中
     */
    public SysResource setMenuStatus(Integer menuStatus) {
        this.menuStatus = menuStatus;
        return this;
    }

    /**
     * 菜单状态。 1正常， 2禁用， 3开发中
     */
    public Integer getMenuStatus() {
        return this.menuStatus;
    }

}