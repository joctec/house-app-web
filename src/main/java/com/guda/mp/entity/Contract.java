package com.guda.mp.entity;

import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.app.core.base.BaseEntity;

/** 
* ClassName: Contract <br/>
* Description: 合同 <br/>
*/ 
public class Contract extends BaseEntity{
	private static final long serialVersionUID = 1L;

	/**
	 * 所属公司/组织id
	 */
    private Long orgId;
	/**
	 * 合同名称
	 */
    private String name;
	/**
	 * 合同状态
	 */
    private Integer contractStatus;
	/**
	 * 合同日期
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date contractDate;
	/**
	 * 合同结束时间
	 */
	@DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endDate;
	/**
	 * 文件
	 */
    private String files;

	public Contract() {
    }

    public Contract(long id) {
        super.id = id;
    }

	/**
	 * 所属公司/组织id
	 */
	public Contract setOrgId(Long orgId){
		this.orgId = orgId;
		return this;
	}
	
	/**
	 * 所属公司/组织id
	 */
	public Long getOrgId(){
		return this.orgId;
	}
	
	/**
	 * 合同名称
	 */
	public Contract setName(String name){
		this.name = name;
		return this;
	}
	
	/**
	 * 合同名称
	 */
	public String getName(){
		return this.name;
	}
	
	/**
	 * 合同状态
	 */
	public Contract setContractStatus(Integer contractStatus){
		this.contractStatus = contractStatus;
		return this;
	}
	
	/**
	 * 合同状态
	 */
	public Integer getContractStatus(){
		return this.contractStatus;
	}
	
	/**
	 * 合同日期
	 */
	public Contract setContractDate(Date contractDate){
		this.contractDate = contractDate;
		return this;
	}
	
	/**
	 * 合同日期
	 */
	public Date getContractDate(){
		return this.contractDate;
	}
	
	/**
	 * 合同结束时间
	 */
	public Contract setEndDate(Date endDate){
		this.endDate = endDate;
		return this;
	}
	
	/**
	 * 合同结束时间
	 */
	public Date getEndDate(){
		return this.endDate;
	}
	
	/**
	 * 文件
	 */
	public Contract setFiles(String files){
		this.files = files;
		return this;
	}
	
	/**
	 * 文件
	 */
	public String getFiles(){
		return this.files;
	}
	
}