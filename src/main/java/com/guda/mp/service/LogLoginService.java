package com.guda.mp.service;

import java.util.Map;

import com.app.core.base.BaseService;
import com.app.core.base.PageQuery;
import com.github.pagehelper.Page;
import com.guda.mp.entity.LogLogin;
import com.guda.mp.vo.LogLoginVo;

public interface LogLoginService extends BaseService<LogLogin> {
    public Page<Map> selectOrder(PageQuery pageQuery, LogLoginVo vo);
    /**
     * 
     * loginTimesByDay:(获取某个用户今日的登陆次数). <br/>
     *
     * @author liuliang
     * @param userType
     * @param userId
     * @return
     * @since JDK 1.6
     */
    public Long loginTimesByDay(Integer userType,Long userId);
}
