package com.guda.mp.service;

import com.guda.mp.entity.Person;
import com.guda.mp.vo.PersonVo;
import com.app.core.base.BaseService;
import com.github.pagehelper.Page;
import com.app.core.base.PageQuery;

public interface PersonService extends BaseService<PersonVo> {
    /*
     * 分页查询
     */
   	Page<PersonVo> selectPage(PageQuery pageParams, PersonVo bean);
   	
   	/*
     * 返回excel下载文件全路径
     */
   	String listExport(PersonVo vo);
}
