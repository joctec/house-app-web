package com.guda.mp.service;

import java.util.Map;

public interface CommonService {

    /**
     * 获取七牛配置
     * 
     * @return
     * @author zhangxf
     * @created 2017年4月5日 下午4:15:07
     */
    Map<String, String> getQiniuUpToken();

}
