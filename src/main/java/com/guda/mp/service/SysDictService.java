package com.guda.mp.service;

import com.guda.mp.entity.SysDict;
import com.guda.mp.vo.SysDictVo;
import com.app.core.base.BaseService;
import com.github.pagehelper.Page;
import com.app.core.base.PageQuery;

public interface SysDictService extends BaseService<SysDictVo> {
    /*
     * 分页查询
     */
   	Page<SysDictVo> selectPage(PageQuery pageParams, SysDictVo bean);
   	
   	/*
     * 返回excel下载文件全路径
     */
   	String listExport(SysDictVo vo);
}
