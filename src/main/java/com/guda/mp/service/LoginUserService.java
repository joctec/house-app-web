package com.guda.mp.service;

import java.util.Map;

import com.app.core.base.BaseService;
import com.app.core.base.PageQuery;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.app.core.session.UserSession;
import com.github.pagehelper.Page;
import com.guda.mp.entity.LoginUser;
import com.guda.mp.vo.LoginUserVo;

public interface LoginUserService extends BaseService<LoginUser> {

    Result<UserSession> login(String loginName, String password, String code);

    /**
     * 分页查询所有用户信息
     */
    public Page<LoginUserVo> selectByPage(PageQuery pageQuery, LoginUserVo bean);

    /**
     * 新增用户
     * 
     * @param bean
     * @return
     */
    public Result<NoData> addSave(LoginUserVo bean);

    /**
     * 更新用户信息
     * 
     * @param bean
     * @return
     */
    public Result<NoData> updateSave(LoginUserVo bean);

    /**
     * 批量删除
     * 
     * @param bean
     * @return
     */
    public Result<NoData> deletes(LoginUserVo bean);

    /**
     * 通过用户ID查询该用户的拥有哪些角色
     * 
     * @param id
     * @return
     */
    public Map<String, Object> selectRoleByUserId(Long id);

    /**
     * 修改密码
     * 
     * @param oldPassWord
     * @param newPassWord
     * @param confirmPassWord
     * @return
     */
    public Result<NoData> updatePassWord(String oldPassWord, String newPassWord);

    /**
     * 获取用户角色,逗号分隔
     * 
     * @param modelMap
     * @return
     */
    String getUserRoles(Long loginUserId);

    /**
     * 从数据库中加载用户的权限
     * 
     * @author zhangxf
     * @created 2017年4月12日 下午2:56:28
     */
    void loadUserPermission(UserSession userSession);

}
