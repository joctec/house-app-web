package com.guda.mp.service.impl;

import java.util.Date;
import java.util.List;

import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.common.Result;
import com.app.util.DateUtil;
import com.guda.mp.dao.MtargetDeviceDao;
import com.guda.mp.dao.SensorDataDao;
import com.guda.mp.dao.SensorSlantDayDao;
import com.guda.mp.service.DataMonitorService;
import com.guda.mp.vo.ChartVo;
import com.guda.mp.vo.MtargetDeviceVo;
import com.guda.mp.vo.SensorDataVo;
import com.guda.mp.vo.SensorSlantDayVo;

@Service
@Transactional
public class DataMonitorServiceImpl implements DataMonitorService {

    @Autowired
    private SensorDataDao sensorDataDao;
    @Autowired
    private SensorSlantDayDao sensorSlantDayDao;
    @Autowired
    private MtargetDeviceDao mtargetDeviceDao;

    @Override
    public ChartVo slantChartData(SensorDataVo vo) {
        SensorDataVo query = new SensorDataVo();
        query.setDeviceId(vo.getDeviceId());
        query.setDeviceVender(vo.getDeviceVender());
        query.setDeviceSn(vo.getDeviceSn());
        query.setStartTime(DateUtil.getDayStartNullOfNow(vo.getStartTime()));
        query.setEndTime(DateUtil.getDayEndNullOfNow(vo.getEndTime()));

        // x
        query.setFunType(11);
        List<SensorDataVo> xList = sensorDataDao.selectSlantChartData(query);
        // y
        query.setFunType(12);
        List<SensorDataVo> yList = sensorDataDao.selectSlantChartData(query);

        ChartVo chartVo = new ChartVo();
        chartVo.setxData(xList);
        chartVo.setyData(yList);

        return chartVo;
    }

    /** 倾斜率日速率图 */
    @Override
    public ChartVo slantDayChartData(SensorSlantDayVo vo) {
        SensorSlantDayVo query = new SensorSlantDayVo();
        query.setDeviceId(vo.getDeviceId());

        if (vo.getStartTime() != null && vo.getEndTime() == null) {
            query.setStartTime(vo.getStartTime());
            query.setEndTime(new Date());
        } else if (vo.getStartTime() == null && vo.getEndTime() == null) {
            query.setEndTime(new Date());
            query.setStartTime(new DateTime(query.getEndTime()).minusDays(30).toDate());
        } else {
            query.setStartTime(DateUtil.getDayStartNullOfNow(vo.getStartTime()));
            query.setEndTime(DateUtil.getDayEndNullOfNow(vo.getEndTime()));
        }

        // X轴数据
        query.setFunType(11);
        List<SensorSlantDayVo> xSlantList = sensorSlantDayDao.selectChartData(query);

        // Y轴数据
        query.setFunType(12);
        List<SensorSlantDayVo> ySlantList = sensorSlantDayDao.selectChartData(query);

        ChartVo chartVo = new ChartVo();
        chartVo.setxData(xSlantList);
        chartVo.setyData(ySlantList);

        return chartVo;
    }

    @Override
    public List<MtargetDeviceVo> getAllDevice(long mtargetId) {
        MtargetDeviceVo query = new MtargetDeviceVo();
        query.setTargetId(mtargetId);
        return mtargetDeviceDao.select(query);
    }

    @Override
    public Result<List<SensorDataVo>> waterLevelChartData(SensorDataVo vo) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Result<List<SensorDataVo>> getSensorChartData(SensorDataVo vo) {
        // TODO Auto-generated method stub
        return null;
    }

}
