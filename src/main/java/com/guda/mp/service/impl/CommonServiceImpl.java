package com.guda.mp.service.impl;

import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.app.core.util.SysConfig;
import com.guda.mp.service.CommonService;
import com.qiniu.util.Auth;

@Service
public class CommonServiceImpl implements CommonService {

    @Override
    public Map<String, String> getQiniuUpToken() {
        Map<String, String> map = new HashMap<String, String>();
        Auth auth = Auth.create(SysConfig.get("qiniu.ACCESS_KEY"), SysConfig.get("qiniu.SECRET_KEY"));
        String bucketname = SysConfig.get("qiniu.bucket");
        String uptoken = auth.uploadToken(bucketname);
        map.put("uptoken", uptoken);
        return map;
    }
}
