package com.guda.mp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;
import com.app.util.BizUtil;
import com.app.util.NumUtil;
import com.github.pagehelper.Page;
import com.guda.mp.dao.TaskCenterSlantDao;
import com.guda.mp.service.TaskCenterSlantService;
import com.guda.mp.vo.TaskCenterSlantVo;

@Service
@Transactional
public class TaskCenterSlantServiceImpl extends BaseServiceImpl<TaskCenterSlantVo> implements TaskCenterSlantService {

    @Autowired
    private TaskCenterSlantDao taskCenterDisplacementDao;

    @Override
    protected BaseDao<TaskCenterSlantVo> getDao() {
        return this.taskCenterDisplacementDao;
    }

    /*
     * 分页查询
     */
    @Override
    public Page<TaskCenterSlantVo> selectPage(PageQuery pageParams, TaskCenterSlantVo bean) {
        return selectBasePage(pageParams, bean);
    }

    @Override
    public String listExport(TaskCenterSlantVo vo) {
        List<List<String>> dataList = new ArrayList<List<String>>();

        /* 分页查询数据 */
        PageQuery pageParams = new PageQuery();
        pageParams.setQueryAll(true);
        // 每次查询2000条
        pageParams.setPageSize(2000);
        pageParams.setPageNum(1);
        Page<TaskCenterSlantVo> page = selectBasePage(pageParams, vo);
        for (TaskCenterSlantVo bean : page) {
            List<String> rowList = new ArrayList<String>();
            rowList.add(bean.getId().toString());
            rowList.add(NumUtil.numToStr(bean.getTaskId()));
            rowList.add(NumUtil.numToStr(bean.getTaskCenterCheckItemId()));
            rowList.add(NumUtil.numToStr(bean.getHouseHeight()));
            rowList.add(NumUtil.numToStr(bean.getXDisplacement()));
            rowList.add(NumUtil.numToStr(bean.getXSlope()));
            rowList.add(NumUtil.numToStr(bean.getXBevelAngle()));
            rowList.add(NumUtil.numToStr(bean.getYDisplacement()));
            rowList.add(NumUtil.numToStr(bean.getYSlope()));
            rowList.add(NumUtil.numToStr(bean.getYBevelAngle()));
            dataList.add(rowList);
        }

        // 要下载的文件名称
        String downFilename = BizUtil.getDownloadFileNameExcel("", RequestContext.getLoginUserId());
        // 生成文件存在项目临时目录
        String tmpDirPath = SysConfig.get("temp.dir");

        // 导出excel
        String downloadFile = tmpDirPath + "/" + downFilename;
        com.app.util.ExcelUtil excelUtil = new com.app.util.ExcelUtil(downloadFile);
        excelUtil.addCol("ID", 30);
        excelUtil.addCol("任务id", 30);
        excelUtil.addCol("任务检查项id", 30);
        excelUtil.addCol("房屋高度", 30);
        excelUtil.addCol("x轴位移量", 30);
        excelUtil.addCol("x轴斜率", 30);
        excelUtil.addCol("x轴斜角", 30);
        excelUtil.addCol("y轴位移量", 30);
        excelUtil.addCol("y轴斜率", 30);
        excelUtil.addCol("y轴斜角", 30);
        excelUtil.setDataList(dataList);
        try {
            excelUtil.wirte();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return downloadFile;
    }
}
