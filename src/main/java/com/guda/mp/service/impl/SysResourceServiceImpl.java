package com.guda.mp.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.github.pagehelper.Page;
import com.guda.mp.dao.SysResourceDao;
import com.guda.mp.service.SysResourceService;
import com.guda.mp.vo.SysResourceVo;

@Service
@Transactional
public class SysResourceServiceImpl extends BaseServiceImpl<SysResourceVo> implements SysResourceService {

    @Autowired
    private SysResourceDao dao;

    @Override
    protected BaseDao<SysResourceVo> getDao() {
        return this.dao;
    }

    /**
     * 查询所有的资源
     * 
     * @param bean
     * @return
     */
    @Override
    public Result<List<SysResourceVo>> selectAllResource(SysResourceVo bean) {
        return Result.success(dao.select(bean));
    }

    /**
     * 
     * 根据PID查询直接下级数据
     * 
     * @param bean
     * @return
     */
    @Override
    public Page<SysResourceVo> selectByPid(PageQuery pageQuery, SysResourceVo bean) {
        super.setPageQuery(pageQuery);
        return (Page<SysResourceVo>) dao.selectByPid(bean);
    }

    /**
     * 通过角色ID查询资源数据
     * 
     * @param id
     * @return
     */
    public Map<String, Object> selectResourceByRoleId(Long id) {
        Map<String, Object> map = new HashMap<String, Object>();
        String names = "";
        String ids = "";
        List<SysResourceVo> dataList = dao.selectResourceByRoleId(id);
        for (int i = 0; i < dataList.size(); i++) {
            names = names + dataList.get(i).getName() + ",";
            ids = ids + dataList.get(i).getId() + ",";
        }
        if (ids.length() > 1) {
            map.put("resourceNames", names.substring(0, names.length() - 1));
            map.put("ids", ids.substring(0, ids.length() - 1));
        }
        return map;
    }

    /**
     * 查询菜单分组数据
     * 
     * @return
     */
    public List<SysResourceVo> selectMenuList(Long userId) {
        List<SysResourceVo> dataList = new ArrayList<SysResourceVo>();
        if (userId != null) {
            dataList = dao.selectMenuListByUserId(userId, 0L);
            for (int i = 0; i < dataList.size(); i++) {
                dataList.get(i).setChildrens(dao.selectMenuListByUserId(userId, dataList.get(i).getId()));
            }
        } else {
            dataList = dao.selectResourceByPid(0L);
            for (int i = 0; i < dataList.size(); i++) {
                dataList.get(i).setChildrens(dao.selectResourceByPid(dataList.get(i).getId()));
            }
        }
        return dataList;
    }

    /**
     * 查询所有权限资源数据
     * 
     * @return
     */
    @Override
    public List<SysResourceVo> selectUserAllResource(Long userId) {
        return dao.selectUserAllResource(userId);
    }

    @Override
    public Result<NoData> add(SysResourceVo bean) {
        try {
            if (StringUtils.isBlank(bean.getCode())) {
                bean.setCode(UUID.randomUUID().toString());
            }
            bean.setCode(bean.getCode().trim());
            dao.add(bean);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                return Result.error("编码已存在: " + bean.getCode());
            } else {
                throw e;
            }
        }
        return Result.success();
    }

    @Override
    public Result<NoData> update(SysResourceVo bean) {
        try {
            if (StringUtils.isBlank(bean.getCode())) {
                bean.setCode(UUID.randomUUID().toString());
            }
            bean.setCode(bean.getCode().trim());
            dao.updateById(bean);
        } catch (Exception e) {
            if (e instanceof DuplicateKeyException) {
                return Result.error("编码已存在: " + bean.getCode());
            } else {
                throw e;
            }
        }
        return Result.success();
    }

}
