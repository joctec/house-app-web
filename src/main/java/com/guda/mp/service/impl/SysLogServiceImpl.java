package com.guda.mp.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.guda.mp.dao.SysLogDao;
import com.guda.mp.entity.SysLog;
import com.guda.mp.service.SysLogService;

@Service
@Transactional
public class SysLogServiceImpl extends BaseServiceImpl<SysLog> implements SysLogService {
	@Autowired
	private SysLogDao sysLogDao;

	@Override
	protected BaseDao<SysLog> getDao() {
		return this.sysLogDao;
	}
}
