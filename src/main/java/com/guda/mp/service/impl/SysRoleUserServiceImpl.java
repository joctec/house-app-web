package com.guda.mp.service.impl;

import com.guda.mp.entity.SysRoleUser;
import com.guda.mp.service.SysRoleUserService;
import com.guda.mp.dao.SysRoleUserDao;
import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SysRoleUserServiceImpl extends BaseServiceImpl<SysRoleUser> implements SysRoleUserService {

	@Autowired
	private SysRoleUserDao dao;

    @Override
    protected BaseDao<SysRoleUser> getDao() {
        return this.dao;
    }
	
}
