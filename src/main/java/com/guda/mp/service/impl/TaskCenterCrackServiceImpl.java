package com.guda.mp.service.impl;

import com.guda.mp.entity.TaskCenterCrack;
import com.guda.mp.vo.TaskCenterCrackVo;
import com.guda.mp.service.TaskCenterCrackService;
import com.guda.mp.dao.TaskCenterCrackDao;
import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.github.pagehelper.Page;
import com.app.util.NumUtil;
import com.app.util.BizUtil;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;
import com.app.util.DateUtil;

import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class TaskCenterCrackServiceImpl extends BaseServiceImpl<TaskCenterCrackVo> implements TaskCenterCrackService {

	@Autowired
	private TaskCenterCrackDao taskCenterCrackDao;

    @Override
    protected BaseDao<TaskCenterCrackVo> getDao() {
        return this.taskCenterCrackDao;
    }
	
	/*
     * 分页查询
     */
    @Override
    public Page<TaskCenterCrackVo> selectPage(PageQuery pageParams, TaskCenterCrackVo bean) {
        return selectBasePage(pageParams, bean);
    }
    
    @Override
    public String listExport(TaskCenterCrackVo vo) {
        List<List<String>> dataList = new ArrayList<List<String>>();

        /* 分页查询数据 */
        PageQuery pageParams = new PageQuery();
        pageParams.setQueryAll(true);
        // 每次查询2000条
        pageParams.setPageSize(2000);
        pageParams.setPageNum(1);
        Page<TaskCenterCrackVo> page = selectBasePage(pageParams, vo);
        for (TaskCenterCrackVo bean : page) {
            List<String> rowList = new ArrayList<String>();
            rowList.add(bean.getId().toString());
			rowList.add(NumUtil.numToStr(bean.getTaskId()));
			rowList.add(NumUtil.numToStr(bean.getTaskCenterCheckItemId()));
			rowList.add(NumUtil.numToStr(bean.getLength()));
			rowList.add(NumUtil.numToStr(bean.getWidth()));
			rowList.add(NumUtil.numToStr(bean.getRoom()));
			rowList.add(NumUtil.numToStr(bean.getDirection()));
			rowList.add(NumUtil.numToStr(bean.getFloorNum()));
			rowList.add(NumUtil.numToStr(bean.getPlace()));
            dataList.add(rowList);
        }

        // 要下载的文件名称
        String downFilename = BizUtil.getDownloadFileNameExcel("", RequestContext.getLoginUserId());
        // 生成文件存在项目临时目录
        String tmpDirPath = SysConfig.get("temp.dir");

        // 导出excel
        String downloadFile = tmpDirPath + "/" + downFilename;
        com.app.util.ExcelUtil excelUtil = new com.app.util.ExcelUtil(downloadFile);
        excelUtil.addCol("ID", 30);
		excelUtil.addCol("任务id", 30);	
		excelUtil.addCol("任务检查项id", 30);	
		excelUtil.addCol("裂缝长度mm", 30);	
		excelUtil.addCol("裂缝宽度mm", 30);	
		excelUtil.addCol("所在房间", 30);	
		excelUtil.addCol("方位", 30);	
		excelUtil.addCol("楼层", 30);	
		excelUtil.addCol("裂缝所在位置", 30);	
        excelUtil.setDataList(dataList);
        try {
            excelUtil.wirte();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return downloadFile;
    }
}
