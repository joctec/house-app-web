package com.guda.mp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;
import com.app.util.BizUtil;
import com.app.util.NumUtil;
import com.github.pagehelper.Page;
import com.guda.mp.dao.TaskCenterSubsidenceDao;
import com.guda.mp.service.TaskCenterSubsidenceService;
import com.guda.mp.vo.TaskCenterSubsidenceVo;

@Service
@Transactional
public class TaskCenterSubsidenceServiceImpl extends BaseServiceImpl<TaskCenterSubsidenceVo>
    implements TaskCenterSubsidenceService {

    @Autowired
    private TaskCenterSubsidenceDao taskCenterSubsidenceDao;

    @Override
    protected BaseDao<TaskCenterSubsidenceVo> getDao() {
        return this.taskCenterSubsidenceDao;
    }

    /*
     * 分页查询
     */
    @Override
    public Page<TaskCenterSubsidenceVo> selectPage(PageQuery pageParams, TaskCenterSubsidenceVo bean) {
        return selectBasePage(pageParams, bean);
    }

    @Override
    public String listExport(TaskCenterSubsidenceVo vo) {
        List<List<String>> dataList = new ArrayList<List<String>>();

        /* 分页查询数据 */
        PageQuery pageParams = new PageQuery();
        pageParams.setQueryAll(true);
        // 每次查询2000条
        pageParams.setPageSize(2000);
        pageParams.setPageNum(1);
        Page<TaskCenterSubsidenceVo> page = selectBasePage(pageParams, vo);
        for (TaskCenterSubsidenceVo bean : page) {
            List<String> rowList = new ArrayList<String>();
            rowList.add(bean.getId().toString());
            rowList.add(NumUtil.numToStr(bean.getTaskId()));
            rowList.add(NumUtil.numToStr(bean.getTaskCenterCheckItemId()));
            rowList.add(NumUtil.numToStr(bean.getName()));
            dataList.add(rowList);
        }

        // 要下载的文件名称
        String downFilename = BizUtil.getDownloadFileNameExcel("", RequestContext.getLoginUserId());
        // 生成文件存在项目临时目录
        String tmpDirPath = SysConfig.get("temp.dir");

        // 导出excel
        String downloadFile = tmpDirPath + "/" + downFilename;
        com.app.util.ExcelUtil excelUtil = new com.app.util.ExcelUtil(downloadFile);
        excelUtil.addCol("ID", 30);
        excelUtil.addCol("任务id", 30);
        excelUtil.addCol("任务检查项id", 30);
        excelUtil.addCol("监测点名称", 30);
        excelUtil.setDataList(dataList);
        try {
            excelUtil.wirte();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return downloadFile;
    }
}
