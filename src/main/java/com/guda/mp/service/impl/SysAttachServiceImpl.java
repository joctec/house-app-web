package com.guda.mp.service.impl;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.common.AppCons;
import com.guda.mp.dao.SysAttachDao;
import com.guda.mp.service.SysAttachService;
import com.guda.mp.vo.SysAttachVo;

@Service
@Transactional
public class SysAttachServiceImpl extends BaseServiceImpl<SysAttachVo> implements SysAttachService {

    @Autowired
    private SysAttachDao sysAttachDao;

    @Override
    protected BaseDao<SysAttachVo> getDao() {
        return this.sysAttachDao;
    }

    @Override
    public void delByRefId(Long refId) {
        if (refId != null) {
            sysAttachDao.deleteByRefId(refId);
        }
    }

    @Override
    public void delByRefIdAndCode(Long refId, String refCode) {
        if (refId != null) {
            sysAttachDao.deleteByRefIdAndCode(refId, refCode);
        }
    }

    @Override
    public List<SysAttachVo> getByRefId(Long refId) {
        List<SysAttachVo> list = sysAttachDao.selectByRefId(refId);
        setFilePath(list);
        return list;
    }

    @Override
    public List<SysAttachVo> getByRefIdAndCode(Long refId, String refCode) {
        List<SysAttachVo> list = sysAttachDao.selectByRefIdAndCode(refId, refCode);
        setFilePath(list);
        return list;
    }

    private void setFilePath(List<SysAttachVo> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        for (SysAttachVo sysAttach : list) {
            if (StringUtils.isNoneBlank(sysAttach.getUrl())) {
                sysAttach.setRelativeUrl(sysAttach.getUrl());
                sysAttach.setUrl(AppCons.FILEUPLOAD_SERVER + sysAttach.getUrl());
            }
        }
    }

    @Override
    public void updateAttach(Long id, String files) {
        delByRefId(id);
        List<JSONObject> fileArray = JSON.parseArray(files, JSONObject.class);
        for (JSONObject jsonObject : fileArray) {
            SysAttachVo sysAttach = new SysAttachVo();
            sysAttach.setRefId(id);
            sysAttach.setUrl(jsonObject.getString("relativeUrl"));
            sysAttach.setName(jsonObject.getString("name"));

            sysAttachDao.add(sysAttach);
        }
    }

    @Override
    public void updateAttach(Long id, String files, String code) {
        delByRefIdAndCode(id, code);
        List<JSONObject> fileArray = JSON.parseArray(files, JSONObject.class);
        for (JSONObject jsonObject : fileArray) {
            SysAttachVo sysAttach = new SysAttachVo();
            sysAttach.setRefId(id);
            sysAttach.setRefCode(code);
            sysAttach.setUrl(jsonObject.getString("relativeUrl"));
            sysAttach.setName(jsonObject.getString("name"));

            sysAttachDao.add(sysAttach);
        }
    }
}
