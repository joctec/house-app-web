package com.guda.mp.service.impl;

import com.guda.mp.entity.SysRoleResource;
import com.guda.mp.service.SysRoleResourceService;
import com.guda.mp.dao.SysRoleResourceDao;
import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class SysRoleResourceServiceImpl extends BaseServiceImpl<SysRoleResource> implements SysRoleResourceService {

	@Autowired
	private SysRoleResourceDao dao;

    @Override
    protected BaseDao<SysRoleResource> getDao() {
        return this.dao;
    }

	@Override
	public boolean deleteByResourceId(Long id) {
		return dao.deleteByResourceId(id);
	}

	@Override
	public boolean deleteByRoleId(Long id) {
		// TODO Auto-generated method stub
		return dao.deleteByRoleId(id);
	}
	
}
