package com.guda.mp.service.impl;

import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.app.util.DateUtil;
import com.app.util.PageHelperUtil;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.guda.mp.dao.LogLoginDao;
import com.guda.mp.entity.LogLogin;
import com.guda.mp.service.LogLoginService;
import com.guda.mp.vo.LogLoginVo;

@Service
@Transactional
public class LogLoginServiceImpl extends BaseServiceImpl<LogLogin> implements LogLoginService {

    @Autowired
    private LogLoginDao dao;

    @Override
    protected BaseDao<LogLogin> getDao() {
        return this.dao;
    }

    @SuppressWarnings("rawtypes")
    public Page<Map> selectOrder(PageQuery pageQuery, LogLoginVo vo) {
        PageHelper.startPage(PageHelperUtil.getPageNum(pageQuery), PageHelperUtil.getPageSize(pageQuery));
        Page<Map> page = (Page<Map>) dao.selectOrder(vo);
        return page;
    }

    public Long loginTimesByDay(Integer userType, Long userId) {
        String day = DateUtil.fomart(new Date(), "yyyyMMdd");
        return dao.loginTimesByDay(userType, userId, day);
    }
}
