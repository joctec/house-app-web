package com.guda.mp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;
import com.app.util.BizUtil;
import com.app.util.NumUtil;
import com.github.pagehelper.Page;
import com.guda.mp.dao.SysDictDao;
import com.guda.mp.service.SysDictService;
import com.guda.mp.vo.SysDictVo;

@Service
@Transactional
public class SysDictServiceImpl extends BaseServiceImpl<SysDictVo> implements SysDictService {

	@Autowired
	private SysDictDao sysDictDao;

    @Override
    protected BaseDao<SysDictVo> getDao() {
        return this.sysDictDao;
    }
	
	/*
     * 分页查询
     */
    @Override
    public Page<SysDictVo> selectPage(PageQuery pageParams, SysDictVo bean) {
        return selectBasePage(pageParams, bean);
    }
    
    @Override
    public String listExport(SysDictVo vo) {
        List<List<String>> dataList = new ArrayList<List<String>>();

        /* 分页查询数据 */
        PageQuery pageParams = new PageQuery();
        pageParams.setQueryAll(true);
        // 每次查询2000条
        pageParams.setPageSize(2000);
        pageParams.setPageNum(1);
        Page<SysDictVo> page = selectBasePage(pageParams, vo);
        for (SysDictVo bean : page) {
            List<String> rowList = new ArrayList<String>();
            rowList.add(bean.getId().toString());
			rowList.add(NumUtil.numToStr(bean.getPid()));
			rowList.add(bean.getName());
			rowList.add(bean.getCode());
			rowList.add(NumUtil.numToStr(bean.getSeq()));
			rowList.add(NumUtil.numToStr(bean.getType()));
			rowList.add(bean.getDescr());
			rowList.add(NumUtil.numToStr(bean.getDelFlag()));
            dataList.add(rowList);
        }

        // 要下载的文件名称
        String downFilename = BizUtil.getDownloadFileNameExcel("", RequestContext.getLoginUserId());
        // 生成文件存在项目临时目录
        String tmpDirPath = SysConfig.get("temp.dir");

        // 导出excel
        String downloadFile = tmpDirPath + "/" + downFilename;
        com.app.util.ExcelUtil excelUtil = new com.app.util.ExcelUtil(downloadFile);
        excelUtil.addCol("ID", 30);
		excelUtil.addCol("", 30);	
		excelUtil.addCol("名称", 30);	
		excelUtil.addCol("", 30);	
		excelUtil.addCol("顺序", 30);	
		excelUtil.addCol("类型", 30);	
		excelUtil.addCol("描述", 30);	
		excelUtil.addCol("删除标志", 30);	
        excelUtil.setDataList(dataList);
        try {
            excelUtil.wirte();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return downloadFile;
    }
}
