package com.guda.mp.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.app.core.common.AppBizCache;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;
import com.app.util.BizUtil;
import com.app.util.DateUtil;
import com.github.pagehelper.Page;
import com.google.common.collect.Lists;
import com.guda.mp.dao.CheckProblemDao;
import com.guda.mp.dao.MonitorTargetCheckPackageDao;
import com.guda.mp.dao.MonitorTargetDao;
import com.guda.mp.dao.TaskAssignDao;
import com.guda.mp.dao.TaskCenterCheckItemDao;
import com.guda.mp.dao.TaskCenterCrackDao;
import com.guda.mp.dao.TaskCenterDao;
import com.guda.mp.dao.TaskCenterProblemDao;
import com.guda.mp.dao.TaskCenterSlantDao;
import com.guda.mp.dao.TaskCenterSubsidenceDao;
import com.guda.mp.service.TaskCenterService;
import com.guda.mp.vo.CheckPackageVo;
import com.guda.mp.vo.CheckProblemVo;
import com.guda.mp.vo.MonitorTargetVo;
import com.guda.mp.vo.TaskAssignVo;
import com.guda.mp.vo.TaskCenterCheckItemVo;
import com.guda.mp.vo.TaskCenterCrackVo;
import com.guda.mp.vo.TaskCenterProblemVo;
import com.guda.mp.vo.TaskCenterSlantVo;
import com.guda.mp.vo.TaskCenterSubsidenceVo;
import com.guda.mp.vo.TaskCenterVo;
import com.qiniu.util.Auth;

@Service
@Transactional
public class TaskCenterServiceImpl extends BaseServiceImpl<TaskCenterVo> implements TaskCenterService {

    @Autowired
    private TaskCenterDao taskCenterDao;
    @Autowired
    private TaskCenterCheckItemDao taskCenterCheckItemDao;
    @Autowired
    private MonitorTargetDao monitorTargetDao;
    @Autowired
    private MonitorTargetCheckPackageDao monitorTargetCheckPackageDao;
    @Autowired
    private TaskCenterProblemDao taskCenterProblemDao;
    @Autowired
    private CheckProblemDao checkProblemDao;
    @Autowired
    private TaskAssignDao taskAssignDao;
    @Autowired
    private TaskCenterSlantDao taskCenterSlantDao;
    @Autowired
    private TaskCenterCrackDao taskCenterCrackDao;
    @Autowired
    private TaskCenterSubsidenceDao taskCenterSubsidenceDao;

    @Override
    protected BaseDao<TaskCenterVo> getDao() {
        return this.taskCenterDao;
    }

    @Override
    public String getQiniuUpToken() {
        Auth auth = Auth.create(SysConfig.get("qiniu.ACCESS_KEY"), SysConfig.get("qiniu.SECRET_KEY"));
        String bucketname = SysConfig.get("qiniu.bucket");
        String uptoken = auth.uploadToken(bucketname);
        return uptoken;
    }

    @Override
    public Page<TaskCenterVo> selectTodayTaskPage(PageQuery pageParams, TaskCenterVo bean) {
        super.setPageQuery(pageParams);
        Long loginUserId = RequestContext.getLoginUserId();
        bean.setUserId(loginUserId);

        Page<TaskCenterVo> page = (Page<TaskCenterVo>) taskCenterDao.todayTaskPage(bean);
        /*
         * 查询检查包数量
         */
        // List<Long> monitorTargetIdList = new ArrayList<>();
        // for (int i = 0; i < page.size(); i++) {
        // TaskCenterVo vo = page.get(i);
        // monitorTargetIdList.add(vo.getMonitorTargetId());
        // }

        // Map<Integer, MonitorTargetCheckPackageVo> countMap =
        // monitorTargetCheckPackageDao.countMonitorTargetCheckPackage(monitorTargetIdList);

        /* 查询历史问题 */
        setTaskProblemLevelCount(page);

        /* 判断延期天数 */
        for (TaskCenterVo taskCenterVo : page) {
            int days = DateUtil.getDaysBetween(taskCenterVo.getTaskDate(), new Date());
            if (days > 0) {
                taskCenterVo.setDelayDay(days);
            }
        }

        return page;
    }

    @Override
    public Page<TaskAssignVo> selectOwnerTask(PageQuery pageParams, TaskAssignVo bean) {
        super.setPageQuery(pageParams);

        Long loginUserId = RequestContext.getLoginUserId();
        // 设置查询用户id
        bean.setUserId(loginUserId);
        Page<TaskAssignVo> page = (Page<TaskAssignVo>) taskAssignDao.select(bean);

        /* 查询历史问题 */
        setTaskAssignProblemLevelCountByMonitorTargetId(page);

        return page;
    }

    @Override
    public Page<TaskCenterVo> selectHistoryTask(PageQuery pageParams, TaskCenterVo query) {
        super.setPageQuery(pageParams);

        Long loginUserId = RequestContext.getLoginUserId();
        query.setUserId(loginUserId);
        Page<TaskCenterVo> page = (Page<TaskCenterVo>) taskCenterDao.selectHistoryTask(query);

        /* 查询历史问题 */
        setTaskProblemLevelCount(page);

        return page;
    }

    @Override
    public Page<TaskCenterVo> selectCompletedTask(PageQuery pageParams, TaskCenterVo query) {
        super.setPageQuery(pageParams);

        Long loginUserId = RequestContext.getLoginUserId();
        query.setUserId(loginUserId);
        query.setCompleteStatus(2);
        Page<TaskCenterVo> page = (Page<TaskCenterVo>) taskCenterDao.selectHistoryTask(query);

        /* 查询历史问题 */
        setTaskProblemLevelCount(page);

        return page;
    }

    @Override
    public Page<TaskCenterVo> selectFocusTask(PageQuery pageParams, TaskCenterVo query) {
        return null;
    }

    /** 设置多个task的问题统计数量 */
    @Override
    public void setTaskProblemLevelCount(List<TaskCenterVo> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }
        List<Long> idList = BizUtil.getBeanId(list);
        List<TaskCenterProblemVo> probList = taskCenterProblemDao.selectCountByIdList(idList);
        for (TaskCenterVo tc : list) {
            for (TaskCenterProblemVo prob : probList) {
                if (tc.getId() == prob.getTaskId()) {
                    Map<String, Long> problemCountMap = tc.getProblemCountMap();
                    if (problemCountMap == null) {
                        problemCountMap = new LinkedHashMap<>();
                        tc.setProblemCountMap(problemCountMap);
                    }
                    problemCountMap.put(prob.getCheckProblemLevel().toString(), prob.getCountNum());
                }
            }
        }
    }

    /** 设置单个task的问题统计数量 */
    @Override
    public void setTaskProblemLevelCount(TaskCenterVo vo) {
        setTaskProblemLevelCount(Lists.newArrayList(vo));
    }

    /** 设置多个task的问题统计数量 */
    private void setTaskProblemLevelCountByMonitorTargetId(List<TaskCenterVo> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<Long> idList = new ArrayList<>();
        for (TaskCenterVo vo : list) {
            idList.add(vo.getMonitorTargetId());
        }

        List<TaskCenterProblemVo> probList = taskCenterProblemDao.selectCountByMonitorTargetIdList(idList);
        for (TaskCenterVo tc : list) {
            for (TaskCenterProblemVo prob : probList) {
                if (tc.getMonitorTargetId() == prob.getMonitorTargetId()) {
                    Map<String, Long> problemCountMap = tc.getProblemCountMap();
                    if (problemCountMap == null) {
                        problemCountMap = new LinkedHashMap<>();
                        tc.setProblemCountMap(problemCountMap);
                    }
                    problemCountMap.put(prob.getCheckProblemLevel().toString(), prob.getCountNum());
                }
            }
        }
    }

    /** 设置taskassign的问题统计数量 */
    private void setTaskAssignProblemLevelCountByMonitorTargetId(List<TaskAssignVo> list) {
        if (CollectionUtils.isEmpty(list)) {
            return;
        }

        List<Long> idList = new ArrayList<>();
        for (TaskAssignVo vo : list) {
            idList.add(vo.getMonitorTargetId());
        }

        List<TaskCenterProblemVo> probList = taskCenterProblemDao.selectCountByMonitorTargetIdList(idList);
        for (TaskAssignVo tc : list) {
            for (TaskCenterProblemVo prob : probList) {
                if (tc.getMonitorTargetId() == prob.getMonitorTargetId()) {
                    Map<String, Long> problemCountMap = tc.getProblemCountMap();
                    if (problemCountMap == null) {
                        problemCountMap = new LinkedHashMap<>();
                        tc.setProblemCountMap(problemCountMap);
                    }
                    problemCountMap.put(prob.getCheckProblemLevel().toString(), prob.getCountNum());
                }
            }
        }
    }

    @Override
    public TaskCenterVo taskDeatail(long taskId) {
        TaskCenterVo vo = taskCenterDao.selectBizById(taskId);
        if (vo == null) {
            return null;
        }

        /* 查询检查包 */
        List<CheckPackageVo> list = monitorTargetCheckPackageDao.selectByMonitorTargetId(vo.getMonitorTargetId());
        vo.setCheckPackageList(list);

        /* 判断延期天数 */
        int days = DateUtil.getDaysBetween(vo.getTaskDate(), new Date());
        if (days > 0) {
            vo.setDelayDay(days);
        }

        /* 下次执行时间 nextTaskDate */
        Date nextTaskDate = null;
        // 周
        if (vo.getCycleType() == 2) {
            nextTaskDate = new DateTime(vo.getTaskDate()).plusDays(7).toDate();
        } // 月
        else if (vo.getCycleType() == 3) {
            nextTaskDate = new DateTime(vo.getTaskDate()).plusMonths(1).toDate();
        } // 年
        else if (vo.getCycleType() == 4) {
            nextTaskDate = new DateTime(vo.getTaskDate()).plusYears(1).toDate();
        }

        vo.setNextTaskDate(nextTaskDate);

        /* 查询历史问题 */
        TaskCenterProblemVo query = new TaskCenterProblemVo();
        query.setTaskId(taskId);
        List<TaskCenterProblemVo> problemList = taskCenterProblemDao.select(query);
        vo.setProblemList(problemList);

        /* 判断是否绑定IC卡 */
        MonitorTargetVo monitorTargetVo = monitorTargetDao.selectById(vo.getMonitorTargetId());
        vo.setBindCardFlag(monitorTargetVo.getBindCardFlag());
        vo.setDataUuid(monitorTargetVo.getDataUuid());

        return vo;
    }

    @Override
    public Result<TaskCenterVo> viewCheckList(long taskId) {
        TaskCenterVo vo = taskCenterDao.selectById(taskId);

        /* 查询房屋的检查项 */
        List<TaskCenterCheckItemVo> checkItemList = monitorTargetCheckPackageDao
            .selectAndCheckItemByTargetId(vo.getMonitorTargetId());

        /* 构造检查包和检查项的层级关系 */
        List<CheckPackageVo> checkPackageVoList = buildCheckPackageAndItem(checkItemList);

        Iterator<TaskCenterCheckItemVo> iterator = checkItemList.iterator();
        while (iterator.hasNext()) {
            TaskCenterCheckItemVo taskCenterCheckItemVo = iterator.next();
            taskCenterCheckItemVo.setTaskId(vo.getId());
            taskCenterCheckItemVo.setOrgId(vo.getOrgId());
            taskCenterCheckItemVo.setProblemNum(0);
            taskCenterCheckItemVo.setIsCheck(0);
            if (taskCenterCheckItemVo.getType() == null || taskCenterCheckItemVo.getType() != 2) {
                iterator.remove();
            }
        }

        /* 查询检查项包含的问题 */
        selectAndSetProblem(taskId, checkItemList);

        TaskCenterVo returnVo = new TaskCenterVo();
        returnVo.setCheckPackageList(checkPackageVoList);
        returnVo.setTaskName(vo.getTaskName());

        return Result.success(returnVo);
    }

    @Override
    public Result<TaskCenterVo> taskBegin(long taskId, String icCardData) {
        TaskCenterVo vo = taskCenterDao.selectById(taskId);
        if (vo.getCompleteStatus().intValue() == 1) {
            /* 查询房屋的检查项 */
            TaskCenterCheckItemVo query = new TaskCenterCheckItemVo();
            query.setTaskId(taskId);
            List<TaskCenterCheckItemVo> checkItemList = taskCenterCheckItemDao.select(query);

            /* 查询检查项包含的问题 */
            selectAndSetProblem(taskId, checkItemList);

            /* 构造检查包和检查项的层级关系 */
            List<CheckPackageVo> checkPackageVoList = buildCheckPackageAndItem(checkItemList);

            TaskCenterVo returnVo = new TaskCenterVo();
            returnVo.setCheckPackageList(checkPackageVoList);
            returnVo.setTaskName(vo.getTaskName());

            return Result.success(returnVo);
        }
        if (vo.getCompleteStatus().intValue() == 2) {
            return Result.error("任务已完成");
        }
        if (vo.getCompleteStatus().intValue() != 0) {
            return Result.error("任务状态不正确:" + vo.getCompleteStatus().intValue());
        }

        /* 更新任务状态 */
        TaskCenterVo update = new TaskCenterVo();
        update.setId(taskId);
        update.setCompleteStatus(1);
        update.setActualStartDate(new Date());
        update.setActualEndDate(null);
        update.setNfcSignIn(StringUtils.isBlank(icCardData) ? 0 : 1);
        taskCenterDao.updateById(update);

        /* 查询房屋的检查项 */
        List<TaskCenterCheckItemVo> checkItemList = monitorTargetCheckPackageDao
            .selectAndCheckItemByTargetId(vo.getMonitorTargetId());

        Iterator<TaskCenterCheckItemVo> iterator = checkItemList.iterator();
        while (iterator.hasNext()) {
            TaskCenterCheckItemVo taskCenterCheckItemVo = iterator.next();
            taskCenterCheckItemVo.setTaskId(vo.getId());
            taskCenterCheckItemVo.setOrgId(vo.getOrgId());
            taskCenterCheckItemVo.setProblemNum(0);
            taskCenterCheckItemVo.setIsCheck(0);
            if (taskCenterCheckItemVo.getType() == null || taskCenterCheckItemVo.getType() != 2) {
                iterator.remove();
            }
        }

        /* 保存任务检查项 */
        /* 先删除后插入 */
        taskCenterCheckItemDao.deleteByTaskCenterId(vo.getId());
        if (CollectionUtils.isNotEmpty(checkItemList)) {
            taskCenterCheckItemDao.addList(checkItemList);
        }

        TaskCenterCheckItemVo query = new TaskCenterCheckItemVo();
        query.setTaskId(taskId);
        checkItemList = taskCenterCheckItemDao.select(query);

        /* 查询检查项包含的问题 */
        selectAndSetProblem(taskId, checkItemList);

        /* 构造检查包和检查项的层级关系 */
        List<CheckPackageVo> checkPackageVoList = buildCheckPackageAndItem(checkItemList);

        TaskCenterVo returnVo = new TaskCenterVo();
        returnVo.setCheckPackageList(checkPackageVoList);
        returnVo.setTaskName(vo.getTaskName());

        return Result.success(returnVo);
    }

    @Override
    public Result<String> taskComplete(Integer type, long taskId, String checkItemStrs) {
        TaskCenterVo vo = taskCenterDao.selectById(taskId);
        if (vo.getCompleteStatus().intValue() == 0) {
            return Result.error("任务未开始");
        }
        if (vo.getCompleteStatus() == 2) {
            return Result.error("任务已完成");
        }
        if (vo.getCompleteStatus().intValue() != 1) {
            return Result.error("任务状态不正确:" + vo.getCompleteStatus().intValue());
        }

        TaskCenterVo update = new TaskCenterVo();
        update.setId(taskId);
        int status = (type == null || type == 2 ? 2 : 1);
        update.setCompleteStatus(status);
        update.setActualEndDate(new Date());
        taskCenterDao.updateById(update);

        // 更新检查项
        if (StringUtils.isNotBlank(checkItemStrs)) {
            List<TaskCenterCheckItemVo> list = new ArrayList<>();
            String[] split = checkItemStrs.split(",");
            for (String str : split) {
                String[] split2 = str.split(":");
                Long taskCenterCheckItemId = Long.valueOf(split2[0]);
                Integer isCheck = Integer.valueOf(split2[1]);
                Integer problemNum = Integer.valueOf(split2[2]);

                TaskCenterCheckItemVo cVo = new TaskCenterCheckItemVo();
                cVo.setId(taskCenterCheckItemId);
                cVo.setIsCheck(isCheck);
                cVo.setProblemNum(problemNum);
                list.add(cVo);
            }

            if (list.size() > 0) {
                taskCenterCheckItemDao.batchUpdateById(list);
            }
        }

        return Result.success();
    }

    @Override
    public Result<TaskCenterVo> viewTaskExecuteInfo(long taskId) {
        TaskCenterVo vo = taskCenterDao.selectById(taskId);
        if (vo.getCompleteStatus().intValue() == 0) {
            return Result.error("任务未开始");
        }

        /* 查询房屋的检查项 */
        TaskCenterCheckItemVo query = new TaskCenterCheckItemVo();
        query.setTaskId(taskId);
        List<TaskCenterCheckItemVo> checkItemList = taskCenterCheckItemDao.select(query);

        /* 查询检查项包含的问题 */
        selectAndSetProblem(taskId, checkItemList);

        /* 构造检查包和检查项的层级关系 */
        List<CheckPackageVo> checkPackageVoList = buildCheckPackageAndItem(checkItemList);

        TaskCenterVo returnVo = new TaskCenterVo();
        returnVo.setCheckPackageList(checkPackageVoList);
        returnVo.setTaskName(vo.getTaskName());

        return Result.success(returnVo);
    }

    private List<CheckPackageVo> buildCheckPackageAndItem(List<TaskCenterCheckItemVo> checkItemList) {
        if (CollectionUtils.isEmpty(checkItemList)) {
            return null;
        }
        Map<Long, List<TaskCenterCheckItemVo>> checkItemMap = new HashMap<>();
        Map<Long, String> checkPackageMap = new HashMap<>();
        for (TaskCenterCheckItemVo checkItemVo : checkItemList) {
            List<TaskCenterCheckItemVo> cList = checkItemMap.get(checkItemVo.getCheckPackageId());
            if (cList == null) {
                cList = new ArrayList<>();
                cList.add(checkItemVo);
                checkItemMap.put(checkItemVo.getCheckPackageId(), cList);
                checkPackageMap.put(checkItemVo.getCheckPackageId(), checkItemVo.getCheckPackageName());
                checkItemVo.setCheckPackageName(null);
            } else {
                cList.add(checkItemVo);
            }
        }
        List<CheckPackageVo> checkPackageVoList = new ArrayList<CheckPackageVo>();
        Set<Entry<Long, List<TaskCenterCheckItemVo>>> entrySet = checkItemMap.entrySet();
        for (Entry<Long, List<TaskCenterCheckItemVo>> entry : entrySet) {
            CheckPackageVo pkg = new CheckPackageVo();
            pkg.setId(entry.getKey());
            pkg.setName(checkPackageMap.get(entry.getKey()));
            pkg.setTaskCenterCheckItemList(entry.getValue());
            checkPackageVoList.add(pkg);
        }
        return checkPackageVoList;
    }

    /** 查询检查项包含的问题 */
    private void selectAndSetProblem(long taskId, List<TaskCenterCheckItemVo> checkItemList) {
        if (CollectionUtils.isEmpty(checkItemList)) {
            return;
        }
        TaskCenterProblemVo problemQuery = new TaskCenterProblemVo();
        problemQuery.setTaskId(taskId);
        // 问题list
        List<TaskCenterProblemVo> problemList = taskCenterProblemDao.select(problemQuery);
        /* list转为map， key:CheckItemId， value:list */
        Map<Long, List<TaskCenterProblemVo>> problemMap = new HashMap<>();
        for (TaskCenterProblemVo problemVo : problemList) {
            Long itemId = problemVo.getTaskCenterCheckItemId();
            if (itemId == null) {
                continue;
            }
            List<TaskCenterProblemVo> rowList = problemMap.get(itemId);
            if (rowList == null) {
                rowList = new ArrayList<>();
                rowList.add(problemVo);
                problemMap.put(itemId, rowList);
            } else {
                rowList.add(problemVo);
            }
        }
        /* 遍历checkItem list， 设置problem */
        for (TaskCenterCheckItemVo checkItemVo : checkItemList) {
            checkItemVo.setProblemList(problemMap.get(checkItemVo.getId()));
        }
    }

    @Override
    public Result<NoData> addProblem(TaskCenterProblemVo taskCenterProblemVo) {
        TaskCenterVo taskCenterVo = taskCenterDao.selectById(taskCenterProblemVo.getTaskId());
        if (taskCenterVo == null) {
            return Result.error("任务不存在");
        }
        TaskCenterVo taskUpdate = new TaskCenterVo();
        taskUpdate.setId(taskCenterProblemVo.getTaskId());
        taskUpdate.setHasProblem(1);
        taskCenterDao.updateById(taskUpdate);

        taskCenterProblemVo.setDelFlag(0);
        taskCenterProblemVo.setStatus(0);
        taskCenterProblemDao.add(taskCenterProblemVo);

        return Result.success();
    }

    @Override
    public Result<List<CheckProblemVo>> queryProblem(CheckProblemVo checkProblemVo) {
        List<CheckProblemVo> list = checkProblemDao.selectOrderPid(checkProblemVo);

        Map<Long, List<CheckProblemVo>> checkItemMap = new HashMap<>();
        Map<Long, String> parentProblemMap = new HashMap<>();
        for (CheckProblemVo pro : list) {
            if (pro.getPid() == 0) {
                List<CheckProblemVo> cList = checkItemMap.get(pro.getId());
                if (cList == null) {
                    cList = new ArrayList<>();
                    checkItemMap.put(pro.getId(), cList);
                    parentProblemMap.put(pro.getId(), pro.getName());
                }
            } else {
                List<CheckProblemVo> cList = checkItemMap.get(pro.getPid());
                if (cList != null) {
                    cList.add(new CheckProblemVo(pro.getId(), pro.getPid(), pro.getName(), pro.getDescr()));
                }
            }
        }

        List<CheckProblemVo> problemList = new ArrayList<>();

        Set<Entry<Long, List<CheckProblemVo>>> entrySet = checkItemMap.entrySet();
        for (Entry<Long, List<CheckProblemVo>> entry : entrySet) {
            CheckProblemVo pkg = new CheckProblemVo();
            pkg.setId(entry.getKey());
            pkg.setName(parentProblemMap.get(entry.getKey()));
            pkg.setChildList(entry.getValue());
            if (entry.getValue().size() > 0) {
                problemList.add(pkg);
            }
        }

        return Result.success(problemList);
    }

    @Override
    public Result<TaskCenterVo> futureTaskDetail(long taskId) {
        TaskAssignVo taskAssignVo = taskAssignDao.selectById(taskId);

        // 查询检查项包含的检查项
        // List<TaskCenterCheckItemVo> checkItemList = taskAssignDao.selectTaskCheckItem(taskId);

        /* 查询检查包 */
        List<CheckPackageVo> pkgList = monitorTargetCheckPackageDao
            .selectByMonitorTargetId(taskAssignVo.getMonitorTargetId());

        /* 查询检查项包含的问题 */
        // TODO

        TaskCenterVo taskCenterVo = new TaskCenterVo();
        taskCenterVo.setTaskName(taskAssignVo.getTaskName());
        taskCenterVo.setCheckPackageList(pkgList);
        taskCenterVo.setCycleType(taskAssignVo.getCycleType());
        taskCenterVo.setDayOfWeek(taskAssignVo.getDayOfWeek());
        taskCenterVo.setDescr(taskAssignVo.getDescr());

        return Result.success(taskCenterVo);
    }

    @Override
    public Result<List<TaskCenterProblemVo>> checkItemProblemList(long taskCenterCheckItemId) {
        TaskCenterProblemVo query = new TaskCenterProblemVo();
        query.setTaskCenterCheckItemId(taskCenterCheckItemId);
        List<TaskCenterProblemVo> list = taskCenterProblemDao.select(query);

        return Result.success(list);
    }

    @Override
    public Result<Long> reportSlantData(TaskCenterSlantVo vo) {
        if (vo.getId() != null) {
            taskCenterSlantDao.updateById(vo);
        } else {
            taskCenterSlantDao.add(vo);
        }

        return Result.success(vo.getId());
    }

    @Override
    public Result<Long> reportCrackData(TaskCenterCrackVo vo) {
        if (vo.getId() != null) {
            taskCenterCrackDao.updateById(vo);
        } else {
            taskCenterCrackDao.add(vo);

            /* 自动产生一条检查问题数据, CheckProblemId固定为47 */
            TaskCenterProblemVo bean = new TaskCenterProblemVo();
            bean.setTaskId(vo.getTaskId());
            bean.setCheckProblemId(47L);
            bean.setCheckProblemName("专业采集问题");
            StringBuilder checkProblemText = new StringBuilder().append("裂缝长度:").append(vo.getLength()).append("裂缝宽度:")
                .append(vo.getWidth()).append("  ");
            if (vo.getFloorNum() != null) {
                checkProblemText.append("所处楼层:").append(vo.getFloorNum()).append("  ");
            }
            if (vo.getRoom() != null) {
                checkProblemText.append("所在房间:").append(AppBizCache.dictDecode("proData_room", vo.getRoom()))
                    .append("  ");
            }
            if (vo.getPlace() != null) {
                checkProblemText.append("所在位置:").append(AppBizCache.dictDecode("proData_place", vo.getPlace()))
                    .append("  ");
            }
            if (vo.getDirection() != null) {
                checkProblemText.append("所在方位:").append(AppBizCache.dictDecode("proData_direction", vo.getDirection()))
                    .append("  ");
            }
            bean.setCheckProblemLevel(2);
            bean.setCheckProblemText(checkProblemText.toString());
            bean.setStatus(0);
            taskCenterProblemDao.add(bean);
        }
        return Result.success(vo.getId());
    }

    @Override
    public Result<Long> reportSubsidenceData(int pointType, long taskId, long taskCenterCheckItemId, String jsonData) {
        jsonData = StringEscapeUtils.unescapeHtml4(jsonData);
        List<TaskCenterSubsidenceVo> voList = JSON.parseArray(jsonData, TaskCenterSubsidenceVo.class);
        taskCenterSubsidenceDao.deleteByPointTypeAndCheckItemId(pointType, taskCenterCheckItemId);

        Date now = new Date();
        Iterator<TaskCenterSubsidenceVo> iterator = voList.iterator();
        while (iterator.hasNext()) {
            TaskCenterSubsidenceVo taskCenterSubsidenceVo = iterator.next();
            if (StringUtils.isBlank(taskCenterSubsidenceVo.getName())) {
                iterator.remove();
                continue;
            }
            taskCenterSubsidenceVo.setPointType(pointType);
            taskCenterSubsidenceVo.setTaskId(taskId);
            taskCenterSubsidenceVo.setTaskCenterCheckItemId(taskCenterCheckItemId);
            taskCenterSubsidenceVo.setReportTime(now);
        }

        taskCenterSubsidenceDao.addList(voList);

        return Result.success();
    }

    @Override
    public Result<NoData> delCrackData(long id) {
        taskCenterCrackDao.deleteById(id);
        return Result.success();
    }

    @Override
    public Result<NoData> delSubsidenceData(long id) {
        taskCenterSubsidenceDao.deleteById(id);
        return Result.success();
    }

    @Override
    public TaskCenterCheckItemVo getItemProData(long taskId, long taskCenterCheckItemId) {
        /* 裂缝 */
        TaskCenterCrackVo crackVo = new TaskCenterCrackVo();
        crackVo.setTaskId(taskId);
        crackVo.setTaskCenterCheckItemId(taskCenterCheckItemId);
        List<TaskCenterCrackVo> crackList = taskCenterCrackDao.select(crackVo);

        /* 偏移 */
        TaskCenterSlantVo slantVo = new TaskCenterSlantVo();
        slantVo.setTaskId(taskId);
        slantVo.setTaskCenterCheckItemId(taskCenterCheckItemId);
        List<TaskCenterSlantVo> slantList = taskCenterSlantDao.select(slantVo);
        TaskCenterSlantVo taskCenterSlantVo = null;
        if (CollectionUtils.isNotEmpty(slantList)) {
            taskCenterSlantVo = slantList.get(0);
        }

        /* 沉降 */
        TaskCenterSubsidenceVo subsidenceVo = new TaskCenterSubsidenceVo();
        subsidenceVo.setTaskId(taskId);
        subsidenceVo.setTaskCenterCheckItemId(taskCenterCheckItemId);
        List<TaskCenterSubsidenceVo> subsidenceList = taskCenterSubsidenceDao.select(subsidenceVo);

        TaskCenterCheckItemVo itemVo = new TaskCenterCheckItemVo();
        itemVo.setSlantData(taskCenterSlantVo);
        itemVo.setCrackData(crackList);
        itemVo.setSubsidenceData(subsidenceList);

        return itemVo;
    }

}
