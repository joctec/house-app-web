package com.guda.mp.service.impl;

import com.guda.mp.entity.Person;
import com.guda.mp.vo.PersonVo;
import com.guda.mp.service.PersonService;
import com.guda.mp.dao.PersonDao;
import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.base.PageQuery;
import com.github.pagehelper.Page;
import com.app.util.NumUtil;
import com.app.util.BizUtil;
import com.app.core.util.SysConfig;
import com.app.core.web.context.RequestContext;
import com.app.util.DateUtil;

import java.util.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PersonServiceImpl extends BaseServiceImpl<PersonVo> implements PersonService {

	@Autowired
	private PersonDao personDao;

    @Override
    protected BaseDao<PersonVo> getDao() {
        return this.personDao;
    }
	
	/*
     * 分页查询
     */
    @Override
    public Page<PersonVo> selectPage(PageQuery pageParams, PersonVo bean) {
        return selectBasePage(pageParams, bean);
    }
    
    @Override
    public String listExport(PersonVo vo) {
        List<List<String>> dataList = new ArrayList<List<String>>();

        /* 分页查询数据 */
        PageQuery pageParams = new PageQuery();
        pageParams.setQueryAll(true);
        // 每次查询2000条
        pageParams.setPageSize(2000);
        pageParams.setPageNum(1);
        Page<PersonVo> page = selectBasePage(pageParams, vo);
        for (PersonVo bean : page) {
            List<String> rowList = new ArrayList<String>();
            rowList.add(bean.getId().toString());
			rowList.add(NumUtil.numToStr(bean.getOrgId()));
			rowList.add(bean.getName());
			rowList.add(NumUtil.numToStr(bean.getGender()));
			rowList.add(DateUtil.yyyyMMddLine(bean.getBirthdate()));
			rowList.add(bean.getIdCard());
			rowList.add(NumUtil.numToStr(bean.getAddress()));
            dataList.add(rowList);
        }

        // 要下载的文件名称
        String downFilename = BizUtil.getDownloadFileNameExcel("", RequestContext.getLoginUserId());
        // 生成文件存在项目临时目录
        String tmpDirPath = SysConfig.get("temp.dir");

        // 导出excel
        String downloadFile = tmpDirPath + "/" + downFilename;
        com.app.util.ExcelUtil excelUtil = new com.app.util.ExcelUtil(downloadFile);
        excelUtil.addCol("ID", 30);
		excelUtil.addCol("所属公司/组织id", 30);	
		excelUtil.addCol("姓名", 30);	
		excelUtil.addCol("性别", 30);	
		excelUtil.addCol("出生日期", 30);	
		excelUtil.addCol("身份证号", 30);	
		excelUtil.addCol("居住地址", 30);	
        excelUtil.setDataList(dataList);
        try {
            excelUtil.wirte();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return downloadFile;
    }
}
