package com.guda.mp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.core.base.BaseDao;
import com.app.core.base.BaseServiceImpl;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.guda.mp.dao.SysResourceDao;
import com.guda.mp.dao.SysRoleDao;
import com.guda.mp.entity.SysRole;
import com.guda.mp.entity.SysRoleResource;
import com.guda.mp.service.SysRoleResourceService;
import com.guda.mp.service.SysRoleService;
import com.guda.mp.vo.SysResourceVo;
import com.guda.mp.vo.SysRoleVo;

@Service
@Transactional
public class SysRoleServiceImpl extends BaseServiceImpl<SysRole> implements SysRoleService {

    @Autowired
    private SysRoleDao dao;
    @Autowired
    private SysRoleResourceService sysRoleResourceService;
    @Autowired
    private SysResourceDao sysResourceDao;

    @Override
    protected BaseDao<SysRole> getDao() {
        return this.dao;
    }

    /**
     * 查询所有的角色
     * 
     * @param bean
     * @return
     */
    public Result<List<SysRole>> selectAllRole(SysRole bean) {
        return Result.success(dao.select(bean));
    }

    /**
     * 通过用户ID查询该用户的拥有哪些角色
     * 
     * @param id
     * @return
     */
    public List<SysRoleVo> selectRoleByUserId(Long id) {
        return dao.selectRoleByUserId(id);
    }

    /**
     * 新增角色
     * 
     * @param bean
     * @return
     */
    public Result<NoData> addSave(SysRoleVo bean) {
        dao.add(bean);
        String[] resourceIds = bean.getResourceIds().split(",");
        for (int i = 0; i < resourceIds.length; i++) {
            SysRoleResource sysRoleResource = new SysRoleResource();
            sysRoleResource.setRoleId(bean.getId());
            sysRoleResource.setResourceId(Long.parseLong(resourceIds[i]));
            sysRoleResourceService.add(sysRoleResource);
        }
        return Result.success();
    }

    /**
     * 更新角色信息
     * 
     * @param bean
     * @return
     */
    public Result<NoData> updateSave(SysRoleVo bean) {
        List<Long> oldRole = new ArrayList<Long>();
        List<Long> newResource = new ArrayList<Long>();
        dao.updateById(bean);
        List<SysResourceVo> dataList = sysResourceDao.selectResourceByRoleId(bean.getId());
        String[] rresoueceids = bean.getResourceIds().split(",");
        // 取出原来的用户角色
        for (int i = 0; i < dataList.size(); i++) {
            oldRole.add(dataList.get(i).getId());
        }
        // 判断哪些是新增加的角色，哪些是要废弃的角色
        for (int i = 0; i < rresoueceids.length; i++) {
            if (oldRole.indexOf(Long.parseLong(rresoueceids[i])) >= 0) {
                oldRole.remove(oldRole.indexOf(Long.parseLong(rresoueceids[i])));
            } else {
                newResource.add(Long.parseLong(rresoueceids[i]));
            }
        }
        // 添加新增近来的角色
        for (int i = 0; i < newResource.size(); i++) {
            SysRoleResource sysRoleResource = new SysRoleResource();
            sysRoleResource.setResourceId(newResource.get(i));
            sysRoleResource.setRoleId(bean.getId());
            sysRoleResourceService.add(sysRoleResource);
        }
        // 删除原来废弃的角色（是物理删除不是逻辑删除）
        for (int i = 0; i < oldRole.size(); i++) {
            sysRoleResourceService.deleteByResourceId(oldRole.get(i));
        }
        return Result.success();
    }

    /**
     * 批量删除
     * 
     * @param bean
     * @return
     */
    public Result<NoData> deletes(SysRoleVo bean) {
        String[] ids = bean.getIds().split(",");
        for (int i = 0; i < ids.length; i++) {
            dao.deleteById(Long.parseLong(ids[i]));
            sysRoleResourceService.deleteByRoleId(Long.parseLong(ids[i]));
        }
        return Result.success();
    }

}
