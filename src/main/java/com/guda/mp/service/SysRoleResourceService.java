package com.guda.mp.service;

import com.guda.mp.entity.SysRoleResource;
import com.app.core.base.BaseService;

public interface SysRoleResourceService extends BaseService<SysRoleResource> {

	public boolean deleteByResourceId(Long id);
	public boolean deleteByRoleId(Long id);
}
