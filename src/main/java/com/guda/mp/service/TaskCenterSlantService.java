package com.guda.mp.service;

import com.app.core.base.BaseService;
import com.app.core.base.PageQuery;
import com.github.pagehelper.Page;
import com.guda.mp.vo.TaskCenterSlantVo;

public interface TaskCenterSlantService extends BaseService<TaskCenterSlantVo> {

    /*
     * 分页查询
     */
    Page<TaskCenterSlantVo> selectPage(PageQuery pageParams, TaskCenterSlantVo bean);

    /*
     * 返回excel下载文件全路径
     */
    String listExport(TaskCenterSlantVo vo);
}
