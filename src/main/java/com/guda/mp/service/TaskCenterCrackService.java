package com.guda.mp.service;

import com.guda.mp.entity.TaskCenterCrack;
import com.guda.mp.vo.TaskCenterCrackVo;
import com.app.core.base.BaseService;
import com.github.pagehelper.Page;
import com.app.core.base.PageQuery;

public interface TaskCenterCrackService extends BaseService<TaskCenterCrackVo> {
    /*
     * 分页查询
     */
   	Page<TaskCenterCrackVo> selectPage(PageQuery pageParams, TaskCenterCrackVo bean);
   	
   	/*
     * 返回excel下载文件全路径
     */
   	String listExport(TaskCenterCrackVo vo);
}
