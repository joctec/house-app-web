package com.guda.mp.service;

import java.util.List;

import com.app.core.common.Result;
import com.guda.mp.vo.ChartVo;
import com.guda.mp.vo.MtargetDeviceVo;
import com.guda.mp.vo.SensorDataVo;
import com.guda.mp.vo.SensorSlantDayVo;

public interface DataMonitorService {

    List<MtargetDeviceVo> getAllDevice(long mtargetId);

    ChartVo slantChartData(SensorDataVo vo);

    ChartVo slantDayChartData(SensorSlantDayVo vo);

    Result<List<SensorDataVo>> waterLevelChartData(SensorDataVo vo);

    Result<List<SensorDataVo>> getSensorChartData(SensorDataVo vo);

}
