package com.guda.mp.service;

import java.util.List;
import java.util.Map;

import com.app.core.base.BaseService;
import com.app.core.base.PageQuery;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.github.pagehelper.Page;
import com.guda.mp.vo.SysResourceVo;

public interface SysResourceService extends BaseService<SysResourceVo> {

    /**
     * 查询所有的资源
     * 
     * @param bean
     * @return
     */
    Result<List<SysResourceVo>> selectAllResource(SysResourceVo bean);

    /**
     * 通过角色ID查询资源数据
     * 
     * @param id
     * @return
     */
    public Map<String, Object> selectResourceByRoleId(Long id);

    /**
     * 查询菜单分组数据
     * 
     * @return
     */
    public List<SysResourceVo> selectMenuList(Long userId);

    List<SysResourceVo> selectUserAllResource(Long userId);

    Result<NoData> add(SysResourceVo bean);

    Result<NoData> update(SysResourceVo bean);

    Page<SysResourceVo> selectByPid(PageQuery pageQuery, SysResourceVo bean);
}
