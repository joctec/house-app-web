package com.guda.mp.service;

import java.util.List;

import com.app.core.base.BaseService;
import com.guda.mp.vo.SysAttachVo;

public interface SysAttachService extends BaseService<SysAttachVo> {

    List<SysAttachVo> getByRefId(Long refId);

    List<SysAttachVo> getByRefIdAndCode(Long refId, String refCode);

    void delByRefId(Long refId);

    void delByRefIdAndCode(Long refId, String refCode);

    void updateAttach(Long id, String files);

    void updateAttach(Long id, String files, String code);
}
