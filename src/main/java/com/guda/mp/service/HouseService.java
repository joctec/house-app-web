package com.guda.mp.service;

import java.util.List;

import com.app.core.base.BaseService;
import com.app.core.base.PageQuery;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.github.pagehelper.Page;
import com.guda.mp.vo.HouseVo;
import com.guda.mp.vo.MonitorTargetVo;
import com.guda.mp.vo.TaskCenterProblemVo;
import com.guda.mp.vo.TaskCenterVo;

public interface HouseService extends BaseService<HouseVo> {

    /**
     * 房屋列表. key是街道名称
     */
    Page<MonitorTargetVo> houseList(PageQuery pageQuery, HouseVo query);

    /**
     * 房屋列表. key是街道名称
     */
    Page<HouseVo> houseListVersion2(PageQuery pageQuery, HouseVo query);

    /**
     * 房屋详情
     */
    HouseVo houseDetail(long monitorTargetId);

    List<TaskCenterProblemVo> houseProblemList(long monitorTargetId);

    List<TaskCenterVo> inspectionRecordList(long monitorTargetId);

    /**
     * 更新房屋地址坐标
     */
    Result<NoData> updateLocaion(long monitorTargetId, Double lat, Double lng);

    Result<NoData> bindCardSuccess(Long targetId);

    List<HouseVo> selectBizList(HouseVo bean);

    Result<NoData> updateHouseImg(Long monitorTargetId, String houseImg);

}
