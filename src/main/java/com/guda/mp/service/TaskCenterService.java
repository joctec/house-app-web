package com.guda.mp.service;

import java.util.List;

import com.app.core.base.BaseService;
import com.app.core.base.PageQuery;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.github.pagehelper.Page;
import com.guda.mp.vo.CheckProblemVo;
import com.guda.mp.vo.TaskAssignVo;
import com.guda.mp.vo.TaskCenterCheckItemVo;
import com.guda.mp.vo.TaskCenterCrackVo;
import com.guda.mp.vo.TaskCenterProblemVo;
import com.guda.mp.vo.TaskCenterSlantVo;
import com.guda.mp.vo.TaskCenterVo;

/**
 * 执行任务相关业务
 * 
 * @author zhangxf
 * @created 2017年5月5日 下午9:18:00
 */
public interface TaskCenterService extends BaseService<TaskCenterVo> {

    /** 获取七牛上传token */
    String getQiniuUpToken();

    /** 今日任务 */
    Page<TaskCenterVo> selectTodayTaskPage(PageQuery pageParams, TaskCenterVo query);

    /** 查询历史任务 */
    Page<TaskCenterVo> selectHistoryTask(PageQuery pageParams, TaskCenterVo query);

    /** 查询已完成任务 */
    Page<TaskCenterVo> selectCompletedTask(PageQuery pageParams, TaskCenterVo query);

    /** 查询自己的负责任务 */
    Page<TaskAssignVo> selectOwnerTask(PageQuery pageParams, TaskAssignVo query);

    /** 查询关注的任务 */
    Page<TaskCenterVo> selectFocusTask(PageQuery pageParams, TaskCenterVo query);

    /** 查询任务详情 */
    TaskCenterVo taskDeatail(long taskId);

    /**
     * 开始执行任务<br>
     * 返回检查包、检查项、检查情况
     */
    Result<TaskCenterVo> taskBegin(long taskId, String icCardData);

    /** 查询任务执行信息 */
    Result<TaskCenterVo> viewTaskExecuteInfo(long taskId);

    /** 完成任务 */
    Result<String> taskComplete(Integer type, long taskId, String checkItemStrs);

    /** 新增巡查问题 */
    Result<NoData> addProblem(TaskCenterProblemVo taskCenterProblemVo);

    /** 查询问题-基础数据 */
    Result<List<CheckProblemVo>> queryProblem(CheckProblemVo checkProblemVo);

    /** 查询将来任务详情 */
    Result<TaskCenterVo> futureTaskDetail(long taskId);

    /** 设置多个task的问题统计数量 */
    void setTaskProblemLevelCount(List<TaskCenterVo> list);

    /** 设置单个task的问题统计数量 */
    void setTaskProblemLevelCount(TaskCenterVo vo);

    /** 查看房屋的检查项列表 */
    Result<TaskCenterVo> viewCheckList(long taskId);

    /** 查询检查项下的问题 */
    Result<List<TaskCenterProblemVo>> checkItemProblemList(long taskCenterCheckItemId);

    /** 上报倾斜数据 */
    Result<Long> reportSlantData(TaskCenterSlantVo vo);

    /** 上报裂缝数据 */
    Result<Long> reportCrackData(TaskCenterCrackVo vo);

    /** 上报沉降数据 */
    Result<Long> reportSubsidenceData(int pointType, long taskId, long taskCenterCheckItemId, String jsonData);

    /** 删除裂缝数据 */
    Result<NoData> delCrackData(long id);

    /** 删除沉降数据 */
    Result<NoData> delSubsidenceData(long id);

    /** 查询检查项 倾斜、裂缝、沉降 数据 */
    TaskCenterCheckItemVo getItemProData(long taskId, long taskCenterCheckItemId);

}
