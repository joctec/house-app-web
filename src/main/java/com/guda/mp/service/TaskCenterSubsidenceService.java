package com.guda.mp.service;

import com.guda.mp.entity.TaskCenterSubsidence;
import com.guda.mp.vo.TaskCenterSubsidenceVo;
import com.app.core.base.BaseService;
import com.github.pagehelper.Page;
import com.app.core.base.PageQuery;

public interface TaskCenterSubsidenceService extends BaseService<TaskCenterSubsidenceVo> {
    /*
     * 分页查询
     */
   	Page<TaskCenterSubsidenceVo> selectPage(PageQuery pageParams, TaskCenterSubsidenceVo bean);
   	
   	/*
     * 返回excel下载文件全路径
     */
   	String listExport(TaskCenterSubsidenceVo vo);
}
