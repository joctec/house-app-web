package com.guda.mp.service;

import java.util.List;

import com.app.core.base.BaseService;
import com.app.core.common.NoData;
import com.app.core.common.Result;
import com.guda.mp.entity.SysRole;
import com.guda.mp.vo.SysRoleVo;

public interface SysRoleService extends BaseService<SysRole> {

    /**
     * 查询所有的角色
     * 
     * @param bean
     * @return
     */
    public Result<List<SysRole>> selectAllRole(SysRole bean);

    /**
     * 通过用户ID查询该用户的拥有哪些角色
     * 
     * @param id
     * @return
     */
    public List<SysRoleVo> selectRoleByUserId(Long id);

    /**
     * 新增角色
     * 
     * @param bean
     * @return
     */
    public Result<NoData> addSave(SysRoleVo bean);

    /**
     * 更新角色信息
     * 
     * @param bean
     * @return
     */
    public Result<NoData> updateSave(SysRoleVo bean);

    /**
     * 批量删除
     * 
     * @param bean
     * @return
     */
    public Result<NoData> deletes(SysRoleVo bean);
}
