package com.guda.mp.dao;

import com.guda.mp.vo.CheckItemVo;
import com.app.core.base.BaseDao;

public interface CheckItemDao extends BaseDao<CheckItemVo> {

}
