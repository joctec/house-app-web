package com.guda.mp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.DeviceDataTransferVo;
import com.guda.mp.vo.SensorSlantDayVo;

public interface SensorSlantDayDao extends BaseDao<SensorSlantDayVo> {

    List<DeviceDataTransferVo> selectLuerOneDayMinValue(@Param("funType") String funType,
        @Param("startTimeLong") Long startTimeLong, @Param("endTimeLong") Long endTimeLong);

    List<DeviceDataTransferVo> selectLuerOneDayMaxValue(@Param("funType") String funType,
        @Param("startTimeLong") Long startTimeLong, @Param("endTimeLong") Long endTimeLong);

    int addSlantDayValue(SensorSlantDayVo bean);

    List<SensorSlantDayVo> selectChartData(SensorSlantDayVo query);

}
