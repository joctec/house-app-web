package com.guda.mp.dao;

import com.guda.mp.vo.ContractVo;
import com.app.core.base.BaseDao;

public interface ContractDao extends BaseDao<ContractVo> {

}
