package com.guda.mp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.SysResourceVo;

public interface SysResourceDao extends BaseDao<SysResourceVo> {

    /**
     * 通过角色ID查询资源数据
     * 
     * @param id
     * @return
     */
    public List<SysResourceVo> selectResourceByRoleId(Long roleId);

    /**
     * 通过pid查询数据
     * 
     * @param roleId
     * @return
     */
    public List<SysResourceVo> selectResourceByPid(Long pid);

    /**
     * 通过登录用户ID查询菜单分组数据
     * 
     * @return
     */
    public List<SysResourceVo> selectMenuListByUserId(@Param("userId") Long userId, @Param("pid") Long pid);

    public List<SysResourceVo> selectUserAllResource(Long userId);

    public List<SysResourceVo> selectByPid(SysResourceVo bean);

}
