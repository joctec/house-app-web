package com.guda.mp.dao;

import com.guda.mp.vo.DeviceDataVo;
import com.app.core.base.BaseDao;

import java.util.List;

public interface DeviceDataDao extends BaseDao<DeviceDataVo> {
	
	List<DeviceDataVo> selectPage(DeviceDataVo bean);
	
}
