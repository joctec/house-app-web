package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.entity.LoginUser;
import com.guda.mp.vo.LoginUserVo;

public interface LoginUserDao extends BaseDao<LoginUser> {

	public List<LoginUserVo> selectByPage(LoginUserVo bean);
}
