package com.guda.mp.dao;

import com.guda.mp.entity.Demo;
import com.app.core.base.BaseDao;

public interface DemoDao extends BaseDao<Demo> {

}
