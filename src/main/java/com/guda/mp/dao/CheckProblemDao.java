package com.guda.mp.dao;

import com.guda.mp.vo.CheckProblemVo;

import java.util.List;

import com.app.core.base.BaseDao;

public interface CheckProblemDao extends BaseDao<CheckProblemVo> {

    List<CheckProblemVo> selectOrderPid(CheckProblemVo checkProblemVo);

}
