package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.HouseVo;

public interface HouseDao extends BaseDao<HouseVo> {

    HouseVo selectByMonitorTargetId(long monitorTargetId);

    List<HouseVo> selectBizList(HouseVo bean);
}
