package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.TaskAssignVo;
import com.guda.mp.vo.TaskCenterCheckItemVo;

public interface TaskAssignDao extends BaseDao<TaskAssignVo> {

    List<TaskAssignVo> selectDateValid();

    List<TaskCenterCheckItemVo> selectTaskCheckItem(long taskId);

}
