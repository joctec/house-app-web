package com.guda.mp.dao;

import com.guda.mp.entity.AddressHierarchy;
import com.guda.mp.vo.AddressHierarchyVo;
import com.app.core.base.BaseDao;
import com.github.pagehelper.Page;

public interface AddressHierarchyDao extends BaseDao<AddressHierarchyVo> {

	Page<AddressHierarchyVo> selectAddressHierarchyVoPage(AddressHierarchyVo bean);

}
