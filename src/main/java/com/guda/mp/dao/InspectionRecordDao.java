package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.InspectionRecordVo;

public interface InspectionRecordDao extends BaseDao<InspectionRecordVo> {

    List<InspectionRecordVo> selectBizPage(InspectionRecordVo bean);

    InspectionRecordVo getRecord(long id);

}
