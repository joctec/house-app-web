package com.guda.mp.dao;

import com.guda.mp.vo.CheckPackageVo;
import com.app.core.base.BaseDao;

public interface CheckPackageDao extends BaseDao<CheckPackageVo> {

}
