package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.ProjectVo;

public interface ProjectDao extends BaseDao<ProjectVo> {

    List<ProjectVo> selectBizPage(ProjectVo bean);

}
