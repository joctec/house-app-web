package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.TaskCenterVo;

public interface TaskCenterDao extends BaseDao<TaskCenterVo> {

    List<TaskCenterVo> todayTaskPage(TaskCenterVo bean);

    List<TaskCenterVo> selectHistoryTask(TaskCenterVo bean);

    List<TaskCenterVo> selectInspectionRecordPage(TaskCenterVo query);

    TaskCenterVo selectBizById(long taskId);

}
