package com.guda.mp.dao;

import com.guda.mp.vo.BaseDictDataVo;
import com.app.core.base.BaseDao;

public interface BaseDictDataDao extends BaseDao<BaseDictDataVo> {

}
