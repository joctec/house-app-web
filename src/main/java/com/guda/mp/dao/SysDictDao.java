package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.SysDictVo;

public interface SysDictDao extends BaseDao<SysDictVo> {

    List<SysDictVo> selectPage(SysDictVo bean);

}
