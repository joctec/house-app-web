package com.guda.mp.dao;

import com.guda.mp.vo.TaskCenterCheckItemVo;
import com.app.core.base.BaseDao;

import java.util.List;

public interface TaskCenterCheckItemDao extends BaseDao<TaskCenterCheckItemVo> {

    List<TaskCenterCheckItemVo> selectPage(TaskCenterCheckItemVo bean);

    int addList(List<TaskCenterCheckItemVo> checkItemList);

    int deleteByTaskCenterId(Long id);

    int batchUpdateById(List<TaskCenterCheckItemVo> list);

}
