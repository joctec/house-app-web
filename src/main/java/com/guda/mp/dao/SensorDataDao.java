package com.guda.mp.dao;

import java.util.List;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.DeviceDataTransferVo;
import com.guda.mp.vo.SensorDataVo;

public interface SensorDataDao extends BaseDao<SensorDataVo> {

    List<SensorDataVo> selectPage(SensorDataVo bean);

    List<SensorDataVo> getSensorChartData(SensorDataVo vo);

    List<DeviceDataTransferVo> getLuerTransferData(DeviceDataTransferVo vo);

    List<DeviceDataTransferVo> selectLuerTransferMinData(DeviceDataTransferVo vo);

    List<DeviceDataTransferVo> selectLuerTransferMaxData(DeviceDataTransferVo vo);

    List<SensorDataVo> selectSlantChartData(SensorDataVo query);
}
