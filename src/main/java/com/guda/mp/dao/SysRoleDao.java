package com.guda.mp.dao;

import com.guda.mp.entity.SysRole;
import com.guda.mp.vo.SysRoleVo;

import java.util.List;

import com.app.core.base.BaseDao;

public interface SysRoleDao extends BaseDao<SysRole> {

	/**
	 * 通过用户ID查询该用户的拥有哪些角色
	 * @param id
	 * @return
	 */
	public List<SysRoleVo> selectRoleByUserId(Long id);
}
