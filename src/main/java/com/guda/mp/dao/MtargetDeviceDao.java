package com.guda.mp.dao;

import com.guda.mp.vo.MtargetDeviceVo;
import com.app.core.base.BaseDao;

import java.util.List;

public interface MtargetDeviceDao extends BaseDao<MtargetDeviceVo> {
	
	List<MtargetDeviceVo> selectPage(MtargetDeviceVo bean);

    MtargetDeviceVo selectByMtargetId(Long mtargetId);
	
}
