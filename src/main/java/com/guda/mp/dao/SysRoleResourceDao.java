package com.guda.mp.dao;

import com.guda.mp.entity.SysRoleResource;
import com.app.core.base.BaseDao;

public interface SysRoleResourceDao extends BaseDao<SysRoleResource> {

	public boolean deleteByResourceId(Long id);
	public boolean deleteByRoleId(Long id);
}
