package com.guda.mp.dao;

import com.guda.mp.vo.CheckPackageItemVo;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.app.core.base.BaseDao;

public interface CheckPackageItemDao extends BaseDao<CheckPackageItemVo> {

    /**
     * 删除某个包下的所有检查项
     * 
     * @param checkPackageId
     * @return
     * @author zhangxf
     * @created 2017年3月23日 下午6:24:21
     */
    int deleteByPackageId(long checkPackageId);

    /**
     * 按检查项id批量增加
     * 
     * @param checkPackageId
     * @param checkItemIdStr
     * @return
     * @author zhangxf
     * @created 2017年3月23日 下午6:26:08
     */
    int addBatch(@Param("list") List<CheckPackageItemVo> list);

}
