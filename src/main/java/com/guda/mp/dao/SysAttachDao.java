package com.guda.mp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.SysAttachVo;

public interface SysAttachDao extends BaseDao<SysAttachVo> {

    List<SysAttachVo> selectByRefId(Long refId);

    List<SysAttachVo> selectByRefIdAndCode(@Param("refId") Long refId, @Param("refCode") String refCode);

    void deleteByRefId(Long refId);

    void deleteByRefIdAndCode(@Param("refId") Long refId, @Param("refCode") String refCode);

}
