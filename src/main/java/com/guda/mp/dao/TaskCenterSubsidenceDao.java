package com.guda.mp.dao;

import com.guda.mp.vo.TaskCenterSubsidenceVo;
import com.app.core.base.BaseDao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface TaskCenterSubsidenceDao extends BaseDao<TaskCenterSubsidenceVo> {

    List<TaskCenterSubsidenceVo> selectPage(TaskCenterSubsidenceVo bean);

    int addList(List<TaskCenterSubsidenceVo> voList);

    void deleteByPointTypeAndCheckItemId(@Param("pointType") int pointType,
        @Param("taskCenterCheckItemId") long taskCenterCheckItemId);

}
