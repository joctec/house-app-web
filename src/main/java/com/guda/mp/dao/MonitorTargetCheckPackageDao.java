package com.guda.mp.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.MapKey;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.CheckPackageVo;
import com.guda.mp.vo.MonitorTargetCheckPackageVo;
import com.guda.mp.vo.TaskCenterCheckItemVo;

public interface MonitorTargetCheckPackageDao extends BaseDao<MonitorTargetCheckPackageVo> {

    List<MonitorTargetCheckPackageVo> selectPage(MonitorTargetCheckPackageVo bean);

    int addList(List<MonitorTargetCheckPackageVo> list);

    int deleteByMonitorTargetId(Long monitorTargetId);

    List<MonitorTargetCheckPackageVo> selectByMonitorTargetIds(List<Long> idList);

    List<CheckPackageVo> selectByMonitorTargetId(Long monitorTargetId);

    /**
     * 查询检查包下检查项明细
     */
    List<TaskCenterCheckItemVo> selectAndCheckItemByTargetId(Long monitorTargetId);

    @MapKey("monitorTargetId")
    Map<Integer, MonitorTargetCheckPackageVo> countMonitorTargetCheckPackage(List<Long> monitorTargetIdList);
}
