package com.guda.mp.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.MapKey;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.HouseVo;
import com.guda.mp.vo.MonitorTargetVo;

public interface MonitorTargetDao extends BaseDao<MonitorTargetVo> {

    List<MonitorTargetVo> getAllNode(MonitorTargetVo bean);

    List<HouseVo> selectInspectionUserOwnerHouse(HouseVo query);

    @MapKey("id")
    Map<Long, Map<Long, String>> selectInspectionUserOwnerHouseStreetName(Long userId);

    List<HouseVo> selectInspectionHouseAll(HouseVo query);

    @MapKey("id")
    Map<Long, Map<Long, String>> selectInspectionHouseStreetNameAll();

}
