package com.guda.mp.dao;

import com.app.core.base.BaseDao;
import com.guda.mp.entity.SysLog;

public interface SysLogDao extends BaseDao<SysLog> {

}
