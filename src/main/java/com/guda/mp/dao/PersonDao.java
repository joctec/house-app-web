package com.guda.mp.dao;

import com.guda.mp.vo.PersonVo;
import com.app.core.base.BaseDao;

public interface PersonDao extends BaseDao<PersonVo> {

}
