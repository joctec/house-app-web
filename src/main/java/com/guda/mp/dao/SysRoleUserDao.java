package com.guda.mp.dao;

import com.guda.mp.entity.SysRoleUser;
import com.app.core.base.BaseDao;

public interface SysRoleUserDao extends BaseDao<SysRoleUser> {

}
