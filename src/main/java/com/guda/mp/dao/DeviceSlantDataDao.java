package com.guda.mp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.DeviceSlantDataVo;

public interface DeviceSlantDataDao extends BaseDao<DeviceSlantDataVo> {

    List<DeviceSlantDataVo> selectPage(DeviceSlantDataVo bean);

    List<DeviceSlantDataVo> selectBiz(DeviceSlantDataVo query);

    List<DeviceSlantDataVo> selectMinData(DeviceSlantDataVo query);

    List<DeviceSlantDataVo> selectMaxData(DeviceSlantDataVo query);

    List<DeviceSlantDataVo> selectNeedAlarm(@Param("alarmValue") Double alarmValue);

}
