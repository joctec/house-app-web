package com.guda.mp.dao;

import com.guda.mp.vo.TaskCenterSlantVo;
import com.app.core.base.BaseDao;

import java.util.List;

public interface TaskCenterSlantDao extends BaseDao<TaskCenterSlantVo> {

    List<TaskCenterSlantVo> selectPage(TaskCenterSlantVo bean);

}
