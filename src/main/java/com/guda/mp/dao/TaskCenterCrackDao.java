package com.guda.mp.dao;

import com.guda.mp.vo.TaskCenterCrackVo;
import com.app.core.base.BaseDao;

import java.util.List;

public interface TaskCenterCrackDao extends BaseDao<TaskCenterCrackVo> {
	
	List<TaskCenterCrackVo> selectPage(TaskCenterCrackVo bean);
	
}
