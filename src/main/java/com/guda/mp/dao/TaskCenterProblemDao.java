package com.guda.mp.dao;

import java.util.List;

import org.apache.ibatis.annotations.Select;

import com.app.core.base.BaseDao;
import com.guda.mp.vo.TaskCenterProblemVo;

public interface TaskCenterProblemDao extends BaseDao<TaskCenterProblemVo> {

    List<TaskCenterProblemVo> selectPage(TaskCenterProblemVo bean);

    @Select("select * from task_center_problem where task_id=#{taskId}")
    List<TaskCenterProblemVo> selectByTaskId(long taskId);

    List<TaskCenterProblemVo> selectByMonitorTargetId(long monitorTargetId);

    /** 设置任务的问题统计数量 */
    List<TaskCenterProblemVo> selectCountByIdList(List<Long> idList);

    /** 设置房屋的问题统计数量 */
    List<TaskCenterProblemVo> selectCountByMonitorTargetIdList(List<Long> list);

}
