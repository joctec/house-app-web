package com.guda.mp.dao;

import com.guda.mp.vo.OrgVo;
import com.app.core.base.BaseDao;

import java.util.List;

public interface OrgDao extends BaseDao<OrgVo> {
	
	List<OrgVo> selectPage(OrgVo bean);
	
}
