package com.guda.mp.dao;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.app.core.base.BaseDao;
import com.guda.mp.entity.LogLogin;
import com.guda.mp.vo.LogLoginVo;

public interface LogLoginDao extends BaseDao<LogLogin> {
    public List<Map> selectOrder(LogLoginVo vo);
    
    /**
     * 
     * loginTimesByDay:(查询某个用户的每日登录次数). <br/>
     *
     * @author liuliang
     * @param userType 用户类型，1党组织，2人员
     * @param userId 用户类型为1时为organid，为2时为personid
     * @param day 日期
     * @return
     * @since JDK 1.6
     */
    public Long loginTimesByDay(@Param("userType")Integer userType,@Param("userId")Long userId,@Param("day")String day);
}
