package com.guda.mp.web.rest.app;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.core.base.BaseController;
import com.app.core.common.Result;
import com.app.core.session.UserSession;
import com.app.core.web.context.RequestContext;
import com.app.core.web.util.AppContextUtil;
import com.guda.mp.service.LoginUserService;

@RestController
@SuppressWarnings("rawtypes")
@RequestMapping(value = "/api")
public class LoginRest extends BaseController {

    @Autowired
    private LoginUserService loginUserService;

    /**
     * 用户登录接口
     */
    @RequestMapping(value = "/login")
    @ResponseBody
    public Result tologin(HttpServletRequest request, HttpServletResponse response, String username, String password) {
        System.out.println("username: " + username);
        Result<UserSession> result = loginUserService.login(username, password, null);
        return result;
    }

    /**
     * 用户退出接口
     */
    @RequestMapping(value = "/logout")
    @ResponseBody
    public Result tologout(HttpServletRequest request, HttpServletResponse response) {
        String sessionId = RequestContext.getUserSession().getSessionId();
        AppContextUtil.delSessionValue(sessionId);
        return Result.success();
    }

    /**
     * 用户退出接口
     */
    @RequestMapping(value = "/test")
    @ResponseBody
    public Result test(HttpServletRequest request, HttpServletResponse response) {
        Object sessionValue = AppContextUtil.getSessionValue("251D08BD77C3446698AF09C2F595DE71");
        System.out.println(sessionValue);
        // AppContextUtil.setSessionValue("251D08BD77C3446698AF09C2F595DE71", "", 1);
        // AppContextUtil.getRedisCache().expriedKey(cacheName, key);
        return Result.success(sessionValue);
    }

}