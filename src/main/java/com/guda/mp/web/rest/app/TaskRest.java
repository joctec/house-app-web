package com.guda.mp.web.rest.app;

import javax.servlet.ServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.core.base.BaseController;
import com.app.core.base.PageQuery;
import com.app.core.common.Result;
import com.app.core.web.util.PageUtil;
import com.github.pagehelper.Page;
import com.guda.mp.service.TaskCenterService;
import com.guda.mp.vo.CheckProblemVo;
import com.guda.mp.vo.TaskAssignVo;
import com.guda.mp.vo.TaskCenterCrackVo;
import com.guda.mp.vo.TaskCenterProblemVo;
import com.guda.mp.vo.TaskCenterSlantVo;
import com.guda.mp.vo.TaskCenterVo;

@RestController
@RequestMapping(value = "/api/task")
@SuppressWarnings("rawtypes")
public class TaskRest extends BaseController {

    @Autowired
    private TaskCenterService taskCenterService;

    /**
     * 
     * test
     */
    @RequestMapping(value = "/test")
    @ResponseBody
    public Result makeToken() {
        System.out.println("--test--");
        return Result.success();
    }

    /**
     * 
     * 获取七牛上传token
     */
    @RequestMapping(value = "/uptoken")
    @ResponseBody
    public Result makeToken(ServletResponse response) {
        return Result.success(taskCenterService.getQiniuUpToken());
    }

    /**
     * 今日任务
     */
    @RequestMapping(value = "/today")
    @ResponseBody
    public Result today(PageQuery pageParams, TaskCenterVo query) {
        Page<TaskCenterVo> page = taskCenterService.selectTodayTaskPage(pageParams, query);
        return Result.success(PageUtil.convertPage(page));
    }

    /**
     * 全部历史任务
     */
    @RequestMapping(value = "/historyTask")
    @ResponseBody
    public Result historyTask(PageQuery pageParams, TaskCenterVo query) {
        Page<TaskCenterVo> page = taskCenterService.selectHistoryTask(pageParams, query);
        return Result.success(PageUtil.convertPage(page));
    }

    /**
     * 查询自己的负责任务
     */
    @RequestMapping(value = "/ownerTask")
    @ResponseBody
    public Result ownerTask(PageQuery pageParams, TaskAssignVo query) {
        Page<TaskAssignVo> page = taskCenterService.selectOwnerTask(pageParams, query);
        return Result.success(PageUtil.convertPage(page));
    }

    /**
     * 全部已完成的任务
     */
    @RequestMapping(value = "/completedTask")
    @ResponseBody
    public Result completedTask(PageQuery pageParams, TaskCenterVo query) {
        Page<TaskCenterVo> page = taskCenterService.selectCompletedTask(pageParams, query);
        return Result.success(PageUtil.convertPage(page));
    }

    /**
     * 全部已完成的任务
     */
    @RequestMapping(value = "/focusTask")
    @ResponseBody
    public Result focusTask(PageQuery pageParams, TaskCenterVo query) {
        Page<TaskCenterVo> page = taskCenterService.selectFocusTask(pageParams, query);
        return Result.success(PageUtil.convertPage(page));
    }

    /**
     * 任务详情
     */
    @RequestMapping(value = "/detail")
    @ResponseBody
    public Result taskDeatail(long taskId) {
        TaskCenterVo vo = taskCenterService.taskDeatail(taskId);
        return Result.success(vo);
    }

    /**
     * 开始执行任务
     */
    @RequestMapping(value = "/begin")
    @ResponseBody
    public Result taskBegin(long taskId, String icCardData) {
        return taskCenterService.taskBegin(taskId, icCardData);
    }

    /**
     * 查看房屋的检查项列表
     */
    @RequestMapping(value = "/viewCheckList")
    @ResponseBody
    public Result viewCheckList(long taskId) {
        return taskCenterService.viewCheckList(taskId);
    }

    /**
     * 查询任务执行信息
     */
    @RequestMapping(value = "/taskExecuteInfo")
    @ResponseBody
    public Result viewTaskExecuteInfo(long taskId) {
        return taskCenterService.viewTaskExecuteInfo(taskId);
    }

    /**
     * 完成任务
     */
    @RequestMapping(value = "/complete")
    @ResponseBody
    public Result taskComplete(Integer type, long taskId, String checkItemStrs) {
        return taskCenterService.taskComplete(type, taskId, checkItemStrs);
    }

    /**
     * 查询问题-基础数据
     */
    @RequestMapping(value = "/queryProblem")
    @ResponseBody
    public Result queryProblem(CheckProblemVo checkProblemVo) {
        return taskCenterService.queryProblem(checkProblemVo);
    }

    /**
     * 新增问题
     */
    @RequestMapping(value = "/addProblem")
    @ResponseBody
    public Result addProblem(TaskCenterProblemVo taskCenterProblemVo) {
        return taskCenterService.addProblem(taskCenterProblemVo);
    }

    /**
     * 查询将来任务详情
     */
    @RequestMapping(value = "/futureTaskDetail")
    @ResponseBody
    public Result futureTaskDetail(long taskId) {
        return taskCenterService.futureTaskDetail(taskId);
    }

    /**
     * 查询检查项下的问题
     */
    @RequestMapping(value = "/checkItem/problemList")
    @ResponseBody
    public Result checkItemProblemList(long taskCenterCheckItemId) {
        return taskCenterService.checkItemProblemList(taskCenterCheckItemId);
    }

    /**
     * 上报倾斜数据
     */
    @RequestMapping(value = "/reportSlantData")
    @ResponseBody
    public Result reportSlantData(TaskCenterSlantVo vo) {
        return taskCenterService.reportSlantData(vo);
    }

    /**
     * 上报裂缝数据
     */
    @RequestMapping(value = "/reportCrackData")
    @ResponseBody
    public Result reportCrackData(TaskCenterCrackVo vo) {
        return taskCenterService.reportCrackData(vo);
    }

    /**
     * 删除裂缝数据
     */
    @RequestMapping(value = "/delCrackData")
    @ResponseBody
    public Result delCrackData(long id) {
        return taskCenterService.delCrackData(id);
    }

    /**
     * 上报沉降数据
     */
    @RequestMapping(value = "/reportSubsidenceData")
    @ResponseBody
    public Result reportSubsidenceData(int pointType, long taskId, long taskCenterCheckItemId, String jsonData) {
        return taskCenterService.reportSubsidenceData(pointType, taskId, taskCenterCheckItemId, jsonData);
    }

    /**
     * 删除沉降数据
     */
    @RequestMapping(value = "/delSubsidenceData")
    @ResponseBody
    public Result delSubsidenceData(long id) {
        return taskCenterService.delSubsidenceData(id);
    }

    /**
     * 查询检查项 倾斜、裂缝、沉降 数据
     */
    @RequestMapping(value = "/getItemProData")
    @ResponseBody
    public Result getItemProData(long taskId, long taskCenterCheckItemId) {
        return Result.success(taskCenterService.getItemProData(taskId, taskCenterCheckItemId));
    }

}