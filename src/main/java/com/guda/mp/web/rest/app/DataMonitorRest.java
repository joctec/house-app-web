package com.guda.mp.web.rest.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.app.core.base.BaseController;
import com.app.core.common.Result;
import com.guda.mp.service.DataMonitorService;
import com.guda.mp.vo.SensorDataVo;
import com.guda.mp.vo.SensorSlantDayVo;

@Controller
@RequestMapping(value = "/api/dataMonitor")
@SuppressWarnings("rawtypes")
public class DataMonitorRest extends BaseController {

    @Autowired
    private DataMonitorService dataMonitorService;

    /** 获取房屋下的传感器 */
    @RequestMapping(value = "/getAllDevice")
    @ResponseBody
    public Result getAllDevice(long mtargetId) {
        return Result.success(dataMonitorService.getAllDevice(mtargetId));
    }

    /** 获取倾斜率图表数据 */
    @RequestMapping(value = "/slantChartData")
    @ResponseBody
    public Result slantChartData(SensorDataVo vo) {
        return Result.success(dataMonitorService.slantChartData(vo));
    }

    /** 获取倾斜率图表数据 */
    @RequestMapping(value = "/slantDayChartData")
    @ResponseBody
    public Result slantDayChartData(SensorSlantDayVo vo) {
        return Result.success(dataMonitorService.slantDayChartData(vo));
    }

    // /** 获取静力水准仪图表数据 */
    // @RequestMapping(value = "/waterLevelChartData")
    // @ResponseBody
    // public Result waterLevelChartData(SensorDataVo vo) {
    // return dataMonitorService.waterLevelChartData(vo);
    // }
    //
    // /** 获取位移计图表数据 */
    // @RequestMapping(value = "/displacementChartData")
    // @ResponseBody
    // public Result displacementChartData(SensorDataVo vo) {
    // return dataMonitorService.getSensorChartData(vo);
    // }
    //
    // /** 获取表面应力计图表数据 */
    // @RequestMapping(value = "/strainChartData")
    // @ResponseBody
    // public Result strainChartData(SensorDataVo vo) {
    // return dataMonitorService.getSensorChartData(vo);
    // }

}
