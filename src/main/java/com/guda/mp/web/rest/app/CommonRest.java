package com.guda.mp.web.rest.app;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.core.base.BaseController;
import com.app.core.common.Result;
import com.app.core.util.SysConfig;

@RestController
@RequestMapping(value = "/api")
@SuppressWarnings("rawtypes")
public class CommonRest extends BaseController {

    /**
     * get app version
     */
    @RequestMapping(value = "/appVersion")
    @ResponseBody
    public Result appVersion() {
        Map<String, String> data = new HashMap<String, String>();
        data.put("version", SysConfig.get("phone.app.version"));
        data.put("url", SysConfig.get("phone.app.android.url"));

        return Result.success(data);
    }

}