package com.guda.mp.web.rest.app;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.app.core.base.BaseController;
import com.app.core.base.PageQuery;
import com.app.core.common.Result;
import com.app.core.web.util.PageUtil;
import com.guda.mp.service.HouseService;
import com.guda.mp.vo.HouseVo;

@RestController
@RequestMapping(value = "/api/house")
@SuppressWarnings("rawtypes")
public class HouseRest extends BaseController {

    @Autowired
    private HouseService houseService;

    /**
     * 房屋列表
     */
    @RequestMapping(value = "/list")
    @ResponseBody
    public Result houseList(PageQuery pageQuery, HouseVo query) {
        return Result.success(PageUtil.convertPage(houseService.houseList(pageQuery, query)));
    }
    /**
     * 房屋列表
     */
    @RequestMapping(value = "/list",params = "version2")
    @ResponseBody
    public Result houseListVersion2(PageQuery pageQuery, HouseVo query) {
        return Result.success(PageUtil.convertPage(houseService.houseListVersion2(pageQuery, query)));
    }
    /**
     * 绑定IC卡成功
     */
    @RequestMapping(value = "/bindCardSuccess")
    @ResponseBody
    public Result bindCardSuccess(Long targetId) {
        return Result.success(houseService.bindCardSuccess(targetId));
    }

    /**
     * 房屋详情
     */
    @RequestMapping(value = "/detail")
    @ResponseBody
    public Result houseDetail(long monitorTargetId) {
        return Result.success(houseService.houseDetail(monitorTargetId));
    }

    /**
     * 房屋问题列表
     */
    @RequestMapping(value = "/problemList")
    @ResponseBody
    public Result houseProblemList(long monitorTargetId) {
        return Result.success(houseService.houseProblemList(monitorTargetId));
    }

    /**
     * 巡检记录列表
     */
    @RequestMapping(value = "/inspectionRecord")
    @ResponseBody
    public Result inspectionRecordList(long monitorTargetId) {
        return Result.success(houseService.inspectionRecordList(monitorTargetId));
    }

    /**
     * 更新房屋地址坐标
     */
    @RequestMapping(value = "/updateLocaion")
    @ResponseBody
    public Result updateLocaion(long monitorTargetId, Double lat, Double lng) {
        return houseService.updateLocaion(monitorTargetId, lat, lng);
    }

    /**
     * 房屋地图
     */
    @RequestMapping(value = "/map/data")
    @ResponseBody
    public Result houseMapData(HouseVo bean) {
        return Result.success(houseService.selectBizList(bean));
    }

    /**
     * 更新房屋图片
     */
    @RequestMapping(value = "/updateHouseImg")
    @ResponseBody
    public Result updateHouseImg(Long monitorTargetId, String houseImg) {
        return houseService.updateHouseImg(monitorTargetId, houseImg);
    }

}