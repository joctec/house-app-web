package com.guda.mp.utils;

import org.apache.commons.lang3.StringUtils;

import com.app.common.enums.H5ClientEnum;

public class ItemUitl {
	/**
	 * 判断商品是否有折扣
	 * */
	public static boolean isHasDiscount(Double discountPrice, Double retailPrice) {
		if (discountPrice != null && retailPrice != null && discountPrice > 0 && discountPrice.doubleValue() < retailPrice.doubleValue()) {
			return true;
		}

		return false;
	}

	/**
	 * 获取商品实际价格
	 * */
	public static Double getActualPrice(Double discountPrice, Double retailPrice) {
		if (isHasDiscount(discountPrice, retailPrice)) {
			return discountPrice;
		}

		return retailPrice;
	}

	/**
	 * 判断商品是否支持此渠道
	 * */
	public static boolean isSupportChannel(H5ClientEnum channel, String notSupportChannel) {
		if (StringUtils.isBlank(notSupportChannel)) {
			return true;
		}

		String[] split = notSupportChannel.split(",");
		for (String one : split) {
			if (channel.getCode().toString().equals(one)) {
				return false;
			}
		}

		return true;
	}

	/**
	 * 判断商品支持的渠道
	 * */
	// public static String getSupportChannel(H5ClientEnum channel, String
	// notSupportChannel) {
	// if (StringUtils.isBlank(notSupportChannel)) {
	// return "All";
	// }
	//
	// String channelMsg = "";
	// String[] split = notSupportChannel.split(",");
	// for (String one : split) {
	// if (!channel.getCode().toString().equals(one)) {
	// }
	// }
	//
	// return true;
	// }
}
