package com.guda.mp.vo;

import com.guda.mp.entity.AddressHierarchy;

/** 
* ClassName: AddressHierarchy <br/>
* Description: 地址层级表 <br/>
*/ 
public class AddressHierarchyVo extends AddressHierarchy{
	private static final long serialVersionUID = 1L;
	
	/**
	 * 省id
	 */
    private Long provinceId;
	/**
	 * 省名字
	 */
    private String provinceName;
    /**
	 * 市id
	 */
    private Long cityId;
	/**
	 * 市名字
	 */
    private String cityName;
    /**
	 * 区id
	 */
    private Long areaId;
	/**
	 * 区名字
	 */
    private String areaName;
	public Long getProvinceId() {
		return provinceId;
	}
	public void setProvinceId(Long provinceId) {
		this.provinceId = provinceId;
	}
	public String getProvinceName() {
		return provinceName;
	}
	public void setProvinceName(String provinceName) {
		this.provinceName = provinceName;
	}
	public Long getCityId() {
		return cityId;
	}
	public void setCityId(Long cityId) {
		this.cityId = cityId;
	}
	public String getCityName() {
		return cityName;
	}
	public void setCityName(String cityName) {
		this.cityName = cityName;
	}
	public Long getAreaId() {
		return areaId;
	}
	public void setAreaId(Long areaId) {
		this.areaId = areaId;
	}
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	
    
    
    
}