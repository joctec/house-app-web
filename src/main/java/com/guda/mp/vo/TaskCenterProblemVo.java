package com.guda.mp.vo;

import com.guda.mp.entity.TaskCenterProblem;

/**
 * ClassName: TaskCenterProblem <br/>
 * Description: 任务-检查问题 <br/>
 */
public class TaskCenterProblemVo extends TaskCenterProblem {

    private static final long serialVersionUID = 1L;

    private Long countNum;

    private Long monitorTargetId;

    public Long getCountNum() {
        return countNum;
    }

    public void setCountNum(Long countNum) {
        this.countNum = countNum;
    }

    public Long getMonitorTargetId() {
        return monitorTargetId;
    }

    public void setMonitorTargetId(Long monitorTargetId) {
        this.monitorTargetId = monitorTargetId;
    }
}