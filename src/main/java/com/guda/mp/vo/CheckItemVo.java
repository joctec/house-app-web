package com.guda.mp.vo;

import com.guda.mp.entity.CheckItem;

/**
 * ClassName: CheckItem <br/>
 * Description: 检查项 <br/>
 */
public class CheckItemVo extends CheckItem {

    private static final long serialVersionUID = 1L;

    /** 检查包名称 */
    private String checkPackageName;

    public String getCheckPackageName() {
        return checkPackageName;
    }

    public void setCheckPackageName(String checkPackageName) {
        this.checkPackageName = checkPackageName;
    }

}