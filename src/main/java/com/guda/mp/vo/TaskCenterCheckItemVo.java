package com.guda.mp.vo;

import java.util.List;

import com.guda.mp.entity.TaskCenterCheckItem;

/**
 * ClassName: TaskCenterCheckItem <br/>
 * Description: 任务-检查项 <br/>
 */
public class TaskCenterCheckItemVo extends TaskCenterCheckItem {

    private static final long serialVersionUID = 1L;

    private Integer type;

    private List<TaskCenterProblemVo> problemList;

    /** 倾斜数据 */
    private TaskCenterSlantVo slantData;
    /** 裂缝数据 */
    private List<TaskCenterCrackVo> crackData;
    /** 沉降数据 */
    private List<TaskCenterSubsidenceVo> subsidenceData;

    public List<TaskCenterProblemVo> getProblemList() {
        return problemList;
    }

    public void setProblemList(List<TaskCenterProblemVo> problemList) {
        this.problemList = problemList;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public TaskCenterSlantVo getSlantData() {
        return slantData;
    }

    public void setSlantData(TaskCenterSlantVo slantData) {
        this.slantData = slantData;
    }

    public List<TaskCenterCrackVo> getCrackData() {
        return crackData;
    }

    public void setCrackData(List<TaskCenterCrackVo> crackData) {
        this.crackData = crackData;
    }

    public List<TaskCenterSubsidenceVo> getSubsidenceData() {
        return subsidenceData;
    }

    public void setSubsidenceData(List<TaskCenterSubsidenceVo> subsidenceData) {
        this.subsidenceData = subsidenceData;
    }

}