package com.guda.mp.vo;

import java.util.List;

import com.guda.mp.entity.InspectionRecord;

/**
 * ClassName: InspectionRecord <br/>
 * Description: 巡检-巡检记录 <br/>
 */
public class InspectionRecordVo extends InspectionRecord {

    private static final long serialVersionUID = 1L;

    private String address;

    // 附件list
    private List<SysAttachVo> attachList;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<SysAttachVo> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<SysAttachVo> attachList) {
        this.attachList = attachList;
    }

}