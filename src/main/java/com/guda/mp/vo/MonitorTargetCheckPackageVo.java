package com.guda.mp.vo;

import com.guda.mp.entity.MonitorTargetCheckPackage;

/**
 * ClassName: MonitorTargetCheckPackage <br/>
 * Description: <br/>
 */
public class MonitorTargetCheckPackageVo extends MonitorTargetCheckPackage {

    private static final long serialVersionUID = 1L;

    private Long countNum;

    public Long getCountNum() {
        return countNum;
    }

    public void setCountNum(Long countNum) {
        this.countNum = countNum;
    }

}