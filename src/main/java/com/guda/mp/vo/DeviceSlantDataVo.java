package com.guda.mp.vo;

import java.util.Date;

import com.guda.mp.entity.DeviceSlantData;

/**
 * ClassName: DeviceSlantData <br/>
 * Description: 设备倾斜数据 <br/>
 */
public class DeviceSlantDataVo extends DeviceSlantData {

    private static final long serialVersionUID = 1L;

    private Date startTime;
    private Date endTime;

    private String mtargetName;
    private String mtargetAddress;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getMtargetName() {
        return mtargetName;
    }

    public void setMtargetName(String mtargetName) {
        this.mtargetName = mtargetName;
    }

    public String getMtargetAddress() {
        return mtargetAddress;
    }

    public void setMtargetAddress(String mtargetAddress) {
        this.mtargetAddress = mtargetAddress;
    }

}