package com.guda.mp.vo;

import java.io.Serializable;
import java.util.Date;

/**
 * ClassName: SensorData <br/>
 * Description: <br/>
 */
public class DeviceDataTransferVo implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 项目编码 */
    private String projectCode;
    /** 设备编码 */
    private String deviceSn;
    /** 传感器编码 */
    private String sensorSn;
    /** 功能类型 */
    private String funType;

    /** 数据时间 */
    private Date dataTime;
    /** 数据时间long数字型 */
    private Long dataTimeLong;
    /** 数据值 */
    private Double val;
    /** 创建时间-同步时间 */
    private Date createTime;

    private Date startTime;
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public String getDeviceSn() {
        return deviceSn;
    }

    public void setDeviceSn(String deviceSn) {
        this.deviceSn = deviceSn;
    }

    public String getSensorSn() {
        return sensorSn;
    }

    public void setSensorSn(String sensorSn) {
        this.sensorSn = sensorSn;
    }

    public String getFunType() {
        return funType;
    }

    public void setFunType(String funType) {
        this.funType = funType;
    }

    public Date getDataTime() {
        return dataTime;
    }

    public void setDataTime(Date dataTime) {
        this.dataTime = dataTime;
    }

    public Long getDataTimeLong() {
        return dataTimeLong;
    }

    public void setDataTimeLong(Long dataTimeLong) {
        this.dataTimeLong = dataTimeLong;
    }

    public Double getVal() {
        return val;
    }

    public void setVal(Double val) {
        this.val = val;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

}