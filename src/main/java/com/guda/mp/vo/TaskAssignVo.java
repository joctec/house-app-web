package com.guda.mp.vo;

import java.util.Map;

import com.guda.mp.entity.TaskAssign;

/**
 * ClassName: TaskAssign <br/>
 * Description: 巡检-巡检任务 <br/>
 */
public class TaskAssignVo extends TaskAssign {

    private static final long serialVersionUID = 1L;

    private String selectHouseJsonData;

    /** 问题数量map，map的key是问题等级， value是数量 */
    private Map<String, Long> problemCountMap;

    public String getSelectHouseJsonData() {
        return selectHouseJsonData;
    }

    public void setSelectHouseJsonData(String selectHouseJsonData) {
        this.selectHouseJsonData = selectHouseJsonData;
    }

    public Map<String, Long> getProblemCountMap() {
        return problemCountMap;
    }

    public void setProblemCountMap(Map<String, Long> problemCountMap) {
        this.problemCountMap = problemCountMap;
    }

}