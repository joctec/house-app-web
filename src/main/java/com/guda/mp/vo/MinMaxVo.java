package com.guda.mp.vo;

import java.io.Serializable;

public class MinMaxVo implements Serializable {

    private static final long serialVersionUID = 1L;
    private Long min;
    private Long max;
    private String type;

    public Long getMin() {
        return min;
    }

    public void setMin(Long min) {
        this.min = min;
    }

    public Long getMax() {
        return max;
    }

    public void setMax(Long max) {
        this.max = max;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
