package com.guda.mp.vo;

import java.util.Date;

import com.guda.mp.entity.SensorData;

/** 
* ClassName: SensorData <br/>
* Description:  <br/>
*/ 
public class SensorDataVo extends SensorData{
	private static final long serialVersionUID = 1L;

    private Date startTime;
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}