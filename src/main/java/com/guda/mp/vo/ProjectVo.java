package com.guda.mp.vo;

import com.guda.mp.entity.Project;

/**
 * ClassName: Project <br/>
 * Description: 项目 <br/>
 */
public class ProjectVo extends Project {

    private static final long serialVersionUID = 1L;

    private String contractName;

    public String getContractName() {
        return contractName;
    }

    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

}