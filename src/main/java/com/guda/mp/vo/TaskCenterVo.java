package com.guda.mp.vo;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.guda.mp.entity.TaskCenter;

/**
 * ClassName: TaskCenter <br/>
 * Description: 任务 <br/>
 */
public class TaskCenterVo extends TaskCenter {

    private static final long serialVersionUID = 1L;

    /** 问题数量 严重 */
    private Integer problemLevel1Nums;
    /** 问题数量 中等 */
    private Integer problemLevel2Nums;
    /** 问题数量 一般 */
    private Integer problemLevel3Nums;

    /** 问题数量list，map的key是问题等级， value是数量 */
    private Map<String, Long> problemCountMap;

    /** 任务检查包 */
    private List<CheckPackageVo> checkPackageList;

    /** 已延迟天数 */
    private Integer delayDay;

    /** 下次执行时间 */
    private Date nextTaskDate;

    /** 检查包数量 */
    private Long checkPackageCount;

    /** 房屋地址 */
    private String hosueAddress;
    /** 房屋地址 */
    private Double lng;
    /** 房屋地址 */
    private Double lat;

    /** 关联问题 */
    private List<TaskCenterProblemVo> problemList;

    /** 是否绑定IC卡 */
    private Integer bindCardFlag;
    /** 房屋卡片uuid */
    private String dataUuid;

    public Integer getProblemLevel1Nums() {
        return problemLevel1Nums;
    }

    public void setProblemLevel1Nums(Integer problemLevel1Nums) {
        this.problemLevel1Nums = problemLevel1Nums;
    }

    public Integer getProblemLevel2Nums() {
        return problemLevel2Nums;
    }

    public void setProblemLevel2Nums(Integer problemLevel2Nums) {
        this.problemLevel2Nums = problemLevel2Nums;
    }

    public Integer getProblemLevel3Nums() {
        return problemLevel3Nums;
    }

    public void setProblemLevel3Nums(Integer problemLevel3Nums) {
        this.problemLevel3Nums = problemLevel3Nums;
    }

    public List<CheckPackageVo> getCheckPackageList() {
        return checkPackageList;
    }

    public void setCheckPackageList(List<CheckPackageVo> checkPackageList) {
        this.checkPackageList = checkPackageList;
    }

    public Integer getDelayDay() {
        return delayDay;
    }

    public void setDelayDay(Integer delayDay) {
        this.delayDay = delayDay;
    }

    public Date getNextTaskDate() {
        return nextTaskDate;
    }

    public void setNextTaskDate(Date nextTaskDate) {
        this.nextTaskDate = nextTaskDate;
    }

    public Long getCheckPackageCount() {
        return checkPackageCount;
    }

    public void setCheckPackageCount(Long checkPackageCount) {
        this.checkPackageCount = checkPackageCount;
    }

    public List<TaskCenterProblemVo> getProblemList() {
        return problemList;
    }

    public void setProblemList(List<TaskCenterProblemVo> problemList) {
        this.problemList = problemList;
    }

    public String getHosueAddress() {
        return hosueAddress;
    }

    public void setHosueAddress(String hosueAddress) {
        this.hosueAddress = hosueAddress;
    }

    public Map<String, Long> getProblemCountMap() {
        return problemCountMap;
    }

    public void setProblemCountMap(Map<String, Long> problemCountMap) {
        this.problemCountMap = problemCountMap;
    }

    public Integer getBindCardFlag() {
        return bindCardFlag;
    }

    public void setBindCardFlag(Integer bindCardFlag) {
        this.bindCardFlag = bindCardFlag;
    }

    public String getDataUuid() {
        return dataUuid;
    }

    public void setDataUuid(String dataUuid) {
        this.dataUuid = dataUuid;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

}