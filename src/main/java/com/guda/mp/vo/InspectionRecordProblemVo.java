package com.guda.mp.vo;

import java.util.Date;
import java.util.List;

import com.guda.mp.entity.InspectionRecordProblem;

/**
 * ClassName: InspectionRecordProblem <br/>
 * Description: 巡检-巡检记录问题 <br/>
 */
public class InspectionRecordProblemVo extends InspectionRecordProblem {

    private static final long serialVersionUID = 1L;

    private Date startTime;
    private Date endTime;
    private Double lng;
    private Double lat;

    // 附件list
    private List<SysAttachVo> attachList;

    public List<SysAttachVo> getAttachList() {
        return attachList;
    }

    public void setAttachList(List<SysAttachVo> attachList) {
        this.attachList = attachList;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Double getLng() {
        return lng;
    }

    public void setLng(Double lng) {
        this.lng = lng;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

}