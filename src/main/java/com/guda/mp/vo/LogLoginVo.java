package com.guda.mp.vo;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.guda.mp.entity.LogLogin;

/** 
* ClassName: LogLogin <br/>
* Description:  <br/>
*/ 
public class LogLoginVo extends LogLogin{
	private static final long serialVersionUID = 1L;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date endTime;
    public Date getStartTime() {
        return startTime;
    }
    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
    public Date getEndTime() {
        return endTime;
    }
    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }
}