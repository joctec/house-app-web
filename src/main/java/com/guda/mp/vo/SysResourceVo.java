package com.guda.mp.vo;

import java.util.List;

import com.guda.mp.entity.SysResource;

/** 
* ClassName: SysResource <br/>
* Description: 用户登录账号表 <br/>
*/ 
public class SysResourceVo extends SysResource{
	private static final long serialVersionUID = 1L;
	
	private List<SysResourceVo> childrens;

	public List<SysResourceVo> getChildrens() {
		return childrens;
	}

	public void setChildrens(List<SysResourceVo> childrens) {
		this.childrens = childrens;
	}
}