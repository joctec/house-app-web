package com.guda.mp.vo;

import java.util.Map;

import com.guda.mp.entity.House;

/**
 * ClassName: House <br/>
 * Description: 房屋信息 <br/>
 */
public class HouseVo extends House {

    private static final long serialVersionUID = 1L;

    private String dataUuid;
    /**
     * 绑卡标志1.绑卡0未绑卡
     */
    private Integer bindCardFlag;

    private String checkPackageIds;

    private String projectName;

    private Long userId;
    /**
     * 监测目标id
     */
    private Long monitorTargetId;

    /** 问题数量map，map的key是问题等级， value是数量 */
    private Map<String, Long> problemCountMap;

    /**
     * 结构类型
     */
    private String structTypeName;
    /**
     * 土地性质
     */
    private String landNatureName;
    /**
     * 产权性质
     */
    private String ownNatureName;
    /**
     * 基础情况类型
     */
    private String baseTypeInfoName;
    /**
     * 房屋场地类型
     */
    private String housePlaceTypeName;
    /**
     * 化学侵蚀类型
     */
    private String chemicalAttackTypeName;
    /**
     * 结构拆改类型
     */
    private String changeTypeName;
    /**
     * 加层改造类型
     */
    private String addLayerTypeName;
    /**
     * 修缮类型
     */
    private String repairTypeName;
    /**
     * 历史灾害类型
     */
    private String historicalDisasterTypeName;
    /**
     * 使用功能变更类型
     */
    private String useFuncChangeTypeName;
    /**
     * 鉴定情况类型
     */
    private String appraisalTypeName;
    /**
     * 图纸资料类型
     */
    private String drawingDataTypeName;
    /**
     * 鉴定等级
     */
    private String appraisalLevelName;

    public String getCheckPackageIds() {
        return checkPackageIds;
    }

    public void setCheckPackageIds(String checkPackageIds) {
        this.checkPackageIds = checkPackageIds;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getMonitorTargetId() {
        return monitorTargetId;
    }

    public void setMonitorTargetId(Long monitorTargetId) {
        this.monitorTargetId = monitorTargetId;
    }

    public Map<String, Long> getProblemCountMap() {
        return problemCountMap;
    }

    public void setProblemCountMap(Map<String, Long> problemCountMap) {
        this.problemCountMap = problemCountMap;
    }

    public String getStructTypeName() {
        return structTypeName;
    }

    public void setStructTypeName(String structTypeName) {
        this.structTypeName = structTypeName;
    }

    public String getLandNatureName() {
        return landNatureName;
    }

    public void setLandNatureName(String landNatureName) {
        this.landNatureName = landNatureName;
    }

    public String getOwnNatureName() {
        return ownNatureName;
    }

    public void setOwnNatureName(String ownNatureName) {
        this.ownNatureName = ownNatureName;
    }

    public String getBaseTypeInfoName() {
        return baseTypeInfoName;
    }

    public void setBaseTypeInfoName(String baseTypeInfoName) {
        this.baseTypeInfoName = baseTypeInfoName;
    }

    public String getHousePlaceTypeName() {
        return housePlaceTypeName;
    }

    public void setHousePlaceTypeName(String housePlaceTypeName) {
        this.housePlaceTypeName = housePlaceTypeName;
    }

    public String getChemicalAttackTypeName() {
        return chemicalAttackTypeName;
    }

    public void setChemicalAttackTypeName(String chemicalAttackTypeName) {
        this.chemicalAttackTypeName = chemicalAttackTypeName;
    }

    public String getChangeTypeName() {
        return changeTypeName;
    }

    public void setChangeTypeName(String changeTypeName) {
        this.changeTypeName = changeTypeName;
    }

    public String getAddLayerTypeName() {
        return addLayerTypeName;
    }

    public void setAddLayerTypeName(String addLayerTypeName) {
        this.addLayerTypeName = addLayerTypeName;
    }

    public String getRepairTypeName() {
        return repairTypeName;
    }

    public void setRepairTypeName(String repairTypeName) {
        this.repairTypeName = repairTypeName;
    }

    public String getHistoricalDisasterTypeName() {
        return historicalDisasterTypeName;
    }

    public void setHistoricalDisasterTypeName(String historicalDisasterTypeName) {
        this.historicalDisasterTypeName = historicalDisasterTypeName;
    }

    public String getUseFuncChangeTypeName() {
        return useFuncChangeTypeName;
    }

    public void setUseFuncChangeTypeName(String useFuncChangeTypeName) {
        this.useFuncChangeTypeName = useFuncChangeTypeName;
    }

    public String getAppraisalTypeName() {
        return appraisalTypeName;
    }

    public void setAppraisalTypeName(String appraisalTypeName) {
        this.appraisalTypeName = appraisalTypeName;
    }

    public String getDrawingDataTypeName() {
        return drawingDataTypeName;
    }

    public void setDrawingDataTypeName(String drawingDataTypeName) {
        this.drawingDataTypeName = drawingDataTypeName;
    }

    public String getAppraisalLevelName() {
        return appraisalLevelName;
    }

    public void setAppraisalLevelName(String appraisalLevelName) {
        this.appraisalLevelName = appraisalLevelName;
    }

    public String getDataUuid() {
        return dataUuid;
    }

    public void setDataUuid(String dataUuid) {
        this.dataUuid = dataUuid;
    }

    public Integer getBindCardFlag() {
        return bindCardFlag;
    }

    public void setBindCardFlag(Integer bindCardFlag) {
        this.bindCardFlag = bindCardFlag;
    }

}