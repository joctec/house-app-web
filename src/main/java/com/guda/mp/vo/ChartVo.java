package com.guda.mp.vo;

import java.io.Serializable;
import java.util.List;

/**
 * ClassName: DeviceSlantData <br/>
 * Description: 设备倾斜数据 <br/>
 */
public class ChartVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private List<Object> allData;

    private List<?> xData;
    private List<?> yData;

    public List<Object> getAllData() {
        return allData;
    }

    public void setAllData(List<Object> allData) {
        this.allData = allData;
    }

    public List<?> getxData() {
        return xData;
    }

    public void setxData(List<?> xData) {
        this.xData = xData;
    }

    public List<?> getyData() {
        return yData;
    }

    public void setyData(List<?> yData) {
        this.yData = yData;
    }

}