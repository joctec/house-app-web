package com.guda.mp.vo;

import java.util.List;

import com.guda.mp.entity.CheckProblem;

/**
 * ClassName: CheckProblem <br/>
 * Description: 检查异常问题 <br/>
 */
public class CheckProblemVo extends CheckProblem {

    private static final long serialVersionUID = 1L;

    private List<CheckProblemVo> childList;

    public CheckProblemVo() {

    }

    public CheckProblemVo(Long id, Long pid, String name, String descr) {
        super.setId(id);
        super.setPid(pid);
        super.setName(name);
        super.setDescr(descr);
    }

    public List<CheckProblemVo> getChildList() {
        return childList;
    }

    public void setChildList(List<CheckProblemVo> childList) {
        this.childList = childList;
    }
}