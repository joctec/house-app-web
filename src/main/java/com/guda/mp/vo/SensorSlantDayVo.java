package com.guda.mp.vo;

import java.util.Date;

import com.guda.mp.entity.SensorSlantDay;

/**
 * ClassName: SensorSlantDay <br/>
 * Description: <br/>
 */
public class SensorSlantDayVo extends SensorSlantDay {

    private static final long serialVersionUID = 1L;

    private Date startTime;
    private Date endTime;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

}