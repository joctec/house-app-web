package com.guda.mp.vo;

import com.guda.mp.entity.SysRole;

/** 
* ClassName: SysRole <br/>
* Description: 用户登录账号表 <br/>
*/ 
public class SysRoleVo extends SysRole{
	private static final long serialVersionUID = 1L;
	
	private String ids;
	private String resourceIds;

	public String getResourceIds() {
		return resourceIds;
	}

	public void setResourceIds(String resourceIds) {
		this.resourceIds = resourceIds;
	}

	public String getIds() {
		return ids;
	}

	public void setIds(String ids) {
		this.ids = ids;
	}
}