package com.guda.mp.vo;

import java.util.List;

import com.guda.mp.entity.MonitorTarget;

/**
 * ClassName: MonitorTarget <br/>
 * Description: 监测目标 <br/>
 */
public class MonitorTargetVo extends MonitorTarget {

    private static final long serialVersionUID = 1L;

    private String checkPackageIds;

    private HouseVo house;

    private List<HouseVo> houseList;

    public String getCheckPackageIds() {
        return checkPackageIds;
    }

    public void setCheckPackageIds(String checkPackageIds) {
        this.checkPackageIds = checkPackageIds;
    }

    public HouseVo getHouse() {
        return house;
    }

    public void setHouse(HouseVo house) {
        this.house = house;
    }

    public List<HouseVo> getHouseList() {
        return houseList;
    }

    public void setHouseList(List<HouseVo> houseList) {
        this.houseList = houseList;
    }

}