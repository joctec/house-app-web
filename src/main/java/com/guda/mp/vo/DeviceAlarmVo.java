package com.guda.mp.vo;

import java.util.Date;

import com.guda.mp.entity.DeviceAlarm;

/**
 * ClassName: DeviceAlarm <br/>
 * Description: <br/>
 */
public class DeviceAlarmVo extends DeviceAlarm {

    private static final long serialVersionUID = 1L;

    private Date startTime;
    private Date endTime;

    private String mtargetName;

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public String getMtargetName() {
        return mtargetName;
    }

    public void setMtargetName(String mtargetName) {
        this.mtargetName = mtargetName;
    }
}