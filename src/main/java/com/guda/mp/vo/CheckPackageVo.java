package com.guda.mp.vo;

import java.util.List;

import com.guda.mp.entity.CheckPackage;

/**
 * ClassName: CheckPackage <br/>
 * Description: 检查包，检查项的集合 <br/>
 */
public class CheckPackageVo extends CheckPackage {

    private static final long serialVersionUID = 1L;

    private List<CheckItemVo> checkItemList;

    private List<TaskCenterCheckItemVo> taskCenterCheckItemList;

    public List<CheckItemVo> getCheckItemList() {
        return checkItemList;
    }

    public void setCheckItemList(List<CheckItemVo> checkItemList) {
        this.checkItemList = checkItemList;
    }

    public List<TaskCenterCheckItemVo> getTaskCenterCheckItemList() {
        return taskCenterCheckItemList;
    }

    public void setTaskCenterCheckItemList(List<TaskCenterCheckItemVo> taskCenterCheckItemList) {
        this.taskCenterCheckItemList = taskCenterCheckItemList;
    }
}