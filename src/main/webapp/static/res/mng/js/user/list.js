var organCode="";
var grid;
$(function(){
    /**
     * 加载组织列表
    */
    grid=$("#list").grid({
		cols:col
		,funcs:funcs
		,param:{all:0,isDel:0}
		,url :base+"/admin/user/list/json"
	});
	
	//搜索
	$("#searchbtn").click(function(){
		grid.addParam({
			status:$("#status").val(),
			userName:$("#userName").val()
		});
	});
	
});

function exportXls(){
	var _organCode="";
	var _status=$("#status").val();
	//获取左侧树选择的党组织
	var node=organTree.getSelectedNodes();
	if(node.length==1){
		_organCode=node[0].id;
	}
	window.location.href=base+"/admin/user/export?organCode="+_organCode+"&status="+_status;
}

var col=[
	{
		name:"登录名"
	 	,code:"loginName"
	 	,width:'200px'
	},{
		name:"党组织名称"
	 	,code:"userName"
	 	,width:'40%'
	},{
		name:"最近登陆时间"
	 	,code:"lastLoginTime"
	 	,width:'200px'
	},{
		name:"账号状态"
	 	,code:"status"
	 	,width:'80px'
	 	,render:function(val){
	 		if(val=="1"){
	 			return "正常";
	 		}else if(val=="2"){
	 			return "<span class='red'>锁定</span>";
	 		}else if(val=="3"){
	 			return "<span class='red'>禁用</span>";
	 		}
	 	}
	}
];

var funcs=[{
		name:"禁用账号"
		,ajax:true
		,confirm:true
		,confirmTxt:"您确认要禁用该账户？"
		,url:base+'/admin/user/updateStatus?status=3'
		,callback:function(result){
			if(result.code=="200"){
				$.ui.alert("禁用成功!");
				grid.reload();
			}else{
				$.ui.alert("禁用失败!");
			}
		}
		,show:function(row){
			if(row.status==1){
				return true;
			}
		}
	},{
		name:"恢复禁用"
		,ajax:true
		,url:base+'/admin/user/updateStatus?status=1'
		,callback:function(result){
			if(result.code=="200"){
				$.ui.alert("恢复禁用成功!");
				grid.reload();
			}else{
				$.ui.alert("恢复禁用失败!");
			}
		},show:function(row){
			if(row.status==3){
				return true;
			}
		}
	},{
		name:"重置密码"
		,ajax:true
		,confirm:true
		,confirmTxt:"您确认重置该用户的密码？"
		,url:base+'/admin/user/repassword'
		,callback:function(result){
			if(result.code=="200"){
				$.ui.alert(result.msg);
				grid.reload();
			}else{
				$.ui.alert("密码重置失败!");
			}
		}
	}
];
