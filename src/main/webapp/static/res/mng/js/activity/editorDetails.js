function getRequest(name){
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i");
    var r = window.location.search.substr(1).match(reg);
    if(null != r){
    	return (r[2]); 
    }
    return null;
}
var id = getRequest("id");
var ue = UE.getEditor('editor');

/**
 * 更新活动详情方法
 */
function update(){
	$.ajax({
	    type: 'POST',
	    url: base+"/admin/activityInfo/update/save" ,
	    data: {id:id,content:ue.getContent()} ,
	    dataType:"json",
	    success: function(data){
	    	if(data.code==200){
	    		alert("更新成功");
	    		window.location.href=base+"/admin/activityInfo/details?id="+id;
	    	}
	    } ,
	    error : function() {  
    	} 
	});
}

$(function(){
	$.ajax({
	    type: 'POST',
	    url: base+"/admin/activityInfo/selectById" ,
	    data: {id:id} ,
	    success: function(data){
	    	console.log(data);
	    	if(data.code==200){
	    		ue.setContent(data.data.content);
	    	}
	    } ,
	    error : function() {  
    	} 
	});
})
