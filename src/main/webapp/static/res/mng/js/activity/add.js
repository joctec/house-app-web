/**
 * 获取参与党组织的名称和code
 * @param nodes
 */
function checkOrganCallBack(nodes) {
	var joinOrgansIds = [];
	var joinOrgansCodes = [];
	var joinOrgansNames = [];
	for (var i = 0; i < nodes.length; i++) {
		joinOrgansIds.push(nodes[i].organId);
		joinOrgansNames.push(nodes[i].name);
		joinOrgansCodes.push(nodes[i].code);
	}
	$("#joinOrgans").val(joinOrgansNames.join(","));
	$("#joinOrgansIds").val(joinOrgansIds.join(","));
	$("#joinOrgansCodes").val(joinOrgansCodes.join(","));
}
/**
 * 根据活动类型在活动类型下方显示不同的积分值并设置相关隐藏域的值，便于往后台传输数据（如果没选活动类型默认该区域隐藏）
 */
function setScore(){
	if($("#hdlx").find("option:selected").val()==-1){
		$("#displayScore").addClass("hide");
	}else{
		var score = $("#hdlx").find("option:selected").attr("scoreOrgan");
		$("#orgScore").html(score);
		$("#integralOrgan").val(score);
		$("#typeName").val($("#hdlx").find("option:selected").html());
		$("#displayScore").removeClass("hide");
	}
}
$(function(){
		//初始化表单验证
		$("#form").validateWrapper();
   		 //表单提交
   		 $("#submit").click(function(){
		 	if(!$("#form").valid()){
		 		return false;
		 	}
		 	$("#form").ajaxSubmit({
		 		dataType:"json",
		 		success:function(result){
		 			if(result.code==200){
		 				window.location.href=base+"/admin/activity/list";
		 			}else{
		 				$.ui.alert(result.msg);
		 			}
		 		}
		 	});
   		 });
	});