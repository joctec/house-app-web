var col=[{name:"党员名称"
		 ,code:"personName"
		 ,width:'10%'
	 },{
	 	name:"活动名称"
	 	,width:'25%'
	 	,code:"activityName"
	 },{name:"党员所在党组织名称"
		,width:'25%'
		,code:"orgName"
	},{name:"活动时间"
		,width:'20%'
		,code:"actTime"
	},{name:"签到时间"
		,width:'20%'
		,code:"createTime"
	}
];

var funcs=[
];

function getRequest(name){
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i");
    var r = window.location.search.substr(1).match(reg);
    if(null != r){
    	return (r[2]); 
    }
    return null;
}

$(function(){
	grid=$("#list").grid({
		cols:col
		,funcs:funcs
		,showfuns:false
		,url :base+"/admin/activity/memberSignInList/json"
		,param:{personId:getRequest("personId")}
	});
});