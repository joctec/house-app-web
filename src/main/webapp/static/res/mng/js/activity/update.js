$(function(){
	//初始化表单验证
	$("#form").validate({
        rules: {
            title: {
                required: true,
                minlength: 5
            },
            zhuti: {
            	required: true,
            	minlength: 5
            }
        }
	 });
	 //表单提交
	 $("#submit").click(function(){
	 	$("#form").mask("请稍候......");
	 	if(!$("#form").valid()){
	 		return false;
	 	}
	 	$("#form").ajaxSubmit({
	 		dataType:"json",
	 		success:function(result){
	 			if(result.code==200){
	 				$("#form").unmask();
	 				window.location.href= base+"/admin/activity/list";
	 			}
	 		}
	 	});
	 });
});
