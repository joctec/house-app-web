var col=[{name:"党员名称"
		 ,code:"personName"
		 ,width:'25%'
	 },{
	 	name:"党员所在党组织名称"
	 	,width:'55%'
	 	,code:"orgName"
	 },{name:"参与活动次数"
		,width:'20%'
		,code:"signInNum"
	}
];

var funcs=[
	{
		name:"查看详情"
		,url:base+"/admin/activityInfo/details"
		,click:function (row){
			window.location.href=base+"/admin/activity/memberSignInList?personId="+row.personId;
		}
	}
];

/**
 * 按条件查询数据方法（可根据起止时间、活动类型进行查询 ）
 */
function selectQuery(){
	var _orgName = $("#orgName").val();
	var _minNum = $("#minNum").val();
	var _maxNum = $("#maxNum").val();
	var _startTime = $("#startTime").val();
	var _endTime = $("#endTime").val();
	grid.addParam({orgName:_orgName,minNum:_minNum,maxNum:_maxNum,startTime:_startTime,endTime:_endTime});
}

$(function(){
	grid=$("#list").grid({
		cols:col
		,funcs:funcs
		,url :base+"/admin/activity/memberJoinSurvey/json"
	});
});