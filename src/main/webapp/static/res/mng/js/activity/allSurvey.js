$(function(){
	/**
	 * 隐藏  最新签到记录情况、活动开展本年排名、活动开展本月排名、活动签到本年排名、活动签到本月排名 的div
	 */
	$("#activitySignIn").css("display","none");
	$("#yearActivityRanking").css("display","none");
	$("#monthActivityRanking").css("display","none");
	$("#yearSignInRanking").css("display","none");
	$("#monthSignInRanking").css("display","none");
	/**
	 * 调用后台接口获取数据
	 */
	$.ajax({
	    type: 'POST',
	    url: base+"/admin/activity/mySurvey/json" ,
	    data: {selectActiveType:2,numSize:8} ,
	    dataType:"json",
	    success: function(resp){
	    	if(resp.code==200){
	    		$("#all").html(resp.data.all);
	    		$("#year").html(resp.data.year);
	    		$("#month").html(resp.data.month);
	    		$("#day").html(resp.data.day);
	    		addModelData(resp.data);
	    		dealPieChart(resp.data.activeType);
	    	}
	    } ,
	    error : function() {  
    	} 
	});
});

function addModelData(data){
	//添加最新活动开展情况数据
	var activeCondition = template('activeConditionModel',{list:data.activeCondition});
	$("#activeCondition").html(activeCondition);
	//添加最新签到记录情况数据
	var activitySignIn = template('activitySignInModel',{list:data.activitySignIn});
	$("#activitySignIn").html(activitySignIn);
	//添加活动开展全部排名数据
	var allActivityRanking = template('activityRankingModel',{list:data.allActivityRanking});
	$("#allActivityRanking").html(allActivityRanking);
	//添加活动开展本年排名数据
	var yearActivityRanking = template('activityRankingModel',{list:data.yearActivityRanking});
	$("#yearActivityRanking").html(yearActivityRanking);
	//添加活动开展本月排名数据
	var monthActivityRanking = template('activityRankingModel',{list:data.monthActivityRanking});
	$("#monthActivityRanking").html(monthActivityRanking);
	//添加活动签到全部排名数据
	var allSignInRanking = template('signInRankingModel',{list:data.allSignInRanking});
	$("#allSignInRanking").html(allSignInRanking);
	//添加活动签到本年排名数据
	var yearSignInRanking = template('signInRankingModel',{list:data.yearSignInRanking});
	$("#yearSignInRanking").html(yearSignInRanking);
	//添加活动签到本月排名数据
	var monthSignInRanking = template('signInRankingModel',{list:data.monthSignInRanking});
	$("#monthSignInRanking").html(monthSignInRanking);

}

/**
 * 在点击最新活动情况、活动开展排名、活动签到排名中的a标签时进行切换
 * @param id
 */
function change(id,thisId){
	$($(thisId).siblings()).removeClass("active");
	$(thisId).addClass("active");
	$($("#"+id).siblings()).css("display","none");
	$("#"+id).css("display","block");
}

/**
 * 处理圆饼图数据
 */
function dealPieChart(activeType){
	var data = [];
	for(var i=0;i<activeType.length;i++){
		data.push( {value:activeType[i].num, name:activeType[i].typeName});
	}
	var myPieChart = echarts.init(document.getElementById('pieChart'));
    // 指定图表的配置项和数据
	var pieChart = {
		    tooltip : {
		        trigger: 'item',
		        formatter: "活动类型: {b} <br/> {a}:{c} "
		    },
		    series : [
		              {
		                  name: '活动次数',
		                  type: 'pie',
		                  radius : '50%',
		                  center: ['50%', '50%'],
		                  data:data,
		                  itemStyle: {
		                      emphasis: {
		                          shadowBlur: 3,
		                          shadowOffsetX: 2,
		                          shadowColor: 'rgba(0, 0, 0, 0.5)'
		                      }
		                  }
		              }
		          ]
		};
    // 使用刚指定的配置项和数据显示图表。
	myPieChart.setOption(pieChart);
	data.splice(0,data.length);
}


