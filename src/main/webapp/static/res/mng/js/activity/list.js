var col=[{ name:"签到人数"
		,code:"signInNum"
		,width:'50px'
		,rendertpl:"tpl-renshu"
	},{name:"活动名称"
		 ,code:"title"
		 ,width:'30%'
		 ,rendertpl:"tpl-title"
	 },{ name:"活动党组织"
		,code:"devOrganName"
		,width:'15%'
	},{ name:"活动详情"
		,code:"details"
		,width:'60px'
		,render:function(val,row){
			if(val==0){
				return "<a title='点击添加活动详情' href='"+base+"/admin/activityInfo/details?id="+row.id+"' class='red'>未维护</a>";	
			}else{
				return "<a title='点击添加活动详情' href='"+base+"/admin/activityInfo/details?id="+row.id+"' class='red'>查看详情</a>";	
			}
		}
	}];

var funcs=[
	{
		name:"活动信息修改"
		,url:base+"/admin/activity/update"
	},
	{
		name:"维护活动详情"
		,url:base+"/admin/activityInfo/details"
	},
	{
		name:"签到记录"
		,url:base+"/admin/activity/sign/list"
	},
	{
		name:"删除数据"
		,ajax:true
		,confirm:true
		,confirmTxt:"删除后不可恢复，您确认要删除这些活动？"
		,click:deletes
	}
];

/**
 * 按条件查询数据方法（可根据起止时间、活动类型进行查询 ）
 */
function selectQuery(){
	var _typeId = $("#typeId").val()==-1?null:$("#typeId").val();
	var _title = $("#title").val();
	var _startTime = $("#startTime").val();
	var _endTime = $("#endTime").val();
	var _detail=$("#detail").val();
	grid.addParam({details:_detail,typeId:_typeId,title:_title,startTime:_startTime,endTime:_endTime});
}

/**
 * 根据活动的id删除某个活动记录的方
 */
function deletes(row){
	$.ui.confirm("删除后不可恢复，您确认要删除该活动？",function(){
		var _ids;
		if(row == undefined){
			_ids = grid.getChecked("id");
		}else{
			_ids = row.id;
		}
		if(_ids==""){
			$.ui.alert("请选择需要删除的活动!");
			return;
		}
		$.ajax({
		    type: 'POST',
		    url: base+"/admin/activity/deletes" ,
		    data: "ids="+_ids ,
		    dataType:"json",
		    success: function(data){
		    	if(data.code==200){
		    		grid.reload();
		    	}
		    } ,
		    error : function() {  
	    	} 
		});
	})
}

function exportFile(){
	$("body").mask("正在生成数据，请稍候。。。");
	$.ajax({
	    type: 'POST',
	    url: base+"/admin/activity/export" ,
	    dataType:"json",
	    global:false,
	    success: function(resp){
	    	if(resp.code==200){
	    		$("body").unmask();
	    		$.ui.tip("正在下载数据，请稍候。。。",3);
	    		window.location.href=base+"/admin/activity/downLoad?fileurl="+resp.data;
	    	}else{
	    		$.ui.alert("生成数据失败，请联系管理员！");
	    	}
	    },
	    error : function() {  
    	} 
	});
}

	$(function(){
		grid=$("#list").grid({
			cols:col
			,funcs:funcs
			,url :base+"/admin/activity/list/json"
		});
	});