var col=[{name:"党组织名称"
		 ,code:"organName"
		 ,width:'40%'
//		 ,rendertpl:"tpl-tile"
	 },{
	 	name:"党组织总数"
	 	,width:'15%'
	 	,code:"allCount"
	 },{name:"已开展组织数"
		,width:'15%'
		,code:"devCount"
	},{name:"未开展组织数"
		 ,width:'15%'
		,code:"notDevCount"
	},{ name:"开展比例"
		,code:"percent"
		,width:'15%'
	}
];

/**
 * 按条件查询数据方法（可根据起止时间、活动类型进行查询 ）
 */
function selectQuery(){
	var _organName = $("#organName").val();
	var _minPercent = $("#minPercent").val();
	var _maxPercent = $("#maxPercent").val();
	var _startTime = $("#startTime").val();
	var _endTime = $("#endTime").val();
	grid.addParam({organName:_organName,minPercent:_minPercent,maxPercent:_maxPercent,startTime:_startTime,endTime:_endTime});
}

$(function(){
	grid=$("#list").grid({
		cols:col
		,showfuns:false
		,url :base+"/admin/activity/organLaunchSurvey/json"
	});
});