var ue = UE.getEditor('editor');
function save(){
	var content=ue.getContent();
	var txt=ue.getContentTxt()
	//判断内容是否超过30个字
	if(txt.length<30){
		$.ui.alert("活动内容详情文字至少为30个字!");
		return ;
	}
	if(!(content=="" || content==undefined || content=="<p></p>")){
		$.ajax({
			type:"post",
			dataType:"json",
			data:{
				content:content,
				id:$("#activityId").val()
			},
			url:base+"/admin/activityInfo/add/save",
			success:function(){
				$.ui.alert("更新活动详情成功!");
			}
		});
	}
}
