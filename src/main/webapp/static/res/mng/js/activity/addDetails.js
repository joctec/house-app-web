var ue = UE.getEditor('editor');
function getRequest(name){
	var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)","i");
    var r = window.location.search.substr(1).match(reg);
    if(null != r){
    	return (r[2]); 
    }
    return null;
}
var id = getRequest("id");

/**
 * 保存添加的详情信息方法
 */
function save(){
	$.ajax({
	    type: 'POST',
	    url: base+"/admin/activityInfo/add/save" ,
	    data: {id:$(".activityId").val(),content:ue.getContent()} ,
	    dataType:"json",
	    success: function(data){
	    	if(data.code==200){
	    		$.ui.alert("保存成功！",function(){
	    			window.location.href=base+"/admin/activityInfo/details?id="+id;
	    		});
	    	}
	    } ,
	    error : function() {  
    	} 
	});
}
