var organTree;

$(function(){
	$(".main_slider").mask("加载中");
	loadOrganTree();
});

//增加列表的查询参数
function addParam(event, treeId, treeNode){
	grid.addParam({
		code:treeNode.id,
		organCode:treeNode.id
	});
}

/**
 * 组织机构树配置
*/
var treeSetting = {
    view:{
        showLine: true
    },
    data:{
        simpleData: {
            enable: true
        }
    },callback: {
        onClick:addParam
    }
};

/**
 * 加载组织机构树，初始化
*/
function loadOrganTree(){
    $.ajax({
        type: "post",
        url: base + "/admin/partyOrgan/orgTreeJson",
        dataType: "json",
        success: function (result) {
        	initOrganTree(result);
        }
    });
};

function initOrganTree(result){
	$(".main_slider").unmask();
    var nodes = new Array();
    for (var i = 0; i < result.data.length; i++) {
        var _organ = result.data[i];
        var _obj = {
            id:_organ.code,
            pId:_organ.autoParentCode,
            code: _organ.code,
            name: _organ.organName,
            name2:_organ.organName+"-"+_organ.organNamePinyin,
            organType:_organ.organType,
            icon:_organ.depth==1?base + "/static/res/mng/img/dh.png":base + "/static/res/mng/img/treeicon.png",
            open:_organ.depth==1?true:false
        };
        nodes.push(_obj);
    }
    organTree = $.fn.zTree.init($("#organTree"), treeSetting, nodes);
}
