var grid;
$(function(){
    grid=$("#list").grid({
		cols:col
		,funcs:funcs
		,param:{
			years:$("#years").val(),
			examTypesId:$("#examTypesId").val()
		},
		url :base+"/admin/examIndex/list/json"
	});
});
//查询
function selectQuery(){
	grid.addParam({
		years:$("#years").val(),
		examTypesId:$("#examTypesId").val()
	});
}
//禁用
function del(){
	var checked=grid.getChecked("id");
	if(checked==""){
		$.ui.alert("请选择需要禁用的数据!");
		return ;
	}
	var ids="";
	for(var i=0;i<checked.length;i++){
		ids=ids+(i==0?"":",")+checked[i];
	}
	$.ajax({
		dataType:"json",
		type:"post",
		url:base+"/admin/examIndex/del",
		data:{
			ids:ids
		},
		success:function(result){
			if(result.code=="200"){
				$.ui.alert("禁用数据成功!");
				grid.reload();
			}else{
				$.ui.alert(result.msg);
			}
		}
	});
}
//禁用
function redel(){
	var checked=grid.getChecked("id");
	if(checked==""){
		$.ui.alert("请选择需要恢复的数据!");
		return ;
	}
	var ids="";
	for(var i=0;i<checked.length;i++){
		ids=ids+(i==0?"":",")+checked[i];
	}
	$.ajax({
		dataType:"json",
		type:"post",
		url:base+"/admin/examIndex/redel",
		data:{
			ids:ids
		},
		success:function(result){
			if(result.code=="200"){
				$.ui.alert("恢复数据成功!");
				grid.reload();
			}else{
				$.ui.alert(result.msg);
			}
		}
	});
}


var col=[
	{
		name:"年度"
	 	,code:"years"
	 	,width:'60px'
	},{
		name:"责任科室"
	 	,code:"manageOrgan"
	 	,width:'90px'
	},{
		name:"责任科室"
	 	,code:"manageOrgan"
	 	,width:'90px'
	},{
		name:"状态"
	 	,code:"isDisable"
	 	,width:'60px'
	 	,render:function(val){
	 		if(val==1){
	 			return "<span class='red'>禁用</span>";
	 		}else{
	 			return "正常";
	 		}
	 	}
	},{
		name:"考核内容"
	 	,code:"content"
	}
];

var funcs=[{
		name:"查看",
		url:base+"/admin/examIndex/update"
	}
];