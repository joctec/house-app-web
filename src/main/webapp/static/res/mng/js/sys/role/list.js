var organCode="";
var organTree;
var grid;
$(function(){
    /**
     * 加载组织列表
    */
    grid=$("#list").grid({
		cols:col
		,funcs:funcs
		,param:{all:0}
		,url :base+"/admin/sysRole/list/json"
	});
	
	/**
	 * 新增党组织
	 */
	$("#addOrgan").click(function(){
		window.location.href=base+"/admin/partyOrgan/add?code="+organCode;
	})
	
	/**
	 * 组织机构搜索
	 */
	$("#searchbtn").click(function(){
		grid.addParam({
			organName:$("#organName").val(),
			isPosition:$("#isPosition").val(),
			all:1
		});
		//清空全局变量
		organCode="";
	});
	
});

/**
 * 加载指定的组织的下级党组织
 */
function loadPartyOrgan(event, treeId, treeNode){
	grid.addParam({
		code:treeNode.id,
		all:0
	});
	//把该code放入全局变量中
	organCode=treeNode.id;
}

var col=[
	{
		name:"角色名称"
	 	,code:"name"
	 		,width:'200px'
	},{
		name:"角色描述"
	 	,code:"descr"
	 	,width:'200px'
	}
];

var funcs=[
   {
	   name:"修改"
	   ,url:path + "/admin/sysRole/update"
   }
];
