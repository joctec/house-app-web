var treeObj;
$(function(){
	/**
	 * 组织机构树配置
	 */
	var treeSetting = {
        view: {
            showLine: true
        },
        data: {
            simpleData: {
                enable: true
            }
        }
        ,check:{
        	enable:true
        }
    };
   
	/**
     * 树，初始化
     */
    var roleId = $("#roleId").val();
    $.ajax({
        type: "post",
        url: base + "/admin/sysRole/getRoleResource?roleId=" + roleId,
        dataType: "json",
        success: function (result) {
            treeObj = $.fn.zTree.init($("#tree"), treeSetting, result.data);
        }
    });
    
    $("#form").validateWrapper();
	
    //表单提交
	 $("#submit").click(function(){
	 	if(!$("#form").valid()){
	 		return false;
	 	}
	 	
	 	//遍历用户选择的节点
	 	var nodes = treeObj.getCheckedNodes(true);
	 	var ids = "";
	 	$.each(nodes, function(index, node){
	 		if(ids != ""){
	 			ids = ids + "," + node.id;
	 		}else{
	 			ids = node.id;
	 		}
	 	});
	 	$("#resourceIds").val(ids);
	 	
	 	$("#form").ajaxSubmit({
	 		dataType:"json",
	 		success:function(resp){
	 			if(resp.code == "200"){
	 				$.ui.alert("保存成功！",function(){
	 					window.location.href=window.location.href;
	 				});
	 			}else if(resp.code == "5003"){
	 				$.ui.alert(resp.data.msg,function(){});
	 				return;
	 			}else{
	 				$.ui.alert(resp.msg,function(){});
	 				return;
	 			}
	 		}
	 	});
	 });
	 
});

