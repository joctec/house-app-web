var layouts;
var bigimgview;
$(function(){
	
	//网页布局
	layouts=$('.wrapers').layout({
		applyDemoStyles    : false
		,north__size       : 46
		,south__size       : 20
		,west__size        : 180
		,north__closable   : false
		,south__closable   : true
		,spacing_open:0
		,west__spacing_open:0
		,north__paneSelector:".header"
		,center__paneSelector:".content"
		,west__paneSelector:".nav"
		,south__paneSelector:".footer"
		,contentSelector:".content"
	});
	
	$("#toggleBtn").click(function(){
		layouts.toggle('west');
	});
	
	//滚动条
	$(".nicescroll").niceScroll({
		touchbehavior:false
		,cursorcolor:"#ccc"
		,cursoropacitymax:1
		,cursorwidth:7
		,cursorborder:"0"
		,cursorborderradius:"10px"
		,background:"none"
		,autohidemode:"scroll"
	});
	
	$.fn.extend({ 
		validateWrapper:function(options){
			if(options == undefined){
				options = {};
			}
			var that = this;
			var opts;
			if(typeof(formRule) != "undefined"){
				opts = $.extend(true, formRule, options);
				if(opts.rules){
					$.each(opts.rules, function(a,b){
						delete opts["rules"][a]["name"];
					});
				}
			}else{
				opts = options;
			}
			$(this).validate(opts);
		}
	});
	
	//给菜单绑定事件
	$(".menuitem").click(function(){
		var next=$(this).next();
		if(!$(this).hasClass("hasSub")){
			return;
		}
		if(next.hasClass("sub_open")){
			return;
		}
		$(".sub_open").slideUp(100);
		$(".sub_open").removeClass("sub_open");
		
		next.addClass("sub_open");
		next.slideDown(100);
//		if(!next.hasClass("open")){
//			$(".open").removeClass("open");
//			$(".open").slideUp(100);
//			next.addClass("open");
//			next.slideDown(100);
//		}
	});
	
	//日期点击
//	$(".dates").click(function(){
//		
//	});
	
	$(document).on("click",".smallimg",function(){
		var _src=$(this).attr("data-big");
		var that=this;
		if(bigimgview){
			bigimgview.content("<img src='"+_src+"' style='width:100%'/>").show(that);
			return ;
		}
		bigimgview=dialog({
			id:"big-img",
			padding:5,
			width:'350',
         	content:"<img src='"+_src+"' style='width:100%'/>"
         }).show(that); 
	});
	
	$(document).on("mouseout",".smallimg",function(){
		try{bigimgview.close().content("");}catch(e){}
	});
});











	
	



