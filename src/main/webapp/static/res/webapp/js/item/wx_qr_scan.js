function wxQrScan(callback) {
	try {
		wx.scanQRCode({
			needResult : 1,
			scanType : [ "qrCode" ],
			success : function(res) {
				var result = res.resultStr;
				addItem(result);
			},
			fail : function(res) {
				$.ui.alert("系统提示:", "调用微信扫码失败:" + JSON.stringify(res));
			}
		});
	} catch (e) {
		$.ui.alert("系统提示:", "扫码异常!");
	}
}
