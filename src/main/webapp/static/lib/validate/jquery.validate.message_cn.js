//定义中文消息
var cnmsg = {
	required : "该项必须输入",
	remote : "请修正该字段",
	email : "请输入正确格式的电子邮件",
	url : "请输入合法的网址",
	date : "请输入合法的日期",
	dateISO : "请输入合法的日期 (ISO).",
	number : "请输入合法的数字",
	digits : "只能输入整数",
	creditcard : "请输入合法的信用卡号",
	equalTo : "请再次输入相同的值",
	accept : "请输入拥有合法后缀名的字符串",
	maxlength : jQuery.validator.format("该项长度最长为{0}"),
	minlength : jQuery.validator.format("该项长度最短为 {0}"),
	rangelength : jQuery.validator.format("该项长度必须大于{0}小于{1}"),
	range : jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的数字"),
	max : jQuery.validator.format("该项最大值为{0}"),
	min : jQuery.validator.format("该项最小值为{0}")
};
jQuery.extend(jQuery.validator.messages, cnmsg);