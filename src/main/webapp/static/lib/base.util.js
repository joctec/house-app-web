var Util = {
	dateFormat:function(date, format){ 
		/**
		 * //使用方法 var now = new Date(); var nowStr = now.format("yyyy-MM-dd
		 * hh:mm:ss"); //使用方法2: var testDate = new Date(); var testStr =
		 * testDate.format("YYYY年MM月dd日hh小时mm分ss秒"); alert(testStr); //示例：
		 * alert(new Date().Format("yyyy年MM月dd日")); alert(new
		 * Date().Format("MM/dd/yyyy")); alert(new Date().Format("yyyyMMdd"));
		 * alert(new Date().Format("yyyy-MM-dd hh:mm:ss"));
		 */
		if(!date){
			return "";
		}
		
		if(date == ""){
			return "";
		}
		
		date = new Date(date);
		
		if(date == "Invalid Date"){
			return "";
		}
		
		var o = { 
		"M+" : date.getMonth()+1, // month
		"d+" : date.getDate(), // day
		"h+" : date.getHours(), // hour
		"m+" : date.getMinutes(), // minute
		"s+" : date.getSeconds(), // second
		"q+" : Math.floor((date.getMonth()+3)/3), // quarter
		"S" : date.getMilliseconds() // millisecond
		} ;
	
		if(/(y+)/.test(format)) { 
			format = format.replace(RegExp.$1, (date.getFullYear()+"").substr(4 - RegExp.$1.length)); 
		} 
	
		for(var k in o) { 
			if(new RegExp("("+ k +")").test(format)) { 
				format = format.replace(RegExp.$1, RegExp.$1.length==1 ? o[k] : ("00"+ o[k]).substr((""+ o[k]).length)); 
			} 
		} 
		return format; 
	},
	check0_9_A_Z:function (str)  
	{  
	     var re =  /^[0-9a-zA-Z]*$/g;  // 判断字符串是否为数字和字母组合 //判断正整数
										// /^[1-9]+[0-9]*]*$/
	     if (!re.test(str)){  
	        return false;  
	     }else{  
	    	 return true;  
	     }  
	},
	/**
	 * data: josn,示例:{"name":"afei"}
	 */
	downloadFile: function(url, data){
		// 定义一个form表单
		var form=$("<form target='_blank'>");
		form.attr("style","display:none");
		form.attr("target","");
		form.attr("method","post");
		form.attr("action", url);
		if(data){
			$.each(data, function(name,val){
				var input1=$("<input>");
				input1.attr("type","hidden");
				input1.attr("name", name);
				input1.attr("value", val);
				form.append(input1);
			});
		}
		// 将表单放置在web中
		$("body").append(form);
		// 表单提交
		form.submit().remove();
	},
	getUrlParam: function (name){
	     var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	     var r = window.location.search.substr(1).match(reg);
	     if(r!=null)return  unescape(r[2]); return null;
	},
	jsonKeyNum : function(obj) {
		var size = 0;
		for (key in obj) {
			size++;
		}
		return size;
	},
	dict : function(dictName, code){
		if (code == undefined || code == null || code.length == 0){
			return '-';
		}
		try {
			var _data = eval("dictJs_" + dictName + "['" +code+ "']");
			if (_data == undefined) {
				return code;
			} else {
				return _data.name;
			}
		} catch (e) {
			return "dict error: " + code;
			console.log("js 字典项转换出错. dictName:"+dictName + ", code:" + code);
		}
	},
	getSearchPamas: function(jquerySelector){
		var params = {};
		var paramsEle = $(jquerySelector).find(".searchParams");
		var tmpFun = {
				setVal: function(params, ele){
					if(ele.val() != ""){
						params[ele.attr("name")] = ele.val();
					}
				}
		};
		$.each(paramsEle, function(index, ele){
			if($(ele).is('input')){
				tmpFun.setVal(params, $(ele));
			}else if($(ele).is('select')){
				tmpFun.setVal(params, $(ele));
			}
		});
		
		return params;
	},
	getSearchPamasStr: function(jquerySelector){
		var jsonParams = this.getSearchPamas(jquerySelector);
		return JSON.stringify(jsonParams);
	},
	ajaxResponse: function(resp, successCallback, errorCallback){
		if (resp.code == 200) {
			if(successCallback){
				successCallback(resp);
			}else{
				$.ui.tip("操作成功!");
			}
		} else {
			//判断是否是表单校验错误
			if(resp.data && resp.data.validateFrom){
				$.ui.tip(resp.data.msg);
			}else{
				$.ui.tip(resp.msg);
			}
			if(errorCallback){
				errorCallback(resp);
			}
		}
	},
	checkbox: function(selector, checkFun){
		$(selector).on("click", function(){
			if($(this).hasClass("ckbox_checked")){
				$(this).removeClass("ckbox_checked");
				if(checkFun){
					checkFun(false);
				}
			}else{
				$(this).addClass("ckbox_checked");
				if(checkFun){
					checkFun(true);
				}
			}
		});
	},
	uuid:function(){
		var s = [];
	    var hexDigits = "0123456789abcdef";
	    for (var i = 0; i < 36; i++) {
	        s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
	    }
	    s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
	    s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
	    s[8] = s[13] = s[18] = s[23] = "-";
	    var uuid = s.join("");
	    return uuid;
	},
	clearForm: function(seletor){
		var eles = $(seletor).find("input");
		for(var i = 0; i < eles.length; i++){
			$(eles[i]).val("");
		}
		
		eles = $(seletor).find("select");
		for(var i = 0; i < eles.length; i++){
			$(eles[i]).val("");
		}
		
		eles = $(seletor).find("textarea");
		for(var i = 0; i < eles.length; i++){
			$(eles[i]).val("");
		}
	},
	setForm: function(seletor, data){
		$.each(data, function(key, index){
			var ele = $(seletor).find("input[name='" +key+ "']");
			if(ele.length > 0){
				$(ele[0]).val(HtmlUtil.decodeHtml(data[key]));
			}
			ele = $(seletor).find("select[name='" +key+ "']");
			if(ele.length > 0){
				$(ele[0]).val(HtmlUtil.decodeHtml(data[key]));
			}
			ele = $(seletor).find("textarea[name='" +key+ "']");
			if(ele.length > 0){
				$(ele[0]).val(HtmlUtil.decodeHtml(data[key]));
			}
		});
	},
	permission: function(code){
		if(!code){
			return true;
		}
		
		var ele = $("input[name='permission_" + code + "']");
		if(ele.length == 0){
			return false;
		}
		
		var ele = $(ele).val();
		if(ele == "true"){
			return true;
		}
		
		return false;
	}
};

var HtmlUtil = {  
		encodeHtml:function (str){  
			var s = "";
			if(str.length == 0) {
				return "";
			}
			
			s = str.replace(/&/g,"&amp;");
			s = s.replace(/</g,"&lt;");
			s = s.replace(/>/g,"&gt;");
			s = s.replace(/ /g,"&nbsp;");
			s = s.replace(/\'/g,"&#39;");
			s = s.replace(/\"/g,"&quot;");
			
			return s;  
		},
		decodeHtml:function (str){  
			var s = "";
			if(str.length == 0) {
				return "";
			}
			
			if(typeof str != "string"){
				return str;
			}
			
			s = str.replace(/&amp;/g,"&");
			s = s.replace(/&lt;/g,"<");
			s = s.replace(/&gt;/g,">");
			s = s.replace(/&nbsp;/g," ");
			s = s.replace(/&#39;/g,"\'");
			s = s.replace(/&quot;/g,"\"");
			
			return s;  
		}
}
