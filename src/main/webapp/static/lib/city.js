
(function($) {
	//默认
	var defaultOptions={
		url:path+"/lietou/region/list",
		levels:2,
		classes:'dashselect',
		startLevel:1,
		id:randomid(10),
		names:['province','city','district'],
		values:[]
	}
	turnMethods = {
		init: function(options) {
			var _domId=$(this).attr('id');
			options['id']=_domId;
			var opts = $.extend({}, defaultOptions, options);
			//生成下拉框
			$(this).empty();
			for(var i=opts.startLevel;i<=opts.levels;i++){
				$(this).append(getSelectDom(opts.id,i,opts.names[i-1],opts.classes));
			}
			//加载第一级
			var _defValue="";
			if(opts.values[0]){
				_defValue=opts.values[0];
			}
			loadData(opts,1,_defValue,"#"+opts.id+"-1");
			//绑定事件
			binding("#"+_domId+" .cityselect",opts);
		}
	}
	

	function binding(id,opts){
		$(id).change(function(){
			var _index=parseInt($(this).attr("sindex"))+1;
			//判断是否存在下级
			var _nextId="#"+opts.id+"-"+_index;
			if($(_nextId).length==1){
				var parentCode=$(this).val();
				loadData(opts,parentCode,opts.values[_index-1],_nextId);
			}
		});
	}
	
	function randomid(len){
		len = len || 5;
	var $chars = 'ABCDEFGHJKMNPQRSTWXYZabcdefhijkmnprstwxyz2345678';
	var maxPos = $chars.length;
	var pwd = '';
	for (i = 0; i < len; i++) {
	pwd += $chars.charAt(Math.floor(Math.random() * maxPos));
	}
	return pwd;
	}
	
	//加载城市数据
	function loadData(opts,parentcode,defaultVal,selectId){
		$.ajax({
			type:"post",
			url:opts.url+"?parentCode="+parentcode,
			success:function(json){
				var data=json.data;
				var options="<option value='-1'>-请选择-</option>";
				for(var i=0;i<data.length;i++){
					var _city=data[i];
					var active="";
					if(defaultVal && defaultVal!="" && defaultVal==_city.regionCode){
						active=" selected='selected'";
						//判断是级别，如果是第一级则需要加载下一级 
						var _index=parseInt($(selectId).attr("sindex"))+1;
						var _nextId="#"+opts.id+"-"+_index;
						if($(_nextId).length==1){
							loadData(opts,_city.regionCode,opts.values[_index-1],_nextId);
						}
					}
					options=options+"<option"+active+" value='"+_city.regionCode+"'>"+_city.regionName+"</option>";
				}
				$(selectId).empty().append(options);
			}
		});
	}
	
	function dec(that, methods, args) {
		if (!args[0] || typeof(args[0])=='object'){
			return methods.init.apply(that, args);
		}else if (methods[args[0]]){
			return methods[args[0]].apply(that, Array.prototype.slice.call(args, 1));
		}else{
			throw turnError(args[0] + ' is not a method or property');
		}
	}
	
	function turnError(message) {

	  function TurnJsError(message) {
	    this.name = "TurnJsError";
	    this.message = message;
	  }
	  TurnJsError.prototype = new Error();
	  TurnJsError.prototype.constructor = TurnJsError;
	  return new TurnJsError(message);
	
	}
	
	function getSelectDom(id,index,name,classes){
		var selecttpl=
			"<div class='city_select_box' style='float:left; padding-right:5px'>"+
				"<select sindex='"+index+"' name='"+name+"' id='"+id+"-"+index+"' class='cityselect "+classes+"' style='min-width:70px;'>"+
				"<option value='-1'>-请选择-</option>"+
				"</select>"+
			"</div>";
			return selecttpl;
	}
	
	//page load方法
	$.extend($.fn, {
	  cityselect: function() {
		  if($(this).attr("data-url")){
			  defaultOptions.url = $(this).attr("data-url");
		  }
	    return dec($(this[0]), turnMethods, arguments);
	  }
	});
	
})(jQuery);