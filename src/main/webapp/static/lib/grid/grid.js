// 创建一个闭包
(function($) {
  // 插件的定义 
  $.fn.grid = function(options) {
    	$this = $(this);
    	var opts = $.extend({}, $.fn.grid.defaults, options);
    	var o = $.meta ? $.extend({}, opts, $this.data()) : opts;//列表配置
    	o.id=$this.attr("id");
    	var param=o.param;//参数
    	var pageNo = param.pageNum > 1 ? param.pageNum : 1;//当前页码
    	var pageSize=10;//每页多少条
    	var totalPage=1;//总页数
    	var totalCount=0;//总条数
    	var gridid=o.id;//表格的id
    	var noPage = o.noPage;//不要分页
    	var simplePage = o.simplePage;//简单分页
    	var gridData = o.gridData;//用本地数据初始化
    	var click = o.click;
    	var initNoLoadData = o.initNoLoadData;
    	
    	var gridInstance = {
    		gridData: null,
    		success: options.success
    	};
    	
    	//加载数据的方法
    	var loadList=function(gopage, gridObj){
    		if(gridData){
    			gridData = gridData ? gridData : [];
    			var gridDataTmp = {
    					data:{
    						rows: gridData,
    						pageNum:1,
    						pageSize:gridData.length,
    						totalPage:1,
    						total:gridData.length
    					}
    			};
    			loadDataCallBack(gridDataTmp);
    			return;
    		}
    		
    		if(!o.url){
    			var gridDataTmp = {
      				  data:{
  	    				rows:[],
  	    				pageNum:1,
  	    				pageSize:10,
  	    				totalPage:1,
  	    				total:0
      				  }
      			};
    			loadDataCallBack(gridDataTmp);
    			return;
    		}

    		if(!gopage){
    			gopage=pageNo;
    		}
    		//判断页码是否符合规则
    		if((gopage>totalPage)){
    			pageNo=totalPage;
    		}else if(gopage<=0){
    			pageNo=1;
    		}
    		param["pageNum"]=gopage;//查询页码
    		$.ajax({
    			type:"POST",
    			url:o.url+"?r="+Math.random(),//随机数防止缓存
    			data:param,
    			success:function(json){
    				//判断是否成功
    				if(json.code!="200"){
    					$.ui.alert(json.msg);
    					return false;
    				}
    				
    				gridInstance.gridData = json;
    				loadDataCallBack(json);
    				if(gridInstance.success){
    					gridInstance.success(json);
    				}
    			}//success end
    		});
    	};
    	
    	var loadDataCallBack = function(json){
			$(".noperate-"+o.id).remove();
			
			/**
			 * 更新页码相关数据
			*/
			pageNo=json.data.pageNum;
	    	pageSize=json.data.pageSize;//每页多少条
	    	totalPage=json.data.totalPage;//总页数
	    	totalCount=json.data.total;//总条数
	    	
			var _cols=opts.cols;//获取列配置
			
			var tplhtml;
			if(o.rowTpl){
				 tplhtml=template(o.rowTpl,{
			  		config:o,
			  		data:json
			  	});
			}else{
				var render = template.compile($.fn.grid.datatpl);//获取list模板
			  	tplhtml = render({
			  		config:o,
			  		data:json,
			  		grid: gridInstance
			  	});//生成模板dom
			}
		  	//清除无数据提示框
		  	$("#nodatas-"+o.id).remove();
		  	$("#grid-"+o.id).html(tplhtml);//加载dom
		  	
		  	//绑定事件
		  	
		  	
  			updatePageBar();//更新分页
  			//绑定全部操作的点击事件
			$(".noperate-"+o.id).on("click",function(event){
		    	event.stopPropagation();
				dialog({
					id:$(this).attr("id")+o.id+"-dialog",
					quickClose: true,
					content:document.getElementById($(this).attr("id")+"-funcbtns")
				}).show($(this)[0]);
			});
			//绑定复选框的点击事件
		    $(".td_check_"+o.id).click(function(event){
		    	event.stopPropagation();
		    	$(this).toggleClass("checked");
		    });
		    //绑定row点击事件
		    $("#grid-"+o.id + " tr").click(function(){
		    	$("#grid-"+o.id).find(".td_check").removeClass("checked");
		    	$(this).find(".td_check").addClass("checked");
		    	if(click){
		    		click($.parseJSON($(this).attr("data")));
		    	}
		    });
	    	
			//为每个操作按钮绑定事件
			$(".funcbtns-"+o.id).children().click(function(){
				event.stopPropagation();
				//关闭弹出菜单
				if(dialog.getCurrent()){
					dialog.getCurrent().close();
				}
				//找到点击的是第几个按钮
				var _index=parseInt($(this).attr("index"));//按钮的下标
				var _rowid=$(this).attr("rowid");//当前行的id
				var _func=o.funcs[_index];//传过来的按钮配置
				var _data=$.parseJSON($("#row-"+o.id+"-"+_rowid).attr("data"));//该行的数据
				//判断是否绑定了点击事件
				if(_func.click){
					_func.click(_data);
					return;
				}
				//判断是否需要确认用户的操作
				if(_func.confirm){
					$.ui.confirm(
						_func.confirmTxt==null?"您确认要进行该操作吗?":_func.confirmTxt,
						function(){
							btnclick(_func,_data);
						}
					);
					return;
				}else{
					btnclick(_func,_data);
				}
			});//为每个操作按钮绑定事件 end
			
			/**
			 * 更新跳转点击事件
			*/
			
  			$("#grid-gopage-"+o.id).on("click",function(){
  				var gopage=$("#grid-gopage-input-"+o.id).val();
  				if (gopage ==null || gopage==""){
			       $("#grid-gopage-input-"+o.id).val(1);
			    }else if(isNaN(gopage)){
			    	//如果非法数字，跳转到第一页
			    	gopage=1;
			    	$("#grid-gopage-input-"+o.id).val(1);
			    }
  				loadList(gopage);
  			});
			
			//如果返回了0条数据
			if(json.data.rows.length==0){
				if($("#nodatas-"+o.id).length==0){
					$("#table_"+o.id).after("<div id='nodatas-"+o.id+"' class='nodatas'>没有数据</div>");
				}
			}
			//数据加载完成后的回调函数
			if(o.loadOver){
				o.loadOver(INSTANCE,json);
			}
		//
			$("#td_check_all_"+o.id).removeClass("checked");
    	};
    	
    	//操作按钮的点击事件
    	var btnclick=function(_func,_data){
    		var _idKey=_func.idName?_func.idName:"id";
    		if(_func.ajax){
				var _refresh=_func.refresh===undefined?true:_func.refresh;
				var _postData={};
				_postData[_idKey]=_data.id;
				ajaxpost(_func.url,_postData,_func.callback,_refresh);
			}else{
				if(_func.url.indexOf("?")!=-1){
					window.location.href=_func.url+"&"+_idKey+"="+_data.id;
				}else{
					window.location.href=_func.url+"?"+_idKey+"="+_data.id;
				}
			}
    	};
    	//ajax请求
    	var ajaxpost=function(url,data,callback,refresh){
    		$("body").mask("努力加载中...");
    		$.ajax({
				type:"post",
				url:url,
				data:data,
				dataType:'json',
				success:function(json){
					$("body").unmask();
					if(json.code!="200"){
						$.ui.alert(json.msg);
					}else{
						if(callback){
							callback(json);//有回调则调用回调，没有回调则判断是否需要刷新列表，默认是刷新
						}else{
							if(refresh){//自动刷新
								loadList(pageNo);
							}
						}
					}
				}
			});
    	}
    	//分页
    	var firstpage=function(gridObj){
    		pageNo=1;
			loadList(pageNo, gridObj);
    	};
    	var prepage=function(){
    		pageNo=pageNo-1;
			loadList(pageNo);
    	};
    	var nextpage=function(){
    		pageNo=pageNo+1;
			loadList(pageNo);
    	};
    	var lastpage=function(){
    		pageNo=totalPage;
			loadList(pageNo);
    	};
    	var reloadpage=function(){
    		alert("reload")
    	};
    	var getChecked=function(key){
    		var arr=new Array();
    		$(".td_check_"+o.id+".checked").each(function(){
    			var _json=$(this).parent().attr("data");
    			if(key){
    				arr.push(eval("("+_json+")")[key]);
    			}else{
    				arr.push(eval("("+_json+")"));
    			}
    		});
    		return arr;
    	};
		//addParam
		var addParam=function(obj){
			param= $.extend({},param, obj);
			//加载第一页
			param['pageNum']=1;
			firstpage();
		};
		//重新设置参数
		var resetparam=function(obj, gridObj){
			if(obj){
				param = obj;
			}else{
				param = {};
			}
			//加载第一页
			param['pageNum']=1;
			firstpage(gridObj);
		};
		var getParam=function(){
			return param;
		};
    	//更新分页条
    	var updatePageBar=function(){
    		//更新数字
    		$("#grid-page-totalCount-"+o.id).html(totalCount);
		  	$("#grid-page-pageSize-"+o.id).html(pageSize);
		  	$("#grid-page-pageNo-"+o.id).html(pageNo);
		  	$("#grid-page-totalPage-"+o.id).html(totalPage);
		  	//首页不能点击的情况，当前第一页
		  	if(pageNo==1 || totalCount==0){
		  		$("#firstpage-"+o.id).addClass("disable");
		  		$("#prepage-"+o.id).addClass("disable");
		  	}else{
		  		$("#firstpage-"+o.id).removeClass("disable");
		  		$("#prepage-"+o.id).removeClass("disable");
		  	}
		  	//下一页不能点击的情况 
		  	if(totalPage==1 || pageNo==totalPage){
		  		$("#nextpage-"+o.id).addClass("disable");
		  		$("#lastpage-"+o.id).addClass("disable");
		  	}else{
		  		$("#nextpage-"+o.id).removeClass("disable");
		  		$("#lastpage-"+o.id).removeClass("disable");
		  	}
    	};
    	
  		//渲染出整个grid模板
  		var render = template.compile($.fn.grid.tpl(noPage, simplePage));
  		var tplhtml = render({
	    	config:o,
	    	id:$this.attr("id")
			});
			$(this).append(tplhtml);
//			加载数据 如果在调用的地方没有传pageNum 那么默认的是加载第一页 如果传了pageNum 则加载传入页数得数据  
			pageNo=param.pageNum > 1 ? param.pageNum : 1;
			if(!initNoLoadData){
				loadList(pageNo);
			}
			//复选框的全选操作
			$("#td_check_all_"+o.id).click(function(){
    		$(this).toggleClass("checked");
    		if($(this).hasClass("checked")){
    			$(".td_check_"+o.id).addClass("checked");
    		}else{
    			$(".td_check_"+o.id).removeClass("checked");
    		}
    	});
    	
		//绑定翻页事件，总共四个按钮
		$("#grid-page-"+o.id+" .gopages").on("click",function(){
			if($(this).hasClass("prepage") && !$(this).hasClass("disable")){
				prepage();
			}else if($(this).hasClass("firstpage") && !$(this).hasClass("disable")){
				firstpage();
			}else if($(this).hasClass("lastpage") && !$(this).hasClass("disable")){
				lastpage();
			}else if($(this).hasClass("nextpage") && !$(this).hasClass("disable")){
				nextpage();
			}
		});
		
		var clearGrid = function(){
		  	//清空tbody
			if($("#grid-"+o.id).html() != ""){
				$("#grid-"+o.id).html("");
			}
		  	if($("#nodatas-"+o.id).length == 0){
		  		$("#table_"+o.id).after("<div id='nodatas-"+o.id+"' class='nodatas'>没有数据</div>");
		  	}
		};
		
		//返回当前对象
		$.extend(gridInstance, {
				option:opts,
				getParam:function(){return getParam();},
				firstPage:firstpage,
				prePage:prepage,
				nextPage:nextpage,
				lastPage:lastpage,
				reload:function(){loadList()},
				pageNum:function(){return pageNo},
				pageSize:function(){return pageSize},
				totalPage:function(){return totalPage},
				totalCount:function(){return totalCount},
				addParam:function(obj){addParam(obj)},
				params:function(obj){
					resetparam(obj, this)
				},
				getChecked:getChecked,
				clearData:clearGrid
		});
		
		return gridInstance;//返回当前对象
  };  
  // 私有函数：debugging    
  function debug($obj) {
    window.console.log('hilight selection count: ');    
  };
  // 插件的defaults    
  $.fn.grid.defaults = {
  	cols:[]
  	,url:""
  	,param:{}
  	,funcs:[]
  	,count:20
  	,cancheck:true
  	,showfuns:true
  };
  
  //表格模板
  $.fn.grid.tpl=function(noPage, simplePage){
  		var html =   '<table id="table_{{id}}" class="grids">'
					+'<thead>'
					+'<tr>'
					+	'{{if config.cancheck}}'
					+	'<th id="td_check_all_{{id}}" class="th td_check td_check_all" width="20px"><div class="ckbox"></div></th>'
					+	'{{/if}}'
					+	'{{if config.index}}'
					+	'<th width="40px" class="th">序号</th>'
					+	'{{/if}}'
					+	'{{each config.cols as col i}}'
					
					+	'{{if col.align}}'
					+    '<th class="th"{{if col.width}} width="{{col.width}}"{{/if}} style="text-align: {{col.align}}">'
					+   '{{else}}'
					+    '<th class="th"{{if col.width}} width="{{col.width}}"{{/if}}>'
					+	'{{/if}}'
					+   '{{col.name}}'
					+    '</th>'
					
					+	'{{/each}}'
					+	'{{if config.showfuns && (config.funs || config.funs != "")}}'
					+   '<th width="{{if config.funcsWidth}}{{config.funcsWidth}}{{else}}60px{{/if}}" class="th">操作</th>'
					+	'{{/if}}'
					+'</tr>'
					+'</thead>'
					+'<tbody id="grid-{{id}}">'
					+'</tbody>'
					+'</table>'
					+'<div id="nodatas-{{id}}" class="nodatas">没有数据</div>';
					if(!noPage){
						if(simplePage){
							html = html +'<div class="gridpage" id="grid-page-{{id}}">'
											+'	<a href="javascript:;" class="gopages firstpage" id="firstpage-{{id}}">首页</a>'
											+'	<a href="javascript:;" class="gopages prepage" id="prepage-{{id}}">上一页</a>'
											+'	<a href="javascript:;" class="gopages nextpage" id="nextpage-{{id}}">下一页</a>'
											+'	<a href="javascript:;" class="gopages lastpage" id="lastpage-{{id}}">末页</a>'
											+'	共<span id="grid-page-totalCount-{{id}}" class="grid-page-totalCount">{{totalCount}}</span>条'
											+'</div>';
						}else{
							html = html +'<div class="gridpage" id="grid-page-{{id}}">'
							+'	<a href="javascript:;" class="gopages firstpage" id="firstpage-{{id}}">首页</a>'
							+'	<a href="javascript:;" class="gopages prepage" id="prepage-{{id}}">上一页</a>'
							+'	<a href="javascript:;" class="gopages nextpage" id="nextpage-{{id}}">下一页</a>'
							+'	<a href="javascript:;" class="gopages lastpage" id="lastpage-{{id}}">末页</a>'
							+'	共<span id="grid-page-totalCount-{{id}}" class="grid-page-totalCount">{{totalCount}}</span>条-每页<span id="grid-page-pageSize-{{id}}" class="grid-page-pageSize">{{pageSize}}</span>条-当前<span id="grid-page-pageNo-{{id}}" class="grid-page-pageNo">{{pageNo}}</span>/<span id="grid-page-totalPage-{{id}}" class="grid-page-totalPage">{{totalPage}}</span>页<input id="grid-gopage-input-{{id}}" class="grid-gopage" type="text" /><a id="grid-gopage-{{id}}" href="javascript:;">跳转</a>'
							+'</div>';
						}
					}
		return html;
  };
	
	$.fn.grid.datatpl='{{each data.data.rows as li i}}'
	+'<tr id="row-{{config.id}}-{{li.id}}" data="{{getMap(li)}}">'
	+	'{{if config.cancheck}}'
	+		'<td class="td td_check td_check_{{config.id}} td_check_{{config.id}}_{{li.id}}"><div class="ckbox"></div></td>'
	+	'{{/if}}'
	+	'{{if config.index}}'
	+	'<td class="td">{{i+1}}</td>'
	+	'{{/if}}'
	+	'{{each config.cols as col j}}'
	
	+	'{{if col.dict}}'
		+    '<td class="td" style="text-align: center">'
	+   '{{else}}'
	
		+	'{{if col.align}}'
		+    '<td class="td" style="text-align: {{col.align}}">'
		+   '{{else}}'
		+    '<td class="td">'
		+	'{{/if}}'
	
	+	'{{/if}}'
	
	+    '{{#getValue(li, col, i, grid)}}'
	+    '</td>'
	+ 	'{{/each}}'
	
	+	'{{if config.funcsLine}}'
		+	'<td class="td">'
		+	'<div id="noperate-{{li.id}}-funcbtns" class="funcbtns-{{config.id}} funcbtns-{{li.id}}">'
		+	'{{each config.funcs as func k}}'

		+	'{{if func.showCon}}'
		+	'{{if func.showCon(li)}}'
			+	'{{if permision(li,func)}}'
			+   '<a rowid="{{li.id}}" index="{{k}}" id="funcbtns-{{k}}" style="margin-right:4px;color:#0088cc"><i class="">{{if func.icon!= null}}{{func.icon}}{{else}}{{/if}}</i>{{func.name}}</a>'
			+	'{{/if}}'
		+	'{{else}}'
		+	'{{/if}}'
		+	'{{else}}'
			+	'{{if permision(li,func)}}'
			+   '<a rowid="{{li.id}}" index="{{k}}" id="funcbtns-{{k}}" style="margin-right:4px;color:#0088cc"><i class="">{{if func.icon!= null}}{{func.icon}}{{else}}{{/if}}</i>{{func.name}}</a>'
			+	'{{/if}}'
		+	'{{/if}}'
		
		+ 	'{{/each}}'
		+	'</div>'
		+	'</td>'
	+	'{{else}}'
		+	'{{if config.showfuns && config.funs}}'
		+	'<td class="td">'
		+	'<a href="javascript:;" id="noperate-{{li.id}}" class="noperate grid-noperate noperate-{{config.id}}">'
		+	'操作<i class=""></i>'
		+	'</a>'
		+	'<div id="noperate-{{li.id}}-funcbtns" class="funcbtns funcbtns-{{config.id}} funcbtns-{{li.id}}">'
		
		+	'{{each config.funcs as func k}}'
		+	'{{if func.showCon}}'
			+	'{{if func.showCon(li)}}'
				+	'{{if permision(li,func)}}'
				+   '<a rowid="{{li.id}}" index="{{k}}" id="funcbtns-{{k}}"><i class="">{{if func.icon!= null}}{{func.icon}}{{else}}{{/if}}</i>{{func.name}}</a>'
				+	'{{/if}}'
			+	'{{else}}'
			+	'{{/if}}'
		+	'{{else}}'
			+	'{{if permision(li,func)}}'
			+   '<a rowid="{{li.id}}" index="{{k}}" id="funcbtns-{{k}}"><i class="">{{if func.icon!= null}}{{func.icon}}{{else}}{{/if}}</i>{{func.name}}</a>'
			+	'{{/if}}'
		+	'{{/if}}'

		+ 	'{{/each}}'
		+	'</div>'
		+	'</td>'
		+	'{{/if}}'
	+	'{{/if}}'
	
	
	
	
	
	+'</tr>'
	+'{{/each}}';
	
// 闭包结束    
})(jQuery);

//根据条件隐藏菜单
template.helper("permision", function(li,func){
	var has = Util.permission(func.permisionCode);
	return has;
});

template.helper("getValue", function(li,col, index, grid){
	var code=col.code;
	var render=col.render;
	var rendertpl=col.rendertpl;
	var url=col.link;
	var dateFormat = col.dateFormat;
	var dict = col.dict;
	var bool = col.bool;
	var gridData = grid.gridData.data;
	if(rendertpl){
		li["_rowIndex"] = index + (gridData.pageSize * (gridData.pageNum-1));
		return template(rendertpl,li);
	}
	if(render){
		return render(li[code],li, index);
	}else{
		var returnValue=null;
		if(code){
			var _arr=code.split(".");
			if(_arr.length==1){
				returnValue=li[code];
			}else{
				returnValue=li[_arr[0]];
				for(var i=1;i<_arr.length;i++){
					returnValue=returnValue[_arr[i]];
				}
			}
			if(dateFormat){
				returnValue = Util.dateFormat(returnValue, dateFormat);
			}
			if(dict){
				returnValue = Util.dict(dict, returnValue);
			}
			
			if(bool){
				if(returnValue == 1){
					returnValue = "是";
				}else if(returnValue == 0){
					returnValue = "否";
				}else {
					returnValue = "-";
				}
			}
			(grid.gridData.data.rows[index])["_" + col.name] = returnValue;
		}
		//如果有url参数，加连接并且追加参数
		if(url){
			url=url+"?id="+li.id;
			returnValue="<a class='ablue' href='"+url+"'>"+returnValue+"</a>";
		}
		return returnValue;
	}
});

template.helper("getMap", function(li){
	return JSON.stringify(li);
});
template.helper("toJsonStr", function(li){
	return JSON.stringify(li);
});
template.helper("fun", function(fun){
	return eval("(" + fun + ")");
});
template.helper("fun", function(fun){
	
	return eval("(" + fun + ")");
});