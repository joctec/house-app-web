jQuery.ui = {
	mask:function(){
		$("body").mask("请稍候...");
	},
	unmask:function(){
		$("body").unmask();
	},
	openWindow:function(con) {
	    var _open_window= dialog({
			id:"id-openwindow" + Math.random(),
			title:con.title,
			cancel:true,
			width:con.width?con.width:400,
			height:con.height?con.height:300,
			padding:con.padding?con.padding:20,
			fixed:true,
			quickClose: true,
	    	content:document.getElementById(con.ele),
	    	cancelValue: '取  消',
	    	okValue:con.okName?con.okName:'确  认',
	    	ok: function () {
	    		if(con.ok){
	    			con.ok();
	    		}
	    		if(con.autoClose==undefined || con.autoClose==true){
	    			return true;
	    		}else{
	    			return false;
	    		}
	    	}
		}).showModal();   
	    
	    return _open_window;
	},
	//弹出框
	alert:function(s,ok) {          
	    var _alert_dialog= dialog({
			id:"id-alert" + Math.random(),
			title:"系统提示:",
			cancel: false,
			width:460,
			height:45,
			padding:25,
			fixed:true,
			quickClose: true,
	    	content:'<table class="artdialogtable"><tr><td><div class="artdialogicons"></div></td><td class="artdialogtxt">'+s+'</td></tr></table>',
	    	cancelValue: '取消',
	    	okValue: '确  认',
	    	ok: function () {
	    		if(ok){
	    			ok();
	    		}
	    	}
		}).showModal();   
	},
	confirm:function(s,ok,cancel){
		var d=dialog({
			id:"id-confirm",
			title:"操作确认:",
			width:400,
			height:35,
			padding:25,
			fixed:true,
			quickClose: true,
	    	content:'<table class="artdialogtable"><tr><td><div class="artdialogicons artdialogicons-confirm"></div></td><td  class="artdialogtxt">'+s+'</td></tr></table>',
	    	cancelValue: '取  消',
	    	okValue: '确  认',
	    	ok: function () {
	    		if(ok){
	    			ok();
	    		}
	    	},
		    cancel: function () {
		    	if(cancel){
		    		cancel();
		    	}
		    }
		}).showModal();
		return d;
	},
	tip:function(content,time,ele){
		if(!time){
			time=2;
		}
		var dialog_tip=dialog({
			id:'id-tips',
			content:"<span class='red'>"+content+"</span>",
			padding:10
		}).show();
		if(ele){
			dialog_tip.show(ele);
		}else{
			dialog_tip.show();
		}
		setTimeout(function () {
			dialog_tip.close().remove();
		}, time*1000);
	},
	loading:function(show,type){
		if(type==null || type ==undefined){
			type=1;
		}
		if(show == null || show == true){
			if(type==1){
				$("#loadings").show();
			}
			$("#webloading").show();
		}else{
			$("#webloading").hide(10);
			$("#loadings").hide();
		}
	},prom:function(callback,tip,len,lesslen){//回调、提示文字、可输入文字长度、最小长度
		if(!len){
			len=100;
		}
		if(!lesslen){
			lesslen=5;
		}
		if(!tip){
			tip="请输入您的意见：";
		}
		dialog({
			id:"id-prom",
			title:"信息输入:",
			width:600,
			padding:25,
			fixed:true,
			padding:10,
			quickClose: true,
	    	content:'<div id="dialog-texarea-msg" class="dialog-texarea-msg">'+tip+'</div><textarea onkeyup="dialogwordCount(this,'+len+','+lesslen+')" id="dialog-teaxarea" class="dialog-teaxarea"></textarea><div class="ui-dialog-textcount">您还可以输入<span id="udt-wordcount" class="red">'+len+'/'+len+'</span><span id="udt-lesscount"> ,  最少输入'+lesslen+'字</span></div>',
	    	cancelValue: '取  消',
	    	okValue: '确  认',
	    	ok: function () {
	    		var nowLen=$("#dialog-teaxarea").val().length;
	    		if(nowLen<lesslen){
	    			$.ui.tip("至少输入"+lesslen+"字！");
	    			return false;
	    		}
	    		if(callback){
	    			callback($("#dialog-teaxarea").val());
	    		}
	    	},
		    cancel: function () {}
		}).showModal();
		$("#dialog-teaxarea").focus();
	},
	loading:function(flag,zz){
		if(flag){
			if(zz){
				$("#zhezhao").removeClass("hide");
			}
			$("#jloading").removeClass("hide");
		}else{
			$("#jloading").addClass("hide");
			$("#zhezhao").addClass("hide");
		}
	}
}

function dialogwordCount(ele,len,lesslen){
	var nowLen=$(ele).val().length;
	$("#udt-wordcount").html((len-nowLen)+"/"+len);
	if(nowLen>len){
		$(ele).val($(ele).val().substring(0,len));
		$("#udt-wordcount").html("0/"+len);
	}
};

JUI={
		alert:function(s,ok) {
			var id="id-alert" + Math.random();
		    var _alert_dialog= dialog({
				id:id,
				title:"系统提示:",
				cancel: false,
				width:460,
				height:45,
				padding:25,
				fixed:true,
				quickClose: true,
		    	content:'<table class="artdialogtable"><tr><td><div class="artdialogicons"></div></td><td class="artdialogtxt">'+s+'</td></tr></table>',
		    	cancelValue: '取消',
		    	okValue: '确  认',
		    	ok: function () {
		    		if(ok){
		    			ok();
		    		}
		    	}
			}).showModal(); 
		    return id;
		},
		openWindow:function(con) {
		    var _open_window= dialog({
				id:"id-openwindow" + Math.random(),
				title:con.title,
				cancel:con.cancel?con.cancel:false,
				width:con.width?con.width:400,
				height:con.height?con.height:300,
				padding:con.padding?con.padding:20,
				fixed:true,
				quickClose: true,
		    	content:document.getElementById(con.ele),
		    	cancelValue: '取  消',
		    	okValue:con.okName?con.okName:'确  认',
		    	ok: function () {
		    		if(con.ok){
		    			con.ok();
		    		}
		    		if(con.autoClose==undefined || con.autoClose==true){
		    			return true;
		    		}else{
		    			return false;
		    		}
		    	}
			}).showModal();
		    return _open_window;
		}
	};
	JUTIL={
		dateFormat:function(longdate,fmt){
			var d = new Date();
			d.setTime(longdate);
			var o = {
				"M+": d.getMonth() + 1, //月份
				"d+": d.getDate(), //日
				"h+": d.getHours() % 12 == 0 ? 12 : d.getHours() % 12, //小时
				"H+": d.getHours(), //小时
				"m+": d.getMinutes(), //分
				"s+": d.getSeconds(), //秒
				"q+": Math.floor((d.getMonth() + 3) / 3), //季度
				"S": d.getMilliseconds() //毫秒
			};
			var week = {
				"0": "\u65e5",
				"1": "\u4e00",
				"2": "\u4e8c",
				"3": "\u4e09",
				"4": "\u56db",
				"5": "\u4e94",
				"6": "\u516d"
			};
			if(/(y+)/.test(fmt)) {
				fmt = fmt.replace(RegExp.$1, (d.getFullYear() + "").substr(4 - RegExp.$1.length));
			}
			if(/(E+)/.test(fmt)) {
				fmt = fmt.replace(RegExp.$1, ((RegExp.$1.length > 1) ? (RegExp.$1.length > 2 ? "\u661f\u671f" : "\u5468") : "") + week[d.getDay() + ""]);
			}
			for(var k in o) {
				if(new RegExp("(" + k + ")").test(fmt)) {
					fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
				}
			}
			return fmt;
		}
	};