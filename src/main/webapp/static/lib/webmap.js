var _baiduMap;
var _lat;
var _lng;
var overlays=new Array();
/**
 * 
 * @param {Object} type 1: 描点，2：区域范围
 * @param {Object} divid ：回调的divid
 */
function showMapDialog(type,divid){
	//设置宽度和高度
	var _w=900;
	var _h=600;
	$("#baidumap").height(_h-50);
	dialog({
		title:"地图定位:",
		width:_w,
		height:_h,
		padding:5,
		fixed:true,
		quickClose: true,
    	content:document.getElementById("baidumapContent"),
    	cancelValue: '清除选择',
    	button: [{
            value: '清空选择',
            callback: function () {
                remove_overlay(type);
    			return false;
            }
        },{
            value: '确认选择',
            callback: function () {
                if(type=="2"){
	    			//判断是否已经绘制
					if(overlays.length==0){
						$.ui.tip("请先在地图上绘制区域范围！");
						return false;
					}
					//如果已经绘制
		    		var _area=getPoints();
		    		$("#"+divid).val(_area);
		    		$("#"+divid+"_tip").html("（已选择）");
	    		}else{
	    			if(!_lat){
	    				$.ui.tip("请在地图上标注位置！");
						return false;
	    			}
	    			$("#"+divid+"_lat").val(_lat);
	    			$("#"+divid+"_lng").val(_lng);
	    			$("#"+divid+"_tip").html("（已定位）");
	    		}
            },
            autofocus: true
       }]
	}).showModal();
	//初始化地图
	initmap(type,divid);
}

function initmap(type,divid){
	_baiduMap = new BMap.Map("baidumap");    // 创建Map实例
	_baiduMap.centerAndZoom(new BMap.Point(118.846457,31.958485), 14);  // 初始化地图,设置中心点坐标和地图级别
	_baiduMap.addControl(new BMap.MapTypeControl());   //添加地图类型控件
	_baiduMap.setCurrentCity("南京");          // 设置地图显示的城市 此项是必须设置的
	_baiduMap.enableScrollWheelZoom(true);     //开启鼠标滚轮缩放
	//标注出江宁区政府的位置,并且设置为该点不允许清除
	addMaker(118.846457,31.958485,{disableMassClear:true});
	//显示行政区域
//	getBoundary();
	//绘制工具栏
	if(type=="2"){
		//如果已经存在设置的范围，则先不开启绘制工具栏
		var _area=$("#"+divid).val();
		if(!_area){
			//加载多边形绘制工具
			drawingMng();
		}else{
			//显示之前绘制的区域
			addDbx(_area);
		}
	}else if(type=="1"){
		_baiduMap.addEventListener("click", mapclick);
		//加载之前选择的点
		if($("#"+divid+"_lat").val()){
			addMaker($("#"+divid+"_lng").val(),$("#"+divid+"_lat").val());
			_lat=$("#"+divid+"_lat").val();
			_lng=$("#"+divid+"_lng").val();
		}
	}
}

//加载多边形
function addDbx(points){
	var _arr=points.split(";");
	var _pointArray=new Array();
	for(var i=0;i<_arr.length;i++){
		var _poArr=_arr[i].split(",");
		_pointArray.push(new BMap.Point(_poArr[0],_poArr[1]));
	}
	var polygon = new BMap.Polygon(_pointArray,styleOptions);
	
	 //当多边形的属性发生变化的时候
	 polygon.addEventListener("lineupdate",function(e){
	 	overlays=[];
	    overlays.push(polygon);
	 });
	
    _baiduMap.addOverlay(polygon);
    polygon.enableEditing();
}

//获取覆盖物上的所有点
function getPoints(){
	//获取所有的点
	var _s="";
	var pointArr=overlays[0].getPath();
	for(var i=0;i<pointArr.length;i++){
		var _lng=pointArr[i].lng;//经度
		var _lat=pointArr[i].lat;//维度
		_s=_s+_lng+","+_lat+";";
	}
	return _s.substr(0,_s.length-1);
}

//开启多边形绘制
function drawingMng(){
	//实例化鼠标绘制工具
    var drawingManager = new BMapLib.DrawingManager(_baiduMap, {
        isOpen: true, //是否开启绘制模式
        enableDrawingTool: false, //是否显示工具栏
        drawingToolOptions: {
            anchor: BMAP_ANCHOR_TOP_LEFT, //位置
            offset: new BMap.Size(5, 5), //偏离值
            scale: 0.6
        },
        circleOptions: styleOptions, //圆的样式
        polylineOptions: styleOptions, //线的样式
        polygonOptions: styleOptions, //多边形的样式
        rectangleOptions: styleOptions //矩形的样式
    });  
    //设置多边形模式
    drawingManager.setDrawingMode(BMAP_DRAWING_POLYGON);
    drawingManager.addEventListener('overlaycomplete', overlaycomplete);
}

//绘制完成
var overlaycomplete = function(e){
    overlays.push(e.overlay);
    //设置可编辑
    e.overlay.enableEditing();    
};

//地图点击事件
function mapclick(e){
	remove_overlay();//清除地图上的点
	_lat=e.point.lat;
	_lng=e.point.lng;
	addMaker(e.point.lng,e.point.lat);
}

//添加覆盖物
function addMaker(lng,lat,config){
	var marker = new BMap.Marker(new BMap.Point(lng,lat)); //
	if(config&&config.disableMassClear){
		marker.disableMassClear();
	}
	_baiduMap.addOverlay(marker);
}
//清除覆盖物
function remove_overlay(type){
	_baiduMap.clearOverlays();
	_lat="";
	_lng="";
	overlays=new Array();
	if(type=="2"){
		//如果是区域模式，则清除范围之后，要开启多边形工具
		drawingMng();
	}
}

var styleOptions = {
    strokeColor:"red",    //边线颜色。
    fillColor:"#fff",      //填充颜色。当参数为空时，圆形将没有填充效果。
    strokeWeight:2,       //边线的宽度，以像素为单位。
    strokeOpacity:1,	   //边线透明度，取值范围0 - 1。
    fillOpacity:0.5,      //填充的透明度，取值范围0 - 1。
    strokeStyle: 'solid' //边线的样式，solid或dashed。
};

function getPoint(el){  
}  
//获取江宁区行政区划
function getBoundary(){       
	var bdary = new BMap.Boundary();
	bdary.get("南京市江宁区", function(rs){       //获取行政区域
		_baiduMap.clearOverlays();        //清除地图覆盖物       
		var count = rs.boundaries.length; //行政区域的点有多少个
      	var pointArray = [];
		for (var i = 0; i < count; i++) {
			var ply = new BMap.Polygon(rs.boundaries[i],styleOptions); //建立多边形覆盖物
			_baiduMap.addOverlay(ply);  //添加覆盖物
			pointArray = pointArray.concat(ply.getPath());
		}
		_baiduMap.setViewport(pointArray);    //调整视野                 
	});   
}

/***
//★★★★在地图上添加区域★★★★
function MapAddRegion(){
	var p = [];//用来存储区域的点
	var polygon;
	var doneDraw = 0; //判断是否绘多边形结束
	//当鼠标单击时
	map.addEventListener("click", function(e){
	map.addEventListener("dblclick", function(e){
		doneDraw = 1;
	});
	//判断是否绘制曲线完毕
	if (doneDraw == 0) {
		p.push(new BMap.Point(e.point.lng, e.point.lat)) //存储曲线上每个点的经纬度
		polygon = new BMap.Polygon(p, {
			strokeColor: "blue",
			strokeWeight: 6,
			strokeOpacity: 0.3
		});
		//当折线上的点小于4时，不绘制，多边形至少要有3个点
		if (p.length < 4) {
			return;
		}
		map.addOverlay(polygon); //绘制曲线 
	}
	polygon.addEventListener("click", function(e){
		var title = "<div><h1>你点击了多边形，弹出信息框</h1><div><input type='hidden' id='hidareaid' value=''>";
		ShowWinInfo(e.point, title);
	});
});
}
=====================上面的方法是在地图上画多边形并且调用弹出信息框的方法=======================================================================================

//★★★★弹出信息窗口（需要一个point就是为某个标记弹出信息框）★★★★
function ShowWinInfo(epoint,shtml){
//marker.addEventListener("click", function(){ 
	//onmouseover或者click
// alert("您点击了标注"); 
var opts = {
width: 250, // 信息窗口宽度 
height: 100, // 信息窗口高度 
title: shtml// 信息窗口标题 
} 
//var myHtml = "<div class='ditu_js'><h1>修改区域名称</h1>";
// myHtml += "<div class='con_v'>";
// myHtml += "<input type='hidden' id='hidareaid' value='" + areaId + "'>";
// myHtml += "<b>名称：</b><input type='text' class='text' style='width: 150px' id='txtName' value='" + pname + "'><br />";

var infoWindow = new BMap.InfoWindow("World", opts); // 创建信息窗口对象 
//map.openInfoWindow(infoWindow, map.getCenter()); // 在地图的正中心位置打开信息窗口 
map.openInfoWindow(infoWindow, epoint); // 在地图的特定位置打开信息窗口 
//});
}
**/