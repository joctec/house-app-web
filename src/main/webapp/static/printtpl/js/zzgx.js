function getParam(){
	var obj={};
	obj.p1=$("#serialNumber").val();//序号
	obj.p2=$("#memberName").val();//党员姓名
	obj.p3=jQuery("#becomeInfo option:selected").text();//正式或预备
	if(obj.p3.indexOf("正式")==-1){
		obj.p3="预备";
	}
	
	obj.p4=$("#fromOrgName").val();//转出党组织
	obj.p5=$("#toOrganName").val();//转入党组织
	
	obj.p6=$("#toOrgNameTitle").val();//抬头
	obj.p7=$("#age").val();//年龄
	obj.p8=$("#idNos").val();//身份证号码
	obj.p9=jQuery("#nations option:selected").text();//民族
	obj.p10=$("#membershipYear").val();//党费缴纳年
	obj.p11=$("#membershipMounth").val();//党费缴纳月
	obj.p12=$("#validityPeriod").val();//有效期
	obj.p13=$("#d1").val();//落款年
	obj.p14=$("#d2").val();//落款月
	obj.p15=$("#d3").val();//落款日
	obj.p16=$("#memberLink").val();//党员联系方式
	obj.p17=$("#organAdress").val();//通讯方式
	obj.p18=$("#organTel").val();//organTel
	obj.p19=$("#organFax").val();//organTel
	obj.p20=$("#organPostCode").val();//organFax
	obj.p21=$("#gender option:selected").text();
	return obj;
}
function prints(){
	//获取页面数据
	var data=getParam();
	//获取打印对象
	var LODOP=getLodop();
	if(!LODOP){
		return false;
	}
	/**
	LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='/web/static/printtpl/01.jpg'>");
	LODOP.SET_SHOW_MODE("BKIMG_LEFT",1);
	LODOP.SET_SHOW_MODE("BKIMG_TOP",1);
//	LODOP.SET_SHOW_MODE("BKIMG_WIDTH","183mm");
	//LODOP.SET_SHOW_MODE("BKIMG_HEIGHT","99mm"); //这句可不加，因宽高比例固定按原图的
	LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",1);	
	**/
	
	/******/
	//系统配置
	//LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='/web/static/printtpl/01.jpg'>");
	LODOP.SET_SHOW_MODE("BKIMG_PRINT",1);
	LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
	//设置尺寸
	LODOP.PRINT_INITA(0,0,794,1123,"");
	//加载介绍信背景
	LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='"+base+"/static/printtpl/01.jpg'>");
	//设置背景在预览和打印的时候显示
	LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",true);
	//流水号
	LODOP.ADD_PRINT_TEXT(128,518,100,20,data.p1);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//党员姓名
	LODOP.ADD_PRINT_TEXT(165,166,210,25,data.p2);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	
	LODOP.ADD_PRINT_TEXT(170,497,89,20,data.p3);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	
	//第二联 正式党员
	LODOP.ADD_PRINT_TEXT(491,98,61,25,data.p3);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	
	//转出
	LODOP.ADD_PRINT_TEXT(208,224,159,25,data.p4);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//转入
	LODOP.ADD_PRINT_TEXT(209,425,187,20,data.p5);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	LODOP.ADD_PRINT_TEXT(251,484,62,20,"2016");
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	LODOP.ADD_PRINT_TEXT(250,568,27,20,"5");
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	LODOP.ADD_PRINT_TEXT(250,616,32,20,"6");
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	LODOP.ADD_PRINT_TEXT(392,544,100,20,"20160010");
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//抬头
	LODOP.ADD_PRINT_TEXT(414,93,248,35,data.p6);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//党员姓名
	LODOP.ADD_PRINT_TEXT(454,143,111,25,data.p2);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//年龄
	LODOP.ADD_PRINT_TEXT(457,389,41,25,data.p7);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//民族
	LODOP.ADD_PRINT_TEXT(455,475,51,30,data.p9);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//身份证号码
	LODOP.ADD_PRINT_TEXT(489,346,296,30,data.p8);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//转出
	LODOP.ADD_PRINT_TEXT(523,117,206,30,data.p4);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//转入
	LODOP.ADD_PRINT_TEXT(522,347,212,35,data.p5);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//党费缴纳到年
	LODOP.ADD_PRINT_TEXT(560,315,60,20,data.p10);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//党费缴纳到月
	LODOP.ADD_PRINT_TEXT(560,393,38,20,data.p11);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//有效期
	LODOP.ADD_PRINT_TEXT(597,173,42,20,data.p12);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//落款年
	LODOP.ADD_PRINT_TEXT(661,440,54,20,data.p13);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//落款月
	LODOP.ADD_PRINT_TEXT(660,519,38,20,data.p14);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	//落款日
	LODOP.ADD_PRINT_TEXT(660,574,42,20,data.p15);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	//党员联系方式
	LODOP.ADD_PRINT_TEXT(698,366,290,20,data.p16);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	//基层党委通讯方式
	LODOP.ADD_PRINT_TEXT(731,366,290,20,data.p17);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	//联系电话
	LODOP.ADD_PRINT_TEXT(766,184,100,20,data.p18);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	//传值
	LODOP.ADD_PRINT_TEXT(766,366,136,20,data.p19);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	//邮编
	LODOP.ADD_PRINT_TEXT(766,556,100,20,data.p20);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",14);
	//第三联
	LODOP.ADD_PRINT_TEXT(884,94,209,20,obj.p4);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	LODOP.ADD_PRINT_TEXT(934,144,89,20,data.p2);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	LODOP.SET_PRINT_STYLEA(0,"Stretch",2);
	LODOP.ADD_PRINT_TEXT(456,311,53,25,data.p21);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.SET_PRINT_STYLEA(0,"Alignment",2);
	LODOP.SET_PRINT_STYLEA(0,"Stretch",2);
	
	
	//设置背景图片也打印
	LODOP.SET_SHOW_MODE("BKIMG_PRINT",1);
	//chrome下会出现弹出窗口30s不操作，提示无响应，该配置可让浏览器不等待响应数据
	LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
	LODOP.SET_PRINT_STYLEA(0,"Stretch",2);
	LODOP.PREVIEW();
	
//	LODOP.PRINT_DESIGN();	
}

function getmemberData(){
	var obj={};
	//获取页面上所有的input
	$("#member_info").find("input").each(function(index,ele){
		obj[$(ele).attr("name")]=$(ele).val();
	});
	$("#member_info").find("select").each(function(index,ele){
		obj[$(ele).attr("name")]=$(ele).find("option:selected").text();
	});
	//获取党员照片
	var _img=$("#memberphoto-thelist").find("img");
	if(_img.length==1){
		obj["photo"]=$(_img[0]).attr("src");
	}
	return obj;
}

function printsMember(){
	//获取页面数据
	var data=getmemberData();
	//获取打印对象
	var LODOP=getLodop();
	if(!LODOP){
		return false;
	}
	/**
	LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='/web/static/printtpl/02.jpg'>");
	LODOP.SET_SHOW_MODE("BKIMG_LEFT",1);
	LODOP.SET_SHOW_MODE("BKIMG_TOP",1);
//	LODOP.SET_SHOW_MODE("BKIMG_WIDTH","183mm");
	//LODOP.SET_SHOW_MODE("BKIMG_HEIGHT","99mm"); //这句可不加，因宽高比例固定按原图的
	LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",1);	
	**/
	//系统配置
	//LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='/web/static/printtpl/01.jpg'>");
	LODOP.SET_SHOW_MODE("BKIMG_PRINT",1);
	LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
	//设置尺寸
	LODOP.PRINT_INITA(0,0,794,1123,"");
	//加载介绍信背景
	LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='"+base+"/static/printtpl/02.jpg'>");
	//设置背景在预览和打印的时候显示
	LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",true);

//	LODOP.PRINT_INITA(0,0,800,1200,"");
//	LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='/web/static/printtpl/02.jpg'>");
	LODOP.SET_SHOW_MODE("BKIMG_LEFT",1);
	LODOP.SET_SHOW_MODE("BKIMG_TOP",1);
//	LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",true);
	LODOP.ADD_PRINT_TEXT(104,122,332,20,"?党支部");
	LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
	//打印图片
//	LODOP.ADD_PRINT_CHART(150,586,177,220,10,"(图表35的table超文本代码)");
	if(data["photo"]){
		LODOP.ADD_PRINT_IMAGE(145,580,177,233,"<img border='0' src='"+data["photo"]+"' />");
		LODOP.SET_PRINT_STYLEA(0,"Stretch",1);//(可变形)扩展缩放模式
	}
	//姓名
	LODOP.ADD_PRINT_TEXT(152,139,152,20,data["partyMember.name"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(152,409,158,20,data["partyMember.gender"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(192,139,154,20,data["partyMember.usedName"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(192,409,160,20,data["partyMember.birthday"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(234,139,155,20,data["partyMember.placeOrigin"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(234,409,161,20,data["partyMember.nation"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(274,139,154,20,data["partyMember.placeBirth"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(274,409,162,20,data["partyMember.marriage"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(316,139,155,20,data["partyMember.idNo"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(316,409,160,20,data["partyMember.mobile"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(358,139,156,20,data["joinDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(358,409,162,20,data["becomeInfo"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(400,139,158,20,data["introducer"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(400,409,100,20,data["becomeDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(400,642,100,20,data["membership"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(440,139,604,20,data["partyMember.home"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(481,139,602,20,data["partyMember.accounAdress"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(522,139,154,20,data["partyMember.identity"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(522,409,350,20,data["partyMember.school"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(564,139,153,20,data["partyMember.degree"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(564,409,116,20,data["partyMember.specialities"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(564,642,111,20,data["partyMember.education"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(604,139,153,20,data["partyMember.workDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(604,409,338,20,data["partyMember.workCompany"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(643,139,605,20,data["partyMember.workAdress"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(686,139,155,20,data["partyMember.technicalPosition"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(686,409,337,20,data["partyMember.positionLevel"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(727,139,152,20,data["partyMember.newLevel"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(727,409,336,20,data["partyMember.lineSituation"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(768,139,151,20,data["partyMember.retireDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(768,409,342,20,data["partyMember.quitDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(808,139,613,42,data["partyMember.quitReason"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(867,139,613,46,data["des"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	//设置背景图片也打印
	LODOP.SET_SHOW_MODE("BKIMG_PRINT",1);
	//chrome下会出现弹出窗口30s不操作，提示无响应，该配置可让浏览器不等待响应数据
	LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
//	LODOP.PREVIEW();
	
	LODOP.PRINT_DESIGN();	
}

/***


		function Design1() {
			var LODOP = getLodop(document.getElementById('LODOP_OB'), document.getElementById('LODOP_EM'));
			//CreateFullBill();
			//LODOP.SET_SHOW_MODE("HIDE_ITEM_LIST",true);//设置对象列表默认处于关闭状态
			//LODOP.SET_SHOW_MODE("TEXT_SHOW_BORDER",1); //设置字符编辑框默认为single	
			LODOP.PRINT_DESIGN();		
		};

	
		function CreatePage() {
			LODOP=getLodop();  
		};

		function myPreview1(){
			CreatePage();
			LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='${base}/static/printtpl/01.jpg'>");
			LODOP.SET_SHOW_MODE("BKIMG_PRINT",1);
			LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
			LODOP.PREVIEW();
		};
		
		function myPrintDesign3(){
			CreatePage();
			LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='${base}/static/printtpl/01.jpg'>");
			//LODOP.SET_SHOW_MODE("BKIMG_LEFT",1);
			//LODOP.SET_SHOW_MODE("BKIMG_TOP",1);
			//LODOP.SET_SHOW_MODE("BKIMG_WIDTH","183mm");
			//LODOP.SET_SHOW_MODE("BKIMG_HEIGHT","99mm"); //这句可不加，因宽高比例固定按原图的
			LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",1);	
			LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
			LODOP.PRINT_DESIGN();		
		};	


***/