var LODOP; //声明为全局变量 
function getmemberData(){
	var obj={};
	//获取页面上所有的input
	$("#member_info").find("input").each(function(index,ele){
		var name=$(ele).val();
		if(name=="请选择"){
			name="";
		}
		obj[$(ele).attr("name")]=name;
	});
	$("#member_info").find("select").each(function(index,ele){
		var name=$(ele).find("option:selected").text();
		if(name=="请选择"){
			name="";
		}
		obj[$(ele).attr("name")]=name;
	});
	//获取党员照片
	var _img=$("#memberphoto-thelist").find("img");
	if(_img.length==1){
		obj["photo"]=$(_img[0]).attr("src");
	}
	return obj;
}

function printsMember(){
	var _bgPath="<img border='0' src='"+base+"/static/printtpl/02.jpg'>";
	//获取页面数据
	var data=getmemberData();
	//获取打印对象
	LODOP=getLodop();
	if(!LODOP){
		return false;
	}
	//系统配置
	//LODOP.ADD_PRINT_SETUP_BKIMG("<img border='0' src='/web/static/printtpl/01.jpg'>");
	LODOP.SET_SHOW_MODE("BKIMG_PRINT",1);
	LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
	//设置尺寸
	LODOP.PRINT_INITA(0,0,794,1123,"");
	//加载介绍信背景
	LODOP.ADD_PRINT_SETUP_BKIMG("");
	//设置背景在预览和打印的时候显示
	LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",true);

	LODOP.PRINT_INITA(0,0,800,1200,"");
	LODOP.ADD_PRINT_SETUP_BKIMG(_bgPath);
	LODOP.SET_SHOW_MODE("BKIMG_LEFT",1);
	LODOP.SET_SHOW_MODE("BKIMG_TOP",1);
	LODOP.SET_SHOW_MODE("BKIMG_IN_PREVIEW",true);
	LODOP.ADD_PRINT_TEXT(104,122,332,20,data["person.organName"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","黑体");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",11);
	//打印图片
//	LODOP.ADD_PRINT_CHART(150,586,177,220,10,"(图表35的table超文本代码)");
	if(data["photo"]){
		LODOP.ADD_PRINT_IMAGE(145,580,177,233,"<img border='0' src='"+data["photo"]+"' />");
		LODOP.SET_PRINT_STYLEA(0,"Stretch",1);//(可变形)扩展缩放模式
	}
	//姓名
	LODOP.ADD_PRINT_TEXT(152,139,152,20,data["person.name"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(152,409,158,20,data["person.gender"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(192,139,154,20,data["person.usedName"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(192,409,160,20,data["person.birthday"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(234,139,155,20,data["person.placeOrigin"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(234,409,161,20,data["person.nation"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(274,139,154,20,data["person.placeBirth"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(274,409,162,20,data["person.marriage"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(316,139,155,20,data["person.idNo"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(316,409,160,20,data["person.mobile"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(358,139,156,20,data["joinDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(358,409,162,20,data["becomeInfo"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(400,139,158,20,data["introducer"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(400,409,100,20,data["becomeDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(400,642,100,20,data["membership"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(440,139,604,20,data["person.home"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(481,139,602,20,data["person.accounPolice"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(522,139,154,20,data["person.identity"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(522,409,350,20,data["person.school"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(564,139,153,20,data["person.degree"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(564,409,116,20,data["person.specialities"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(564,642,111,20,data["person.education"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(604,139,153,20,data["person.workDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(604,409,338,20,data["person.workCompany"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(643,139,605,20,data["person.workAdress"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(686,139,155,20,data["person.technicalPosition"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(686,409,337,20,data["person.positionLevel"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(727,139,152,20,data["person.newLevel"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(727,409,336,20,data["person.lineSituation"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(768,139,151,20,data["person.retireDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(768,409,342,20,data["person.quitDate"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(808,139,613,42,data["person.quitReason"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	LODOP.ADD_PRINT_TEXT(867,139,613,46,data["des"]);
	LODOP.SET_PRINT_STYLEA(0,"FontName","华文仿宋");
	LODOP.SET_PRINT_STYLEA(0,"FontSize",12);
	//设置背景图片也打印
	LODOP.SET_SHOW_MODE("BKIMG_PRINT",1);
	//chrome下会出现弹出窗口30s不操作，提示无响应，该配置可让浏览器不等待响应数据
	LODOP.SET_SHOW_MODE("NP_NO_RESULT",true);
//	LODOP.PREVIEW();
	
	LODOP.PRINT_DESIGN();
	return false;
}